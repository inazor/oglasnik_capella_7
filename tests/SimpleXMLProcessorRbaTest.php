<?php

use Phalcon\Http\Request\FileInterface;
use \Baseapp\Library\SimpleXMLProcessorRba;

class SimpleXMLProcessorRbaTest extends PHPUnit_Framework_Testcase
{
    /**
     * @param FileInterface|string $file Filepath (as a string) or a \Phalcon\Http\Request\FileInterface
     *
     * @dataProvider providerFilenames
     */
    public function testFileProcessing($file, array $expected)
    {
        if ($file instanceof FileInterface) {
            $filepath = $file->getTempName();
            $filename = $file->getName();
        } else {
            $filepath = $file;
            $filename = basename($filepath);
        }

        $processor = new SimpleXMLProcessorRba();
        $processor->open($filepath);

        $processor->parse();
        $processor->close();

        if ($processor->called()) {
            $stats = $processor->getStats();
            $this->assertEquals($expected, $stats);
        } else {
            $this->fail('XML processing callbacks have not been called for file `' . $filename . '`');
        }
    }

    public function providerFilenames()
    {


        return array(
            array(
                'xml/izvadak1100153885-25.07.2016.xml',
                array('elements' => 260, 'pbo_matches' => 69, 'pbo_duplicates' => 0, 'completed' => 0)
            ),
            array(
                'xml/izvadak1100153885-22.07.2016.xml',
                array('elements' => 198, 'pbo_matches' => 43, 'pbo_duplicates' => 0, 'completed' => 0)
            ),
            array(
                'xml/izvadak1100153885-21.07.2016.xml',
                array('elements' => 217, 'pbo_matches' => 55, 'pbo_duplicates' => 0, 'completed' => 0)
            ),
            array(
                'xml/izvadak1100153885-20.07.2016.xml',
                array('elements' => 160, 'pbo_matches' => 39, 'pbo_duplicates' => 0, 'completed' => 0)
            ),
            array(
                // This is an old file, and they have since changed the position of our PBO,
                // but previously this file had
                'xml/izvadak1100153885-03.06.2016_fixed.xml',
                array('elements' => 95, 'pbo_matches' => 0, 'pbo_duplicates' => 0, 'completed' => 0)
            ),
        );
    }
}
