<?php

use \Baseapp\Library\Utils;

class UtilsTest extends PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider providerGetFqpnDefaults
     */
    public function testGetFqpnWithDefaultOptions($filename, $expected_path) {
        $path = Utils::get_fqpn($filename);
        $this->assertEquals($expected_path, $path);
    }

    public function providerGetFqpnDefaults() {
        return array(
            array('čćšđž-ČĆŠĐŽ-[]!/$#:.ext', false),
            array('1.tar.gz', '/00/01/1.tar.gz'),
            array('000', '/00/00/000'),
            array('0000', '/00/00/0000'),
            array('0.jpg', '/00/00/0.jpg'),
            array('1.jpeg', '/00/01/1.jpeg'),
            array('01.jpg', '/00/10/01.jpg'),
            array('001.jpg', '/01/00/001.jpg'),
            array('0001.jpg', '/10/00/0001.jpg'),
            array('00001.jpg', '/10/00/00001.jpg'),
            array('1.jpg', '/00/01/1.jpg'),
            array('2.jpg', '/00/02/2.jpg'),
            array('3.jpg', '/00/03/3.jpg'),
            array('4.jpg', '/00/04/4.jpg'),
            array('10.jpg', '/00/01/10.jpg'),
            array('11.jpg', '/00/11/11.jpg'),
            array('12.jpg', '/00/21/12.jpg'),
            array('13.jpg', '/00/31/13.jpg'),
            array('14.jpg', '/00/41/14.jpg'),
            array('100.jpg', '/00/01/100.jpg'),
            array('111.jpg', '/01/11/111.jpg'),
            array('112.jpg', '/02/11/112.jpg'),
            array('113.jpg', '/03/11/113.jpg'),
            array('123.jpg', '/03/21/123.jpg'),
            array('231.jpg', '/01/32/231.jpg'),
            array('0001.jpg', '/10/00/0001.jpg'),
            array('0002.jpg', '/20/00/0002.jpg'),
            array('0010.jpg', '/01/00/0010.jpg'),
            array('1000.jpg', '/00/01/1000.jpg'),
            array('1111.jpg', '/11/11/1111.jpg'),
            array('1112.jpg', '/21/11/1112.jpg'),
            array('1113.jpg', '/31/11/1113.jpg'),
            array('10000.jpg', '/00/00/10000.jpg'),
            array('10001.jpg', '/10/00/10001.jpg'),
            array('10002.jpg', '/20/00/10002.jpg'),
            array('10012.jpg', '/21/00/10012.jpg'),
            array('10123.jpg', '/32/10/10123.jpg'),
            array('123456.jpg', '/65/43/123456.jpg'),
            array('1234567890.jpg', '/09/87/1234567890.jpg'),
            array('12345678910111213.jpg', '/31/21/12345678910111213.jpg'),

            array('e4d909c290d0fb1ca068ffaddf22cbd0.jpg', '/e4/d9/e4d909c290d0fb1ca068ffaddf22cbd0.jpg'),
            array('d41d8cd98f00b204e9800998ecf8427e', '/d4/1d/d41d8cd98f00b204e9800998ecf8427e'),

            array('de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3.jpg', '/de/9f/de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3.jpg'),
            array('da39a3ee5e6b4b0d3255bfef95601890afd80709', '/da/39/da39a3ee5e6b4b0d3255bfef95601890afd80709')
        );
    }

    /**
     * @dataProvider providerGetFqpnMiscParams
     */
    public function testGetFqpnMiscParams($filename, $expected, $options) {
        $path = Utils::get_fqpn($filename, $options);
        $this->assertEquals($expected, $path);
    }

    public function providerGetFqpnMiscParams() {
        $provider_data = array();

        $build_data = function($data) use (&$provider_data) {
            $data['options']['create_missing_dirs'] = false;
            foreach ($data['tests'] as $filename => $expected) {
                $provider_data[] = array($filename, $expected, $data['options']);
            }
        };

        $opt1 = array(
            'options' => array(
                'partition_length' => 3,
                'num_partitions' => 3,
                'basedir' => '/a/b/'
            ),
            'tests' => array(
                // filename => expected partitioned FQPN
                '123456.jpg' => '/a/b/000/654/321/123456.jpg',
                '1234567890.jpg' => '/a/b/098/765/432/1234567890.jpg',
                '12345678910111213.jpg' => '/a/b/312/111/019/12345678910111213.jpg',
                'e4d909c290d0fb1ca068ffaddf22cbd0.jpg' => '/a/b/e4d/909/c29/e4d909c290d0fb1ca068ffaddf22cbd0.jpg',
                'd41d8cd98f00b204e9800998ecf8427e' => '/a/b/d41/d8c/d98/d41d8cd98f00b204e9800998ecf8427e',
                'de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3.jpg' => '/a/b/de9/f2c/7fd/de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3.jpg',
                'da39a3ee5e6b4b0d3255bfef95601890afd80709' => '/a/b/da3/9a3/ee5/da39a3ee5e6b4b0d3255bfef95601890afd80709',
                '1.tar.gz' => '/a/b/000/000/001/1.tar.gz'
            )
        );
        $build_data($opt1);

        // Test to make sure trailing slash in basedir doesn't matter
        $opt2                       = $opt1;
        $opt2['options']['basedir'] = '/a/b';
        $build_data($opt2);

        // Test partitioning with 1 level and 1 char only
        $opt3 = array(
            'options' => array(
                'partition_length' => 1,
                'num_partitions' => 1,
                'basedir' => '/a/b/'
            ),
            'tests' => array(
                // filename => expected partitioned FQPN
                '123456.jpg' => '/a/b/6/123456.jpg',
                '1234567890.jpg' => '/a/b/01234567890.jpg',
                '12345678910111213.jpg' => '/a/b/3/12345678910111213.jpg',
                'e4d909c290d0fb1ca068ffaddf22cbd0.jpg' => '/a/b/e/e4d909c290d0fb1ca068ffaddf22cbd0.jpg',
                'd41d8cd98f00b204e9800998ecf8427e' => '/a/b/d/d41d8cd98f00b204e9800998ecf8427e',
                'de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3.jpg' => '/a/b/d/de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3.jpg',
                'da39a3ee5e6b4b0d3255bfef95601890afd80709' => '/a/b/d/da39a3ee5e6b4b0d3255bfef95601890afd80709',
                '1.tar.gz' => '/a/b/1/1.tar.gz'
            )
        );
        $build_data($opt3);

        // Test partitioning when num_partitions = 0 (split into as many as possible 'partition_length' long buckets)
        // based on string length...
        $opt4 = array(
            'options' => array(
                'partition_length' => 1,
                'num_partitions' => 0,
                'basedir' => '/'
            ),
            'tests' => array(
                '123456.jpg' => '/6/5/4/3/2/1/123456.jpg',
                '1234567890.jpg' => '/0/9/8/7/6/5/4/3/2/1/1234567890.jpg',
                '12345678910111213.jpg' => '/3/1/2/1/1/1/0/1/9/8/7/6/5/4/3/2/1/12345678910111213.jpg',
                'e4d909c290d0fb1ca068ffaddf22cbd0.jpg' => '/e/4/d/9/0/9/c/2/9/0/d/0/f/b/1/c/a/0/6/8/f/f/a/d/d/f/2/2/c/b/d/0/e4d909c290d0fb1ca068ffaddf22cbd0.jpg',
                '1.tar.gz' => '/1/1.tar.gz'
            ),
        );
        $build_data($opt4);

        // TODO: Test fqpn partitioning when partition_length = 0 && num_partitions = 3 ?
        // TODO: Test fqpn partitioning when partition_length = 7 && num_partitions = 3|7|9 ?

        return $provider_data;
    }

    public function testGetFqpnWithDirectoryLikeInputAndExtraTrailingSeparators() {
        $options = array(
            'create_missing_dirs' => false,
            'num_partitions' => 1,
            'partition_length' => 3
        );

        // basedir with trailing slash
        $options['basedir'] = '/some/other/place/';
        $this->assertEquals('/some/other/place/pat/pathname', Utils::get_fqpn('/some/pathname/', $options));
        $this->assertEquals('/some/other/place/pat/pathname', Utils::get_fqpn('/some/pathname', $options));

        // basedir without trailing slash
        $options['basedir'] = '/some/other/place';
        $this->assertEquals('/some/other/place/pat/pathname', Utils::get_fqpn('/some/pathname/', $options));
        $this->assertEquals('/some/other/place/pat/pathname', Utils::get_fqpn('/some/pathname', $options));
    }

    /**
     * Test partitioning when partition_length = 3 && num_partitions = 0
     * This should just split evenly based on string length (if possible).
     *
     * @dataProvider providerGetFqpnPartitioningEvenlyForPartitionLengthOnly
     */
    public function testGetFqpnPartitioningEvenlyForPartitionLengthOnly($filename, $expected, $options) {
        $path = Utils::get_fqpn($filename, $options);
        $this->assertEquals($expected, $path);
    }

    public function providerGetFqpnPartitioningEvenlyForPartitionLengthOnly() {
        $options = array(
            'partition_length' => 3,
            'num_partitions' => 0,
            'basedir' => '/a/b',
            'create_missing_dirs' => false
        );
        return array(
            array('1.tar.gz', '/a/b/001/1.tar.gz', $options),
            array('1.jpg', '/a/b/001/1.jpg', $options),
            array('2.jpg', '/a/b/002/2.jpg', $options),
            array('10.jpg', '/a/b/001/10.jpg', $options),
            array('11.jpg', '/a/b/011/11.jpg', $options),
            array('123.jpg', '/a/b/321/123.jpg', $options),
            array('1234.jpg', '/a/b/432/1234.jpg', $options),
            array('12345.jpg', '/a/b/543/12345.jpg', $options),

            array('123456.jpg', '/a/b/654/321/123456.jpg', $options),
            array('1234567890.jpg', '/a/b/098/765/432/1234567890.jpg', $options),
            array('12345678910111213.jpg', '/a/b/312/111/019/876/543/12345678910111213.jpg', $options),

            array('e4d909c290d0fb1ca068ffaddf22cbd0.jpg', '/a/b/e4d/909/c29/0d0/fb1/ca0/68f/fad/df2/2cb/e4d909c290d0fb1ca068ffaddf22cbd0.jpg', $options),
            array('d41d8cd98f00b204e9800998ecf8427e', '/a/b/d41/d8c/d98/f00/b20/4e9/800/998/ecf/842/d41d8cd98f00b204e9800998ecf8427e', $options),

            array('de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3.jpg', '/a/b/de9/f2c/7fd/25e/1b3/afa/d3e/85a/0bd/17d/9b1/00d/b4b/de9f2c7fd25e1b3afad3e85a0bd17d9b100db4b3.jpg', $options),
            array('da39a3ee5e6b4b0d3255bfef95601890afd80709', '/a/b/da3/9a3/ee5/e6b/4b0/d32/55b/fef/956/018/90a/fd8/070/da39a3ee5e6b4b0d3255bfef95601890afd80709', $options),
        );
    }

    /**
     * Test partitioning when partition_length = 0 && num_partitions = 0
     * This should just return the basedir prefix and not do any partitioning?
     *
     * @dataProvider providerGetFqpnWithoutPartitioning
     */
    public function testGetFqpnWithoutPartitioning($filename, $expected, $options)
    {
        $path = Utils::get_fqpn($filename, $options);
        $this->assertEquals($expected, $path);
    }

    public function providerGetFqpnWithoutPartitioning()
    {
        $options = array(
            'partition_length' => 0,
            'num_partitions' => 0,
            'basedir' => '/',
            'create_missing_dirs' => false
        );
        return array(
            array('1.jpg', '/1.jpg', $options),
            array('1.jpg', '/1.jpg', $options),
            array('123456.jpg', '/123456.jpg', $options),
            array('1234567890.jpg', '/1234567890.jpg', $options),
            array('12345678910111213.jpg', '/12345678910111213.jpg', $options),
            array('e4d909c290d0fb1ca068ffaddf22cbd0.jpg', '/e4d909c290d0fb1ca068ffaddf22cbd0.jpg', $options),
            array('1.tar.gz', '/1.tar.gz', $options)
        );
    }

    public function testRemoveAccents()
    {
        $strings = array(
            'čćšđž-ČĆŠĐŽ' => 'ccsdz-CCSDZ',
            'üÿÄËÏÖÜŸåÅ' => 'uyAEIOUYaA',
            'áéíóúÁÉÍÓÚ' => 'aeiouAEIOU',
        );

        foreach ($strings as $orig => $expected) {
            $this->assertEquals($expected, Utils::remove_accents($orig));
        }
    }

    public function testSanitizeFilename()
    {
        $filenames = array(
            'hello\u0000world' => 'hellou0000world',
            'hello\0world' => 'hello0world',
            "hello\nworld" => 'hello-world',
            '[]!"#\"!$_+:' => '',
            '@-.' => ''
        );

        foreach ($filenames as $orig => $expected) {
            $this->assertSame($expected, Utils::sanitize_filename($orig));
        }
    }

    /**
     * Test both normalized and non-normalized filename extension parsing
     *
     * @dataProvider providerGetFilenameExtension
     */
    public function testGetFilenameExtension($filename, $expected, $normalized)
    {
        $extension = Utils::get_filename_extension($filename, $normalized);
        $this->assertEquals($expected, $extension);
    }

    public function providerGetFilenameExtension()
    {
        return array(
            array('1.jpg', 'jpg', true),
            array('1.jpg', 'jpg', false),

            array('/1.jpg', 'jpg', true),
            array('/1.jpg', 'jpg', false),

            array('/1.jpeg', 'jpeg', true),
            array('/1.jpeg', 'jpeg', false),

            array('something.tar.gz', 'tar.gz', true),
            array('something.tar.gz', 'gz', false),

            array('no-extension', '', true),
            array('no-extension', '', false),

            array('just/a/path/no-extension', '', true),
            array('just/a/path/no-extension', '', false),

            array('/just/a/path/no-extension', '', true),
            array('/just/a/path/no-extension', '', false),

            array('', '', true),
            array('', '', false),

            array(false, '', true),
            array(false, '', false),
        );
    }

    /**
     * Test url-fixing
     *
     * @dataProvider providerTestFixUrl
     */
    public function testFixUrl($input, $default_scheme, $expected)
    {
        $fixed_url = Utils::fix_url($input, $default_scheme);
        $this->assertSame($expected, $fixed_url);
    }

    public function providerTestFixUrl()
    {
        return array(
            // input, default_scheme, expected
            array('example.com', null, 'http://example.com'),
            array('example.com', 'http', 'http://example.com'),
            array('example.com', 'https', 'https://example.com'),
            array('examplecom', null, 'http://examplecom'),
            array('http://example.com', null, 'http://example.com'),
            array('https://example.com', null, 'https://example.com'),
            array('.example.com', null, 'http://.example.com'),
            array('.example.com.', null, 'http://.example.com.'),
            array('sub.domain.tld', null, 'http://sub.domain.tld'),
            array('htp://example.com', null, 'http://example.com'),
            array('ftp://something.somewhere', null, 'http://something.somewhere'),
            array('ftp://something.somewhere', 'https', 'https://something.somewhere'),
            array('allowing.non-allowed.scheme-via-default.param', 'scp', 'scp://allowing.non-allowed.scheme-via-default.param'),
            array(false, null, null),
            array('', null, null),
            array(null, null, null),
            array(' ', null, null),
            array(0, null, null),
            array(1, null, null)
        );
    }

    /**
     * Test slugify
     *
     * @dataProvider providerSlugify
     */
    public function testSlugify($input, $expected) {
        $slug = Utils::slugify($input);
        $this->assertSame($expected, $slug);
    }

    public function providerSlugify()
    {
        return array(
            // input, lowercase, expected
            array('A æ Übérmensch på høyeste nivå! И я люблю PHP! ﬁ', 'a-ae-uebermensch-paa-hoeyeste-nivaa-i-ya-lyublyu-php'),
            array('-- This is an example ű of an ------ article   - @@@ ..,.%&*£%$&*(*', 'this-is-an-example-u-of-an-article-atatat'),
            // updated after https://github.com/cocur/slugify/issues/50
            array('čćšđž', 'ccsdz'),
            array('ČĆŠĐŽ', 'ccsdz'),
            array('--', ''),
            array(' ', ''),
            array('  -- -', ''),
            array('АлександрТкачев', 'aleksandrtkachev'),
            array('СтанаПивашевић', 'stanapivashevic'),
        );
    }

    /**
     * @dataProvider providerParseCurrency
     */
    public function testParseCurrency($input, $expected)
    {
        $result = Utils::parse_currency($input);
        $this->assertSame($expected, $result);
    }

    public function providerParseCurrency()
    {
        return array(
            array('1,10 HRK', 1.10),
            array('1 000 000.00', 1000000.0),
            array('$1 000 000.21', 1000000.21),
            array('£1.10', 1.10),
            array('$123 456 789', 123456789.0),
            array('$123,456,789.12', 123456789.12),
            array('$123 456 789,12', 123456789.12),
            array('1.10', 1.1),
            array(',,,,.10', .1),
            array('1.000', 1000.0),
            array('1,000', 1000.0)
        );
    }

    /**
     * Testing Utils::kn2lp
     *
     * @dataProvider providerKn2Lp
     */
    public function testKn2Lp($input, $expected)
    {
        $amount_lp = Utils::kn2lp($input);
        $this->assertSame($expected, $amount_lp);
    }

    public function providerKn2Lp()
    {
        return array(
            array('0,00', '0'),
            array('0,0', '0'),
            array('0.00', '0'),
            array('0.0', '0'),
            array('0', '0'),
            array(0, '0'),
            array(0.0, '0'),
            array('1', '100'),
            array(1, '100'),
            array(1.00, '100'),
            array('1.0', '100'),
            array('1,0', '100'),
            array('1.00', '100'),
            array('1,00', '100'),
            array('1.000', '100000'),
            array('1,000', '100000'),
            array(10, '1000'),
            array('10', '1000'),
            array(10.00, '1000'),
            array('100', '10000'),
            array('12345', '1234500'),
            array(100, '10000'),
            array(100.00, '10000'),
            array('1,25', '125'),
            array(1.25, '125'),
            array('1.234,56', '123456'),
            array('1,234.56', '123456'),
            array(1234.56, '123456'),
            array(123456.78, '12345678'),
            array('123456', '12345600')
        );
    }

    /**
     * Testing Utils::lp2kn
     *
     * @dataProvider providerLp2Kn
     */
    public function testLp2Kn($input, $expected)
    {
        $amount_lp = Utils::lp2kn($input);
        $this->assertSame($expected, $amount_lp);
    }

    public function providerLp2Kn()
    {
        return array(
            array(0, '0.00'),
            array('0', '0.00'),
            array('', '0.00'),
            array('0,00', '0.00'),
            array('0,0', '0.00'),
            array('0.00', '0.00'),
            array('0.0', '0.00'),

            array('1', '0.01'),
            array(1, '0.01'),
            array(1.00, '0.01'),
            array('1.0', '0.01'),
            array('1,0', '0.01'),
            array('1.00', '0.01'),
            array('1,00', '0.01'),

            array(1000, '10.00'),
            array('1.000', '10.00'),
            array('1,000', '10.00'),

            array(10000, '100.00'),
            array('10000', '100.00'),
            array(10000.00, '100.00'),

            array(10, '0.10'),
            array('10', '0.10'),

            array(100, '1.00'),
            array(100.00, '1.00'),
            array('100', '1.00'),

            array(125, '1.25'),
            array('125', '1.25'),

            // array('1,25', 0.0125),
            array(1.25, '0.01'),
            array('1,25', '0.01'),
            array('1.25', '0.01'),

            array(12345, '123.45'),
            array('12345', '123.45'),
            array(123456, '1234.56'),
            array('123456', '1234.56'),

            array('1.234,56', '12.34'),
            array('1,234.56', '12.34'),
            array(1234.56, '12.34'),
            array(123456.78, '1234.56'),
        );
    }

    /**
     * Testing Utils::format_money()
     * @dataProvider providerFormatMoney
     */
    public function testFormatMoney($input, $force_decimals, $expected)
    {
        $money = Utils::format_money($input, $force_decimals);
        $this->assertSame($expected, $money);
    }

    public function providerFormatMoney()
    {
        return array(
            array('0', false, '0'),
            array('0', true, '0,00'),
            array(0, false, '0'),
            array(0, true, '0,00'),

            array('1', false, '1'),
            array('1', true, '1,00'),
            array(1, false, '1'),
            array(1, true, '1,00'),

            array('1.2', false, '1,20'),
            array('1.2', true, '1,20'),
            array(1.2, false, '1,20'),
            array(1.2, true, '1,20'),

            array(124, false, '124'),
            array(124, true, '124,00'),
            array('124', false, '124'),
            array('124', true, '124,00'),
            array('124.00', false, '124'),
            array('124.00', true, '124,00'),
            array(124.00, false, '124'),
            array(124.00, true, '124,00'),

            array('1234.56', false, '1.234,56'),
            array('1234.56', true, '1.234,56'),
            array(1234.56, false, '1.234,56'),
            array(1234.56, true, '1.234,56'),
            array('1234.00', false, '1.234'),
            array('1234.00', true, '1.234,00'),

            array(123456, false, '123.456'),
            array(123456, true, '123.456,00'),

            array(123456.78, false, '123.456,78'),
            array(123456.78, true, '123.456,78'),
        );
    }

    /**
     * Testing Utils::build_pbo()
     * @dataProvider providerPbo
     */
    public function testBuildPbo($input, $expected)
    {
        $pbo = Utils::build_pbo($input);
        $this->assertSame($expected, $pbo);
    }

    public function providerPbo()
    {
        return array(
            array('1', '900000001'),
            array('10', '900000010'),
            array('101', '900000101'),
            array('999999', '900999999'),
            array('1000000', '901000000'),
            array('1000001', '901000001'),
            array('10000010', '910000010'),
            array('100000000', '100000000'),
            array('100000010', '100000010'),
            array('100000010', '100000010'),
            array('110000011', '110000011'),
        );
    }

    /**
     * Testing Utils::str_has_pbo_prefix()
     * @dataProvider providerPboPrefix
     */
    public function testStrHasPboPrefix($pbo, $expected)
    {
        $has_prefix = Utils::str_has_pbo_prefix($pbo);
        $this->assertSame($expected, $has_prefix);
    }

    public function providerPboPrefix()
    {
        return array(
            // these should all be treated as prefixed
            array('900000001', true),
            array('900000010', true),
            array('900000101', true),
            array('900999999', true),
            array('901000000', true),
            array('901000001', true),
            array('900000909', true),

            // These should not be considered as pbos
            array('909', false),
            array('900', false),
            array('901', false),
            array('9999', false),
            array('90000001', false),

            // TODO: what about this case? how to build a regex for this? it should be done if possible and not too complicated
            // array('910000010', true),

            // these are indistinguishable
            array('100000000', false),
            array('100000010', false),
            array('100000010', false),
            array('110000011', false),
        );
    }

    /**
     * Testing Utils::str_remove_pbo_prefix()
     * @dataProvider providerPboPrefixRemove
     */
    public function testStrRemovePboPrefix($pbo, $expected)
    {
        $result = Utils::str_remove_pbo_prefix($pbo);
        $this->assertSame($expected, $result);
    }

    public function providerPboPrefixRemove()
    {
        return array(
            array('900000001', '1'),
            array('900000010', '10'),
            array('900000101', '101'),
            array('900000909', '909'),
            array('900999999', '999999'),
            array('901000000', '1000000'),
            array('901000001', '1000001'),

            // these are not really pbos, so shouldn't be touched
            array('909', '909'),
            array('900', '900'),
            array('901', '901'),
            array('9999', '9999'),
            array('90000001', '90000001'),

            // TODO: this is also a weird case, with the same cause -- the regex in Utils::str_remove_pbo_prefix()
            // does not currently handle this properly...
            // array('910000010', '10000010'),

            array('100000000', '100000000'),
            array('100000010', '100000010'),
            array('100000010', '100000010'),
            array('110000011', '110000011'),
        );
    }

    /**
     * @dataProvider providerTruncate
     */
    public function testTruncate($input, $length, $output)
    {
        $result = Utils::str_truncate_html($input, $length);
        $this->assertEquals($output, $result);
    }

    /**
     * Various test cases for Utils::str_truncate_html()
     * @return array
     */
    public function providerTruncate()
    {
        return array(
            // No truncation required
            array(
                '<b>&lt;Hello&gt;</b> <img src="world.png" alt="" /> world!',
                15,
                '<b>&lt;Hello&gt;</b> <img src="world.png" alt="" /> world!',
            ),
            // Truncation happens in outer text part, self closing tag and unpaired br tag,
            // and spurious </h2>s (one inside tags, one at top level)
            array(
                '<b></h2>&lt;Hello&gt;</b></h2> <br><img src="world.png" alt="" /> world!',
                10,
                '<b></h2>&lt;Hello&gt;</b></h2> <br><img src="world.png" alt="" /> w&hellip;',
            ),
            // Truncation happens inside tags - close some tags afterwards
            array(
                '<hr><table><tr><td>Heck, </td><td>throw</td></tr><tr><td>in a</td><td>table</td></tr></table>',
                10,
                '<hr><table><tr><td>Heck, </td><td>thro&hellip;</td></tr></table>',
            ),
            // Correct # of chars with html entities? (2 digit and 4 digit)
            array(
                '<em><b>&lt;Hello&gt;</b>&#20;&#8225;world!</em>',
                10,
                '<em><b>&lt;Hello&gt;</b>&#20;&#8225;w&hellip;</em>'
            ),
        );
    }

    /**
     * @dataProvider providerScreaming
     */
    public function testNoScreaming($input, $length, $expected)
    {
        $result = Utils::str_no_screaming($input, $length);
        $this->assertEquals($expected, $result);
    }

    /**
     * @covers Utils::str_no_screaming()
     * @return array
     */
    public function providerScreaming()
    {
        return array(
            array(
                'DERAČINA',
                8,
                'deračina'
            ),
            // Convert croatian chars to lowercase properly
            array(
                'ČĆŠĐŽ',
                5,
                'čćšđž'
            ),
            // Leave as is since word length is greater than the uppercased string
            array(
                'ČĆŠĐŽ',
                6,
                'ČĆŠĐŽ'
            ),
            array(
                'HTC',
                4,
                'HTC'
            ),
            array(
                'HTC',
                3,
                'htc'
            ),
            // Mixed-case examples are impossible to deal with currently
            array(
                'iPHONE',
                4,
                'iphone'
            )
        );
    }


    /**
     * @dataProvider providerPunctuation
     */
    public function testPunctuation($input, $expected)
    {
        $result = Utils::str_normalize_punctuation($input);
        $this->assertEquals($expected, $result);
    }

    /**
     * @covers Utils::str_normalize_punctuation()
     * @return array
     */
    public function providerPunctuation()
    {
        return array(
            // First array element contains the input, second one is the expected output
            array(
                '!!',
                '',
            ),
            array(
                '!!1',
                '1'
            ),
            array(
                '!!! Ovo je NEŠTO s DERAČINOM !!!!',
                'Ovo je NEŠTO s DERAČINOM !'
            ),
            array(
                'ČĆŠĐŽ',
                'ČĆŠĐŽ',
            ),
            array(
                '........',
                ''
            ),
            array(
                '...',
                ''
            ),
            array(
                '__',
                '',
            ),
            array(
                '____',
                '',
            ),
            array(
                '(((',
                '',
            ),
            array(
                ')))',
                '',
            ),
            array(
                '----',
                '',
            ),
            array(
                ';;;',
                '',
            ),
            array(
                '::;;',
                ''
            ),
            // Have to preserve newlines but trim them form the end
            array(
                '::;;' . "\n",
                '',
            ),
            array(
                "!!! Ovo je NEŠTO\ns\nDERAČINOM !!!!",
                "Ovo je NEŠTO\ns\nDERAČINOM !"
            )
        );
    }

    public function providerQuoteUnquotedWordsWithDots()
    {
        return array(
            // First array element contains the input, second one is the expected output
            array(
                'no dots',
                'no dots'
            ),
            array(
                'single',
                'single'
            ),
            array(
                'windows 8.1',
                'windows "8.1"',
            ),
            array(
                'www.njuskalo.hr',
                '"www.njuskalo.hr"',
            ),
            array(
                'oglasnik.hr',
                '"oglasnik.hr"'
            ),
            array(
                '"this.is.quoted" already',
                '"this.is.quoted" already'
            ),
            array(
                '"this.is.quoted" windows 8.1',
                '"this.is.quoted" windows "8.1"'
            )
        );
    }

    /**
     * @dataProvider providerQuoteUnquotedWordsWithDots
     */
    public function testQuoteUnquotedWordsWithDots($input, $expected)
    {
        $result = Utils::quote_unquoted_words_with_dots($input);
        $this->assertEquals($expected, $result);
    }

    public function providerInflateViewCountNumbers()
    {
        return array(
            // First array element contains the input, second one is the expected output
            array(
                -1,
                4
            ),
            array(
                0,
                4
            ),
            array(
                1,
                4
            ),
            array(
                2,
                9,
            ),
            array(
                3,
                13,
            ),
            array(
                4,
                18
            ),
            array(
                5,
                22
            ),
            array(
                6,
                27
            ),
            array(
                7,
                31,
            ),
            array(
                8,
                36
            ),
            array(
                9,
                40
            ),
            array(
                10,
                45
            ),
            array(
                11,
                48,
            ),
            array(
                51,
                207
            ),
            array(
                101,
                356
            ),
            array(
                1000,
                2154
            ),
            array(
                1001,
                2155
            )
        );
    }

    /**
     * @dataProvider providerInflateViewCountNumbers
     */
    public function testInflateViewCountNumbers($input, $expected)
    {
        $result = Utils::inflate_view_count_numbers($input);
        $this->assertEquals($expected, $result);
    }
}
