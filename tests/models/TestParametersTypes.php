<?php

class TestParametersTypes extends \Phalcon\Mvc\Model
{
    // Base attributes

    public $id;                 // varchar(32)  - UPPERCASE name of parameters type (PK)
    public $class;              // varchar(64)  - classes to use on menus
    public $accept_dictionary;  // tinyint(1)   - if parameter is driven by dictionary
    public $can_other;          // tinyint(1)   - if parameter can have 'Other' so we can display additional text field

    public function initialize()
    {
        parent::initialize();
        $this->hasMany('id', 'TestParameters', 'type_id', array(
            'alias' => 'Parameters'
        ));
    }

}
