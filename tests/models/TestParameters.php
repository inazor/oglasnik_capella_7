<?php

class TestParameters extends \Phalcon\Mvc\Model
{

    public $id;
    public $name;
    public $type_id = 'TEXT';
    public $dictionary_id;

    public function initialize()
    {
        // Parameter can have one or no dictionary attached to it
        $this->hasOne(
            'dictionary_id', 'TestDictionaries', 'id',
            array(
                'alias' => 'Dictionary'
            )
        );
    }

    public function getSource()
    {
        return 'parameters';
    }

}
