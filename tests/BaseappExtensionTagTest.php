<?php

use \Baseapp\Extension\Tag;

/**
 * Some basic tests to make sure our \Baseapp\Extension\Tag behaves as the
 * extended original + test our 'disabled' option additions...
 */
class BaseappExtensionTagTests extends PHPUnit_Framework_TestCase
{

    protected static $resultset = null;

    public static function setUpBeforeClass()
    {
        self::$resultset = \Baseapp\Models\Roles::find();
    }

    // normalize EOLs, otherwise tests fail on Windows due to use of PHP_EOL and
    // the fixtures not including EOL but simple \n (and even further complicated
    // by git changing newlines on checkout, if so configured)
    /**
     * Normalizes EOLs into "\n" otherwise some tests fail on Windows due to
     * \Phalcon's use of PHP_EOL and the test markup not including PHP_EOL but \n
     * @param $str
     *
     * @return mixed
     */
    private function normalize_newlines($str)
    {
        return str_replace("\r\n", "\n", $str);
    }

    public function testSelectBasics()
    {
        $data = array(
            'status',
            array('Active' => array('A1' => 'A One', 'A2' => 'A Two'), 'B' => 'B One')
        );

        $html = <<<HTML
<select id="status" name="status">
	<optgroup label="Active">
	<option value="A1">A One</option>
	<option value="A2">A Two</option>
	</optgroup>
	<option value="B">B One</option>
</select>
HTML;
        $html = $this->normalize_newlines($html);

        $ret = Tag::selectStatic($data);
        $ret = $this->normalize_newlines($ret);
        $this->assertEquals($ret, $html);

        $html = <<<HTML
<select id="status" name="status">
	<optgroup label="Active">
	<option value="A1" selected="selected">A One</option>
	<option value="A2">A Two</option>
	</optgroup>
	<option value="B">B One</option>
</select>
HTML;
        Tag::setDefault('status', 'A1');

        $ret = Tag::selectStatic($data);
        // normalize EOLs, otherwise tests fail on Windows due to use of PHP_EOL and
        // the fixtures not including EOL but simple \n, end even further complicated
        // by git changing newlines on checkout on different platforms too...

        $this->assertEquals($this->normalize_newlines($ret), $this->normalize_newlines($html));
    }

    public function testSelectDisabledArray()
    {
        $expected_markup = <<<HTML
<select class="form-control" id="parent_id" name="parent_id">
	<option value="">Choose...</option>
	<optgroup label="Group 1">
	<option value="1.1">Option 1.1</option>
	</optgroup>
	<optgroup label="Group 2">
	<option value="2.1" disabled="disabled">Option 2.1</option>
	<option value="2.2" selected="selected">Option 2.2</option>
	</optgroup>
	<optgroup disabled="disabled" label="Group 3">
	<option value="3.1">Option 3.1</option>
	<option value="3.2">Option 3.2</option>
	<option value="3.3">Option 3.3</option>
	</optgroup>
</select>
HTML;

        $markup_data = array(
            'Group 1' => array(
                '1.1' => 'Option 1.1'
            ),
            'Group 2' => array(
                '2.1' => 'Option 2.1',
                '2.2' => 'Option 2.2'
            ),
            'Group 3' => array(
                '3.1' => 'Option 3.1',
                '3.2' => 'Option 3.2',
                '3.3' => 'Option 3.3'
            )
        );

        $selected = '2.2';
        // $disabled = array('Group 3', '3.1', '3.2', '3.3', '2.1');
        $disabled = array('Group 3', '2.1');

        Tag::setDefault('parent_id', $selected);

        $dropdown = Tag::select(array(
            'parent_id',
            $markup_data,
            'disabled' => $disabled,
            'useEmpty' => true,
            'class' => 'form-control'
        ));

        $this->assertEquals($this->normalize_newlines($expected_markup), $this->normalize_newlines($dropdown));
    }

    public function testSelectDisabledCallback()
    {
        $expected_markup = <<<HTML
<select id="test" name="test">
	<option value="1.1" disabled="disabled">Option 1.1</option>
	<option value="2.1">Option 2.1</option>
	<option value="2.2" disabled="disabled" selected="selected">Option 2.2</option>
	<option value="3.1">Option 3.1</option>
	<option value="3.2" disabled="disabled">Option 3.2</option>
	<option value="3.3">Option 3.3</option>
</select>
HTML;

        $markup_data = array(
            '1.1' => 'Option 1.1',
            '2.1' => 'Option 2.1',
            '2.2' => 'Option 2.2',
            '3.1' => 'Option 3.1',
            '3.2' => 'Option 3.2',
            '3.3' => 'Option 3.3'
        );

        $disable_even_options = function($value) {
            static $cnt = 0;
            $cnt++;
            if ($cnt & 1) {
                return true;
            }
            return false;
        };

        Tag::setDefault('test', '2.2');
        $dropdown = Tag::select(array(
            'test',
            $markup_data,
            'disabled' => $disable_even_options,
            'useEmpty' => false
        ));
        $this->assertEquals($this->normalize_newlines($expected_markup), $this->normalize_newlines($dropdown));
    }

    public function testUsingResultset()
    {
        $resultset = self::$resultset;

        // Building expected markup from the resultset
        $expected_markup = <<<MARKUP
<select id="roles" name="roles">

MARKUP;
        $resultset_arr = $resultset->toArray();
        foreach ($resultset_arr as $result) {
            $expected_markup .= "\t<option value=\"" . $result['id'] . "\">" . $result['name'] . "</option>\n";
        }
        $expected_markup .= <<<MARKUP
</select>
MARKUP;

        // Using the Tag class to build the same thing hopefully
        $dropdown = Tag::select(array(
            'roles',
            $resultset,
            'using' => array('id', 'name'),
            'useEmpty' => false
        ));

        $this->assertEquals($this->normalize_newlines($expected_markup), $this->normalize_newlines($dropdown));
    }

    /**
     * @expectedException \Phalcon\Tag\Exception
     */
    public function testThrowsUsingRequiredWhenResultsetUsed()
    {
        $dropdown = Tag::select(array(
            'roles',
            self::$resultset
        ));
    }

    /**
     * @expectedException \Phalcon\Tag\Exception
     */
    public function testThrowsIfUsingIsSetButInvalid()
    {
        $dropdown = Tag::select(array(
            'roles',
            self::$resultset,
            'using' => true
        ));
    }

    public function testOptionsWithExtraAttributes()
    {
        $expected_markup = <<<HTML
<select id="test" name="test">
	<option value="1.1" data-test="data-test-value" test="test-value">Option 1.1</option>
	<optgroup label="Option 2 group">
	<option value="2.1">Option 2.1</option>
	<option value="2.2">Option 2.2</option>
	<option value="2.3">Option 2.3</option>
	</optgroup>
	<optgroup label="Option 3 group with attributes">
	<option value="3.1" data-something="some-value">Option 3.1</option>
	<option value="3.2" something-else="some-other-value" selected="selected">Option 3.2</option>
	<option value="3.3">Option 3.3</option>
	</optgroup>
	<option value="4.1">Option 4.1</option>
	<option value="5.1">Option 6.1</option>
	<option value="5.2">Option 5.2</option>
	<option value="5.3">Option 5.3</option>
</select>
HTML;
        $markup_data = array(
            '1.1' => array(
                'text' => 'Option 1.1',
                'attributes' => array(
                    'data-test' => 'data-test-value',
                    'test' => 'test-value'
                )
            ),
            'Option 2 group' => array(
                '2.1' => 'Option 2.1',
                '2.2' => array(
                    'text' => 'Option 2.2',
                ),
                '2.3' => array(
                    'text' => 'Option 2.3',
                    'attributes' => array(),
                )
            ),
            'Option 3 group with attributes' => array(
                '3.1' => array(
                    'text' => 'Option 3.1',
                    'attributes' => array(
                        'data-something' => 'some-value'
                    ),
                ),
                '3.2' => array(
                    'text' => 'Option 3.2',
                    'attributes' => array(
                        'something-else' => 'some-other-value'
                    )
                ),
                '3.3' => 'Option 3.3'
            ),
            '4.1' => 'Option 4.1',
            '5.1' => 'Option 6.1',
            '5.2' => 'Option 5.2',
            '5.3' => 'Option 5.3'
        );

        Tag::setDefault('test', '3.2');
        $dropdown = Tag::select(array(
            'test',
            $markup_data,
            'useEmpty' => false
        ));

        $this->assertEquals($this->normalize_newlines($expected_markup), $this->normalize_newlines($dropdown));
    }

}
