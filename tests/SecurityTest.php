<?php

/**
 * Some basic tests to make sure our Auth component is doing fine
 */
class SecurityTest extends PHPUnit_Framework_TestCase
{
    protected static $auth = null;

    public static function setUpBeforeClass()
    {
        self::$auth = \Baseapp\Library\Auth::instance();
    }

    public function testHashMethods()
    {
        $hash = self::$auth->hash_password('test');
        $this->assertNotEmpty($hash);
        $this->assertTrue(self::$auth->check_hashed_password('test', $hash));
    }

    public function testHashLength()
    {
        $this->assertEquals(60, strlen(self::$auth->hash_password('p4ssw0rd')));
    }

    public function testHash()
    {
        $hash = self::$auth->hash_password('foo');
        $this->assertEquals($hash, crypt('foo', $hash));
    }

    public function testSessionCsrfTokens()
    {
        $session = new Phalcon\Session\Adapter\Files();
        // CLI can hardly do anything about sessions for obvious reasons
        $this->assertFalse($session->start());
        $this->assertFalse($session->isStarted());

        // but we can inject what we need
        @session_start();

        $di = Phalcon\Di::getDefault();
        $di->set('session', $session, true);
        self::$auth->setDI($di);

        // $token_key = self::$auth->getTokenKey();
        $token_key = 'something';
        $token = self::$auth->get_csrf_token($token_key);
        $this->assertNotEmpty($token);
        $this->assertTrue(self::$auth->check_csrf_token($token_key, $token));

        // todo: test time limited tokens

        session_destroy();
    }

} 
