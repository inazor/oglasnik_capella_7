<?php


abstract class ModelsTestCase extends PHPUnit_Framework_TestCase
{
    /**
     * @var \Phalcon\DiInterface
     */
    protected static $di;
    protected static $original_di;

    public function __construct()
    {
        spl_autoload_register(array($this, 'modelsAutoloader'));
    }

    public function __destruct()
    {
        spl_autoload_unregister(array($this, 'modelsAutoloader'));
    }

    public function modelsAutoloader($className)
    {
        $className = str_replace('\\', '/', $className);
        $model_filename = 'models/' . $className . '.php';
        if (file_exists($model_filename)) {
            require $model_filename;
        }
    }

    public static function setUpBeforeClass()
    {
        self::$original_di = \Phalcon\Di::getDefault();
        Phalcon\Di::reset();

        $di = new Phalcon\Di();

        $di->set('modelsManager', function() {
            return new Phalcon\Mvc\Model\Manager();
        }, true);

        $di->set('modelsMetadata', function() {
            return new Phalcon\Mvc\Model\Metadata\Memory();
        }, true);

        // override the db connection to point to a different database for tests
        $di->set('db', function() {
            require 'config/config.db.php';
            return new Phalcon\Db\Adapter\Pdo\Mysql($config_mysql);
        }, true);

        // hook up sql query logging/profiling
        $log_events  = false;
        $log_queries = false;
        if ($log_events || $log_queries) {
            $manager = new \Phalcon\Events\Manager();

            if ($log_events) {
                $event_logger = new \Phalcon\Logger\Adapter\File(ROOT_PATH . '/logs/models-events-tests.log');
                $manager->attach(
                    'model',
                    function ($event, $model) use ($event_logger) {
                        /* @var $event Phalcon\Events\Event */
                        $event_logger->log($event->getType() . ' in ' . get_class($model), \Phalcon\Logger::INFO);
                        $debug_details = false;
                        if ($debug_details) {
                            $source = $event->getSource();
                            $data   = var_export($source->toArray(), true);
                            $event_logger->log("Event Trace:\n" . \Baseapp\Library\Debug::trace(), \Phalcon\Logger::DEBUG);
                            $event_logger->log("Model data (" . get_class($source) . "):\n" . $data, \Phalcon\Logger::DEBUG);
                        }
                    }
                );
                $di->getShared('modelsManager')->setEventsManager($manager);
            }

            if ($log_queries) {
                $profiler_logger = new \Phalcon\Logger\Adapter\File(ROOT_PATH . '/logs/db-profiling-tests.log');
                $query_logger    = new \Baseapp\Library\Profiler\QueryLogger($profiler_logger);
                $manager->attach('db', $query_logger);
                $di->getShared('db')->setEventsManager($manager);
            }
        }

        self::$di = $di;
    }

    public static function tearDownAfterClass()
    {
        self::$di = null;
        \Phalcon\Di::setDefault(self::$original_di);
        self::$original_di = null;
    }

    // ran before every test
    public function setUp()
    {
        $this->truncateTable('categories');
        $this->truncateTable('categories_many_roots');
        $this->truncateTable('parameters');
        $this->truncateTable('dictionaries');
    }

    // ran after every test
    public function tearDown()
    {
        $this->truncateTable('categories');
        $this->truncateTable('categories_many_roots');
        $this->truncateTable('parameters');
        $this->truncateTable('dictionaries');
    }

    /**
     * Empties a table in the database.
     *
     * @param string $table
     *
     * @return boolean
     */
    public function emptyTable($table)
    {
        $connection = self::$di->get('db');
        return $connection->delete($table);
    }

    /**
     * Disables FOREIGN_KEY_CHECKS and truncates the db table
     *
     * @param string $table Table name
     * @param bool $disable_fk_checks
     *
     * @return bool Result of truncate operation
     */
    public function truncateTable($table, $disable_fk_checks = true)
    {
        /* @var $db \Phalcon\Db\Adapter\Pdo\Mysql */
        $db = self::$di->get('db');

        if ($disable_fk_checks) {
            $db->execute('SET FOREIGN_KEY_CHECKS = 0');
        }
        $success = $db->execute("TRUNCATE TABLE `$table`");
        if ($disable_fk_checks) {
            $db->execute('SET FOREIGN_KEY_CHECKS = 1');
        }

        return $success;
    }
} 
