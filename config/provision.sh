#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

#PHALCON_BRANCH='2.1.x'
#PHALCON_BRANCH='2.0.x'
PHALCON_BRANCH='phalcon-v2.0.13'
MYSQL_ROOT_PASS='pass'
MYSQL_DB_NAME='oglasnik_dev'
HOME=/home/vagrant
DEPLOYS=/var/www/oglasnik
SYNCDIR=/var/www/oglasnik.local
cd ~

echo -e "\n--- Setting up $SYNCDIR ownership...\n"
f=$SYNCDIR
while [[ $f != "/var/www" ]]; do chown -R vagrant:www-data $f; f=$(dirname $f); done;

echo -e "\n--- Creating and setting up $DEPLOYS ownership...\n"
mkdir -p $DEPLOYS
f=$DEPLOYS
while [[ $f != "/var/www" ]]; do chown -R vagrant:www-data $f; f=$(dirname $f); done;

echo -e "--- Ignore client locale settings...\n"
sed -i 's/AcceptEnv/#AcceptEnv/' /etc/ssh/sshd_config

echo -e "\n--- Get rid of cloud-init\n"
echo 'datasource_list: [ None ]' | tee /etc/cloud/cloud.cfg.d/90_dpkg.cfg
dpkg-reconfigure -f noninteractive cloud-init

echo -e "\n--- Fix missing locale crap...\n"
echo -e 'LANG=en_US.UTF-8\nLC_ALL=en_US.UTF-8' > /etc/default/locale

echo -e "\n--- Add our sources.list...\n";
cp $SYNCDIR/config/sources.list /etc/apt/sources.list.d/

echo -e "\n--- Add custom repos (MariaDB 10.1, PHP 5.6.x)...\n"
# MariaDB 10.1
apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
add-apt-repository 'deb [arch=amd64,i386] http://ftp.hosteurope.de/mirror/mariadb.org/repo/10.1/ubuntu trusty main'
# PHP5.6.x
add-apt-repository ppa:ondrej/php

echo -e "\n--- Updating packages list...\n"
apt-get -q update

echo -e "\n--- Installing required common packages...\n"
apt-get -y install software-properties-common nano git-core git unzip &&

echo -e "\n--- Installing Apache2...\n"
apt-get -y install apache2 libapache2-mod-xsendfile
#mv /var/www/html/index.html /var/www/html/index.html_backup
a2enmod expires
a2enmod rewrite
a2enmod ssl
a2enmod xsendfile
a2ensite default-ssl
echo 'ServerName localhost' > /etc/apache2/conf-available/fqdn.conf
a2enconf fqdn
cp $SYNCDIR/config/apache.conf /etc/apache2/sites-available/myapache.conf && a2ensite myapache.conf

echo -e "\n--- Installing Imagemagick...\n"
apt-get -y install imagemagick

echo -e "--- Installing PHP 5.6...\n"
apt-get -y install php5.6 php5.6-apcu php5.6-bcmath php5.6-bz2 php5.6-cli php5.6-curl php5.6-common php5.6-dom php5.6-gd php5.6-igbinary php5.6-intl php5.6-imagick php5.6-mbstring php5.6-mcrypt php5.6-memcached php5.6-memcache php5.6-mysqli php5.6-redis php5.6-xdebug php5.6-xsl php5.6-zip
cp $SYNCDIR/config/php.ini /etc/php/5.6/mods-available/myphp.ini &&
phpenmod -v 5.6 myphp
phpdismod -v 5.6 xdebug
#phpenmod -v 5.6 mcrypt intl curl

echo "<?php phpinfo();" > /var/www/html/info.php

echo -e "--- Installing MariaDB & PhpMyAdmin...\n"

# MySQL
debconf-set-selections <<< "mysql-server mysql-server/root_password password $MYSQL_ROOT_PASS"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $MYSQL_ROOT_PASS"
apt-get install -y mariadb-server php5.6-mysqlnd
cp $SYNCDIR/config/mysql.cnf /etc/mysql/conf.d/mysql.cnf &&
service mysql restart &&
mysql_tzinfo_to_sql /usr/share/zoneinfo | mysql -u root -p${MYSQL_ROOT_PASS} mysql

# phpMyAdmin
debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $MYSQL_ROOT_PASS"
debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password"
apt-get -y install phpmyadmin
echo "\$cfg['LoginCookieValidity'] = 14400;" >> /etc/phpmyadmin/config.inc.php

echo -e "\n--- Installing Memcached...\n"
apt-get -y install php5.6-memcache php5.6-memcached memcached

echo -e "\n--- Installing Beanstalked...\n"
apt-get -y install beanstalkd

echo -e "\n--- Installing Phalcon...\n"
apt-get -y install php5.6-dev gcc libpcre3-dev re2c &&
git clone -b $PHALCON_BRANCH --depth=1 git://github.com/phalcon/cphalcon.git &&
cd ~/cphalcon/build/ &&
./install &&
cd ~ &&
rm -rf cphalcon/ &&
echo 'extension=phalcon.so' > /etc/php/5.6/mods-available/phalcon.ini &&
phpenmod -v 5.6 phalcon

#echo -e "\n--- Installing Zephir...\n"
#git clone https://github.com/phalcon/zephir
#cd zephir
#./install-json
#./install -c
#cd ~

#echo -e "\n--- Installing Composer...\n"
#curl -sS https://getcomposer.org/installer | php
#mv composer.phar /usr/local/bin/composer
#echo 'PATH="$HOME/.composer/vendor/bin:$PATH"' >> .profile
#echo 'Defaults !secure_path' > /etc/sudoers.d/00-keep-env-path
#cho 'Defaults env_keep+="PATH"' >> /etc/sudoers.d/00-keep-env-path

#echo -e "--- Installing Phalcon DevTools...\n"
#composer global require phalcon/devtools:dev-master
#ln -fs /home/vagrant/.composer/vendor/bin/phalcon.php /home/vagrant/.composer/vendor/bin/phalcon

#echo -e "--- Installing Codeception...\n"
#composer global require codeception/codeception

echo -e "\n--- Auto security update...\n"
cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades

chown -R vagrant:vagrant $HOME

echo -e "\n--- Upgrade needed stuff...\n"
apt-get -y upgrade

echo -e "\n--- Removing unneeded stuff...\n"
apt-get -y autoremove

echo -e "\n--- Restarting Apache2...\n"
service apache2 restart

echo -e "\n--- Importing database(s)...\n"
mysql -u root -p${MYSQL_ROOT_PASS} -e "create database ${MYSQL_DB_NAME}";
mysql -u root -p${MYSQL_ROOT_PASS} < $SYNCDIR/sql/avus.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/audit-tables-create.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/ads-offline-export-history-create.sql
echo -e "\n--- Extracting large dump file...\n";
cd $SYNCDIR/sql/ && unzip oglasnik-prod-facelift-dump.zip
echo -e "\n--- Importing large dump file (this might take a while...)\n"
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/oglasnik-prod-facelift-dump.sql
rm $SYNCDIR/sql/oglasnik-prod-facelift-dump.sql

echo -e "\n--- Executing some more SQL updates/inserts...\n";
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-05-23-spinid-parameters-types.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-07-14-users-profile-images.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-07-07-users-shops.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-07-08-show-more-for-filters.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-07-29-favorite-ads-notes.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-01-location-country-calling-codes.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-03-sold-field-on-ads-model.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-09-image-styles.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-11-cms_categories.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-11-cms-articles.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-11-users_shops_featured_homepage.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-14-categories-json-field.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-14-sections-json-field.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-15-generic-images.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-15-image-styles-renames.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-20-drop-media-id-from-users-shops.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-23-infraction-reports.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-25-ads-totals-cache.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-25-users-messages.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-08-25-drop-sections-categories.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-10-04-ads-additional-data-alter-via-sms-column.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-10-10-parameters-types-spinidgw-insert.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2016-10-21-add-expiry-index-to-ads-table.sql
mysql -u root -p${MYSQL_ROOT_PASS} ${MYSQL_DB_NAME} < $SYNCDIR/sql/2017-02-28-cms-category-templates.sql
