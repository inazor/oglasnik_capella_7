function get_matching_parameters() {
    var $matching_parameters = $('#matching_parameters');
    var $matching_parameters_container = $matching_parameters.closest('div.row');
    var $moveBtn = $('#changeAdsCategoryModalMoveBtn');
    var selected_category_id = $.trim($('#new_category_id').selectpicker('val'));

    $matching_parameters_container.addClass('hidden');
    $moveBtn.addClass('disabled').data('target-category-id', '');

    if ('' !== selected_category_id && parseInt(selected_category_id)) {
        $.get(
            '/admin/category-parameterization/compare/' + $('#category_id').val() + '/' + selected_category_id,
            function(json){
                if (json.status) {
                    $moveBtn.removeClass('disabled').data('target-category-id', selected_category_id);
                    if (json.count > 0) {
                        var compare_results = [];
                        $.each(json.data, function(i, name){
                            compare_results.push(name);
                        });
                        $matching_parameters.removeClass('text-danger').html('These parameters can be transferred to selected category<br/><span class="text-success">' + compare_results.join(', ') + '.</span><br /><span class="text-danger">All other parameters (if they exist) will be lost!</span>');
                    } else {
                        $matching_parameters.addClass('text-danger').html('There are no parameters in common. If you choose to move this ad to this category you will loose all the data!');
                    }
                } else {
                    var error_msg = 'Something went wrong... We were unable to compare categories';
                    if ('undefined' !== typeof json.msg && $.trim(json.msg)) {
                        error_msg = $.trim(json.msg);
                    }
                    $matching_parameters.addClass('text-danger').html(error_msg);
                }
                $matching_parameters_container.removeClass('hidden');
            },
            'json'
        );
    }
}

function get_users_public_phones() {
    var username = $.trim($('#user_id').data('username'));
    if (username) {
        $.get(
            '/ajax/userInfo/' + username,
            function(json){
                if (json.status) {
                    $('#phone1').val(json.data.phone1);
                    $('#phone2').val(json.data.phone2);
                } else {
                    if ('undefined' !== typeof json.msg && $.trim(json.msg)) {
                        alert(json.msg);
                    }
                }
            },
            'json'
        );
    }
}

function save_remark() {
    var $remark = $('#ads_remark');
    $.post(
        '/admin/ads/saveRemark', {
            '_csrftoken': $('#_csrftoken').val(),
            'ad_id'     : $remark.data('ad-id'),
            'remark'    : $remark.val()
        },
        function (json) {
            if (!json.status) {
                if ('undefined' !== typeof json.msg) {
                    if ('undefined' !== typeof json.msg && $.trim(json.msg)) {
                        alert("Ooops!\n" + json.msg);
                    }
                }
            }
        },
        'json'
    );
}

/**
 * Adds and handles "Sentence case" buttons for the backend Ad edit UI/form (certain fields only).
 */
var Sentencer = {
    do: function(str) {
        str = str.toLowerCase();
        var b = true;
        var c = '';
        for (var d = 0, l = str.length; d < l; d++) {
            var e = str.charAt(d);
            if (/\.|\!|\?|\n|\r/.test(e)) {
                b = true;
            } else if ($.trim(e) != '' && true == b) {
                e = e.toUpperCase();
                b = false;
            }
            c += e;
        }
        return c;
    },
    space_commas: function(str){
        return str.replace(/,([^\s])/g, ", $1");
    },
    space_dots: function(str){
        return str.replace(/\.([^\s])/g, ". $1");
    },
    init: function(){
        var self = this;

        var fields = [
            '#parameter_description',
            '#parameter_description_offline',
            '#parameter_title'
        ];

        var $button_tpl = $('<button class="btn btn-primary btn-xs pull-right sentencer" type="button">Sentence case</button>');

        // Click handler for our sentencer button
        var handler = function(e) {
            var $this = $(this);
            var $el = $($this.data('for'));
            if ($el.length > 0) {
                var val = $el.val();
                var new_val = self.do(val);
                new_val = self.space_commas(new_val);
                new_val = self.space_dots(new_val);
                $el.val(new_val);
            }
        };

        // Build a hash of our target elements and each element's specific button
        var targets = {};
        for (var i = 0, l = fields.length; i < l; i++) {
            var field = fields[i];
            var $button = $button_tpl.clone();
            $button.attr('data-for', field);
            $button.on('click', handler);
            targets[field] = $button;
        }

        // Inject our buttons for specified target elements
        for (var k in targets) {
            if (targets.hasOwnProperty(k)) {
                var $target_el = $(k).prev();
                if ($target_el.length > 0) {
                    $target_el.append(targets[k]);
                }
            }
        }
    }
};

/**
 * Show/Hide warning for old ad's offline product/option in case when the product will not be exported to AVUS.
 */
var old_offline_product        = null,
    old_offline_product_option = null;

function warn_about_old_offline_products() {
    var scheduled = $('#product-section-title').data('offline-exportable');

    if (!scheduled) {
        var $offline_product_warning  = $('#offline-product-warning');
        var $product_section          = $('section.product-section');
        if ($product_section.length > 0) {
            var $offline_products = $product_section.find('.products-group.products-group-offline');
            if ($offline_products.length > 0) {
                // collect currently selected product/option
                var $selected_offline_product        = $offline_products.find('input[name="products-offline"]:checked');
                old_offline_product                  = $selected_offline_product.val();
                var $selected_offline_product_option = $offline_products.find('select.select-product-options:enabled').find(':selected');
                old_offline_product_option           = $selected_offline_product_option.val();

                // reposition the warning after the '<h5>Offline</h5>'
                var $offline_products_label = $offline_products.find('>h5');
                $offline_product_warning.insertAfter($offline_products_label);

                // add change events
                var $all_offline_products = $offline_products.find('input[name="products-offline"]');
                $all_offline_products.change(function(){
                    toggle_old_offline_warning_text();
                });
                var $all_offline_products_options = $offline_products.find('select.select-product-options');
                $all_offline_products_options.change(function(){
                    toggle_old_offline_warning_text();
                });
            }
        }
    }
}
function toggle_old_offline_warning_text() {
    var $offline_product_warning = $('#offline-product-warning');
    var $offline_products        = $('section.product-section .products-group.products-group-offline');
    if ($offline_products.length > 0 && old_offline_product && old_offline_product_option) {
        // adding slight delay (moving to bottom of execution stack) just to be sure that we get the right 'enabled' 
        // select box before testing the condition to show/hide warning message
        setTimeout(function(){
            var $selected_offline_product        = $offline_products.find('input[name="products-offline"]:checked');
            var $selected_offline_product_option = $offline_products.find('select.select-product-options:enabled').find(':selected');
            var curr_offline_product             = $selected_offline_product.val();
            var curr_offline_product_option      = $selected_offline_product_option.val();

            if (old_offline_product == curr_offline_product && old_offline_product_option == curr_offline_product_option) {
                $offline_product_warning.show();
            } else {
                $offline_product_warning.hide();
            }
        }, 10);
    } else {
        $offline_product_warning.hide();
    }
}

$(document).ready(function(){
    var $infractionReports = $('#infraction-reports');
    if ($infractionReports.length) {
        $infractionReports.handleInfractionReports();
    }
    Sentencer.init();
    $('#save_remark').click(function(){save_remark();});
    var $user_id = $('#user_id');
    var $user_id_input = $('#user_id_input');
    if ('undefined' === typeof $user_id_input || $user_id_input.length == 0) {
        $user_id_input = null;
    }
    if ($user_id_input) {
        var user_filter = $user_id_input.magicSuggest({
            allowFreeEntries: false,
            autoSelect: true,
            highlight: false,
            selectFirst: true,
            hideTrigger: true,
            maxSelection: 1,
            minChars: 1,
            displayField: 'username',
            noSuggestionText: 'No users found matching your search',
            placeholder: 'Choose a user',
            useTabKey: true,
            required: true,
            resultAsString: true,
            data: function(q) {
                var usersData = [];
                if ($.trim(q)) {
                    $.ajax({
                        async: false,
                        url: '/ajax/users/' + $.trim(q),
                        cache: false,
                        dataType: 'json'
                    }).done(function(json_data) {
                        usersData = json_data;
                    });
                }
                return usersData;
            },
            renderer: function(data){
                var userRemark = '';
                if (data.remark) {
                    userRemark = '<br/><span class="fa fa-fw"></span><small><span class="fa fa-warning text-danger"></span> ' + data.remark + '</small>';
                }
                var userIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
                var userRow = '<div>' + userIcon + ' <span class="username">' + data.username + '</span>' + (data.full_name ? '<span class="full_name">[' + data.full_name + ']</span>' : '') + '<br><span class="fa fa-fw"></span><small>' + data.email + '</small>' + userRemark + '</div>';
                return userRow;
            },
            selectionRenderer: function(data){
                var userRemark = '';
                if (data.remark) {
                    userRemark = '<span class="fa fa-warning fa-fw text-danger" data-toggle="tooltip" data-type="html" data-placement="right" title="' + data.remark + '"></span>';
                }
                var userIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
                var userRow = userRemark + userIcon + ' ' + data.username;
                return userRow;
            }
        });

        var $uid = $user_id.val();
        if ($.trim($uid)) {
            var $username = $user_id.data('username');
            if ('undefined' !== typeof $username && $.trim($username)) {
                user_filter.setSelection([{id: parseInt($uid), username: $.trim($username)}]);
            }
        }

        $(user_filter).on('selectionchange', function(e, m){
            var selectedItem = this.getSelection();
            if (selectedItem.length == 1) {
                selectedItem = selectedItem[0];
            } else {
                selectedItem = {id: '', username: ''};
            }
            $user_id.val(selectedItem.id).attr('data-username', selectedItem.username).data('username', selectedItem.username);
            get_users_public_phones();
        });
    }

    $('.moreless_items').moreless();

    $('select.moderation-reasons').selectpicker({
        style: 'btn btn-default btn-sm',
        size: 8,
        liveSearch: true,
        title: 'Choose reason',
        noneSelectedText: 'Choose reason',
        noneResultsText: 'No reasons match',
        mobile: OGL.is_mobile_browser,
        showSubtext: false
    });

    var $moderation = $('#moderation');
    $moderation.change(function(e){
        $('#moderation_reason_box, .moderation-reason-group, #reason_email_box').hide();
        if (parseInt($(this).find(':selected').data('has-reasons'))) {
            $('#moderation_reason_box, #moderation_reason_' + $(this).val() + '_box, #reason_email_box').show();
        }
    });
    $moderation.trigger('change');

    $('#refreshPhoneNumbers').click(function(){
        get_users_public_phones();
    });

    // change ad's category
    var $new_category_dropdown = $('#new_category_id');
    $new_category_dropdown.selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Choose category',
        noneSelectedText: 'Choose category',
        noneResultsText: 'No categories match',
        mobile: OGL.is_mobile_browser,
        showSubtext: true
    });
    $new_category_dropdown.on('change', get_matching_parameters);
    $('#changeCategoryBtn').click(function(){
        $('#changeAdsCategoryModal').modal({
            backdrop: 'static',
            keyboard: true
        }).on('shown.bs.modal', function(e){
            $('button.dropdown-toggle').trigger('click');
        });
    });
    $('#changeAdsCategoryModalMoveBtn').click(function(){
        if ('undefined' !== typeof $(this).data('target-category-id') && parseInt($.trim($(this).data('target-category-id')))) {
            $('#category_id').val(parseInt($.trim($(this).data('target-category-id'))));
            // submit the form immediately.
            $('button[name="save"]').click();
        }
    });

    // Show AVUS-related warnings when needed
    warn_about_old_offline_products();
});
