$(document).ready(function(){
    $('#url').generateSlug('#name');

    // TODO: this is a quick fix to apply title attribute on options so we can have clean text when option i selected
    $('#parent_id option').each(function(i, elem){
        $(elem).attr('title', $.trim($(elem).text().replace(/^\-*/,'')));
    });

    $('#parent_id').selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Select categories',
        noneResultsText: 'No categories match',
        mobile: OGL.is_mobile_browser
    });
    $('#type').selectpicker({
        style: 'btn btn-default',
        size: 8,
        title: 'Select category type',
        mobile: OGL.is_mobile_browser
    });
    $('#template').selectpicker({
        style: 'btn btn-default',
        size: 8,
        title: 'Select category template',
        mobile: OGL.is_mobile_browser
    });
});
