var hasChanges = false;
var $categorySelectPicker = $('#category_ids');
var $categorySorter = $('div.category-sorter');
var $categorySorterUL = $categorySorter.find('ul.sortable');

function initCategorySelectpickerAndSorter() {
    $categorySelectPicker.change(function(event){
        updateCategorySorter();
    });

    $categorySorterUL.find('.actions-cat .category-remove').click(function(){
        removeCategorySorterBit($(this).closest('.sortable_li').data('id'), true);
    });
}

function getSelectedCategoriesDataFromSelectpicker() {
    var selectedCategoriesData = [];
    $categorySelectPicker.find(':selected').each(function(i, option) {
        selectedCategoriesData.push({
            id  : parseInt($(option).attr('value')),
            name: $.trim($(option).attr('title'))
        });
    });
    return selectedCategoriesData.length ? selectedCategoriesData : null;
}

function updateCategorySorter() {
    var selectedCategoriesData = getSelectedCategoriesDataFromSelectpicker();
    var selectedCategoryIDs = [];
    var categoriesToRemoveFromSorter = [];
    if (selectedCategoriesData) {
        $(selectedCategoriesData).each(function(i, categoryData) {
            selectedCategoryIDs.push(categoryData.id);
            // see if we have current category in the sorter, if yes, do nothing
            var $foundCategorySortBit = $categorySorter.find('#item_' + categoryData.id);
            if (0 === $foundCategorySortBit.length) {
                // add sort bit
                insertCategorySorterBit(categoryData);
            }
        });
    }
    // run through current sorting bits and queue them for removal...
    $categorySorterUL.find('li.sortable_li').each(function(i, li){
        var liCategoryID = $(li).data('id');
        if ('undefined' !== typeof liCategoryID && parseInt(liCategoryID) && -1 === selectedCategoryIDs.indexOf(parseInt(liCategoryID))) {
            categoriesToRemoveFromSorter.push(parseInt(liCategoryID));
        }
    });
    if (categoriesToRemoveFromSorter.length) {
        removeCategorySorterBit(categoriesToRemoveFromSorter);
    }
}

function insertCategorySorterBit(categoryData) {
    $categorySorterUL.append(
        $('<li/>')
            .addClass('sortable_li')
            .attr('id', 'item_' + categoryData.id)
            .attr('data-id', categoryData.id).data('id', categoryData.id)
            .append(
                $('<div/>')
                    .addClass('sortable_div')
                    .append(
                        $('<div/>')
                            .addClass('sortable_row')
                            .append(
                                $('<div/>')
                                    .addClass('drag-me ui-sortable-handle')
                                    .append(
                                        $('<span/>')
                                            .addClass('fa fa-arrows fa-fw')
                                    )
                            )
                            .append(
                                $('<div/>')
                                    .addClass('name-cat')
                                    .append(
                                        $('<span/>')
                                            .addClass('name')
                                            .text(categoryData.name)
                                    )
                            )
                            .append(
                                $('<div/>')
                                    .addClass('actions-cat')
                                    .append(
                                        $('<span/>')
                                            .addClass('category-remove cursor-pointer')
                                            .append(
                                                $('<span/>')
                                                    .addClass('fa fa-trash-o text-danger')
                                            )
                                            .click(function(){
                                                removeCategorySorterBit($(this).closest('.sortable_li').data('id'), true);
                                            })
                                    )
                            )
                    )
            )
    ).fadeIn('fast', function(){
        // (!!!) this method is in list-sortable.js (!!!)
        reorder_last_serialization = $('.sortable').sortable('serialize');
        reorder_check_changes();
    });
}

function removeCategorySorterBit(val, uncheckInSelectpicker) {
    if ('undefined' === typeof uncheckInSelectpicker) {
        uncheckInSelectpicker = false;
    }

    var values = [];
    if ($.isArray(val)) {
        values = val;
    } else if ('object' !== typeof val && parseInt($.trim(val)) == $.trim(val)) {
        values.push(parseInt(val));
    }

    if (values.length) {
        $(values).each(function(i, id){
            var $categoryLI = $categorySorterUL.find('#item_' + $.trim(id));
            if ('undefined' !== typeof $categoryLI && 1 == $categoryLI.length) {
                $categoryLI.fadeOut('fast', function() {
                    $categoryLI.remove();

                    if (uncheckInSelectpicker) {
                        $categorySelectPicker.find('option[value="' + $.trim(id) + '"]').prop('selected', false);
                        $categorySelectPicker.selectpicker('refresh');
                    }

                    // (!!!) this method is in list-sortable.js (!!!)
                    reorder_last_serialization = $('.sortable').sortable('serialize');
                    reorder_check_changes();
                });
            }
        });
    }
}

$(document).ready(function(){
    $('#url').generateSlug('#name');

    initCategorySelectpickerAndSorter();

    $categorySelectPicker.selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        selectedTextFormat: 'count>3',
        title: 'Select categories',
        noneSelectedText: 'Select categories',
        noneResultsText: 'No categories match',
        mobile: OGL.is_mobile_browser
    });

    $('#location').selectpicker({
        style: 'btn btn-default',
        mobile: OGL.is_mobile_browser
    });

    var $sectionIcon = $('#section_icon');
    $sectionIcon.selectpicker({
        style: 'btn btn-default',
        size: 8,
        title: 'Select section\'s icon',
        mobile: OGL.is_mobile_browser
    });

    var $linkToSpecificCategory = $('#json_link_to_specific_category');
    $linkToSpecificCategory.selectpicker({
        style: 'btn btn-default',
        size: 8,
        title: 'Select category to link to',
        mobile: OGL.is_mobile_browser
    });
    $('#link_type').change(function(){
        var linkType = parseInt($(this).val());
        switch(linkType) {
            case 1:
                $('#custom-link-box').hide();
                $('#json_custom_link').val('');
                $('#specific-category-link-box').show();
                break;
            case 2:
                $('#custom-link-box').show();
                $('#specific-category-link-box').hide();
                $('#json_link_to_specific_category').selectpicker('val', '');
                break;
            default:
                $('#custom-link-box, #specific-category-link-box').hide();
                $('#json_custom_link').val('');
                $('#json_link_to_specific_category').selectpicker('val', '');
        }
    });

    $('input[type=text],select,textarea').change(function(){
        hasChanges = true;
    });

    $deleteBackgroundPicBtn = $('#deleteBackgroundPicBtn');
    if ($deleteBackgroundPicBtn.length) {
        $deleteBackgroundPicBtn.click(function(){
            $deleteBackgroundPicBtn.closest('.backgroundPicPreview').fadeOut('fast', function(){
                $deleteBackgroundPicBtn.closest('.backgroundPicPreview').remove();
                $('#deleteBackgroundPic').val('delete');
            });
        });
    }

    $deleteSponsorshipLogoBtn = $('#deleteSponsorshipLogoBtn');
    if ($deleteSponsorshipLogoBtn.length) {
        $deleteSponsorshipLogoBtn.click(function(){
            $deleteSponsorshipLogoBtn.closest('.sponsorshipPreview').fadeOut('fast', function(){
                $deleteSponsorshipLogoBtn.closest('.sponsorshipPreview').remove();
                $('#deleteSponsorshipLogo').val('delete');
            });
        });
    }

    $('#frm_section').submit(function(event){
        $('#json_categories').val(reorder_final_ids.join(','));
    });

});
