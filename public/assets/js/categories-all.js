$(document).ready(function(){
    if (jQuery().masonry && $('.masonry-categories .masonry-item').length) {
        $('.masonry-categories').masonry({
            itemSelector: '.masonry-item',
        });
        $('.masonry-categories').imagesLoaded().progress(function(){
            $('.masonry-categories').masonry('layout');
        });
    }
});
