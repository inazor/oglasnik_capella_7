var googleMapObject = null,
    has_blueimp = ('undefined' !== typeof blueimp);

if (has_blueimp) {  // init blueimp stuff...
    /**
     * Custom Factory for rendering `spin/360` slide types.
     *
     * @param obj
     * @param callback
     * @returns {*}
     */
    blueimp.Gallery.prototype.spinFactory = function (obj, callback) {
        var $element = $('<div>')
            .addClass('spin-content')
            .attr('title', obj.title);
        // .css('height', '520px');

        var href = obj.href;
        var psv;
        psv = new PhotoSphereViewer({
            panorama: href,
            container: $element[0],
            loading_msg: 'Učitavanje 360 panorame...',
            //loading_imh: '',
            //loading_html:'',
            time_anim: 1000,
            navbar: true,
            // autoload:false,
            smooth_user_moves: false,
            navbar_style: {
                backgroundColor: 'rgba(58, 67, 77, 0.7)'
            }
        });

        this.setTimeout(callback, [{
            type: 'load',
            target: $element[0]
        }]);

        return $element[0];
    };

    /**
     * Custom Factory for rendering `spingw/360` slide types.
     *
     * @param obj
     * @param callback
     * @returns {*}
     */
    blueimp.Gallery.prototype.spingwFactory = function (obj, callback) {
        var id = obj.href;

        var $element = $('<div>')
            .addClass('spingw-content')
            .attr('title', obj.title);

        // Move existing gw element into the wrapper created above
        var $gw_element = jQuery('#gw-element-' + id);
        if ($gw_element.length) {
            $gw_element.appendTo($element);
        }

        this.setTimeout(callback, [{
            type: 'load',
            target: $element[0]
        }]);

        return $element[0];
    };

    var $gallery      = $('#galerija');
    var $thumbnails   = $gallery.find('ul.thumbnails');
    var $thumbnailsLI = $thumbnails.find('li');

    /**
     * Helper method to get all media files suitable for gallery
     */
    function getAvailableMedia(galleryStyle) {
        if ('undefined' === typeof galleryStyle) {
            galleryStyle = 'carousel';
        }
        var availableMedia = [];

        var title = 'undefined' !== typeof $thumbnails.data('title') && $.trim($thumbnails.data('title')) ? $.trim($thumbnails.data('title')) : null;
        // get all images
        $thumbnailsLI.filter('[data-' + galleryStyle + '-url]').each(function(i, elem){
            var $el = $(elem);
            var type = $el.data('type');
            var $img = $el.find('img');
            var imageFile = {
                href: $el.data(galleryStyle + '-url'),
                thumbnail: $img.attr('src'),
                orient: $img.attr('data-orient'),
                width: $img.attr('data-width'),
                height: $img.attr('data-height'),
                class: $el.data('class')
            };
            if ('' !== type && 'undefined' !== typeof type) {
                imageFile.type = type;
            }
            if ('' !== title) {
                imageFile.title = title;
            }
            availableMedia.push(imageFile);
        });

        // TODO/FIXME:
        // Videos have their own fullscreen implementations,
        // so this code shouldn't really be needed any more, but
        // it calls getAvailableVideoFiles() which then does a whole lot more
        // and produces the video thumbnails that we need...

        var videoMedia = getAvailableVideoFiles();
        if (videoMedia.length > 0) {
            $.each(videoMedia, function (i, video) {
                availableMedia.push(video);
            });
        }

        return availableMedia;
    }

    // get potential video uris (youtube or vimeo)
    function getAvailableVideoFiles() {
        // simple "static" var to make sure we only build thumbs once
        getAvailableVideoFiles.doneThumbs = getAvailableVideoFiles.doneThumbs || false;

        var foundVideoMedia = [];

        var $container = $('.oglas-details');
        if ($container.length > 0) {
            $.each($container.getAllURIParameters('video'), function(i, foundVideo){
                var videoFile = {
                    href: foundVideo.url,
                    type: 'text/html'
                };
                if ('youtube' === foundVideo.type) {
                    videoFile.title = 'YouTube video';
                    videoFile.youtube = foundVideo.video_id;
                } else if ('vimeo' === foundVideo.type) {
                    videoFile.title = 'Vimeo video';
                    videoFile.vimeo = foundVideo.video_id;
                }
                if (foundVideo.poster && 0 == $thumbnailsLI.filter('[data-carousel-url="' + foundVideo.url + '"]').length) {
                    videoFile.poster = foundVideo.poster;
                    if (!getAvailableVideoFiles.doneThumbs) {
                        appendVideoThumb(foundVideo);
                    }
                }
                foundVideoMedia.push(videoFile);
            });
            // Mark doneThumbs after passing over the results above once
            getAvailableVideoFiles.doneThumbs = true;
        }

        return foundVideoMedia;
    }

    function appendVideoThumb(video) {
        $thumbnails.append(
            $('<li/>')
                .addClass('video')
                .attr('data-carousel-url', video.poster).data('carousel-url', video.poster)
                .attr('data-type', video.type).data('type', video.type)
                .attr('data-class', 'video').data('class', 'video')
                // .attr('data-fullscreen-url', foundVideo.poster).data('fullscreen-url', foundVideo.poster)
                .append(
                    $('<img>')
                        .attr('src', video.poster)
                        .attr('height', 80)
                        .attr('width', 80)
                )
        );

        // TODO/FIXME: see if this really works on the new layout (and is it needed at all... nuke if not)
/*
        // handle removal of found parameter from it's original container
        var $parameterElem = video.elem.closest('.URLParameter');
        if ('undefined' !== typeof $parameterElem) {
            // see if it's parent has only 1 elem... if so, remove the parent aswell
            if ($parameterElem.siblings().size() === 1) {
                // as we're dealing with a classic <section> as a container for parameters, we have to see if
                // parent <section> has only 1 .dbParam class, if so, this is the only parameter here
                // so, instead of removing the parent, we'll remove the entire <section>
                if ($parameterElem.closest('section').find('.dbParam').size() === 1) {
                    $parameterElem.closest('section').remove();
                } else {
                    // there are other parameters here, so just remove this one...
                    video.elem.closest('.URLParameter').remove();
                }
            } else {
                video.elem.closest('.URLParameter').remove();
            }
        }
*/

        // Refreshing the global list of thumbnails this spaghetti relies on,
        // otherwise video thumbs (which are created dynamically above) aren't
        // properly clickable
        $thumbnailsLI = $thumbnails.find('li');
    }

    function setActiveThumbnail(idx) {
        if ($thumbnailsLI.length) {
            $thumbnailsLI.removeClass('active');
            $thumbnailsLI.eq(idx).addClass('active');
        }
    }

    /**
     * Moves the element holding the rendered spingw/360 thingy back to the specified
     * gallery's current slide content (in case it's not there already).
     * This happens at various points since we're moving the <div data-gw> around depending
     * on what is opened.
     * Bails early if the specified gallery's current slide is not of 'spingw/360' type.
     *
     * @param gallery
     */
    function repositionGwElement(gallery) {
        var idx = gallery.getIndex();
        var o = gallery.list[idx];
        var slide_el = gallery.slides[idx];

        // Don't bother if called when not needed
        if (!o || 'spingw/360' !== o.type) {
            return;
        }

        var gw_el = document.getElementById('gw-element-' + o.href) || null;
        // If slide doesn't contain gw_el, append it into the slide's firstChild
        if (gw_el && !jQuery.contains(slide_el, gw_el)) {
            slide_el.firstChild.appendChild(gw_el);
        }
    }

    // setup galleryCarousel and return it
    function galleryCarousel(items, has_360) {
        var galleryOptions = {
            container: '#blueimp-image-carousel',
            stretchImages: true,
            carousel: true,
            youTubeClickToPlay: false,
            clearSlides: false,
            unloadElements: false,
            youTubePlayerVars: { 'wmode':'transparent', 'autoplay':1 },
            onslide: function(idx, slide) {
                setActiveThumbnail(idx);
                // Add extra classes to the .slide container so that we can tweak stuff
                var o = this.list[idx];
                if (o) {
                    var $slide = jQuery(slide);
                    // Mark images that aren't big enough, but only if it's 
                    // not our no-thumb-svg thingy...
                    if (!o.class || o.class.indexOf('no-thumb-svg') <= 0) {
                        if (o.width < 520 || o.height < 450 ) {
                            $slide.addClass('bg-size-auto small-original-upload');
                        }
                    } else {
                        // no-thumb-svg treated as 'cover'
                        $slide.addClass('cover');
                        $slide.addClass(o.class);
                    }
                    if (o.orient) {
                        $slide.addClass('orientation-' + o.orient);
                    }
                }
            },
            onslideend: function(idx, slide) {
                repositionGwElement(this);
            },
            onopened: function(){
                // Extremely hacky way to turn off swipe support for carousel when there's a 360 in there...
                // Can't turn it off per slide easily (which would be ideal for our use case)
                // List of events is lifted from https://github.com/blueimp/Gallery/blob/master/js/blueimp-gallery.js#L1250
                if (this.support.touch && has_360) {
                    this.slidesContainer.off('touchstart touchmove touchend touchcancel', this.proxyListener);
                }
            }
        };

        // We decided to stop autoplay if 360s are on the page at some point...
        if (has_360) {
            galleryOptions.startSlideshow     = false;
            galleryOptions.emulateTouchEvents = false;
        }

        return blueimp.Gallery(items, galleryOptions);
    }

    // initialize stuff
    function initialize_blueimp_galleries() {
        if (!has_blueimp || $gallery.length == 0) {
            return;
        }

        var has_360 = jQuery('.spin360').length > 0;

        // Grab needed items once
        var carousel_items = getAvailableMedia('carousel');
        var fullscreen_items = getAvailableMedia('fullscreen');
        var carousel_gallery = galleryCarousel(carousel_items, has_360);

        // initialize full screen for clicks on the carousel
        var $galleryCarousel = $('#blueimp-image-carousel');
        var $galleryCarouselSlides = $galleryCarousel.find('.slides');
        if ($galleryCarouselSlides.length > 0) {
            $galleryCarouselSlides.on('click', '.slide', function(event){
                var $el = $(this);
                var idx = $el.index();

                // Don't trigger fullscreen for videos
                if ($el.find('.video-content').length > 0) {
                    return true;
                }

                // Don't trigger fullscreen for PSV 360 spinners, they have their own fs buttons hopefully
                if ($el.find('.spin-content').length > 0) {
                    return true;
                }

                var fullscreen_options = {
                    container: '#blueimp-fullscreen-gallery',
                    index: idx,
                    event: event,
                    youTubeClickToPlay: false,
                    hidePageScrollbars: false,
                    clearSlides: false,
                    unloadElements: false,
                    youTubePlayerVars: { 'wmode':'transparent', 'autoplay':1 },
                    closeOnSwipeUpOrDown: false,
                    onopen: function() {
                        // Pause the carousel if it's in autoplay mode
                        if (this.options.startSlideshow) {
                            carousel_gallery.pause();
                        }
                    },
                    onclose: function() {
                        carousel_gallery.slide(this.getIndex());
                    },
                    onclosed: function() {
                        // This doesn't appear to change anything if it's already at that index,
                        // and it is, since we're pausing/opening fullscreen on clicks
                        carousel_gallery.slide(this.getIndex());

                        // Reposition GW spin when closed too, since just changing slides doesn't work reliably
                        repositionGwElement(carousel_gallery);

                        // Continue playing carousel slideshow only if the option says so
                        // (it's turned off for 360s)
                        if (this.options.startSlideshow) {
                            carousel_gallery.play();
                        }

                        // Add classes to show controls
                        $galleryCarousel.addClass('blueimp-gallery-controls');
                    },
                    onslide: function(idx, slide) {
                        repositionGwElement(this);
                    }
                };

                if (has_360) {
                    fullscreen_options.emulateTouchEvents = false;
                }

                var fullscreen_gallery = blueimp.Gallery(fullscreen_items, fullscreen_options);
            });
        }

        // Activate the matching slide in carousel_gallery when a corresponding thumb is clicked
        if ($thumbnailsLI.length > 0) {
            $thumbnailsLI.on('click', function(){
                var $el = $(this);
                var idx = $el.index();
                carousel_gallery.slide(idx);
            });
        }
    }
}

jQuery(document).ready(function() {
    if (has_blueimp) {
        initialize_blueimp_galleries();
    }
    var $googleMapContainer = $('#google_map_container');
    if ($googleMapContainer.length > 0) {
        var $showNearByOnMap = $('#show-near-by-on-map');
        var $scrollToMap = $('a.scrollToMap');
        var classifiedId = parseInt($showNearByOnMap.data('classified-id'));
        googleMapObject = $googleMapContainer.initGoogleMap({
            'add_marker': true,
            'marker_title': 'Trenutni oglas',
            'classifiedId': classifiedId,
            'map': {
                'zoomControl': false,
                'scrollwheel': false,
                'draggable' : false
            }
        });
        $showNearByOnMap.change(function(){
            if ($(this).prop('checked')) {
                googleMapObject.showNearBy();
            } else {
                googleMapObject.hideNearBy();
            }
        });
        if ($scrollToMap.length) {
            $scrollToMap.click(function(event){
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: $googleMapContainer.offset().top - 50
                }, 500);
            });
        }
    }
    $('#frm_modal_contact').ajaxifyModalForm();
    $('#btnFBShare').socialShare({'service':'facebook'});
    $('#btnGooglePlusShare').socialShare({'service':'googleplus'});
    $('#btnTwitterShare').socialShare({'service':'twitter'});
    $('#btnEmailShare').socialShare({'service':'email'});

    $('.ad-details').tabbifyClassifiedsDetails();
});
