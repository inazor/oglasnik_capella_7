/**
 * GoogleMaps initialization method
 */
(function ($) {
    function GMApi($this, settings) {
        this.$map_container = $this;
        this.map_container_id = $this.attr('id');
        this.map = null;
        this.map_center = {
            'lat': 0,
            'lng': 0
        };
        this.map_zoom = 15;
        this.map_address = null;
        this.marker = null;
        this.nearByMarkers = [];
        this.$input_field = null;
        this.fetchNearBy = false;
        this.classifiedInfoBox = null;
        this.settings = {
            'input_field': false,
            'drag_marker': false,
            'add_marker': false,
            'marker_title': null,
            'save_place': null,
            'classifiedId': null,
            'map': {
                'zoomControl': true,
                'scaleControl': false,
                'scrollwheel': true,
                'draggable': true
            }
        };

        this.loadedMarkers = {};
        this.oms = null;
        this.spiderified = false;

        var selfObj = this;

        if ('object' === typeof settings) {
            $.extend(this.settings, settings);
        }
        if ('undefined' !== typeof this.$map_container && 1 === this.$map_container.length) {
            if ('undefined' !== typeof this.$map_container.data('address')) {
                this.map_address = $.trim(this.$map_container.data('address'));
            }
            if ('undefined' !== typeof this.$map_container.data('lat')) {
                this.map_center.lat = parseFloat(this.$map_container.data('lat'));
            }
            if ('undefined' !== typeof this.$map_container.data('lng')) {
                this.map_center.lng = parseFloat(this.$map_container.data('lng'));;
            }
            if (this.map_center.lat || this.map_center.lng || this.map_address) {
                if (this.map_address) {
                    var geocoder = new google.maps.Geocoder();
                    if (geocoder) {
                        geocoder.geocode({'address': this.map_address}, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                                    var map_center = results[0].geometry.location;
                                    selfObj.drawMap(map_center);
                                }
                            }
                        });
                    }
                } else {
                    var map_center = new google.maps.LatLng(this.map_center.lat, this.map_center.lng);
                    selfObj.drawMap(map_center);
                }
            }
        }
    }

    GMApi.prototype.drawMap = function(map_center) {
        var selfObj = this;

        var map_options = {
            zoom: this.map_zoom,
            center: map_center,
            zoomControl: this.settings.map.zoomControl,
            scaleControl: this.settings.map.scaleControl,
            scrollwheel: this.settings.map.scrollwheel,
            draggable: this.settings.map.draggable
        };
        if ('undefined' != typeof this.$map_container.data('zoom') && parseInt(this.$map_container.data('zoom'))) {
            this.map_zoom = parseInt(this.$map_container.data('zoom'));
            map_options.zoom = this.map_zoom;
        }
        this.map = new google.maps.Map(
            document.getElementById(this.map_container_id),
            map_options
        );

        if (this.settings.add_marker || this.settings.drag_marker) {
            if (this.settings.input_field) {
                this.$input_field = this.settings.drag_marker ? jQuery('#' + this.settings.input_field) : null;
                if ('undefined' === typeof this.$input_field || 1 !== this.$input_field.length) {
                    this.$input_field = null;
                }
            }

            if (this.settings.add_marker) {
                this.setMarker(map_center.lat(), map_center.lng());
            } else {
                google.maps.event.addListener(this.map, 'click', function(event) {
                    selfObj.setMarker(event.latLng.lat(), event.latLng.lng());
                });
            }
        }

        google.maps.event.addListener(this.map, 'dragend', function(event) {
            selfObj.refreshNearBy();
        });

        // re-center the map on window resize
        google.maps.event.addDomListener(window, 'resize', function(){
            var center = selfObj.map.getCenter();
            google.maps.event.trigger(selfObj.map, 'resize');
            selfObj.map.setCenter(center);
        });

        if ('undefined' !== typeof OverlappingMarkerSpiderfier) {
            this.oms = new OverlappingMarkerSpiderfier(this.map, {nearbyDistance:40, keepSpiderfied:true, markersWontMove:true, markersWontHide:true});
            var iw = new google.maps.InfoWindow();
            this.oms.addListener('click', function (marker, event) {
                iw.setContent(marker.desc);
                iw.open(selfObj.map, marker);
            });
            this.oms.addListener('spiderfy', function(spiderifiedMarkers, unspiderifiedMarkers) {
                selfObj.spiderified = true;
            });
            this.oms.addListener('unspiderfy', function(spiderifiedMarkers, unspiderifiedMarkers){
                selfObj.spiderified = false;
            });
        }
    };

    GMApi.prototype.changeView = function(lat, lng, zoom) {
        this.map.panTo(new google.maps.LatLng(lat, lng));
        this.map.setZoom(zoom);
    };

    GMApi.prototype.setMarker = function(lat, lng) {
        if (this.marker) {
            this.marker.setPosition(new google.maps.LatLng(lat, lng));
        } else {
            this.marker = new google.maps.Marker({
                animation: 'DROP',
                draggable: this.settings.drag_marker,
                position: new google.maps.LatLng(lat, lng),
                map: this.map,
                icon: '/assets/img/icn-location-blue-stroke.png',
                title: (this.settings.drag_marker ? 'Odabrana lokacija' : (this.settings.marker_title ? this.settings.marker_title : 'Lokacija na karti'))
            });

            if (this.settings.drag_marker && this.$input_field) {
                var input_field = this.$input_field;
                var marker = this.marker;
                google.maps.event.addListener(marker, 'dragend', function() {
                    input_field.val(marker.getPosition().lat() + ',' + marker.getPosition().lng());
                });
            }
        }

        if (this.$input_field) {
            this.$input_field.val(this.marker.getPosition().lat() + ',' + this.marker.getPosition().lng());
        }
    };

    GMApi.prototype.buildInfoWindowMarkup = function(classified) {
        var markup = '<div data-classified-id="' + classified.id + '" class="ad-box ad-box-default mapPreview">';
        markup += '<div class="image-wrapper"><a href="' + classified.frontend_url + '" class="classified-img">' + classified.thumb + '</a></div>';
        markup += '<h3><a href="' + classified.frontend_url + '">' + classified.title + '</a></h3>';
        if (classified.price) {
            markup += '<div class="classified-footer">';
            markup += '<span class="price-kn">' + (classified.price.main ? classified.price.main : '') + '</span>';
            markup += '<span class="price-euro">' + (classified.price.other ? classified.price.other : '') + '</span>';
            markup += '</div>';
        }
        markup += '</div>';

        return markup;
    };

    GMApi.prototype.clearNearByMarkers = function() {
        if (this.nearByMarkers.length) {
            $(this.nearByMarkers).each(function(i, marker){
                marker.setMap(null);
            });
            this.nearByMarkers = [];
            this.loadedMarkers = {};
            if (this.oms) {
                this.oms.clearMarkers();
            }
        }
    };

    GMApi.prototype.refreshNearBy = function() {
        if (this.fetchNearBy) {
            // this.clearNearByMarkers();

            var _self = this;

            var apiURL = '/ajax/getNearBy';
            var bounds = this.map.getBounds().toJSON();
            jQuery.post(
                apiURL, 
                {
                    'classifiedId': _self.settings.classifiedId,
                    'bounds': JSON.stringify(bounds)
                },
                function(json) {
                    if (json.status) {
                        if (json.classifieds.length) {
                            // this.clearNearByMarkers();
                            $(json.classifieds).each(function(i, classified){
                                // Skip already loaded markers
                                if (_self.loadedMarkers[classified.id]) {
                                    return;
                                }

                                var markup = _self.buildInfoWindowMarkup(classified);
                                var nearByMarker = new google.maps.Marker({
                                    animation: 'DROP',
                                    position: new google.maps.LatLng(classified.lat, classified.lng),
                                    map: _self.map,
                                    icon: '/assets/img/icn-location-dark-stroke.png',
                                    title: classified.title,
                                    desc: markup
                                });

                                _self.nearByMarkers.push(nearByMarker);
                                _self.loadedMarkers[classified.id] = true;

                                if (_self.oms) {
                                    _self.oms.addMarker(nearByMarker);
                                }
                            });

                            // trying to auto-spiderify stuff that needs to be
                            if (_self.oms) {
                                // Look for any markers near the main marker first, but only if it's not already done
                                if (!_self.spiderified) {
                                    var nearMarkers = _self.oms.markersNearMarker(_self.marker, true);
                                    if (nearMarkers && nearMarkers[0]) {
                                        google.maps.event.trigger(nearMarkers[0], 'click');
                                    }
                                }
                            }
                        }
                    }
                },
                'json'
            );
        }
    };

    GMApi.prototype.disableDraggableIfEnabled = function() {
        if (this.settings.map.draggable) {
            this.settings.map.draggable = false;
            this.map.setOptions(this.settings.map);
        }
    };

    GMApi.prototype.enableDraggableIfDisabled = function() {
        if (!this.settings.map.draggable) {
            this.settings.map.draggable = true;
            this.map.setOptions(this.settings.map);
        }
    };

    GMApi.prototype.showNearBy = function() {
        this.enableDraggableIfDisabled();
        this.fetchNearBy = true;
        this.refreshNearBy();
    };

    GMApi.prototype.hideNearBy = function() {
        this.disableDraggableIfEnabled();
        this.fetchNearBy = false;
        this.clearNearByMarkers();
    };

    jQuery.fn.initGoogleMap = function(settings) {
        return new GMApi(this, settings);
    };
}(jQuery));
