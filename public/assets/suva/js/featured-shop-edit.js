/*global category_tree_items:false */
var $published_at_datetimepicker,
    $expires_at_datetimepicker;

function import_children(children, initial_level) {
    $.each(children, function(i, elem){
        var prefix = '-'.repeat((elem.level - initial_level) - 1) + ' ';
        var $option = $('<option/>').val(elem.id).text(prefix + elem.name);
        var subtext = elem.path.text.split(' › ').slice(0, -1).join(' › ');
        if (subtext !== elem.name) {
            $option.attr('data-subtext', elem.path.text).data('subtext', subtext);
        }
        $('#additional_category_ids').append($option);
        if ('undefined' !== typeof elem['children']) {
            import_children(elem['children'], initial_level);
        }
    });
}

function populate_additional_category_dropdown(parent_id) {
    var $additional_cat_select = $('#additional_category_ids');
    var $additional_cat_options = $additional_cat_select.find('option');
    $additional_cat_options.remove();
    $additional_cat_select.selectpicker('refresh');
    if (parseInt(parent_id) && 'undefined' !== typeof category_tree_items[parent_id]) {
        var initial_level = parseInt(category_tree_items[parent_id]['level']);
        var parents_children = category_tree_items[parent_id]['children'];
        if ('undefined' !== typeof parents_children) {
            import_children(parents_children, initial_level);
        }
    }
    // Number of options might've changed above, we need new state here
    var $new_options = $additional_cat_select.find('option');
    if ($new_options.size() == 0) {
        $additional_cat_select.prop('disabled', true);
        $('#additional_cat_div').hide();
    } else {
        $additional_cat_select.prop('disabled', false);
        $('#additional_cat_div').show();
    }
    $additional_cat_select.selectpicker('refresh');
}

function load_paginated_results($container, $sender) {
    $.get(
        $sender.data('page-url'),
        function(json) {
            if (json.status) {
                $container.html(json.data);
                ajaxify_pagination($container);
            }
            shopping_windows_editor.init();
        },
        'json'
    );
}

function ajaxify_pagination($container) {
    $container.find('ul.pagination a').each(function(i, elem){
        $(elem)
            .attr('data-page-url', $(elem).attr('href'))
            .data('page-url', $(elem).attr('href'))
            .click(function(){
                load_paginated_results($container, $(this));
                return false;
            });
    });
}

var shopping_windows_editor = {
    init: function () {
        // Build the collection of ads already present in the loaded shopping window
        var collection = {};
        $('.featured-shops-box-preview .classifieds div[data-ad-id]').each(function (i, el) {
            var $el = $(el);
            var id = $el.attr('data-ad-id');
            if (id) {
                // (key is prefixed with an underscore to avoid Chrome's optimization of not
                // maintaining key order for numeric indexes - see http://stackoverflow.com/a/3550355)
                collection['_' + id] = id;
            }
        });
        this.collection = collection;

        // bind clicks
        var self_ = this;
        $('.add-to-shopping-window').on('click', function (e) {
            e.preventDefault();
            var ad_id = $(this).parent().prev().attr('data-ad-id');
            self_.add(ad_id);
        });
        $('.remove-from-shopping-window').on('click', function (e) {
            e.preventDefault();
            var ad_id = $(this).parent().prev().attr('data-ad-id');
            self_.remove(ad_id);
        });

        // refresh ui state
        this.refresh_ui();

        // make sure links within the shopping window open in new windows to not loose changes
        this.change_links_target();
    },
    change_links_target: function () {
        $('.featured-shops-box-preview').find('a').attr('target', '_blank');
    },
    refresh_ui: function () {
        // go through the list of available ads and enable/disable their buttons accordingly
        var $boxes = $('.selectable-ads').find('.post-box');
        var self_ = this;
        $boxes.each(function (i, box) {
            var $box = $(box);
            var $box_actions = $box.next();
            var box_ad_id = $box.attr('data-ad-id');

            // Ads without an image can no longer be added to the shopping window. For some reason...
            var no_img = $box.find('img.no-thumb');
            if (no_img.length > 0) {
                $box_actions.find('.add-to-shopping-window').attr('disabled', 'disabled').addClass('disabled');
                $box_actions.find('.no-image-warning').removeClass('hidden');
            }

            if (self_.is_added(box_ad_id)) {
                // hide add link, show delete link
                $box_actions.find('.add-to-shopping-window').removeClass('hidden').addClass('hidden');
                $box_actions.find('.remove-from-shopping-window').removeClass('hidden');
            } else {
                // hide delete link, show add link
                $box_actions.find('.remove-from-shopping-window').removeClass('hidden').addClass('hidden');
                $box_actions.find('.add-to-shopping-window').removeClass('hidden');
            }
        });

        // Hide shopping window's .row.featured if it's empty
        var $row = $('.featured-shops-box-preview .classifieds .row.featured');
        var $ads = $row.find('div[data-ad-id]');
        if ($ads.length <= 0) {
            // hide if empty
            $row.addClass('hidden');
        } else {
            // show if it was hidden (until we added a new first item)
            $row.removeClass('hidden');
        }

        // Show/hide .shop-about when needed
        var current_total = this.num_keys(this.collection);
        var $about = $('.featured-shops-box-preview .shop-about');
        if (current_total <= 0) {
            $about.removeClass('hidden');
            // hide the .featured row if it's empty
        } else {
            if (!($about.hasClass('hidden'))) {
                $about.addClass('hidden');
            }
        }

        // Refresh the hidden input containing the ids
        $('#ads').val(this.get_collection_ids_string());
    },
    add: function (id) {
        if (!id || id <= 0) {
            return;
        }

        if (this.is_added(id)) {
            return;
        }

        // check if max reached
        var current_total = this.num_keys(this.collection);
        if (current_total >= this.max_items) {
            // TODO: show modal warning about max items
            alert('Max shopping window items reached. Remove an existing one before adding a new one');
            return;
        }

        // add to collection
        this.collection['_' + id] = id;

        // create a clone and append it to the shopping window
        var $clone = this.create_ad_clone(id);
        if ($clone) {
            $('.featured-shops-box-preview .classifieds .row.featured').append($clone);
        }

        this.refresh_ui();
    },
    remove: function (id) {
        if (!id || id <= 0) {
            return;
        }

        if (this.is_added(id)) {
            // remove from collection
            delete this.collection['_' + id];

            // delete box
            var $box = $('.featured-shops-box-preview div[data-ad-id="' + id + '"]');
            if ($box.length > 0) {
                $box.remove();
            }

            this.refresh_ui();
        }
    },
    create_ad_clone: function (id) {
        var $box = $('.selectable-ads div[data-ad-id="' + id + '"]');
        var $clone;
        if ($box.length > 0) {
            var src = $box.find('.img-wrapper img').attr('src');
            var title = $box.find('h3').text();
            var href = $box.find('h3').attr('data-href');

            $clone = $('<div/>').addClass('col-lg-4 col-md-4').attr('data-ad-id', id).data('ad-id', id);

            var $link_img = $('<a/>').attr('href', href).attr('target', '_blank');
            var $link_title = $('<a/>').attr('href', href).attr('target', '_blank').text(title);

            var $img = ($('<img/>').attr('src', src).attr('width', 120).attr('height', 90).attr('title', title));
            $link_img.html($img);

            var $img_wrap = $('<div/>').addClass('hidden-sm hidden-xs').append($link_img);
            $clone.append($img_wrap);

            var $p = $('<p/>').addClass('excerpt').append($link_title);
            $clone.append($p);
        }

        return $clone;
    },
    is_added: function (id) {
        return (this.collection['_' + id] || false);
    },
    collection: {},
    max_items: 3,
    num_keys: function (o) {
        var num = 0;
        for (var k in o) {
            if (o.hasOwnProperty(k)) {
                num++;
            }
        }
        return num;
    },
    get_collection_ids_string: function () {
        var values = [];
        for (var k in this.collection) {
            if (this.collection.hasOwnProperty(k)) {
                values.push(this.collection[k]);
            }
        }

        return values.join(',');
    }
};

$(document).ready(function(){
    $('select').selectpicker({
        style: 'btn btn-default',
        size: 10,
        noneSelectedText: '',
        noneResultsText: '',
        mobile: OGL.is_mobile_browser
    });

    var $main_cat_select = $('#main_category_id');
    var $additional_cat_select = $('#additional_category_ids');

    $main_cat_select.on('change', function(){
        populate_additional_category_dropdown($(this).val());
    });
    $main_cat_select.trigger('change');

    var preselect_data = $additional_cat_select.data('preselect');
    if ('undefined' !== typeof preselect_data) {
        var preselect = $.trim(preselect_data).split(',');
        $additional_cat_select.selectpicker('val', preselect);
    }

    var dpicker_options = {
        format: 'DD.MM.YYYY HH:mm',
        locale: 'hr'
    };
    $('#published_at_datetimepicker').datetimepicker(dpicker_options);
    $published_at_datetimepicker = $('#published_at_datetimepicker').data('DateTimePicker');
    $('#expires_at_datetimepicker').datetimepicker(dpicker_options);
    $expires_at_datetimepicker = $('#expires_at_datetimepicker').data('DateTimePicker');
    $('#published_at_datetimepicker').on('dp.change', function(e) {
        $expires_at_datetimepicker.minDate(e.date);
    });
    $('#expires_at_datetimepicker').on('dp.change', function(e) {
        $published_at_datetimepicker.maxDate(e.date);
    });

    /*$('#type_tab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('#type').val($(e.target).data('type'));
    });*/

    var $all_ads_container = $('#all_ads_container');
    ajaxify_pagination($all_ads_container);

    shopping_windows_editor.init();
});
