
/**
 * Handle category_id select change
 */
function handle_category_id_select_change() {
    var selected_category_id = $.trim($('#category_id').selectpicker('val'));
    if ('' !== selected_category_id && parseInt(selected_category_id)) {
        $('#createNewAddModalCreateBtn').removeClass('disabled');
    } else {
        $('#createNewAddModalCreateBtn').addClass('disabled');
    }
}

/**
 * Submits the form with selected category_id
 */
function createNewAddModalCreateBtn_Click() {
    // bootstrap handles this automatically, but let it be here just in case ;)
    if (!$('#createNewAddModalCreateBtn').hasClass('disabled')) {
        $('#frm_createNewAdd').submit();
    }
}

$(document).ready(function(){
    
    var $cat_id_dropdown = $('#category_id');
    $cat_id_dropdown.selectpicker({
        style: 'btn btn-default',
        size: 8,
        liveSearch: true,
        title: 'Choose category',
        noneSelectedText: 'Choose category',
        noneResultsText: 'No categories match',
        mobile: OGL.is_mobile_browser,
        showSubtext: true
    });

    $cat_id_dropdown.on('change', handle_category_id_select_change);

    $('#createNewAddModalCreateBtn').on('click', createNewAddModalCreateBtn_Click);

   $('#createNewAdd').on('click', function(){
        handle_category_id_select_change();
        $('#createNewAddModal').modal({
            backdrop: 'static',
            keyboard: true
        }).on('shown.bs.modal', function(e){
            // trigger a click on the dropdown toggle to show the category selector/search when modal opens
            $('button.dropdown-toggle').trigger('click');
        });
    });
});




function getUserName( field_name ) {    
    var $n_sales_rep_id = $('#'+ field_name);
    var $n_sales_rep_id_input = $('#' + field_name + '_input');
    if ('undefined' === typeof $n_sales_rep_id_input || $n_sales_rep_id_input.length == 0) {
        $n_sales_rep_id_input = null;
    }
    if ($n_sales_rep_id_input) {
        var user_filter = $n_sales_rep_id_input.magicSuggest({
            allowFreeEntries: false,
            autoSelect: true,
            highlight: false,
            selectFirst: true,
            hideTrigger: true,
            maxSelection: 1,
            minChars: 1,
            displayField: 'username',
            noSuggestionText: 'No users found matching your search',
            placeholder: 'Choose a user',
            useTabKey: true,
            required: true,
            resultAsString: true,
            data: function(q) {
                var usersData = [];
                if ($.trim(q)) {
                    $.ajax({
                        async: false,
                        url: '/ajax/users/' + $.trim(q),
                        cache: false,
                        dataType: 'json'
                    }).done(function(json_data) {
                        usersData = json_data;
                    });
                }
                return usersData;
            },
            renderer: function(data){
                var userRemark = '';
                if (data.remark) {
                    userRemark = '<br/><span class="fa fa-fw"></span><small><span class="fa fa-warning text-danger"></span> ' + data.remark + '</small>';
                }
                var userIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
                var userRow = '<div>' + userIcon + ' <span class="username">' + data.username + '</span>' + (data.full_name ? '<span class="full_name">[' + data.full_name + ']</span>' : '') + '<br><span class="fa fa-fw"></span><small>' + data.email + '</small>' + userRemark + '</div>';
                return userRow;
            },
            selectionRenderer: function(data){
                var userRemark = '';
                if (data.remark) {
                    userRemark = '<span class="fa fa-warning fa-fw text-danger" data-toggle="tooltip" data-type="html" data-placement="right" title="' + data.remark + '"></span>';
                }
                var userIcon = '<span class="fa fa-' + data.icon + ' fa-fw"></span>';
                var userRow = userRemark + userIcon + ' ' + data.username;
                return userRow;
            }
        });

        var $uid = $n_sales_rep_id.val();
        if ($.trim($uid)) {
            var $username = $n_sales_rep_id.data('username');
            if ('undefined' !== typeof $username && $.trim($username)) {
                user_filter.setSelection([{id: parseInt($uid), username: $.trim($username)}]);
            }
        }

        $(user_filter).on('selectionchange', function(e, m){
            var selectedItem = this.getSelection();
            if (selectedItem.length == 1) {
                selectedItem = selectedItem[0];
            } else {
                selectedItem = {id: '', username: ''};
            }
            $n_sales_rep_id.val(selectedItem.id).attr('data-username', selectedItem.username).data('username', selectedItem.username);
        });
      console.log(user_filter)
      
    }
    
    
    
    
}
    

getUserName('n_sales_rep_id');
getUserName('n_agency_id');

