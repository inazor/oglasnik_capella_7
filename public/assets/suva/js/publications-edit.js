$(document).ready(function(){
    var $start_date = $('#start_date').datepicker({
        format: 'dd.mm.yyyy',
        weekStart: 1,
        language: 'hr',
        autoclose: true
    });
});
