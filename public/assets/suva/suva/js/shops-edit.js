var $user_id = null;
var $published_at_datetimepicker,
    $expires_at_datetimepicker;

function slug_is_username() {
    return $user_id.data('username') === $('#slug').val();
}

$(document).ready(function(){
    $user_id = $('#user_id');
    var user_filter = $('#user_id_input').magicSuggest({
        allowFreeEntries: false,
        autoSelect: true,
        highlight: false,
        selectFirst: true,
        hideTrigger: true,
        maxSelection: 1,
        minChars: 1,
        displayField: 'username',
        noSuggestionText: 'No users found matching your search',
        placeholder: 'Choose a user',
        useTabKey: true,
        required: true,
        resultAsString: true,
        data: function(q) {
            var usersData = [];
            var search = $.trim(q);
            if (search) {
                $.ajax({
                    async: false,
                    url: '/ajax/users/' + search,
                    cache: false,
                    dataType: 'json'
                }).done(function(json_data) {
                    usersData = json_data;
                });
            }
            return usersData;
        },
        renderer: function(data){
            var userIcon = (data.type == 'Companies' ? '<span class="fa fa-globe fa-fw"></span>' : '<span class="fa fa-user fa-fw"></span>');
            var userRow = '<div>' + userIcon + ' <span class="username">' + data.username + '</span>' + (data.full_name ? '<span class="full_name">[' + data.full_name + ']</span>' : '') + '<br><span class="fa fa-fw"></span><small>' + data.email + '</small></div>';
            return userRow;
        },
        selectionRenderer: function(data){
            var userIcon = (data.type == 'Companies' ? '<span class="fa fa-globe fa-fw"></span>' : '<span class="fa fa-user fa-fw"></span>');
            var userRow = userIcon + ' ' + data.username;
            return userRow;
        }
    });

    $(user_filter).on('selectionchange', function(e, m){
        var selectedItem = this.getSelection();
        if (selectedItem.length == 1) {
            selectedItem = selectedItem[0];
        } else {
            selectedItem = {id: '', username: ''};
        }
        if (slug_is_username()) {
            $('#slug').val(selectedItem.username);
        }
        $user_id.val(selectedItem.id).attr('data-username', selectedItem.username).data('username', selectedItem.username);
    });

    var $uid = $user_id.val();
    if ($.trim($uid)) {
        var $username = $user_id.data('username');
        if ('undefined' !== typeof $username && $.trim($username)) {
            user_filter.setSelection([{id: parseInt($uid), username: $.trim($username)}]);
        }
    }

    $('textarea#about').ckeditor({
        customConfig: '/assets/backend/js/ckeditor_basic_config.js'
    });

    var dpicker_options = {
        format: 'DD.MM.YYYY HH:mm',
        locale: 'hr'
    };
    $('#published_at_datetimepicker').datetimepicker(dpicker_options);
    $published_at_datetimepicker = $('#published_at_datetimepicker').data('DateTimePicker');
    $('#expires_at_datetimepicker').datetimepicker(dpicker_options);
    $expires_at_datetimepicker = $('#expires_at_datetimepicker').data('DateTimePicker');
    $('#published_at_datetimepicker').on('dp.change', function(e) {
        $expires_at_datetimepicker.minDate(e.date);
    });
    $('#expires_at_datetimepicker').on('dp.change', function(e) {
        $published_at_datetimepicker.maxDate(e.date);
    });

    $('#media_fileupload').fileupload({
        url: '/ajax/upload',
        dataType: 'json',
        dropZone: null,
        pasteZone: null,
        limitMultiFileUploads: 1,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        done: function (e, data) {
            if ('undefined' !== data.result) {
                if (data.result.ok) {
                    $('#media_id').val(data.result.media[0].id);
                    $('#media_preview').html(data.result.media[0].preview.tag);
                } else {
                    // TODO: handle failure in some way...
                    alert('Something went wrong...');
                }
            }
        }
    }).prop('disabled', !$.support.fileInput)
      .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
