<?php

namespace Baseapp\Models;

use Baseapp\Library\Utils;
use Phalcon\Mvc\Model\Resultset;

/**
 * Audit Model
 */
class Audit extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        $this->hasMany('id', 'AuditDetail', 'audit_id', array(
            'alias' => 'details'
        ));
    }
/*
    public function onValidationFails()
    {
        // var_dump($this->getMessages()); exit;
        throw new \Phalcon\Exception('Audit model validation failed');
    }
*/

    /**
     * Builds and returns the SQL join statement used for the backend search UI
     *
     * @param string $model Model classname
     * @param array $fields
     * @param string $q
     * @param string $mode
     *
     * @return string
     */
    public static function build_search_sql($model = null, $fields = array(), $q = '', $mode = 'exact')
    {
        $params = Utils::build_search_sql_params($fields, $q, $mode);

        // Build (raw) sql WHERE clause if needed
        $where_sql = '';
        if ($model || !empty($params)) {
            $where_sql = 'WHERE ';
            $where_parts = array();

            $like_or_equals = 'LIKE';
            if ('exact' === $mode || 'phrase' === $mode) {
                $like_or_equals = '=';
            }

            if (!empty($params)) {
                foreach ($params as $bind_name => $value ) {
                    // TODO: check and harden against SQLi
                    $where_parts[] = sprintf('a.%s ' . $like_or_equals . ' \'%s\'', str_replace(':', '', $bind_name), $value);
                }
            }

            if ($model) {
                $model_name = explode('\\', $model);
                $model_name = $model_name[count($model_name) - 1];
                $where_sql .= 'a.model_name LIKE \'%' . $model_name . '\'';

                if (count($where_parts)) {
                    $where_sql .= ' AND ';
                }
            }

            if (count($where_parts)) {
                $where_sql .= '(' . implode(' OR ', $where_parts) . ')';
            }
        }

        // Old-school baby!
        $sql = "
            SELECT
                a.id as id,
                a.user_id as user_id,
                a.username as username,
                a.model_name as model_name,
                a.model_pk_val as model_pk_val,
                a.controller as controller,
                a.action as action,
                a.params as params,
                a.message as message,
                a.ip as ip,
                a.type as type,
                a.created_at as created_at
            FROM
                audit AS a
                $where_sql
            ORDER BY
                a.id DESC";

        return trim($sql);
    }

    /**
     * Augments the passed-by-reference array of \Phalcon\Mvc\Model\ResultsetInterface objects
     * with detail record(s) for each log entry (object).
     *
     * It does so using a single SQL query (by building a list of IDs to fetch).
     *
     * Each object in the array is given a 'details' property which is itself an array
     * containing one or more respective AuditDetail objects (of type \Phalcon\Mvc\Model\ResultsetInterface).
     *
     * @param array &$logs An array of log records returned by executing the query built by build_search_sql()
     */
    public static function hydrate_audit_details(&$logs)
    {
        $ids = array();

        if (empty($logs)) {
            return;
        }

        foreach ($logs as $log) {
            $ids[] = $log->id;
        }

        $condition = 'audit_id IN (' . implode(',', $ids) . ')';
        $details = AuditDetail::find($condition);
        $details->setHydrateMode(Resultset::HYDRATE_OBJECTS);

        foreach ($logs as $k => $log) {
            $logs[$k]->details = array();
            foreach ($details as $detail) {
                if ($detail->audit_id == $log->id) {
                    // Break the possibly huge strings for display purposes
                    if (!empty($detail->old_value)) {
                        $detail->old_value = Utils::soft_break(Utils::esc_html($detail->old_value));
                    }
                    if (!empty($detail->new_value)) {
                        $detail->new_value = Utils::soft_break(Utils::esc_html($detail->new_value));
                    }
                    // TODO: maybe add a flag if the detail row should be visible
                    // on page load (if it matches the search value or we want to
                    // highlight it etc...) -- will also need some tweaks to view/js
                    $logs[$k]->details[] = $detail;
                }
            }
        }
    }

}
