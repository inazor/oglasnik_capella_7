<?php

namespace Baseapp\Models;

use Baseapp\Bootstrap;
use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Library\Utils;
use Phalcon\Di;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Mvc\Model\ResultsetInterface;

class AdsHomepage extends BaseModelBlamable
{
    public $id;
    public $ad_id;
    public $expires_at;
    public $active;
    public $created_at;
    public $modified_at;
    public $product;
    public $duration = null;

    public function beforeCreate()
    {
        $this->created_at = Utils::getRequestTime();
    }

    public function beforeUpdate()
    {
        $this->modified_at = Utils::getRequestTime();
    }

    public function getSource()
    {
        return 'ads_homepage';
    }

    public function markExpired()
    {
        $sql = "UPDATE `" . $this->getSource() . "` SET `active` = 0 WHERE `active` = 1 AND `expires_at` < :current_timestamp";

        $conn = $this->getWriteConnection();
        $conn->execute($sql, array(':current_timestamp' => Utils::getRequestTime()));

        return $conn->affectedRows();
    }

    public static function insertOrUpdate(Ads $ad, ProductsInterface $product, $duration_string = null)
    {
        $now = Utils::getRequestTime();

        $record = self::findFirst('ad_id = ' . $ad->id);
        if (!$record) {
            $record = new self();
            $record->ad_id = $ad->id;
        }

        $record->duration = $duration_string;
        $record->product = serialize($product);
        $record->active = 1;

        if (!empty($duration_string)) {
            $duration_days = $product->getExtrasDuration();
            $expires_at    = $now + (86400 * $duration_days); // now + x days worth of seconds

            $record->expires_at = $expires_at;
        }

        $saved = $record->save();
        if (!$saved) {
            Bootstrap::log($record->getMessages());
        }

        return $saved;
    }

    public static function deleteByAdId($ad_id)
    {
        if ($ad_homepage = self::findFirst('ad_id = ' . $ad_id)) {
            return $ad_homepage->delete();
        }

        return false;
    }

    public static function setActiveForAdId($ad_id, $value)
    {
        $ad_ids = array();

        if (is_array($ad_id)) {
            $ad_ids = $ad_id;
        } elseif (strpos($ad_id, ',') !== false) {
            $ad_ids = explode(',', $ad_id);
        } elseif ((int) $ad_id) {
            $ad_ids[] = (int) $ad_id;
        }

        if (count($ad_ids)) {
            if (count($ad_ids) == 1) {
                $ads_homepage = self::findFirst(array(
                    'conditions' => 'ad_id = :ad_id:',
                    'bind'       => array(
                        'ad_id'   => $ad_ids[0]
                    )
                ));
            } else {
                $ads_homepage = self::find(array(
                    'conditions' => 'ad_id IN ({ad_id:array})',
                    'bind'       => array(
                        'ad_id'   => $ad_ids
                    )
                ));
            }

            $value = (int) $value;

            if ($ads_homepage) {
                return $ads_homepage->update(array('active' => $value));
            }
        }

        return false;
    }

    public static function findActiveAndNonExpired()
    {
        $now = Utils::getRequestTime();

        return self::find('active = 1 AND expires_at > ' . $now);
    }

    /**
     * @param ResultsetInterface $resultset
     *
     * @return array
     */
    public static function getAdIdsArray(ResultsetInterface $resultset)
    {
        $ad_ids = array();

        foreach ($resultset as $result) {
            $ad_ids[] = $result->ad_id;
        }

        return $ad_ids;
    }

    /**
     * @return array|mixed|null
     */
    public static function getAds()
    {
        $resultset = self::findActiveAndNonExpired();
        $ids = self::getAdIdsArray($resultset);

        $ads = null;
        if (!empty($ids)) {
            // Exclude any ads that are in age-restricted categories
            $excluded_categories = CategoriesSettings::getAgeRestrictedCategoryIdsList();
            $restricted_category_ids = array();
            if (!empty($excluded_categories)) {
                $restricted_category_ids = array_keys($excluded_categories);
            }

            $builder = new Builder();
            $builder->columns(array('Baseapp\Models\Ads.*'));
            $builder->from('Baseapp\Models\Ads');
            $builder->inWhere('Baseapp\Models\Ads.id', $ids);
            $builder->andWhere('Baseapp\Models\Ads.is_spam = 0 AND Baseapp\Models\Ads.active = 1 AND Baseapp\Models\Ads.moderation = "ok"');
            if (!empty($restricted_category_ids)) {
                $builder->andWhere('Baseapp\Models\Ads.category_id NOT IN (' . implode(',', $restricted_category_ids) . ')');
            }
            $builder->orderBy('Baseapp\Models\Ads.published_at DESC');
            $ads = $builder->getQuery()->execute();

            $ads = Ads::getEnrichedFrontendBasicResultsArray($ads);
        }

        return $ads;
    }

    public static function getRandomlyOrderedAds()
    {
        $ads = self::getAds();

        if (!empty($ads) && is_array($ads)) {
            shuffle($ads);
        }

        return $ads;
    }

    public static function buildCarouselMarkupFromAds($ads)
    {
        $markup = null;
        $foundAds = count($ads);

        $simpleView = Di::getDefault()->getShared('simpleView');

        if ($foundAds) {
            $markup = '<div class="featured-classifieds">' . PHP_EOL;
            $markup .= '    <div class="row grid-layout">' . PHP_EOL;

            foreach ($ads as $ad) {
                $markup .= $simpleView->render(
                    'chunks/ads-grid-single',
                    array(
                        'ad' => $ad,
                        'adsGridSingleClass' => 'featured-classified col-md-3 col-xs-6',
                        'marginBottomLg' => ' '
                    )
                );
            }

            $markup .= '    </div>' . PHP_EOL;

            if ($foundAds > 4) {
                $markup .= '    <div class="row">' . PHP_EOL;
                $markup .= '        <div class="col-sm-4 col-sm-offset-4">' . PHP_EOL;
                $markup .= '            <nav>' . PHP_EOL;
                $markup .= '                <ul class="pager clearfix">' . PHP_EOL;
                $markup .= '                    <li class="prev"><a href="#" data-dir="prev">&lt;</a></li>' . PHP_EOL;
                $markup .= '                    <li class="number"></li>' . PHP_EOL;
                $markup .= '                    <li class="next"><a href="#" data-dir="next">&gt;</a></li>' . PHP_EOL;
                $markup .= '                </ul>' . PHP_EOL;
                $markup .= '            </nav>' . PHP_EOL;
                $markup .= '        </div>' . PHP_EOL;
                $markup .= '    </div>' . PHP_EOL;
            }

            $markup .= '</div>' . PHP_EOL;
        }

        return $markup;
    }
}
