<?php

namespace Baseapp\Models;

use Phalcon\Di;
use Phalcon\Mvc\Model\Resultset;
use Baseapp\Bootstrap;
use Baseapp\Library\Validations\Dictionaries as DictionariesValidations;
use Baseapp\Traits\FormModelControllerTrait;
use Baseapp\Traits\NestedSetActions;
use Baseapp\Traits\MemcachedMethods;
use Baseapp\Library\Behavior\NestedSet as NestedSetBehavior;

/**
 * Dictionaries Model
 */
class Dictionaries extends BaseModelBlamable
{
    use FormModelControllerTrait;
    use NestedSetActions;
    use MemcachedMethods;

    const MEMCACHED_KEY = 'dictionary';
    const CACHE_TIME    = 0; // 0 => forever; null for no caching at all

    /**
     * Dictionaries initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->addBehavior(new NestedSetBehavior(array(
            'hasManyRoots'   => true,
            'rootAttribute'  => 'root_id',
            'rightAttribute' => 'rght',
            'parentAttribute' => 'parent_id'
        )));

        $this->hasMany('id', __NAMESPACE__ . '\Parameters', 'dictionary_id', array(
            'alias' => 'Parameters'
        ));
    }

    /**
     * Add new dictionary or dictionary item (if $parent is not null)
     *
     * @param \Phalcon\Http\RequestInterface $request
     * @param \Baseapp\Models\Dictionaries|null $parent
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request, $parent = null)
    {
        $this->populate_data_with_post_values($request);
        if (null == $parent) {
            $this->parent_id = null;
        }

        $validation = new DictionariesValidations();
        if (null === $parent) {
            $extra_conditions = array(
                'lft = :lft: AND level = :level:',
                array(
                    'lft' => 1,
                    'level' => 1
                )
            );
            $validation_message = 'Dictionary name should be unique';
        } else {
            $extra_conditions = array(
                'parent_id = :parent_id: AND level = :level:',
                array(
                    'parent_id' => $parent->id,
                    'level' => $parent->level + 1
                )
            );
            $validation_message = 'Dictionary value name should be unique';
        }
        if (null === $parent) {
            $validation->add('slug', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => 'Matching slug cannot be empty'
            )));
            $validation->add('slug', new \Baseapp\Extension\Validator\Uniqueness(array(
                'model' => '\Baseapp\Models\Dictionaries',
                'message' => 'Mathcing slug should be unique',
            )));
        }
        $validation->add('name', new \Baseapp\Extension\Validator\Uniqueness(array(
            'model' => '\Baseapp\Models\Dictionaries',
            'extra_conditions' => $extra_conditions,
            'message' => $validation_message,
        )));

        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            $refreshMemcachedType = 'siblings';
            if (null == $parent) {
                $insert_result = $this->saveNode();
                $refreshMemcachedType = 'all';
            } else {
                $insert_result = $this->appendTo($parent);
            }

            if (true === $insert_result) {
                // refresh memcached
                if ('siblings' == $refreshMemcachedType) {
                    $this->refreshMemcachedSiblings();
                } else {
                    $this->refreshMemcachedDictionaryTree();
                }
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Save existing dictionary or dictionary item (if $parent is not null)
     *
     * @param \Phalcon\Http\RequestInterface $request
     * @param \Baseapp\Models\Dictionaries|null $parent
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request, $parent = null)
    {
        $validation = new DictionariesValidations();

        $name_lc      = mb_strtolower($this->name, 'UTF-8');
        $name_post_lc = mb_strtolower($request->getPost('name'), 'UTF-8');
        if ($name_lc != $name_post_lc) {
            if (null === $parent) {
                $extra_conditions = array(
                    'lft = :lft: AND level = :level:',
                    array(
                        'lft'   => 1,
                        'level' => 1
                    )
                );
                $validation_message = 'Dictionary name should be unique';
            } else {
                $extra_conditions = array(
                    'parent_id = :parent_id: AND level = :level:',
                    array(
                        'parent_id' => $parent->id,
                        'level'     => $parent->level + 1
                    )
                );
                $validation_message = 'Dictionary value name should be unique';
            }
            $validation->add('name', new \Baseapp\Extension\Validator\Uniqueness(array(
                'model'            => '\Baseapp\Models\Dictionaries',
                'extra_conditions' => $extra_conditions,
                'message'          => $validation_message,
            )));
        }
        if (null === $parent) {
            $validation->add('slug', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => 'Matching slug cannot be empty'
            )));
            if ($this->slug != $request->getPost('slug')) {
                $validation->add('slug', new \Baseapp\Extension\Validator\Uniqueness(array(
                    'model'   => '\Baseapp\Models\Dictionaries',
                    'message' => 'Mathcing slug should be unique',
                )));
            }
        }

        $messages = $validation->validate($request->getPost());

        $this->populate_data_with_post_values($request);
        if ($this->parent_id == $this->id) {
            $this->parent_id = null;
        }

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            $refreshMemcachedType = 'siblings';
            if ($parent) {
                // if item was moved to another parent then $parent->id and $this->parent()->id differ!
                if ((int) $this->parent()->id != (int) $parent->id) {
                    // $parent = self::findFirst($parent->id);
                    // $this->root_id = self::findFirst($parent->id)->root()->id;
                    $save_result = $this->moveAsLast($parent);
                    $refreshMemcachedType = 'all';
                } else {
                    // $this->root_id = self::findFirst($parent->id)->root()->id;
                    $save_result = $this->saveNode();
                }
            } else {
                $save_result = $this->saveNode();
            }
            if (true === $save_result) {
                // refresh memcached
                if ('siblings' == $refreshMemcachedType) {
                    $this->refreshMemcachedSiblings();
                } else {
                    $this->refreshMemcachedDictionaryTree();
                }
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Helper function so we can get children of the current dictionary and not to expose real function name (children) :)
     */
    public function values()
    {
        return static::getCachedChildren($this->id);
    }

    private static function getChildrenFromDB($id)
    {
        return static::find(array(
            'conditions' => 'parent_id = :dictionary_id: AND active = 1',
            'bind'       => array(
                'dictionary_id' => $id
            ),
            'order'      => 'lft ASC',
            'hydration'  => Resultset::HYDRATE_OBJECTS
        ));
    }

    public static function getCachedChildren($id)
    {
        $dictionaryChildren = null;
        $cacheKey           = static::MEMCACHED_KEY . '_' . $id;
        $memcache           = null;

        if (null !== static::CACHE_TIME && $memcache = static::getMemcache()) {
            // try to get dictionary's children from cache
            $cachedChildren = $memcache->get($cacheKey);
            if ($cachedChildren) {
                $dictionaryChildren = $cachedChildren;
                if (!$dictionaryChildren || count($dictionaryChildren) == 0) {
                    $dictionaryChildren = null;
                } else {
                    //Bootstrap::log($cacheKey . ' found in cache');
                }
            }
        }

        if (!$dictionaryChildren) {
            $dictionaryChildren = static::getChildrenFromDB($id);

            if (null !== static::CACHE_TIME && $memcache) {
                $memcache->save($cacheKey, $dictionaryChildren, static::CACHE_TIME);
                //Bootstrap::log($cacheKey . ' SAVED to cache');
            }
        }

        return $dictionaryChildren;
    }

    public function refreshMemcachedDictionaryTree()
    {
        // get all unique parent_id's from current dictionary root_id and regenerate every one of them
        $rows = static::find(array(
            'conditions' => 'root_id = :root_id: AND parent_id IS NOT NULL AND active = 1',
            'bind'       => array(
                'root_id' => $this->root_id
            ),
            'distinct'   => 'parent_id'
        ));

        if ($rows) {
            foreach($rows as $row) {
                $children = static::getChildrenFromDB($row->parent_id);
                if ($children && count($children)) {
                    $row->refreshMemcachedSiblings();
                }
            }
        }
    }

    public function refreshMemcachedSiblings()
    {
        if (null !== static::CACHE_TIME && $memcache = static::getMemcache()) {
            $cacheKey = static::MEMCACHED_KEY . '_' . $this->parent_id;
            $siblingsToCache = static::getChildrenFromDB($this->parent_id);
            if ($siblingsToCache && count($siblingsToCache)) {
                $memcache->save($cacheKey, $siblingsToCache, static::CACHE_TIME);
            } else {
                // check if cachekey exists, if so, delete it
                if ($memcache->exists($cacheKey, static::CACHE_TIME)) {
                    $memcache->delete($cacheKey);
                }
            }
        }
    }

    /**
     * Helper function to get depth of a dictionary (number of levels where data is stored... It is assumed that all
     * dictionaries will have at least 2 levels -> first level is the name of the dictionary, and second level are it'd
     * direct descendats, or how we are calling them here - dictionary values). So, the logic is that this function will
     * return and array of possible levels only for those dictionaries that have more than 2 levels (like vehicle
     * make/model hierarchy or HI-FI equipment with Manufacturer/Model hierarchy and so on...)
     *
     * @return array Array with possible levels is returned, or in case where only 2 levels exist, an empty array is
     *               returned
     */
    public function getLevels()
    {
        // as dictionaries are handeled as a tree, first level nodes are actually Dictionary names, so direct
        // descendants of a specific dictionary have level = 2, and so on... For user interface, and for easier
        // understanding of 'levels' we will 'ignore' Dictionary name level, and start counting from the direct
        // descendats level as first one...

        $maxLevel = $this->maximum(array(
            'root_id = :root_id:',
            'column' => 'level',
            'bind' => array(
                'root_id' => $this->root_id
            )
        ));

        $levels = array();
        if ($maxLevel > 2) {
            $maxLevel = $maxLevel - 1;

            for ($i = 1; $i <= $maxLevel; $i++) {
                $levels[] = $i;
            }
        }

        return $levels;
    }

    /**
     * Returns an array containing no more than $values_per_level "dict values"
     * (in this case node names) for each level of the tree that the
     * current node belongs to.
     *
     * Throws if no tree or dictionary is specified and we're not an existing node.
     *
     * @param int $dictionary_id_or_tree
     * @param int|array|null $values_per_level
     *
     * @return array
     * @throws \Exception
     */
    public function getExampleTreeValues($values_per_level = 3, $dictionary_id_or_tree = null)
    {
        $values = array();
        $tree = null;

        if (null === $dictionary_id_or_tree) {
            // No dict or tree specified, but we're in multi-root mode
            // and we don't have a way to know which tree to fetch here since
            // nothing was provided to us except: $model = new Dictionaries();
            if (!isset($this->id)) {
                throw new \Exception('No dictionary id or tree specified and no known node available');
            } else {
                $root = $this->getRoot();
                $tree = $root->getTree();
            }
        } else {
            if (is_int($dictionary_id_or_tree)) {
                $tree = $this->getTree($dictionary_id_or_tree);
            }
            if (is_array($dictionary_id_or_tree)) {
                $tree = $dictionary_id_or_tree;
            }
        }

        if ($tree) {

            $group_by_level_without_first = function() use ($tree) {
                $level_tree = array();
                foreach ($tree as $id => $node) {
                    if ($id > 0) {
                        if ($node->level > 1) {
                            $level_tree[$node->level][$id] = $node;
                        }
                    }
                }
                unset($id, $node);
                return $level_tree;
            };

            $level_tree = $group_by_level_without_first();
            $levels_cnt = 0;
            unset($tree);
            foreach ($level_tree as $level => $nodes) {
                $values[$levels_cnt] = array();
                $values_cnt = 0;
                foreach ($nodes as $id => $node) {
                    if ($values_cnt < $values_per_level) {
                        $values[$levels_cnt][] = $node->name;
                    }
                    $values_cnt++;
                }
                $levels_cnt++;
            }
        }

        return $values;
    }

    public function hasChildren($get_from_cache = true)
    {
        $has_children = false;
        $children = $get_from_cache ? static::getCachedChildren($this->id) : static::getChildrenFromDB($this->id);
        if ($children && count($children)) {
            $has_children = true;
        }
        return $has_children;
    }

}
