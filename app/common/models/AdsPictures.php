<?php

namespace Baseapp\Models;

/**
 * AdsPictures Model
 */
class AdsPictures extends BaseModelBlamable
{
    /**
     * AdsPictures initialize
     */
    public function initialize()
    {
        parent::initialize();

        $this->belongsTo(
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias' => 'Ad',
                'foreignKey' => array(
                    'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
                )
            )
        );
    }

}
