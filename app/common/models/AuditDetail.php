<?php

namespace Baseapp\Models;

/**
 * AuditDetail Model
 */
class AuditDetail extends \Phalcon\Mvc\Model
{

    public function initialize()
    {
        $this->belongsTo('audit_id', 'Audit', 'id');
    }

}
