<?php

namespace Baseapp\Models;

use Phalcon\Mvc\Model\Resultset;
use Baseapp\Bootstrap;
use Baseapp\Library\Validations\Parameters as ParametersValidations;
use Baseapp\Models\Dictionaries;
use Baseapp\Traits\FormModelControllerTrait;

/**
 * Parameters Model
 */
class Parameters extends BaseModelBlamable
{
    use FormModelControllerTrait;

    // Parameters' base attributes

    const MEMCACHED_KEY = 'parameter';
    const CACHE_TIME    = 0; // 0 => forever; null for no caching at all

    public $id;
    public $name;
    public $type_id = 'TEXT';
    public $dictionary_id;

    /**
     * @var \Phalcon\Http\Request
     */
    public $request;

    /**
     * Parameters initialize
     */
    public function initialize()
    {
        parent::initialize();

        // Every parameter is defined by its type
        $this->belongsTo(
            'type_id', __NAMESPACE__ . '\ParametersTypes', 'id',
            array(
                'alias' => 'ParameterType',
                'foreignKey' => true
            )
        );

        // Parameter can have one or no dictionary attached to it
        $this->hasOne(
            'dictionary_id', __NAMESPACE__ . '\Dictionaries', 'id',
            array(
                'alias' => 'Dictionary'
            )
        );

        // Parameter can be located in many fieldsets
        $this->hasMany(
            'id', __NAMESPACE__ . '\CategoryFieldsetsParameters', 'parameter_id',
            array(
                'alias' => 'CategoryFieldsets'
            )
        );

        // Parameter can be in many ads
        $this->hasMany(
            'id', __NAMESPACE__ . '\AdsParameters', 'parameter_id',
            array(
                'alias' => 'ParameterAds'
            )
        );
    }

    /**
     * @param ParametersValidations $validation
     */
    protected function check_dictionary_validation($validation)
    {
        if ((int) $this->ParameterType->accept_dictionary) {
            $validation->add('dictionary_id', new \Phalcon\Validation\Validator\PresenceOf(array(
                'message' => 'This type of parameter should have a dictionary attached to it!'
            )));
        } else {
            $this->dictionary_id = null;
        }
    }

    /**
     * Add new parameter
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request)
    {
        $this->populate_data_with_post_values($request);

        $validation = new ParametersValidations();

        $this->check_dictionary_validation($validation);

        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->create()) {
                $this->refreshMemcachedData();
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Save changes for an existing Parameter
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request)
    {
        $this->populate_data_with_post_values($request);

        // avoid validation for certain fields that haven't been changed
        $unique_if_changed = array('name');
        $require_unique = array();
        foreach ($unique_if_changed as $field) {
            if ($this->$field != $request->getPost($field)) {
                $require_unique[] = $field;
            }
        }

        $validation = new ParametersValidations();

        $this->check_dictionary_validation($validation);

        $validation->set_unique($require_unique);
        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                $this->refreshMemcachedData();
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    public static function getParameterFromDB($id)
    {
        return static::findFirst(array(
            'id = :parameter_id:',
            'bind'       => array(
                'parameter_id' => $id
            ),
            'hydration'  => Resultset::HYDRATE_OBJECTS
        ));
    }

    public static function getCached($id)
    {
        $parameter = null;
        $cacheKey  = static::MEMCACHED_KEY . '_' . $id;
        $memcache  = null;

        if (null !== static::CACHE_TIME && $memcache = static::getMemcache()) {
            // try to get dictionary's children from cache
            $cachedParameter = $memcache->get($cacheKey);
            if ($cachedParameter) {
                $parameter = $cachedParameter;
                //Bootstrap::log($cacheKey . ' found in cache');
            }
        }

        if (!$parameter) {
            $parameter = static::getParameterFromDB($id);

            if (null !== static::CACHE_TIME && $memcache) {
                $memcache->save($cacheKey, $parameter, static::CACHE_TIME);
                //Bootstrap::log($cacheKey . ' SAVED to cache');
            }
        }

        return $parameter;
    }

    public function refreshMemcachedData()
    {
        if (null !== static::CACHE_TIME && $memcache = static::getMemcache()) {
            $cacheKey = static::MEMCACHED_KEY . '_' . $this->id;
            if ($parameterToCache = static::getParameterFromDB($this->id)) {
                $memcache->save($cacheKey, $parameterToCache, static::CACHE_TIME);
            } else {
                // check if cachekey exists, if so, delete it
                if ($memcache->exists($cacheKey, static::CACHE_TIME)) {
                    $memcache->delete($cacheKey);
                }
            }
        }
    }


    public function getDictionaryValues($dictionaryID = null)
    {
        if (null === $dictionaryID && $this->dictionary_id) {
            $dictionaryID = $this->dictionary_id;
        }

        return Dictionaries::getCachedChildren($dictionaryID);
    }


}
