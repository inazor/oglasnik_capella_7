<?php

namespace Baseapp\Models;

use Baseapp\Library\Validations\Sections as SectionsValidations;
use Phalcon\Mvc\Model\Relation;
use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Traits\CategoryTreeHelpers;
use Baseapp\Traits\FormModelControllerTrait;
use Phalcon\Di;
use Phalcon\Mvc\Model\Query\Builder;

/**
 * Sections Model
 */
class Sections extends BaseModelBlamable
{
    use CategoryTreeHelpers;
    use FormModelControllerTrait;

    const LINK_TYPE_DEFAULT  = 0;
    const LINK_TYPE_CATEGORY = 1;
    const LINK_TYPE_CUSTOM   = 2;

    protected $seo_fields_map = array(
        'title'       => 'name',
        'description' => 'excerpt'
    );

    /**
     * Sections initialize
     */
    public function initialize()
    {
        parent::initialize();
    }

    public function beforeSave()
    {
        // handle the json field
        $json = (array) $this->json;
        if (!empty($json)) {
            $json = json_encode($json);
        } else {
            $json = null;
        }

        $this->json = $json;
    }

    public function afterFetch()
    {
        // handle the json field
        $this->json = $this->json ? json_decode($this->json) : new \stdClass;
    }

    /**
     * Get all connected categories
     *
     * @return null|\Baseapp\Models\Categories[]
     */
    public function getCategories($ids = null)
    {
        $categories = null;
        $orderBy    = null;

        $builder = new Builder();
        $builder->columns(array('c.*'));
        $builder->addFrom('Baseapp\Models\Categories', 'c');

        $runQuery = true;
        if ($ids && is_array($ids) && count($ids)) {
            $orderBy = 'FIELD(c.id, ' . implode(',', $ids) . ')';
            $builder->where('c.id IN (' . implode(',', $ids) . ')');
        } elseif ($category_ids = (isset($this->json->categories) ? $this->json->categories : null)) {
            $orderBy = 'FIELD(c.id, ' . implode(',', $this->json->categories) . ')';
            $builder->where('c.id IN (' . implode(',', $category_ids) . ')');
        } else {
            $runQuery = false;
        }

        if ($runQuery) {
            if ($orderBy) {
                $builder->orderBy($orderBy);
            }
            $categories = $builder->getQuery()->execute();
        }

        return $categories;
    }

    /**
     * @param bool $includeChildCategories
     *
     * @return array
     */
    public function getCategoryIDs($includeChildCategories = false)
    {
        $sectionCategoryIDs = array();

        if ($rows = $this->getCategories()) {
            $tree = $this->getDI()->get(Categories::MEMCACHED_KEY);

            foreach ($rows as $row) {
                $categoryID = (int) $row->id;
                if ($includeChildCategories) {
                    $sectionCategoryIDs = array_merge($sectionCategoryIDs, $this->getDescendantIdsWithTransactionTypeFromTreeArray($tree[$categoryID]));
                } else {
                    $sectionCategoryIDs[] = $categoryID;
                }
            }
        }

        return $sectionCategoryIDs;
    }

    /**
     * Returns available locations (defined as ENUM in the db currently)
     * @return array
     */
    public function getAvailableLocations()
    {
        $options = $this->get_field_options('location');

        // fill array keys with values so that \Phalcon\Select behaves properly...
        $return_options = array();
        foreach ($options as $k => $v) {
            $return_options[$v] = $v;
        }

        return $return_options;
    }

    /**
     * Gets the sort order for the last ordered section in specific location
     * @param  string $location Location of section
     * @return integer          sort_order value
     */
    private function _getLastSortOrder($location)
    {
        $lastSortOrder = 0;

        if ($lastSection = Sections::findFirst(array(
            'conditions' => 'location = :location:',
            'bind'       => array('location' => $location),
            'order'      => 'sort_order DESC',
        ))) {
            $lastSortOrder = $lastSection->sort_order;
        }

        return $lastSortOrder;
    }

    /**
     * Add a new Section
     *
     * @param \Phalcon\Http\RequestInterface $request
     * @param array $category_ids
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request, $category_ids = array())
    {
        $this->populate_data_with_post_values($request);
        $this->populate_model_json_with_POST($request);

        $this->sort_order = $this->_getLastSortOrder($this->location) + 1;

        $validation = new SectionsValidations();

        $available_locations = $this->getAvailableLocations();
        $validation->add('location', new \Phalcon\Validation\Validator\InclusionIn(array(
            'message' => 'Location must be one of: ' . implode(', ', $available_locations),
            'domain'  => $available_locations
        )));

        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Save Category data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     * @param array $category_ids
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request, $category_ids = array())
    {
        // TODO: do we need to update/re-calculate sort order in case location changed?

        // avoid validation for certain fields that haven't been changed
        $unique_if_changed = array('url');
        $require_unique = array();
        foreach ($unique_if_changed as $field) {
            if ($this->$field != $request->getPost($field)) {
                $require_unique[] = $field;
            }
        }
        $validation = new SectionsValidations();
        $validation->set_unique($require_unique);

        $available_locations = $this->getAvailableLocations();
        $validation->add('location', new \Phalcon\Validation\Validator\InclusionIn(array(
            'message' => 'Location must be one of: ' . implode(', ', $available_locations),
            'domain'  => $available_locations
        )));

        $messages = $validation->validate($request->getPost());

        $this->populate_data_with_post_values($request);
        $this->populate_model_json_with_POST($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    /**
     * Helper method to populate model's json field with POST values
     * @param  \Phalcon\Http\RequestInterface $request
     */
    protected function populate_model_json_with_POST($request)
    {
        $this->json->icon = $request->getPost('section_icon', 'string', null);
        if ($this->json->categories = $request->getPost('json_categories', 'string', null)) {
            $section_categories = explode(',', $this->json->categories);
            foreach ($section_categories as $k => $section_category) {
                $section_categories[$k] = (int) $section_category;
            }
            $this->json->categories = $section_categories;
            $this->json->categories_preview_count = $request->getPost('json_categories_preview_count', 'int', 0);
        } else {
            $this->json->categories_preview_count = null;
        }
        $this->json->link_type = (int) $request->getPost('link_type', 'int', self::LINK_TYPE_DEFAULT);

        switch ($this->json->link_type) {
            case self::LINK_TYPE_CATEGORY:
                $this->json->link_to_specific_category = $request->getPost('json_link_to_specific_category', 'int', null);
                unset($this->json->custom_link);
                break;

            case self::LINK_TYPE_CUSTOM:
                $this->json->custom_link = $request->getPost('json_custom_link', 'string', null);
                unset($this->json->link_to_specific_category);
                break;

            default:
                unset($this->json->link_to_specific_category, $this->json->custom_link);
        }

        // run through first-level json properties and remove those with null or empty string (leave false for now!)
        foreach ($this->json as $property => $value) {
            if (!is_array($value) && (null === $value || '' === trim($value))) {
                unset($this->json->$property);
            }
        }
    }

    /**
     * Active/Inactive category, depending on the previous state
     *
     * @return bool Whether the update was successful or not
     */
    public function activeToggle()
    {
        $this->disable_notnull_validations();
        $this->active = (int) !$this->active;
        $result = $this->update();
        $this->enable_notnull_validations();
        return $result;
    }

    public function get_url()
    {
        $link = null;

        $link_type = isset($this->json->link_type) && (int) $this->json->link_type ? $this->json->link_type : self::LINK_TYPE_DEFAULT;

        switch ($link_type) {
            case self::LINK_TYPE_CATEGORY:
                if (isset($this->json->link_to_specific_category)) {
                    $categoryID = (int) $this->json->link_to_specific_category;
                    $di = $this->getDI();
                    if ($tree = $di->get(Categories::MEMCACHED_KEY)) {
                        if (isset($tree[$categoryID]) && $category = $tree[$categoryID]) {
                            $link = $di->get('url')->get($category->url);
                        }
                    }
                }
                break;

            case self::LINK_TYPE_CUSTOM:
                if (isset($this->json->custom_link) && trim($this->json->custom_link)) {
                    $link = trim($this->json->custom_link);
                }
                break;
        }

        if ($link) {
            return $link;
        }

        $route_prefix = 'oglasi';

        if (isset($this->url)) {
            $url  = '/' . $route_prefix . '/' . $this->url;

            // Use the 'url' resolver service if present, making it more flexible
            $di = $this->getDI();
            if ($di->has('url')) {
                $url = $di->get('url')->get($route_prefix . '/' . $this->url);
            }

            $link = $url;
        }

        return $link;
    }

    public function get_url_target()
    {
        $target    = null;
        $link_type = isset($this->json->link_type) && (int) $this->json->link_type ? (int) $this->json->link_type : self::LINK_TYPE_DEFAULT;

        switch ($link_type) {
            case self::LINK_TYPE_CUSTOM:
                $target = '_blank';
                break;
        }
        unset($link_type);

        return $target;
    }

    /**
     * @return array|null
     */
    public function getAgeRestriction()
    {
        $section_categories = $this->getCategories();
        $max_age_allowed = 0;
        $restriction = null;

        if ($section_categories) {
            foreach ($section_categories as $section_category) {
                $category_age_restriction = $section_category->getAgeRestriction();
                if ($category_age_restriction) {
                    if (intval($category_age_restriction['min_age']) > $max_age_allowed) {
                        $max_age_allowed = intval($category_age_restriction['min_age']);
                        $restriction = $category_age_restriction;
                    }
                }
            }
        }

        // Override url to match the section (instead of the default one, which is the category's url)
        if (null !== $restriction) {
            $restriction['url'] = $this->get_url();
        }

        return $restriction;
    }

    /**
     * @param $location
     * @param bool $preview Whether to only display preview of section's categories or all of them
     *
     * @return array
     */
    public static function getTreeStructured($location, $preview = false)
    {
        static $sections = null;
        static $cleanedSections = array();

        if (null === $sections) {

            $builder = new \Phalcon\Mvc\Model\Query\Builder();
            $builder->columns(array('section.*'));
            $builder->addFrom('Baseapp\Models\Sections', 'section');
            $builder->where('section.active = 1 AND section.location = :location:', array('location' => $location));
            $builder->orderBy('section.sort_order ASC');
            $rows = $builder->getQuery()->execute();

            if ($rows) {
                $di          = Di::getDefault();
                $tree        = $di->get(Categories::MEMCACHED_KEY);
                $sections    = array();
                $section_ids = array();

                foreach ($rows as $section) {
                    $section_ids[] = $section->id;
                    $frontend_url  = $section->get_url();
                    $sections[$section->id] = array(
                        'name'                => $section->name,
                        'url'                 => '/' . $section->url,
                        'frontend_url'        => $frontend_url,
                        'frontend_url_target' => $section->get_url_target(),
                        'excerpt'             => $section->excerpt,
                        'children'            => array(),
                        'actualChildrenCount' => 0,
                        'json'                => $section->json,
                        'sponsorshipLogo'     => null,
                        'backgroundPic'       => null
                    );

                    $sectionCategories = isset($section->json->categories) ? $section->json->categories : $section->getCategoryIDs();
                    foreach($sectionCategories as $category_id) {
                        if ($category = isset($tree[$category_id]) ? $tree[$category_id] : null) {
                            $sections[$section->id]['children'][] = array(
                                'name'         => $category->name,
                                'url'          => '/' . $category->url,
                                'frontend_url' => $di->get('url')->get($category->url)
                            );
                        }
                    }
                    $sections[$section->id]['actualChildrenCount'] = count($sections[$section->id]['children']);
                    if ($preview) {
                        // find out how many categories should be in preview
                        $previewCount = isset($section->json->categories_preview_count) ? (int) $section->json->categories_preview_count : 0;
                        if ($previewCount > 0) {
                            if ($previewCount > count($sections[$section->id]['children'])) {
                                $previewCount = count($sections[$section->id]['children']);
                            }
                            $sections[$section->id]['children'] = array_slice($sections[$section->id]['children'], 0, $previewCount);
                        }
                    }
                }

                $sponsorshipLogos = GenericImages::findRelated(get_class($rows[0]), $section_ids, 'sponsorshipLogo');
                if (!empty($sponsorshipLogos)) {
                    foreach ($sections as $section_id => $section) {
                        foreach ($sponsorshipLogos as $logo) {
                            if ($logo->model_pk_val == $section_id) {
                                $sections[$section_id]['sponsorshipLogo'] = $logo;
                            }
                        }
                    }
                }

                $backgroundPics = GenericImages::findRelated(get_class($rows[0]), $section_ids, 'backgroundPic');
                if (!empty($backgroundPics)) {
                    foreach ($sections as $section_id => $section) {
                        foreach ($backgroundPics as $bgPic) {
                            if ($bgPic->model_pk_val == $section_id) {
                                $sections[$section_id]['backgroundPic'] = $bgPic;
                            }
                        }
                    }
                }

                $cleanedSections = array();
                // cleanup sections with only one category...
                foreach ($sections as $section_id => $section) {
                    $cleanedSections[$section_id] = $section;
                    $sectionLinkType              = isset($section['json']->link_type) && (int) $section['json']->link_type ? (int) $section['json']->link_type : self::LINK_TYPE_DEFAULT;

                    if (1 === $section['actualChildrenCount'] && self::LINK_TYPE_DEFAULT === $sectionLinkType) {
                        $cleanedSections[$section_id]['frontend_url'] = $section['children'][0]['frontend_url'];
                        $cleanedSections[$section_id]['children'] = array();
                    }
                }
            }
        }

        return $cleanedSections;
    }

    public function getSponsorshipLogo()
    {
        if (isset($this->id) && $this->id) {
            return GenericImages::findFirstRelated(get_class($this), $this->id, 'sponsorshipLogo');
        }

        return null;
    }

    public function getBackgroundPic()
    {
        if (isset($this->id) && $this->id) {
            return GenericImages::findFirstRelated(get_class($this), $this->id, 'backgroundPic');
        }

        return null;
    }

    public function getOrCreateSponsorshipLogo()
    {
        if (isset($this->id) && $this->id) {
            $sponsorshipLogo = $this->getSponsorshipLogo();
            if (!$sponsorshipLogo) {
                $sponsorshipLogo = new GenericImages();
                $sponsorshipLogo->model_name   = get_class($this);
                $sponsorshipLogo->model_pk_val = $this->id;
                $sponsorshipLogo->model_field  = 'sponsorshipLogo';
            }

            return $sponsorshipLogo;
        }

        return null;
    }

    public function getOrCreateBackgroundPic()
    {
        if (isset($this->id) && $this->id) {
            $backgroundPic = $this->getBackgroundPic();
            if (!$backgroundPic) {
                $backgroundPic = new GenericImages();
                $backgroundPic->model_name   = get_class($this);
                $backgroundPic->model_pk_val = $this->id;
                $backgroundPic->model_field  = 'backgroundPic';
            }

            return $backgroundPic;
        }

        return null;
    }

}
