<?php

namespace Baseapp\Models;

use Baseapp\Library\Utils;
use Phalcon\Mvc\Model\Relation;
use Phalcon\Mvc\Model\Resultset;

/**
 * Tags Model
 */
class Tags extends BaseModelBlamable
{
    /**
     * Tags initialize
     */
    public function initialize()
    {
        parent::initialize();
        $this->hasManyToMany(
            'id', __NAMESPACE__ . '\CmsArticlesTags', 'tag_id',
            'article_id', __NAMESPACE__ . '\CmsArticles', 'id',
            array(
                'alias' => 'Articles'
            )
        );
    }

    /**
     * @param string|null $search_term
     *
     * @return array
     */
    public static function getSuggestions($search_term = null)
    {
        if ($search_term) {
            $tags = self::find(array(
                'columns'    => 'raw as name',
                'conditions' => 'raw LIKE CONCAT(\'%\', :search_term:, \'%\')',
                'bind'       => array(
                    'search_term' => trim($search_term)
                ),
                'order'      => 'raw ASC',
                'hydration'  => Resultset::HYDRATE_ARRAYS
            ));
        } else {
            $tags = self::find(array(
                'columns'    => 'raw as name',
                'order'      => 'raw ASC',
                'hydration'  => Resultset::HYDRATE_ARRAYS
            ));
        }

        return $tags->toArray();
    }

    /**
     * Get tag's id (or, in case $add_if_new is true, create a new tag in case it doesn't exist)
     *
     * @param string $raw_tag
     * @param bool $add_if_new
     *
     * @return int|null
     */
    public static function get_id($raw_tag, $add_if_new = false)
    {
        $tag_id = null;

        $raw_tag = trim($raw_tag);
        if ($raw_tag) {
            $tag = self::findFirst(array(
                'conditions' => 'raw = :raw_tag:',
                'bind'       => array(
                    'raw_tag' => $raw_tag
                )
            ));

            if ($tag && $tag->id) {
                $tag_id = $tag->id;
            } elseif ($add_if_new) {
                $new_tag = new self();
                $new_tag->raw = $raw_tag;
                $new_tag->slug = Utils::slugify($raw_tag);
                $new_tag->create();

                if ($new_tag->id) {
                    $tag_id = $new_tag->id;
                }
            }
        }

        return $tag_id;
    }

}
