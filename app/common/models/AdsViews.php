<?php

namespace Baseapp\Models;

class AdsViews extends BaseModelBlamable
{
    public $ad_id;
    public $type;
    public $period;
    public $count = 0;

    public function initialize()
    {
        $this->belongsTo(
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias'      => 'Ad',
                'reusable'   => true
            )
        );
        $this->setSource('ads_views');

        parent::initialize();
    }
}
