<?php

namespace Baseapp\Extension;

/**
 * Our extended version of \Phalcon\Validation
 *
 * TODO: find if it's possible to build this in a way that would support validation chains grouped by field.
 * (instead-of/along-with the global validation chain that works in Phalcon by default (see 'cancelOnFail' option of each validator)
 */
class Validation extends \Phalcon\Validation
{
    /**
     * Has ability to translate the default message for validator type using gettext
     *
     * @param string $type validator type
     *
     * @return string
     */
    public function getDefaultMessage($type)
    {
        if (!isset($this->_defaultMessages[$type])) {
            return '';
        }

        // Translate default messages using gettext
        // return __($this->_defaultMessages[$type]);
        return $this->_defaultMessages[$type];
    }
}
