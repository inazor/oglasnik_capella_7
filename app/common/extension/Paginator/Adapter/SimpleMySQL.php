<?php

namespace Baseapp\Extension\Paginator\Adapter;

use Phalcon\Paginator\Exception as PaginatorException;
use Phalcon\Paginator\AdapterInterface;

/**
 * Baseapp\Extension\Paginator\Adapter\SimpleMySQL
 *
 * A rather simple MySQL pagination adapter which automatically builds and
 * executes the appropriate COUNT sql query based on the sql you provide to it.
 * It also calculates and appends the appropriate LIMIT/OFFSET SQL clauses.
 *
 *<code>
 *$paginator = new Baseapp\Extension\Paginator\Adapter\SimpleMySQL(
 *    array(
 *        'sql' => 'SELECT * FROM ...', // without the LIMIT clause!
 *        'limit' => 2,
 *        'page' => $current_page,
 *    )
 *);
 *</code>
 */
class SimpleMySQL implements AdapterInterface
{
    /**
     * Number of records to show per page.
     */
    protected $limit = null;

    /**
     * Current page in pagination
     */
    protected $page = 1;

    /**
     * @var string SQL statement to execute (without LIMIT clause)
     */
    protected $sql = null;

    protected $bind_params = null;

    protected $total = null;

    /**
     * Adapter constructor
     *
     * @param array $config
     *
     * @throws PaginatorException
     */
    public function __construct(array $config = null)
    {
        if (!is_array($config) || empty($config)) {
            throw new PaginatorException('$config is required');
        }
        $keys = array('limit', 'page', 'sql');
        foreach ($keys as $key) {
            if (!isset($config[$key])) {
                throw new PaginatorException(sprintf('"%s" is a required $config parameter and is not present', $key));
            } else {
                $this->$key = $config[$key];
            }
        }
    }

    public function getCurrentPage()
    {
        return $this->page;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getSQL()
    {
        return $this->sql;
    }

    public function setLimit($limit)
    {
        $this->limit = (int) $limit;
        return $this;
    }

    public function setSQL($sql)
    {
        $this->sql = $sql;
        return $this;
    }

    public function setCurrentPage($page)
    {
        $this->page = $page;
        return $this;
    }

    public function getBindParams()
    {
        return $this->bind_params;
    }

    public function setBindParams($params)
    {
        $this->bind_params = $params;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * Rewrites the query into a "SELECT COUNT(*)" query (if reasonably-easily doable).
     *
     * @borrowed-from Pear::Pager
     *
     * @param string $sql query
     * @return bool|string Rewritten query OR false if the query can't be rewritten
     */
    public function rewrite_count_query($sql)
    {
        if (preg_match('/^\s*SELECT\s+\bDISTINCT\b/is', $sql) ||
            preg_match('/\s+GROUP\s+BY\s+/is', $sql) ||
            preg_match('/\s+UNION\s+/is', $sql)) {
            // zyt: Pear::Pager skips group by queries completely, but
            // there are some simple 'group by' queries we can try rewriting...
            if (preg_match('/\s+GROUP\s+BY\s+/is', $sql)) {
                return $this->rewrite_group_by_to_count_query($sql);
            }
            return false;
        }
        $open_parenthesis = '(?:\()';
        $close_parenthesis = '(?:\))';
        $subquery_in_select = $open_parenthesis . '.*\bFROM\b.*' . $close_parenthesis;
        $pattern = '/(?:.*' . $subquery_in_select . '.*)\bFROM\b\s+/Uims';
        if (preg_match($pattern, $sql)) {
            return false;
        }
        $subquery_with_limit_order = $open_parenthesis . '.*\b(LIMIT|ORDER)\b.*' . $close_parenthesis;
        $pattern = '/.*\bFROM\b.*(?:.*' . $subquery_with_limit_order . '.*).*/Uims';
        if (preg_match($pattern, $sql)) {
            return false;
        }
        $query_count = preg_replace('/(?:.*)\bFROM\b\s+/Uims', 'SELECT COUNT(*) FROM ', $sql, 1);
        list($query_count, ) = preg_split('/\s+ORDER\s+BY\s+/is', $query_count);
        list($query_count, ) = preg_split('/\bLIMIT\b/is', $query_count);
        return trim($query_count);
    }

    /**
     * @param $sql string
     *
     * @return mixed
     */
    public function rewrite_group_by_to_count_query($sql) {
        if (preg_match('/\s+GROUP\s+BY\s+/is', $sql)) {
            $tokens = preg_split('/\s+GROUP\s+BY\s+/is', $sql);
            list($group_fields, ) = preg_split('/\s+ORDER\s+BY\s+/is', $tokens[1]);
            $query_count = preg_replace('/(?:.*)\bFROM\b\s+/Uims', 'SELECT COUNT(DISTINCT ' . $group_fields . ') FROM ', $sql, 1);
            list($query_count, ) = preg_split('/\s+GROUP\s+BY\s+/is', $query_count);
            list($query_count, ) = preg_split('/\s+ORDER\s+BY\s+/is', $query_count);
            list($query_count, ) = preg_split('/\bLIMIT\b/is', $query_count);
            return $query_count;
        }
    }

    /**
     * Returns a slice of the resultset to show in the pagination
     *
     * @return \stdClass|void
     * @throws PaginatorException
     */
    public function getPaginate()
    {
        $page_no = (int) $this->page;
        if ($page_no <= 0) {
            $page_no = 1;
        }

        $limit = (int) $this->limit;
        if ($limit <= 0) {
            throw new PaginatorException('The amount of records per page is 0 or less');
        }

        $sql = $this->sql;

        // Adding the limit/offset sql clauses
        $offset = $limit * ($page_no - 1);
        if ($offset < $limit) {
            $sql .= ' LIMIT ' . $limit;
        } else {
            $sql .= ' LIMIT ' . $limit . ' OFFSET ' . $offset;
        }

        $page = new \stdClass();
        $page->first = 1;
        $page->before = 1;

        $di = \Phalcon\Di::getDefault();
        /**
         * @var $db \Phalcon\Db\AdapterInterface
         */
        $db = $di->getShared('db');

        // Fire the actual query and store the results
        $result = $db->query($sql, $this->bind_params);
        $result->setFetchMode(\Phalcon\Db::FETCH_OBJ);
        $page->items = $result->fetchAll();

        /**
         * TODO/FIXME: In theory, in between these two queries,
         * the DB could have been changed, and the count would
         * then be wrong. Which means we either have to use
         * SQL_CALC_FOUND_ROWS thingy (which means rewriting the
         * query rewriting logic at least), or wrap this shit in a
         * transaction? But it's not worth it probably, so fuck it,
         * it is what it is for now.
         */

        $total_count = 0;
        // Fire the count query and get the result only if the above
        // query actually returned something and if we haven't been given a pre-calculated total earlier
        // for performance reasons
        $total = $this->getTotal();
        if (null !== $total) {
            $total_count = $total;
        }
        if (!empty($page->items) && !$total_count) {
            // Transform the sql we got into a count sql query
            $sql_count = $this->rewrite_count_query($sql);

            $result    = $db->query($sql_count, $this->bind_params);
            $result->setFetchMode(\Phalcon\Db::FETCH_BOTH);

            $results     = $result->fetchAll();
            $total_count = $results[0][0];
        }

        // Compute page numbers and make sure they're within bounds
        $total_pages = $total_count / $limit;
        $total_pages_int = intval($total_pages);
        if ($total_pages != $total_pages_int) {
            $total_pages = $total_pages_int + 1;
        }

        // Don't allow setting larger page than the last one
        if ($page_no > $total_pages) {
            $page_no = $total_pages;
        }

        // Modify previous/before page number if not on first page
        if (1 !== $page_no) {
            $page->before = $page_no - 1;
        }

        // Make sure next page number is within bounds
        $next = $total_pages;
        if ($page_no < $total_pages) {
            $next = $page_no + 1;
        }

        $page->next = $next;
        $page->last = $total_pages;
        $page->current = $page_no;
        $page->total_pages = $total_pages;
        $page->total_items = $total_count;

        return $page;
    }
}
