<?php
/**
 * Custom OIB validator.
 *
 * Example usage:
 * $validation->add('oib', new \Baseapp\Extension\Validator\Oib(array(
 *     'messageInvalidLen' => 'Upisani OIB nema traženi broj znamenki',
 *     'messageDigit' => 'Upisani OIB trebao bi se sastojati samo od brojki',
 *     'message' => 'Upisani OIB nije ispravan'
 * )));
 *
 * @atuhor zytzagoo
 */

namespace Baseapp\Extension\Validator;

use Phalcon\Validation;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class Oib extends Validator implements ValidatorInterface
{
    public function validate(Validation $validator, $attribute)
    {
        $value = $validator->getValue($attribute);
        $label = $this->getOption('label');

        // prep label
        if (empty($label)) {
            $label = $validator->getLabel($attribute);

            if (empty($label)) {
                $label = $attribute;
            }
        }

        // not numeric
        if (!is_numeric($value)) {
            $message = $this->getOption('messageDigit');
            $replace_pairs = array(':field' => $label);

            if (empty($message)) {
                $message = $validator->getDefaultMessage('Digit');
            }

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute, 'Digit'));
            return false;
        }

        // invalid len
        if (strlen($value) !== 11) {
            $message = $this->getOption('messageInvalidLen');
            $replace_pairs = array(':field' => $label);

            if (empty($message)) {
                $message = $validator->getDefaultMessage('InvalidLen');
            }

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute, 'InvalidLen'));
            return false;
        }

        $a = 10;

        for ($i = 0; $i < 10; $i++) {
            $a = $a + intval(substr($value, $i, 1), 10);
            $a = $a % 10;

            if ($a == 0) {
                $a = 10;
            }

            $a *= 2;
            $a = $a % 11;
        }

        $control = 11 - $a;
        if ($control == 10) {
            $control = 0;
        }

        $valid = $control === intval(substr($value, 10, 10), 10);

        if (!$valid) {
            $message = $this->getOption('message');
            $replace_pairs = array(':field' => $label);

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute));
            return false;
        }

        return true;
    }
}
