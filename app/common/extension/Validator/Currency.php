<?php
/**
 * Custom Currency validator.
 *
 * Example usage:
 * $validation->add('currency', new \Baseapp\Extension\Validator\Currency(array(
 *     'messageDigit' => 'Upisana cijena bi se trebala sastojati isključivo od brojki i decimalnog zareza',
 *     'message' => 'Upisana cijena nije ispravna'
 * )));
 */

namespace Baseapp\Extension\Validator;

use Phalcon\Validation;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class Currency extends Validator implements ValidatorInterface
{
    public function validate(Validation $validator, $attribute)
    {
        $value = $validator->getValue($attribute);
        $label = $this->getOption('label');

        // prep label
        if (empty($label)) {
            $label = $validator->getLabel($attribute);

            if (empty($label)) {
                $label = $attribute;
            }
        }

        // not numeric
        if (!is_numeric($value)) {
            $message = $this->getOption('messageDigit');
            $replace_pairs = array(':field' => $label);

            if (empty($message)) {
                $message = $validator->getDefaultMessage('Digit');
            }

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute, 'Digit'));
            return false;
        }

        // if the price is required, it must be a positive number!
        $valid = $value > 0;

        if (!$valid) {
            $message = $this->getOption('message');
            $replace_pairs = array(':field' => $label);

            $validator->appendMessage(new \Phalcon\Validation\Message(strtr($message, $replace_pairs), $attribute));
            return false;
        }

        return true;
    }

}
