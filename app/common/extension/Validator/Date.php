<?php

namespace Baseapp\Extension\Validator;

use Baseapp\Traits\ValidatorTrait;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;

class Date extends Validator implements ValidatorInterface
{
    use ValidatorTrait;

    protected function validateSingle($value)
    {
        if ($value && !$this->validateDateTime($value)) {
            $this->validator->appendMessage(new Message($this->getMessage(), $this->attribute, 'Date'));
            return false;
        }

        return true;
    }

    private function validateDateTime($value)
    {
        if (!$this->isSetOption('format')) {
            throw new Exception('`format` option must be set!');
        }

        $date = \DateTime::createFromFormat($this->getOption('format'), $value);
        if (!$date) {
            return false;
        } else {
            return true;
        }
    }

    private function getMessage()
    {
        $message = $this->getOption('message');
        if (!$message) {
            $message = 'Date is not valid.';
        }

        return $message;
    }
}
