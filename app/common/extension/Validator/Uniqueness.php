<?php

namespace Baseapp\Extension\Validator;

use Phalcon\Db\Adapter\Pdo as DbAdapter;
use Phalcon\Di;
use Phalcon\DiInterface;
use Phalcon\Validation;
use Phalcon\Validation\Message;
use Phalcon\Validation\Validator;
use Phalcon\Validation\ValidatorInterface;
use Phalcon\Validation\Exception as ValidationException;

/**
 * Uniqueness Validator
 */
class Uniqueness extends Validator implements ValidatorInterface
{
    /**
     * Database connection
     * @var DbAdapter
     */
    private $db;

    public function __construct(array $options = array(), $db = null)
    {
        parent::__construct($options);

        // Try to get the db connection from default DI
        if (null === $db) {
            $di = Di::getDefault();

            if ($di instanceof DiInterface && $di->has('db')) {
                $db = $di->get('db');
            }
        }

        if (!($db instanceof DbAdapter)) {
            throw new ValidationException('Uniqueness Validator requires a database connection');
        }

        // Check back-compat options and try to set what we can
        if (false === $this->isSetOption('table')) {
            if ($model = $this->getOption('model')) {
                $instance = new $model;
                // $this->setOption('table', $instance->getSchema() . $instance->getSource());
                $this->setOption('table', $instance->getSource());
                unset($instance);
            } else {
                throw new ValidationException('Uniqueness Validator requires the `table` or `model` option!');
            }
        }

        // Not used currently, but might come in handy...
        /*
        if (false === $this->isSetOption('column')) {
            if ($attribute = $this->getOption('attribute')) {
                $this->setOption('column', $attribute);
            } else {
                throw new ValidationException('Uniqueness Validator requires the `column` or `attribute` option to be set!');
            }
        }
        */

        $this->db = $db;
    }

    /**
     * Executes the validation
     *
     * @param Validation $validation
     * @param string $field field name
     *
     * @return boolean
     *
     * @throws ValidationException
     */
    public function validate(Validation $validation, $field)
    {
        $value     = $validation->getValue($field);
        $model     = $this->getOption('model');
        $attribute = $this->getOption('attribute');

        if (empty($model)) {
            throw new ValidationException('Model must be set');
        }

        // TODO: document this
        if (empty($attribute)) {
            $attribute = $field;
        }

        $table  = $this->getOption('table');
        // $column = $this->getOption('column');

        $binds = array(
            'value' => $value
        );

        if ($except = $this->getOption('except')) {
            $binds['except'] = $except;
            $sql = 'SELECT COUNT(*) AS `cnt` FROM `' . $table . '` WHERE ' . $attribute . ' = :value AND ' . $attribute . ' != :except';
        } elseif ($conditions = $this->getOption('extra_conditions')) {
            $extra_conditions = $this->doubleToSingleColonMarkers($conditions[0]);
            $extra_conditions_binds = $conditions[1];
            if (!empty($extra_conditions_binds)) {
                $binds = array_merge($binds, $extra_conditions_binds);
            }
            $sql = 'SELECT COUNT(*) AS `cnt` FROM `' . $table . '` WHERE ' . $attribute . ' = :value AND ' . $extra_conditions;
        } else {
            $sql = 'SELECT COUNT(*) AS `cnt` FROM `' . $table . '` WHERE ' . $attribute . ' = :value';
        }

        // Hit the database
        $result = $this->db->fetchOne(
            $sql,
            \Phalcon\Db::FETCH_ASSOC,
            $binds
        );

        if ($result['cnt']) {
            $label = $this->getOption('label');

            if (empty($label)) {
                $label = $validation->getLabel($field);

                if (empty($label)) {
                    $label = $field;
                }
            }

            $message = $this->getOption('message');
            $replacePairs = array(':field' => $label);

            if (!$message) {
                $message = 'Already taken. Choose another!';
            }

            $validation->appendMessage(new Message(strtr($message, $replacePairs), $field, 'Uniqueness'));
            return false;
        }

        return true;
    }

    private function doubleToSingleColonMarkers($str)
    {
        $regex = '/(:(\w+):)/';
        $replaced = preg_replace_callback(
            $regex,
            function (array $matches) {
                return ':' . $matches[2];
            },
            $str
        );

        return $replaced;
    }
}
