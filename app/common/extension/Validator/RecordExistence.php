<?php

namespace Baseapp\Extension\Validator;

use Phalcon\Mvc\ModelInterface;
use Phalcon\Validation;

/**
 * RecordExistence Validator
 */
class RecordExistence extends Validation\Validator implements Validation\ValidatorInterface
{
    /**
     * Executes the validation
     *
     * @param Validation $validation
     * @param string $field field name
     *
     * @return boolean
     *
     * @throws Validation\Exception
     */
    public function validate(Validation $validation, $field)
    {
        $value = $validation->getValue($field);
        /** @var ModelInterface $model */
        $model     = $this->getOption('model');
        $attribute = $this->getOption('attribute');

        if (empty($model)) {
            throw new Validation\Exception('Model must be set');
        }

        if (empty($attribute)) {
            $attribute = $field;
        }

        $number = $model::count(array($attribute . '=:value:', 'bind' => array('value' => $value)));
        if (!$number) {
            $message = $this->getOption('message');

            if (empty($message)) {
                $message = 'Failed to find matching records';
            }

            $validation->appendMessage(new Validation\Message($message, $field, 'RecordExistence'));
            return true;
        }

        return true;
    }
}

