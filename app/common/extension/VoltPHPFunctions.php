<?php

namespace Baseapp\Extension;

use Baseapp\Bootstrap;
use Baseapp\Library\Debug;

/**
 * PHP Functions in Volt
 */
class VoltPHPFunctions
{
    /**
     * Compile any function call in a template
     *
     * @param string $name function name
     * @param mixed $arguments function args
     *
     * @return string compiled function
     */
    public function compileFunction($name, $arguments)
    {
        if (function_exists($name)) {
            return $name . '(' . $arguments . ')';
        }

        if ('dump' === $name) {
            return 'Baseapp\Library\Debug::dump(' . $arguments . ')';
        }

        // Override Volt's version() function to include our own version info too,
        // and allow potential arguments to be passed to it for the future...
        if ('version' === $name) {
            return 'Baseapp\Bootstrap::get_version(' . $arguments . ')';
        }
    }

    /**
     * Compile isset (and potentially other) filters
     *
     * @param string $name filter name
     * @param mixed $arguments filter args
     *
     * @return string compiled filter
     */
    public function compileFilter($name, $arguments)
    {
        if ('isset' === $name) {
            return '(isset(' . $arguments . ') ? ' . $arguments . ' : null)';
        }

        if ('label' === $name) {
            return 'Baseapp\Library\Tool::label(' . $arguments . ')';
        }

        if ('soft_break' === $name) {
            return 'Baseapp\Library\Utils::soft_break(' . $arguments . ')';
        }

        if ('truncate' === $name) {
            return 'Baseapp\Library\Utils::str_truncate(' . $arguments . ')';
        }

        if ('truncate_middle' === $name) {
            return 'Baseapp\Library\Utils::str_truncate_middle(' . $arguments . ')';
        }

        if ('slugify' === $name) {
            return 'Baseapp\Library\Utils::slugify(' . $arguments . ')';
        }

        if ('money' === $name) {
            return 'Baseapp\Library\Utils::format_money(' . $arguments . ')';
        }

        if ('day_date' === $name) {
            return 'Baseapp\Library\DateUtils::day_date(' . $arguments . ')';
        }

        if ('stripsometags' === $name) {
            return 'strip_tags(' . $arguments . ', "<br><br/>")';
        }

        if ('category_name_markup' === $name) {
            return 'Baseapp\Library\Utils::category_name_markup(' . $arguments . ')';
        }
    }
}
