<?php

namespace Baseapp\Extension;

use Baseapp\Library\Images\Color;
use Baseapp\Library\Images\Size;

class Image extends \Phalcon\Image\Adapter\Imagick
{

    const JPG_QUALITY = 90;
    const PNG_QUALITY = 90;

    protected $orientation_checked = false;

    /**
     * Calls some of our additional stuff on save automatically and
     * allows us to completely ignore the provided quality param :)
     *
     * @param null $file
     * @param int|null $quality
     *
     * @return bool
     */
    public function save($file = null, $quality = null)
    {
        // TODO: should this be optional?
        $this->set_compression_quality_and_strip();

        $im     = $this->getInternalImInstance();
        $result = $im->writeImage($file);

        return $result;
    }

    /**
     * Overriding crop() to do cropThumbnailImage() if no x/y offsets are specified.
     * Also allows creating crops larger than the original image by specifying $target_width and/or $target_height.
     * In that case the original is centered on both axis within the specified target box.
     *
     * In any case we should end up with a fixed size image. If the original is larger than the
     * specified box, the image is scaled down first and cropped from the center.
     *
     * If offsets are specified then parent::crop() is used (which in turn uses \Imagick::cropImage()).
     *
     * @param int $width
     * @param int $height
     * @param null|int $offset_x
     * @param null|int $offset_y
     * @param null|int $target_width
     * @param null|int $target_height
     *
     * @return $this
     */
    public function crop($width, $height, $offset_x = null, $offset_y = null, $target_width = null, $target_height = null)
    {
        if (null !== $offset_x || null !== $offset_y) {
            // If specific x and/or y offsets are given, crop the original image
            // error_log(sprintf('cropping to %sx%s with offsets x=%s,y=%s', $width, $height, $offset_x, $offset_y));
            parent::crop($width, $height, $offset_x, $offset_y);
        } else {
            // Just width/height given, use cropThumbnailImage()
            $im = $this->getInternalImInstance();
            $im->cropThumbnailImage($width, $height);
        }

        if (null !== $target_width && null !== $target_height) {
            $this->resize_canvas($target_width, $target_height);
        }

        $this->update_dimensions();

        return $this;
    }

    /**
     * @param null|int $width
     * @param null|int $height
     * @param string $anchor
     * @param bool $relative
     * @param string $bgcolor
     *
     * @return $this
     */
    public function resize_canvas($width = null, $height = null, $anchor = 'center', $relative = false, $bgcolor = '#424242')
    {
        $im = $this->getInternalImInstance();

        $original_width  = $im->getImageWidth();
        $original_height = $im->getImageHeight();

        // Check if only width or height is set
        $width  = is_null($width) ? $original_width : intval($width);
        $height = is_null($height) ? $original_height : intval($height);

        // Check if relative width/height
        if ($relative) {
            $width  = $original_width + $width;
            $height = $original_height + $height;
        }

        // Check for negative width/height
        $width  = ($width <= 0) ? $width + $original_width : $width;
        $height = ($height <= 0) ? $height + $original_height : $height;

        $canvas = new \Imagick();
        $canvas->newImage($width, $height, $bgcolor);

        $canvas_size = (new Size($width, $height))->align($anchor);
        $image_size  = (new Size($original_width, $original_height))->align($anchor);
        $canvas_pos  = $image_size->relativePosition($canvas_size);
        $image_pos   = $canvas_size->relativePosition($image_size);

        if ($width <= $original_width) {
            $dst_x = 0;
            $src_x = $canvas_pos->x;
            $src_w = $canvas_size->width;
        } else {
            $dst_x = $image_pos->x;
            $src_x = 0;
            $src_w = $original_width;
        }
        if ($height <= $original_height) {
            $dst_y = 0;
            $src_y = $canvas_pos->y;
            $src_h = $canvas_size->height;
        } else {
            $dst_y = $image_pos->y;
            $src_y = 0;
            $src_h = $original_height;
        }

        // Make image area transparent to keep transparency even if background-color is set
        $pixel = $canvas->getImagePixelColor(0, 0);
        $color = new Color($pixel);
        $fill = $color->getHex();
        $fill = $fill == '#ff0000' ? '#00ff00' : '#ff0000';

        $rect = new \ImagickDraw();
        $rect->setFillColor($fill);
        $rect->rectangle($dst_x, $dst_y, $dst_x + $src_w - 1, $dst_y + $src_h - 1);
        $canvas->drawImage($rect);
        $canvas->transparentPaintImage($fill, 0, 0, false);

        // Crop original to calculated dimensions
        // $im->cropImage($src_w, $src_h, $src_x, $src_y);

        // Copy image into new canvas
        $canvas->compositeImage($im, \Imagick::COMPOSITE_DEFAULT, $dst_x, $dst_y);
        $canvas->setImagePage(0, 0, 0, 0);

        // Set same format as original
        $canvas->setImageFormat($im->getImageFormat());

        // Replace existing image with the newly created one
        // $im->setImageExtent($canvas->getImageWidth(), $canvas->getImageHeight());
        $im->setImage($canvas);

        $this->update_dimensions();

        return $this;
    }

    /**
     * Overriding resize() in order to be able to rotate automatically before resizing (which
     * is done as soon as create_style_image() is called) and so that we can use
     * Imagick::resizeImage() directly, so that we get visually nicer thumbnails.
     * Also allows passing $fit parameter to Imagick::resizeImage() directly.
     *
     * @param null $width
     * @param null $height
     * @param null $master
     * @param bool $fit
     *
     * @return $this
     */
    public function resize($width = null, $height = null, $master = null, $fit = true)
    {
        // TODO: check $master and maybe set $fit accordingly?

        $im = $this->getInternalImInstance();
        $im->resizeImage($width, $height, \Imagick::FILTER_LANCZOS, 1, $fit);

        $this->update_dimensions();

        return $this;
    }

    /**
     * Proportionally resize image only if larger than the specified $width & $height (that is, avoid upscaling)
     *
     * @param $width
     * @param $height
     *
     * @return $this
     */
    public function resize_down($width, $height) {
        $current_w = $this->getWidth();
        $current_h = $this->getHeight();

        $ratio = $current_w / $current_h;
        $w = $ratio >= 1 && $width < $current_w ? $width : 0;
        $h = $ratio < 1 && $height < $current_h ? $height : 0;
        if ($w || $h) {
            $this->resize($w, $h, $master = null, $fit = false);
        }

        return $this;
    }

    /**
     * Combines cropping and resizing to format the image in a "smart" way.
     * Finds the best-fitting aspect ratio of the specified width and height on the
     * current image automatically, cuts it out and re-sizes it to the given dimensions.
     * Pass an optional Closure callback as the third parameter to prevent possible
     * upscaling/upsizing and a custom position of the cutout as the fourth parameter.
     *
     * @param int $width The width the image will be re-sized to after cropping out the best fitting aspect ratio
     * @param null $height Optional. The height the image will be re-sized to after cropping out the best fitting aspect
     *                     ratio. If no height is given, uses the same value as width.
     * @param \Closure $callback Optional. Closure callback defining a constraint to prevent unwanted upscaling.
     * @param string $position Optional. Defines the crop position. By default the best fitting aspect ratio is centered.
     *
     * @return $this
     */
    public function fit($width, $height = null, \Closure $callback = null, $position = 'center')
    {
        $width = (int) $width;
        if (null === $height) {
            $height = $width;
        } else {
            $height = (int) $height;
        }

        $image_size   = new Size($this->getWidth(), $this->getHeight());
        $cropped_size = $image_size->fit(new Size($width, $height), $position);

        // error_log('fitting ' . $width . 'x' . $height . ' with cropped size: ' . $cropped_size->getWidth() . 'x' . $cropped_size->getHeight() . ' with pivot x: ' . $cropped_size->pivot->x . ', y: ' . $cropped_size->pivot->y);

        // Crop image
        $this->crop($cropped_size->width, $cropped_size->height, $cropped_size->pivot->x, $cropped_size->pivot->y);

        return $this;
    }

    /**
     * @param null|string $logo_filepath Path to a file on disk which will be added to the top right corner
     * @param null|string $text Text which will be written on top if the image in the bottom right corner
     *
     * @return $this
     */
    public function add_default_watermark($logo_filepath = null, $text = null)
    {
        // Slap a logo if specified and it exists
        if (null !== $logo_filepath && file_exists($logo_filepath)) {
            $this->composite_logo($logo_filepath);
        }

        if (null !== $text) {
            $text = (string) $text;
            $this->composite_text($text);
        }

        return $this;
    }

    protected function composite_text($text, $font_filepath = null)
    {
        // Use a default font file if not explicitly specified
        if (null === $font_filepath) {
            $font_filepath = APP_DIR . '/common/misc/georgia.ttf';
        }

        $font_sizes = array(11, 13);
        $font_size = $font_sizes[0];

        // Set a larger font size for larger images
        // TODO: maybe use some simple math to scale this automatically?
        $im = $this->getInternalImInstance();
        if ($im->getImageWidth() >= 500) {
            $font_size = $font_sizes[1];
        }

        $text_shadowed = $this->create_shadowed_watermark_text($font_filepath, $font_size, $text);

        // Now composite the watermark image over the original
        // This sucks, but compositeImage() does not yet support gravity
        // and annotateImage() alone looks wrong on cropped images.
        // But the way it's done below works. For now :)
        // BTW, the goal was to position the watermark to the lower right corner with a bit of padding
        $coords = $this->gravity_to_coords($im, $text_shadowed, 'bottomright', 10, 10);
        $im->compositeImage($text_shadowed, \IMagick::COMPOSITE_OVER, $coords['x'], $coords['y']);

        $text_shadowed->destroy();
        unset($text_shadowed);
    }

    /**
     * Places another image over the current one (for watermarking).
     *
     * @param string $filepath Path to file which will be composited over the image
     * @param string $position Named gravity string/coordinates (see Image::gravity_to_coords())
     */
    protected function composite_logo($filepath, $position = 'topright') {
        $logo = $this->create_watermark_image($filepath);
        $im = $this->getInternalImInstance();

        // TODO: automatically scale the $logo for large/small images
        // based on some proportions of $logo and the current image?
        // TODO: We could also have a png/svg logo that could get plastered over the entire image?

        // Reducing the logo size for smaller images if it's too big
        /*
        $image_width = $im->getImageWidth();
        if ($image_width < 500) {
            $logo->scaleImage($image_width / 3, 0);
        }
        */

        // Slap the logo over the original image
        $coords = $this->gravity_to_coords($im, $logo, $position, 10, 10);
        $im->compositeImage($logo, \IMagick::COMPOSITE_OVER, $coords['x'], $coords['y']);

        // Free up some memory
        $logo->destroy();
        unset($logo);
    }

    /**
     * Translates named gravity coordinates into x and y pixel positions/coordinates for placing
     * $watermark into $image (based on dimensions of $image and $watermark).
     *
     * @param \Imagick $image
     * @param \Imagick $watermark
     * @param string $gravity
     * @param int $x_offset
     * @param int $y_offset
     *
     * @return array Computed coordinates (array('x' => int, 'y' => int ))
     */
    protected function gravity_to_coords(&$image, &$watermark, $gravity, $x_offset = 0, $y_offset = 0) {
        switch ($gravity) {
            case 'upperleft':
            case 'topleft':
            case 'ul':
            case 'tl':
                $x = $x_offset;
                $y = $y_offset;
                break;
            case 'upperright':
            case 'topright':
            case 'ur':
            case 'tr':
                $x = $image->getImageWidth() - $watermark->getImageWidth() - $x_offset;
                $y = $y_offset;
                break;
            case 'lowerright':
            case 'bottomright':
            case 'lr':
            case 'br':
                $x = $image->getImageWidth() - $watermark->getImageWidth() - $x_offset;
                $y = $image->getImageHeight() - $watermark->getImageHeight() - $y_offset;
                break;
            case 'lowerleft':
            case 'bottomleft':
            case 'll':
            case 'bl':
                $x = $x_offset;
                $y = $image->getImageHeight() - $watermark->getImageHeight() - $y_offset;
                break;
            case 'center':
            case 'c':
            default:
                $x = ($image->getImageWidth() - $watermark->getImageWidth()) / 2 + $x_offset;
                $y = ($image->getImageHeight() - $watermark->getImageHeight()) / 2 + $y_offset;
                break;
        }
        return array('x' => $x, 'y'=> $y);
    }

    /**
     * Create and return an Imagick object holding the watermark image/logo.
     *
     * @param $filepath
     *
     * @return \Imagick
     */
    protected function create_watermark_image($filepath) {
        return new \Imagick($filepath);
    }

    /**
     * Create and return an \Imagick object holding the watermark text
     *
     * @param string $font Path to font file
     * @param int/float $font_size
     * @param string $text Text to write
     * @return \Imagick
     */
    protected function create_shadowed_watermark_text($font, $font_size, $text) {
        $wm = new \Imagick();
        $draw = new \ImagickDraw();

        $draw->setFont($font);
        $draw->setFontSize($font_size);
        $draw->setGravity(\Imagick::GRAVITY_SOUTHEAST);

        $metrics = $wm->queryFontMetrics($draw, $text);
        $w = $metrics['textWidth'];
        $h = $metrics['textHeight'];

        // 'none' is for transparent bg
        $wm->newImage($w, $h, 'none');
        $draw->setFillColor('white');
        $wm->annotateImage($draw, 0, 0, 0, $text);

        // Creating the text shadow this way produces far better results
        $shadow = clone $wm;
        $shadow->setImageBackgroundColor(new \ImagickPixel('black'));
        $shadow->shadowImage(100, 0.5, 1, 1);
        $shadow->compositeImage($wm, \Imagick::COMPOSITE_OVER, 0, 0);

        $wm->destroy();
        $draw->destroy();

        return $shadow;
    }

    /**
     * Sets quality/compression options and strips data to reduce size
     */
    protected function set_compression_quality_and_strip() {
        $im = $this->getInternalImInstance();

        $format      = strtolower($im->getImageFormat());
        $compression = \IMagick::COMPRESSION_UNDEFINED;
        $quality     = self::JPG_QUALITY;

        if ('png' === $format) {
            $compression = \Imagick::COMPRESSION_ZIP;
            $quality     = self::PNG_QUALITY;
            $im->setInterlaceScheme(\IMagick::INTERLACE_PLANE);
        } elseif ('jpeg' === $format) {
            $compression = \Imagick::COMPRESSION_JPEG;
            $quality     = self::JPG_QUALITY;
            $im->setInterlaceScheme(\IMagick::INTERLACE_LINE);
        }

        $im->setCompression($compression);
        $im->setImageCompression($compression);

        $im->setCompressionQuality($quality);
        $im->setImageCompressionQuality($quality);

        // Strip data
        $im->stripImage();
    }

    /**
     * Overriding so that IDE doesn't freak out due to Phalcon's idetools marking
     * \Phalcon\Image\Adapter\Imagick::getInternalImInstance() as void
     *
     * @return \Imagick
     */
    public function getInternalImInstance()
    {
        return parent::getInternalImInstance();
    }

    /**
     * Updates internal _width and _height properties to match
     * the values of the Imagick instance. If no instance is specified,
     * the internal one is updated by default.
     *
     * Usually called when we've done an operation directly on the
     * internal Imagick instance and we want $this->getWidth() and
     * $this->getHeight() to return the new values.
     *
     * @param \Imagick || null $im
     */
    private function update_dimensions($master = null)
    {
        if (null === $master) {
            $master = $this->getInternalImInstance();
        }

        $this->_width = $master->getImageWidth();
        $this->_height = $master->getImageHeight();
    }

    /**
     * Checks EXIF orientation and rotates the image accordingly.
     *
     * @return $this
     */
    public function auto_rotate()
    {
        // Short circuit in case it's already done
        if (true === $this->orientation_checked) {
            return $this;
        }

        // Check if not already checked
        if (false === $this->orientation_checked) {
            $im          = $this->getInternalImInstance();

            /**
             * zyt: 24.08.2015.
             * Older ImageMagick versions get exif thumbnail orientation and assume its the same as the main image
             * orientation, which is not necessarily the case.
             * Newer versions have it fixed, however they're not in official Ubuntu repos yet (and prod is on
             * Ubuntu for some reason).
             * Luckily, `\exif_read_data()` doesn't suffer the same issue.
             */
            // $orientation = $im->getImageOrientation();
            $exif = @exif_read_data($this->_file, 'IFD0'); // suppressing warnings from corrupted images
            $orientation = isset($exif['Orientation']) ? (int) $exif['Orientation'] : null;
            $rotated     = false;

            switch ($orientation) {
                case \Imagick::ORIENTATION_BOTTOMRIGHT: // 3
                    // rotate 180 degrees
                    $im->rotateImage('#fff', 180);
                    $rotated = true;
                    break;

                case \Imagick::ORIENTATION_RIGHTTOP: // 6
                    // rotate 90 degrees CW
                    $im->rotateImage('#fff', 90);
                    $rotated = true;
                    break;

                case \Imagick::ORIENTATION_LEFTBOTTOM: // 8
                    // rotate 90 degrees CCW
                    $im->rotateimage('#fff', -90);
                    $rotated = true;
                    break;
            }

            // Now that it's auto-rotated, make sure the EXIF data
            // is correct in case the EXIF gets saved with the image!
            if ($rotated) {
                $im->setImageOrientation(\Imagick::ORIENTATION_TOPLEFT);
                $this->update_dimensions($im);
            }
            $this->orientation_checked = true;
        }

        return $this;
    }

/*
    protected function handle_cmyk()
    {
        $im = $this->getInternalImInstance();

        if ($im->getImageColorspace() == \Imagick::COLORSPACE_CMYK) {
            $profiles = $im->getImageProfiles('*', false);
            // Check if ICC profile(s) exist
            $has_icc_profile = (array_search('icc', $profiles) !== false);
            // If it doesn't have a CMYK ICC profile, add one
            if (false === $has_icc_profile) {
                $icc_cmyk = file_get_contents(APP_DIR . '/common/misc/color-profiles/USWebUncoated.icc');
                $im->profileImage('icc', $icc_cmyk);
                unset($icc_cmyk);
            }
            // Then add an RGB profile
            $icc_rgb = file_get_contents(APP_DIR . '/common/misc/color-profiles/sRGB_v4_ICC_preference.icc');
            $im->profileImage('icc', $icc_rgb);
            // For some reason the only way to actually get the images to not be
            // negatives on *nix, and still show up as rgb in IE6 was to avoid explicitly
            // setting the colorspace to rgb. Just adding the profiles above seemed to do the trick. *shrugs*
            // $im->setImageColorSpace(Imagick::COLORSPACE_RGB);
            unset($icc_rgb);
        }
    }
*/
}
