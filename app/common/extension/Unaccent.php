<?php

namespace Baseapp\Extension;

/**
 * Unaccent filter removes croatian accented characters from a string
 *
 * @package Baseapp\Extension
 */
class UnaccentFilter implements \Phalcon\Filter\UserFilterInterface
{
    /**
     * Removes croatian accented characters from a string
     *
     * @param string $string
     *
     * @return string
     */
    public function filter($string)
    {
        return str_replace(
            array('š', 'ć', 'č', 'ž', 'đ', 'Š', 'Ć', 'Ž', 'Đ'),
            array('s', 'c', 'c', 'z', 'd', 'S', 'C', 'Z', 'D'),
            $string
        );
    }
}
