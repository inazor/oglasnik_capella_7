<?php

namespace Baseapp\Library\PDF417\Renderers;

use Baseapp\Library\PDF417\BarcodeData;
use Baseapp\Library\PDF417\RendererInterface;

use DOMImplementation;

class SvgRenderer implements RendererInterface
{
    private $options = [
        'pretty' => true,
        'scale' => 3,
        'ratio' => 3,
        'color' => '#000'
    ];

    public function __construct(array $options = [])
    {
        // Merge given options with defaults
        foreach ($options as $key => $value) {
            if (isset($this->options[$key])) {
                $this->options[$key] = $value;
            }
        }
    }

    public function getContentType()
    {
        return 'image/svg+xml';
    }

    public function render(BarcodeData $data)
    {
        $pixelGrid = $data->getPixelGrid();
        $height = count($pixelGrid);
        $width = count($pixelGrid[0]);

        $options = $this->options;

        // Apply scaling & aspect ratio
        $scaleX = $options['scale'];
        $scaleY = $options['scale'] * $options['ratio'];

        $width *= $scaleX;
        $height *= $scaleY;

        $doc = $this->createDocument();

        // Root document
        $svg = $doc->createElement('svg');
        $svg->setAttribute('height', $height);
        $svg->setAttribute('width', $width);
        $svg->setAttribute('version', '1.1');
        $svg->setAttribute('xmlns', 'http://www.w3.org/2000/svg');

        // Add description node
        $text = 'HUB3 barcode generated by http://www.oglasnik.hr/';
        $description = $doc->createElement('description', $text);
        $svg->appendChild($description);

        // Create the group
        $group = $doc->createElement('g');
        $group->setAttribute('id', 'barcode');
        $group->setAttribute('fill', $options['color']);
        $group->setAttribute('stroke', 'none');

        // Add barcode elements to group
        foreach ($pixelGrid as $y => $row) {
            foreach ($row as $x => $item) {
                if ($item === false) {
                    continue;
                }

                $rect = $doc->createElement('rect');
                $rect->setAttribute('x', $x * $scaleX);
                $rect->setAttribute('y', $y * $scaleY);
                $rect->setAttribute('width', $scaleX);
                $rect->setAttribute('height', $scaleY);

                $group->appendChild($rect);
            }
        }

        $svg->appendChild($group);
        $doc->appendChild($svg);

        return $doc->saveXML();
    }

    /** Creates a DOMDocument for SVG. */
    protected function createDocument()
    {
        $impl = new DOMImplementation();

        $docType = $impl->createDocumentType(
            'svg',
            '-//W3C//DTD SVG 1.1//EN',
            'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'
        );

        $doc = $impl->createDocument(null, null, $docType);
        $doc->formatOutput = true;

        return $doc;
    }
}
