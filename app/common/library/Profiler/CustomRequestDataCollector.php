<?php

namespace Baseapp\Library\Profiler;

use Baseapp\Library\Debug;
use Fabfuel\Prophiler\DataCollectorInterface;

class CustomRequestDataCollector implements DataCollectorInterface
{
    public function getTitle()
    {
        return 'Request';
    }

    public function getIcon()
    {
        return '<i class="fa fa-arrow-circle-o-down"></i>';
    }

    public function getData()
    {
        $data = [
            'SERVER' => $_SERVER,
            'GET' => $_GET,
            'POST' => $_POST,
            'COOKIE' => $_COOKIE,
            'FILES' => $_FILES,
        ];

        foreach ($data as $k => $bag) {
            $data[$k] = Debug::dump($bag);
        }

        return $data;
    }
}
