<?php

namespace Baseapp\Library\Images;

abstract class AbstractColor
{
    /**
     * Initiates color object from int
     *
     * @param int $value
     */
    abstract public function initFromInteger($value);

    /**
     * Initiates color object from an array
     *
     * @param array $value
     */
    abstract public function initFromArray($value);

    /**
     * Initiates color object from a string
     *
     * @param string $value
     */
    abstract public function initFromString($value);

    /**
     * Initiates color object from an \ImagickPixel object
     *
     * @param \ImagickPixel $value
     */
    abstract public function initFromObject($value);

    /**
     * Initiates color object from given R, G and B values
     *
     * @param int $r
     * @param int $g
     * @param int $b
     */
    abstract public function initFromRgb($r, $g, $b);

    /**
     * Initiates color object from given R, G, B and A values
     *
     * @param int $r
     * @param int $g
     * @param int $b
     * @param float $a
     */
    abstract public function initFromRgba($r, $g, $b, $a);

    /**
     * Returns the current color's integer value
     *
     * @return integer
     */
    abstract public function getInt();

    /**
     * Returns the current color's hexadecimal value
     *
     * @param string $prefix
     * @return string
     */
    abstract public function getHex($prefix);

    /**
     * Returns the current color's RGB(A) values as an array
     *
     * @return array
     */
    abstract public function getArray();

    /**
     * Returns the current color's RGBA string
     *
     * @return string
     */
    abstract public function getRgba();

    /**
     * Determines if current color is different from given color
     *
     * @param AbstractColor $color
     * @param int $tolerance
     * @return bool
     */
    abstract public function differs(AbstractColor $color, $tolerance = 0);

    /**
     * Creates new instance
     *
     * @param string $value
     */
    public function __construct($value = null)
    {
        $this->parse($value);
    }

    /**
     * Parses given value as color
     *
     * @param $value
     *
     * @return $this
     * @throws Exception
     */
    public function parse($value)
    {
        switch (true) {
            case is_string($value):
                $this->initFromString($value);
                break;
            case is_int($value):
                $this->initFromInteger($value);
                break;
            case is_array($value):
                $this->initFromArray($value);
                break;
            case is_object($value):
                $this->initFromObject($value);
                break;
            case is_null($value):
                $this->initFromArray(array(255, 255, 255, 0));
                break;
            default:
                throw new Exception("Color format ({$value}) cannot be read.");
        }
        return $this;
    }

    /**
     * Formats current color instance into given format
     *
     * @param string $type
     *
     * @return mixed
     * @throws Exception
     */
    public function format($type)
    {
        switch (strtolower($type)) {
            case 'rgba':
                return $this->getRgba();
            case 'hex':
                return $this->getHex('#');
            case 'int':
            case 'integer':
                return $this->getInt();
            case 'array':
                return $this->getArray();
            case 'obj':
            case 'object':
                return $this;
            default:
                throw new Exception("Color format ({$type}) is not supported.");
        }
    }

    /**
     * Reads RGBA values from string into array
     *
     * @param $value
     *
     * @return array|bool
     * @throws Exception
     */
    protected function rgbaFromString($value)
    {
        // Parse color string in hex format (like #cccccc or cccccc or ccc)
        $hexPattern = '/^#?([a-f0-9]{1,2})([a-f0-9]{1,2})([a-f0-9]{1,2})$/i';
        // Parse color string in format rgb(140, 140, 140)
        $rgbPattern = '/^rgb ?\(([0-9]{1,3}), ?([0-9]{1,3}), ?([0-9]{1,3})\)$/i';
        // Parse color string in format rgba(255, 0, 0, 0.5)
        $rgbaPattern = '/^rgba ?\(([0-9]{1,3}), ?([0-9]{1,3}), ?([0-9]{1,3}), ?([0-9.]{1,4})\)$/i';
        if (preg_match($hexPattern, $value, $matches)) {
            $result = array();
            $result[0] = strlen($matches[1]) == '1' ? hexdec($matches[1] . $matches[1]) : hexdec($matches[1]);
            $result[1] = strlen($matches[2]) == '1' ? hexdec($matches[2] . $matches[2]) : hexdec($matches[2]);
            $result[2] = strlen($matches[3]) == '1' ? hexdec($matches[3] . $matches[3]) : hexdec($matches[3]);
            $result[3] = 1;
        } elseif (preg_match($rgbPattern, $value, $matches)) {
            $result = array();
            $result[0] = ($matches[1] >= 0 && $matches[1] <= 255) ? (int) $matches[1] : 0;
            $result[1] = ($matches[2] >= 0 && $matches[2] <= 255) ? (int) $matches[2] : 0;
            $result[2] = ($matches[3] >= 0 && $matches[3] <= 255) ? (int) $matches[3] : 0;
            $result[3] = 1;
        } elseif (preg_match($rgbaPattern, $value, $matches)) {
            $result = array();
            $result[0] = ($matches[1] >= 0 && $matches[1] <= 255) ? (int) $matches[1] : 0;
            $result[1] = ($matches[2] >= 0 && $matches[2] <= 255) ? (int) $matches[2] : 0;
            $result[2] = ($matches[3] >= 0 && $matches[3] <= 255) ? (int) $matches[3] : 0;
            $result[3] = ($matches[4] >= 0 && $matches[4] <= 1) ? $matches[4] : 0;
        } else {
            throw new Exception("Unable to read color ({$value}).");
        }

        return $result;
    }
}
