<?php

namespace Baseapp\Library\Products\Offline;

use Baseapp\Library\Products\Offline;

class IstaknutiBoja extends Offline
{
    protected $id = 'istaknuti-boja';
    protected $code = 'IstaBPT';
    protected $title = 'Istaknuti boja oglas';
    protected $description = '<p>Želite nešto drugo? Istaknite se bojom i budite sigurni da vaš oglas neće ostati nezamijećen! Za redovito pojavljivanje uzmite paket objava i ostvarite popust.</p>';

    protected $avus_adformat_single = 'Istaknuti boja single T';
    protected $avus_adlayout_single = 'Istaknuti_Col';
    protected $avus_adformat_multi  = 'Istaknuti boja pack T';
    protected $avus_adlayout_multi  = 'Istaknuti_Col';

    protected $options = array(
        '1 objava' => '7600',
        '2 objave' => '14900',
        '4 objave' => '28100',
        '8 objava' => '56200'
    );

    protected $extras = false;
}
