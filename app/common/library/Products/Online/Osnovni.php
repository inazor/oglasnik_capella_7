<?php

namespace Baseapp\Library\Products\Online;

use Baseapp\Library\Products\Online;

class Osnovni extends Online
{
    protected $id = 'osnovni';
    protected $title = 'Osnovni/Mali oglas';
    protected $description = <<<HTML
<ul>
<li>Standardni mali oglas</li>
<li>Pojedine kategorije se naplaćuju</li>
<li>Najnoviji oglasi su na vrhu; poredani po datumu objave</li>
<li>Ne zaboravite na opciju „Skok na vrh“ kako bi uvijek bili ispred konkurencije</li>
</ul>
HTML;

    protected $options = array(
        '30 dana' => 0,
        '60 dana' => 0,
        '90 dana' => 0
    );

    protected $extras = false;

    protected $push_up = '500';

    protected $payment_required_before_publication = false;
}
