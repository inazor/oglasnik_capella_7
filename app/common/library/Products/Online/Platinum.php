<?php

namespace Baseapp\Library\Products\Online;

use Baseapp\Library\Products\Online;

/**
 * Platinum is similar to Pinky (actually, it's exactly the same) but used in different categories.
 * Even so, it's only available from the backend too, regular users shouldn't be able to purchase it.
 */
class Platinum extends Online
{
    protected $id = 'platinum';
    protected $title = 'Super Top oglas';
    protected $description = 'Opis super top oglasa.';

    protected $disabled = false;
    protected $orderable = false;
    protected $upgradable = false;

    protected $options = array(
        '7 dana' => '50000'
    );
    protected $extras = false;
    protected $push_up = false;
    protected $sort_idx = 5;
}
