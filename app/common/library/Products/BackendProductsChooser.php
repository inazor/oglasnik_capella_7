<?php

namespace Baseapp\Library\Products;

class BackendProductsChooser extends GroupedProductsChooser
{
    /**
     * Overridden GroupedProductsChooser::getMarkup() for backend purposes.
     * For now just returning slightly different markup since we don't need product descriptions there etc...
     *
     * @param array $post_data
     *
     * @return string
     */
    public function getMarkup($post_data = array())
    {
        $markup = '';

        // If nothing is passed in, use post data given to the selector
        if (empty($post_data)) {
            $post_data = $this->getPostData();
        }

        foreach ($this->selector->getGrouped() as $group => $group_products) {
            $group_hidden = '';

            // Add a toggling checkbox before the offline group
            if ('offline' === $group) {
                $checkbox_checked = isset($post_data['offline-products']) ? ' checked="checked"' : '';
                if (empty($checkbox_checked)) {
                    $group_hidden = ' hidden';
                }
                $markup .= '
                    <div class="checkbox-toggle">
                        <div class="checkbox checkbox-primary">
                            <input type="checkbox"' . $checkbox_checked . ' class="target-toggler" data-toggle-target=".products-group-offline" name="offline-products" id="offline-products">
                            <label for="offline-products"><span class="text-primary">Tiskano izdanje</span></label>
                        </div>
                    </div>';
            }

            $markup .= '<div class="products-group products-group-' . $group . $group_hidden . '">';
            $markup .= '<h5>' . ucfirst($group) . '</h5>';

            foreach ($group_products as $k => &$product) {
                /* @var $product Base */
                $markup .= $product->getBackendChooserMarkup();
            }
            unset($product);

            $markup .= '</div>';
        }

        return $markup;
    }
}
