<?php

use Phalcon\Mvc\Model\Behavior;
use Phalcon\Mvc\Model\BehaviorInterface;

/**
 * Allows dynamically changing the underlying source table used for a model by
 * providing a resetSource() method handler which does the dirty work.
 *
 * @link http://forum.phalconphp.com/discussion/1788/changing-model-source-table-dynamically
 */
class ResetSource extends Behavior implements BehaviorInterface
{

    public function missingMethod(\Phalcon\Mvc\ModelInterface $model, $method, $arguments = array())
    {
        switch ($method) {
            case 'resetSource':
                $mgr = $model->getModelsManager();
                $mgr->__destruct();
                $mgr->setModelSource($model, $arguments[0]);
                return TRUE;
                break;
        }
    }

}
