<?php

namespace Baseapp\Library;


class MessagesCollector extends \Phalcon\Mvc\User\Component
{

    protected $messages = array();

    public function __construct($messages = array(), $bucket = 'default')
    {
        $this->setMessages($messages, $bucket);
    }

    public static function create($messages = array(), $bucket = 'default')
    {
        return new self($messages, $bucket);
    }

    public function setMessages($messages = array(), $bucket = 'default')
    {
        $this->messages[$bucket] = $messages;
    }

    public function getMessages($bucket = 'default')
    {
        $messages = array();
        if (isset($messages[$bucket])) {
            $messages = $messages[$bucket];
        }
        return $messages;
    }

    public function addMessage($message, $bucket = 'default')
    {
        $this->messages[$bucket][] = $message;
    }

} 
