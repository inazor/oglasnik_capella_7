<?php

namespace Baseapp\Library\Mapping;

use Baseapp\Console;
use Baseapp\Library\Utils;
use Baseapp\Library\RawDB;
use Baseapp\Library\Email;
use Phalcon\Mvc\User\Component;

class MigrateAvusPhoneNumbers extends Component
{
    use MigrationTrait;

    private $di;
    private $import_first_started = 0;
    private $import_started = 0;
    private $import_ended = 0;
    private $source_db;
    private $target_db;
    private $current_timestamp;
    private $sql_limit = 500;
    private $processed = 0;
    private $remaining = null;
    private $use_pcntl = 1;
    private $last_id = 0;
    private $order = 'ASC';
    private $counters_total = array(
        'ok'        => 0,
        'duplicate' => 0,
        'error'     => 0,
        'fixed'     => 0
    );
    private $counters = array(
        'ok'        => 0,
        'duplicate' => 0,
        'error'     => 0,
        'fixed'     => 0
    );
    private $actionName = 'migratePhoneNumbers';
    private $log2email = null;
    private $log = array(
        'msgs'      => array(),
        'errors'    => array()
    );

    public function __construct($options = null)
    {
        $this->import_started = isset($_SERVER['REQUEST_TIME_FLOAT']) ? $_SERVER['REQUEST_TIME_FLOAT'] : microtime(true);
        $this->import_first_started = $this->import_started;
        $this->di = $this->getDI();
        $this->current_timestamp = Utils::getRequestTime();
        // configure and initialize source db
        $source_db_config = array(
            'host'     => 'sql.oglasnik.hr',
            'username' => 'npongrac',
            'password' => 'fief7eej',
            'dbname'   => 'oglasnik',
            'options'  => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES latin1',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $this->source_db = new RawDB($source_db_config);
        // configure and initialize target db
        $target_db_config = array(
            'options'      => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $this->target_db = new RawDB($target_db_config);

        if (!empty($options)) {
            $first_time = empty($options['remaining']);

            if ($first_time) {
                $this->add2Log('-- Setting up options..');
                $this->add2Log('--');
            }
            $this->options = $options;
            if (!empty($options['log2email'])) {
                $this->log2email = trim($options['log2email']);
                if ($first_time) {
                    $this->add2Log('-- log2email => ' . $this->log2email);
                }
            }
            if (!empty($options['actionName'])) {
                $this->actionName = trim($options['actionName']);
                if ($first_time) {
                    $this->add2Log('-- actionName => ' . $this->actionName);
                }
            }
            if (!empty($options['sql_limit'])) {
                $this->sql_limit = intval($options['sql_limit']);
                if ($first_time) {
                    $this->add2Log('-- sql_limit => ' . $this->sql_limit);
                }
            }
            if (!empty($options['processed'])) {
                $this->processed = intval($options['processed']);
            }
            if (!empty($options['remaining'])) {
                $this->remaining = intval($options['remaining']);
            }
            if (!empty($options['pcntl'])) {
                $this->use_pcntl = intval($options['pcntl']);
            }
            if (!empty($options['last_id'])) {
                $this->last_id = intval($options['last_id']);
            }
            if (!empty($options['order'])) {
                $this->order = trim($options['order']);
            }
            if (!empty($options['counters'])) {
                $counters_arr = explode('_', trim($options['counters']));
                $this->counters_total = array(
                    'ok'        => !empty($counters_arr[0]) ? (int)$counters_arr[0] : 0,
                    'duplicate' => !empty($counters_arr[1]) ? (int)$counters_arr[1] : 0,
                    'error'     => !empty($counters_arr[2]) ? (int)$counters_arr[2] : 0,
                    'fixed'     => !empty($counters_arr[3]) ? (int)$counters_arr[3] : 0,
                );
            }
            if ($first_time) {
                $this->add2Log('--------------------------------------------------------');
            }
        }
    }

    private function get_run_argv()
    {
        $run_argv = array(
            ROOT_PATH . '/private/index.php',
            'import',
            $this->actionName
        );

        if ($this->log2email) {
            $run_argv[] = 'log2email=' . $this->log2email;
        }
        if ($this->sql_limit) {
            $run_argv[] = 'sql_limit=' . $this->sql_limit;
        }
        if ($this->processed) {
            $run_argv[] = 'processed=' . $this->processed;
        }
        if ($this->remaining) {
            $run_argv[] = 'remaining=' . $this->remaining;
        }
        if ($this->use_pcntl == 0) {
            $run_argv[] = 'pcntl=false';
        }
        if ($this->last_id > 0) {
            $run_argv[] = 'last_id=' . $this->last_id;
        }
        if ($this->order == 'DESC') {
            $run_argv[] = 'order=DESC';
        }
        if ($this->counters_total['ok'] || $this->counters_total['duplicate'] || $this->counters_total['error'] || $this->counters_total['fixed']) {
            $run_argv[] = 'counters=' . $this->counters_total['ok'] . '_' . $this->counters_total['duplicate'] . '_' . $this->counters_total['error'] . '_' . $this->counters_total['fixed'];
        }

        return $run_argv;
    }

    private function get_run_command()
    {
        $run_command = $_SERVER['_'] . ' ' . ROOT_PATH . '/private/index.php import ' . $this->actionName;

        if ($this->log2email) {
            $run_command .= ' log2email=' . $this->log2email;
        }
        if ($this->sql_limit) {
            $run_command .= ' sql_limit=' . $this->sql_limit;
        }
        if ($this->processed) {
            $run_command .= ' processed=' . $this->processed;
        }
        if ($this->remaining) {
            $run_command .= ' remaining=' . $this->remaining;
        }
        if ($this->use_pcntl == 0) {
            $run_command .= ' pcntl=0';
        }
        if ($this->last_id > 0) {
            $run_command .= ' last_id=' . $this->last_id;
        }
        if ($this->order == 'DESC') {
            $run_command .= ' order=DESC';
        }
        if ($this->counters_total['ok'] || $this->counters_total['duplicate'] || $this->counters_total['error'] || $this->counters_total['fixed']) {
            $run_command .= ' counters=' . $this->counters_total['ok'] . '_' . $this->counters_total['duplicate'] . '_' . $this->counters_total['error'] . '_' . $this->counters_total['fixed'];
        }

        return $run_command;
    }

    /**
     * Validate import category id
     *
     * Import category id (as far as we know) is a string with even number
     * of characters (digits), and every category/subcategory is
     * represented with a two-digit code..
     *
     * @param string $import_category_id
     * @return bool
     */
    protected function valid_import_category_id($import_category_id)
    {
        $import_category_id = trim($import_category_id);

        if (strlen($import_category_id) % 2 == 0) {
            return true;
        }
        $this->add2Log('Import category: ' . $import_category_id . ' is not valid?', 'error');

        return false;
    }

    protected function fixPhoneNumber($str, $country_code = null)
    {
        $phone_number = trim($str);
        $country_code = trim($country_code) ? trim($country_code) : 'HR';

        if (trim($str)) {
            $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
            try {
                $phoneData = $phoneUtil->parse($str, $country_code);
                if ($phone_number = $phoneUtil->isValidNumber($phoneData)) {
                    $phone_number = $phoneUtil->format($phoneData, \libphonenumber\PhoneNumberFormat::E164);
                }
            } catch (\libphonenumber\NumberParseException $e) {
                // handle this?
            }
        }

        return $phone_number;
    }

    protected function toUTF8($str)
    {
        $str = iconv('ISO-8859-2', 'UTF-8', $str);
        $str = str_replace('&#8364;', '€', $str);
        return $str;
    }

    public function sendMail()
    {
        if ($this->log2email && !is_bool($this->log2email) && !is_numeric($this->log2email)) {
            $this->import_ended = microtime(true);
            $time_spent = $this->import_ended - $this->import_started;
            $time_spent_min = (int)($time_spent/60);
            $time_spent_sec = $time_spent - ($time_spent_min*60);

            $time_spent_total = $this->import_ended - $this->import_first_started;
            $time_spent_total_arr = explode('.', $time_spent_total);
            $time_spent_total = date('H:i:s', $time_spent_total_arr[0]) . '.' . $time_spent_total_arr[1];

            $esitmated_import_end = microtime(true) + round(($time_spent/1000) * $this->remaining_ads);

            $body  = 'TOTALS:' . PHP_EOL;
            $body .= '========================================================' . PHP_EOL;
            $body .= 'Imported so far  : ' . $this->processed_ads . '(' . round(($this->processed_ads/($this->processed_ads+$this->remaining_ads))*100, 2) . '%)' .  PHP_EOL;
            $body .= 'Remaining        : ' . $this->remaining_ads . PHP_EOL;
            $body .= 'Time spent       : ' . $time_spent_total . PHP_EOL;
            $body .= 'Estimated finish : ' . date('H:i:s', $esitmated_import_end) . PHP_EOL;
            $body .= '--------------------------------------------------------' . PHP_EOL;
            $body .= ' This cycle: ' . PHP_EOL;
            $body .= ' - Imported      : ' . $this->counters['imported'] . PHP_EOL;
            $body .= ' - Duplicate     : ' . $this->counters['duplicate'] . PHP_EOL;
            $body .= ' - Errored       : ' . $this->counters['error'] . PHP_EOL;
            $body .= ' - Time spent    : ' . sprintf("%02d", $time_spent_min) . 'm ' . sprintf("%02d", $time_spent_sec). 's ' . PHP_EOL;
            $body .= ' - Average speed : ' . round(($time_spent / (intval($this->counters['imported']) ? intval($this->counters['imported']) : 1)), 4) . 'ms / ad' . PHP_EOL;
            if (count($this->log['errors'])) {
                $body .= '-[ Errors ]---------------------------------------------' . PHP_EOL;
                $body .= PHP_EOL . implode(PHP_EOL, $this->log['errors']) . PHP_EOL;
            }
            $body .= '-[ Messages ]-------------------------------------------' . PHP_EOL;
            $body .= PHP_EOL . implode(PHP_EOL, $this->log['msgs']) . PHP_EOL;
            $body .= '========================================================' . PHP_EOL;

            @mail(
                $this->log2email,
                '[' . $this->processed_ads . '/' . ($this->processed_ads+$this->remaining_ads) . '] - Oglasnik.hr ' . $this->actionName,
                $body
            );
        }
    }

    private function add2Log($msg, $type = 'info')
    {
        $msg = trim($msg);
        if ($type == 'info') {
            $this->log['msgs'][] = date('d.m.Y @ H:i:s') . ' ' . $msg;
        } else {
            $this->log['errors'][] = date('d.m.Y @ H:i:s') . ' ' . $msg;
        }

        if ($this->log2email) {
            echo $msg . PHP_EOL;
        }
    }

    private function getIdsFromArray($rows, $key = 'id')
    {
        $ids = array();

        foreach($rows as $row) {
            if (trim($row[$key])) {
                $ids[] = trim($row[$key]);
            }
        }

        return $ids;
    }

    public function migratePhoneNumbers()
    {
        $ads_missing_phone_numbers = 'SELECT a.id, a.import_id FROM ads a INNER JOIN users u ON a.user_id = u.id WHERE a.import_id IS NOT NULL AND a.import_id > :last_id AND a.phone1 IS NULL AND a.phone2 IS NULL AND u.phone1 IS NULL AND u.phone2 IS NULL ORDER BY a.id ' . $this->order . ' LIMIT 0, ' . $this->sql_limit;
        $ads_missing_phone_count   = 'SELECT COUNT(*) FROM ads a INNER JOIN users u ON a.user_id = u.id WHERE a.import_id IS NOT NULL AND a.import_id > :last_id AND a.phone1 IS NULL AND a.phone2 IS NULL AND u.phone1 IS NULL AND u.phone2 IS NULL ORDER BY a.id ' . $this->order;
        $params = array(
            'last_id'  => $this->last_id
        );

        $this->add2Log('--------------------------------------------------------');
        $printable_query = $ads_missing_phone_numbers;
        foreach ($params as $key => $val) {
            $printable_query = str_ireplace(':' . $key, $val, $ads_missing_phone_numbers);
        }
        $this->add2Log($printable_query);
        $this->add2Log('--------------------------------------------------------');

        if ($this->remaining === null) {
            // count remaining for import...
            $this->remaining = (int)$this->target_db->findFirst($ads_missing_phone_count, $params, true);
        }

        $rows = $this->target_db->find($ads_missing_phone_numbers, $params);
        $found_rows = $rows && count($rows) ? count($rows) : 0;
        if ($found_rows) {
            $this->add2Log('Migrating ' . $found_rows . ' row' . ($found_rows > 1 ? 's' : '') . ($this->remaining ? ' out of ' . $this->remaining . ' remaining!' : ''));
            $this->add2Log('--------------------------------------------------------');

            $source_ids = $this->getIdsFromArray($rows, 'import_id');
            if (count($source_ids)) {
                $source_rows = $this->source_db->find("SELECT a.id, a.phone1, a.phone2, u.phone 'user_phone1', u.phone2 'user_phone2' FROM ads a LEFT JOIN users u ON a.submitted_by = u.id WHERE a.id IN (" . implode(',', $source_ids) . ")");

                if ($source_rows) {
                    foreach($source_rows as $i => $source_row) {
                        $this->last_id = $source_row['id'];

                        $update_fields = array();
                        $update_params = array();
                        if (trim($source_row['phone1'])) {
                            $update_fields[] = 'phone1 = :phone1';
                            $update_params['phone1'] = trim($source_row['phone1']);
                        } elseif (trim($source_row['user_phone1'])) {
                            $update_fields[] = 'phone1 = :phone1';
                            $update_params['phone1'] = trim($source_row['user_phone1']);
                        }
                        if (trim($source_row['phone2'])) {
                            $update_fields[] = 'phone2 = :phone2';
                            $update_params['phone2'] = trim($source_row['phone2']);
                        } elseif (trim($source_row['user_phone2'])) {
                            $update_fields[] = 'phone2 = :phone2';
                            $update_params['phone2'] = trim($source_row['user_phone2']);
                        }

                        if (count($update_params)) {
                            $update_params['import_id'] = $source_row['id'];
                            $update_sql = 'UPDATE ads SET ' . implode(', ', $update_fields) . ' WHERE import_id = :import_id';

                            $updated = $this->target_db->update(
                                $update_sql,
                                $update_params
                            );

                            if ($updated) {
                                $this->counters['ok']++;
                                $this->counters_total['ok']++;
                                $this->add2Log(($i+1) . '. Migrate OK ' . $source_row['id'] . ' -> ' . (count($update_params) - 1));
                            }
                        }
                        $this->processed++;
                        $this->remaining--;
                    }
                }
            }
        }

        if ($this->remaining > 0) {
            $this->add2Log('--------------------------------------------------------');
            $this->add2Log('-- RESTARTING TASK -> ' . $this->remaining . ' rows remaining!');
            $this->add2Log('--------------------------------------------------------');
            $this->sendMail();
            sleep(1);

            \pcntl_exec($_SERVER['_'], $this->get_run_argv());
        }

        return $this->counters_total;
    }

}
