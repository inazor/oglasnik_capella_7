<?php

namespace Baseapp\Library\Validations;


abstract class Ads
{
    const MODEL = '\Baseapp\Models\Ads';

    public function __construct() {
        $this->validation = new \Baseapp\Extension\Validation();
        $this->setup();
    }

    // validation implementations in inherited/concrete classes should reside here
    abstract public function setup();

    public function get()
    {
        return $this->validation;
    }

    public function getMessages()
    {
        return $this->get()->getMessages();
    }

    public function validate($data)
    {
        return $this->get()->validate($data);
    }

}
