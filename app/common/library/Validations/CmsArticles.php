<?php

namespace Baseapp\Library\Validations;


class CmsArticles
{
    protected $unique = array();

    public function __construct() {
        $this->validation = new \Baseapp\Extension\Validation();
        $this->setup();
    }

    public function setup()
    {
        $this->validation->add('category_id', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Category cannot be empty'
        )));

        $this->validation->add('title', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Title cannot be empty'
        )));

        $this->validation->add('slug', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'SLUG cannot be empty'
        )));

        $this->validation->add('content', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Content cannot be empty'
        )));

        $this->validation->setLabels(array(
            'category_id' => 'Category',
            'title' => 'Title',
            'slug' => 'SLUG',
            'content' => 'Content'
        ));
    }

    public function add($field, $validator)
    {
        $this->validation->add($field, $validator);
    }

    public function set_unique($unique) {
        $this->unique = $unique;
    }

    public function get()
    {
        $this->add_uniques();
        return $this->validation;
    }

    private function add_uniques() {
        if (is_array($this->unique) && !empty($this->unique)) {
            foreach ($this->unique as $field_name) {
                $this->validation->add($field_name, new \Baseapp\Extension\Validator\Uniqueness(array(
                    'model' => '\Baseapp\Models\CmsArticles',
                    'message' => ucfirst($field_name) . ' already exists'
                )));
            }
        }
    }

    public function validate($data)
    {
        return $this->get()->validate($data);
    }

    public function getMessages() {
        return $this->validation->getMessages();
    }

}
