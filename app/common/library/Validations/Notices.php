<?php

namespace Baseapp\Library\Validations;


class Notices
{
    public function __construct() {
        $this->validation = new \Baseapp\Extension\Validation();
        $this->setup();
    }

    public function setup()
    {
        $this->validation->add('title', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Title cannot be empty'
        )));

        $this->validation->add('message', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Message cannot be empty'
        )));

        $this->validation->add('received_at', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Date when message should be received by users cannot be empty'
        )));

        $this->validation->setLabels(array(
            'title'       => 'Title',
            'message'     => 'Message',
            'received_at' => 'Receive date'
        ));
    }

    public function add($field, $validator)
    {
        $this->validation->add($field, $validator);
    }

    public function get()
    {
        return $this->validation;
    }

    public function validate($data)
    {
        return $this->get()->validate($data);
    }

    public function getMessages() {
        return $this->validation->getMessages();
    }

}
