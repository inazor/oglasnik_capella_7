<?php

namespace Baseapp\Library\Validations;

class UsersBackendRelaxed extends UsersBackend
{
    public function setup()
    {
        $this->validation->add('username', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Username cannot be empty'
        )));
        $this->validation->add('email', new \Phalcon\Validation\Validator\PresenceOf(array(
            'message' => 'Email cannot be empty'
        )));
        $this->validation->add('email', new \Phalcon\Validation\Validator\Email(array(
            'message' => 'Email incorrect'
        )));
        $this->validation->setLabels(array(
            'username' => 'Username',
            'email' => 'E-mail',
        ));
    }
}
