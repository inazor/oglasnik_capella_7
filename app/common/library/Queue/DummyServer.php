<?php

namespace Baseapp\Library\Queue;

/**
 * DummyServer
 *
 * Replaces Beanstalk Queue with a dummy server that doesn't blow up when put() is called so
 * existing code can work even when queue is disabled via config (or not available at all).
 */
class DummyServer
{
    /**
     * Simulates putting a job in the queue
     *
     * @param mixed $job
     *
     * @return bool
     */
    public function put($job)
    {
        return true;
    }

    /**
     * Simulates choosing a tube by just returning true...
     *
     * @param string $tube
     *
     * @return bool
     */
    public function choose($tube)
    {
        return true;
    }
}
