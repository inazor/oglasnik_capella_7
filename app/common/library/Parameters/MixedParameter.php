<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Models\CategoriesFieldsetsParameters as FieldsetParameter;
use Baseapp\Models\Parameters as Parameter;
use Baseapp\Library\Utils;

class MixedParameter
{
    private $parameter = null;

    public function __construct(FieldsetParameter $fieldset_parameter, $module = 'backend')
    {
        if ($parameter = Parameter::getCached($fieldset_parameter->parameter_id)) {
            switch ($parameter->type_id) {
                case 'DEPENDABLE_DROPDOWN':
                    $this->parameter = new DependableDropdownParameter($module);
                    break;

                case 'LOCATION':
                    $this->parameter = new LocationParameter($module);
                    break;

                case 'TITLE':
                    $this->parameter = new TitleParameter($module);
                    break;

                case 'DESCRIPTION':
                    $this->parameter = new DescriptionParameter($module);
                    break;

                case 'CURRENCY':
                    $this->parameter = new CurrencyParameter($module);
                    break;

                case 'UPLOADABLE':
                    $this->parameter = new UploadableParameter($module);
                    break;

                case 'DATEINTERVAL':
                    $this->parameter = new DateIntervalParameter($module);
                    break;

                case 'DROPDOWN':
                    $this->parameter = new DropdownParameter($module);
                    break;

                case 'DATE':
                    $this->parameter = new DateParameter($module);
                    break;

                case 'YEAR':
                    $this->parameter = new YearParameter($module);
                    break;

                case 'MONTHYEAR':
                    $this->parameter = new MonthYearParameter($module);
                    break;

                case 'NUMBER':
                    $this->parameter = new NumberParameter($module);
                    break;

                case 'NUMBERINTERVAL':
                    $this->parameter = new NumberIntervalParameter($module);
                    break;

                case 'RADIO':
                    $this->parameter = new RadioParameter($module);
                    break;

                case 'CHECKBOX':
                    $this->parameter = new CheckboxParameter($module);
                    break;

                case 'CHECKBOXGROUP':
                    $this->parameter = new CheckboxGroupParameter($module);
                    break;

                case 'TEXT':
                    $this->parameter = new TextParameter($module);
                    break;

                case 'TEXTAREA':
                    $this->parameter = new TextareaParameter($module);
                    break;

                case 'URL':
                    $this->parameter = new UrlParameter($module);
                    break;

                default:
                    $this->parameter = null;
            }
        }

        if ($this->parameter) {
            $this->parameter->setFieldsetParameter($fieldset_parameter);
        }
    }

    public function get()
    {
        return $this->parameter;
    }

}
