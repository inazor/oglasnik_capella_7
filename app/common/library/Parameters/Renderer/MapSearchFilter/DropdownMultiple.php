<?php

namespace Baseapp\Library\Parameters\Renderer\MapSearchFilter;

use Baseapp\Library\Parameters\Renderer\MapSearchFilter;

class DropdownMultiple extends MapSearchFilter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => true,
            'full_row'    => false
        );

        if (!$this->is_hidden) {
            $parameter_id        = 'parameter_' . $this->fieldset_parameter->category_id . '_' . $this->parameter->id;
            $parameter_name      = 'ad_params_' . $this->fieldset_parameter->category_id . '_' . $this->parameter->id;
            $markup_data['name'] = 'ad_params_' . $this->parameter->id;

            if (isset($this->data) && isset($this->data[$parameter_name]) && is_array($this->data[$parameter_name]) && count($this->data[$parameter_name])) {
                $parameter_values = array();

                foreach ($this->data[$parameter_name] as $val) {
                    if (intval($val)) {
                        $parameter_values[] = intval($val);
                    }
                }

                if (count($parameter_values)) {
                    $markup_data['email_agent'] = true;

                    $model_alias = 'ap_' . $this->parameter->id;
                    $markup_data['filter'] = array(
                        'model' => 'Baseapp\Models\AdsParameters',
                        'alias' => $model_alias
                    );

                    if (count($parameter_values) == 1) {
                        $markup_data['filter']['filter'] = array(
                            'where_conditions' => '(['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id: AND ['.$model_alias.'].value = :parameter_' . $this->parameter->id . '_value:)',
                            'where_params' => array(
                                'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                                'parameter_' . $this->parameter->id . '_value' => $parameter_values[0]
                            ),
                            'where_types' => array(
                                'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT,
                                'parameter_' . $this->parameter->id . '_value' => \PDO::PARAM_INT
                            )
                        );
                    } else {
                        $markup_data['filter']['filter'] = array(
                            'where_conditions' => '(['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id: AND ['.$model_alias.'].value IN (' . implode(',', $parameter_values) . '))',
                            'where_params' => array(
                                'parameter_' . $this->parameter->id . '_id' => $this->parameter->id
                            ),
                            'where_types' => array(
                                'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT
                            )
                        );
                    }
                }
            } else {
                $parameter_values = array();
            }

            $filter_label = $this->parameter_settings->label_text;
            if (isset($this->parameter_settings->is_searchable_options->label_text) && !empty($this->parameter_settings->is_searchable_options->label_text)) {
                $filter_label = trim($this->parameter_settings->is_searchable_options->label_text);
            }

            $markup_data['html'] .= '    <div class="col-md-3 col-sm-6">' . PHP_EOL;
            $markup_data['html'] .= '        <div class="form-group onlyDeselect">' . PHP_EOL;
            $markup_data['html'] .= '            <label for="' . $parameter_id . '">' . $filter_label . '</label>' . PHP_EOL;
            $markup_data['html'] .= '            <select id="' . $parameter_id . '" name="' . $parameter_name . '[]" data-type="select" data-default="" multiple="multiple" data-bit-name="' . $filter_label . '">' . PHP_EOL;

            // for loop begin
            $dictionary_level_values = $this->parameter->getDictionaryValues($this->parameter->dictionary_id);
            $liveSearch = 'false';
            if ($dictionary_level_values) {
                if (count($dictionary_level_values) > 20) {
                    $liveSearch = 'true';
                }
                foreach ($dictionary_level_values as $value) {
                    $selected_option = in_array(intval($value->id), $parameter_values) ? ' selected="selected"' : '';
                    $markup_data['html'] .= '                <option value="' . ((int) $value->id) . '"' . $selected_option . '>' . trim($value->name) . '</option>' . PHP_EOL;
                }
            }
            // for loop end;

            $markup_data['html'] .= '            </select>' . PHP_EOL;
            $markup_data['html'] .= '        </div>' . PHP_EOL;
            $markup_data['html'] .= '    </div>' . PHP_EOL;

            $style = 'btn btn-default';

            $markup_data['js'] .= <<<JS

    \$('#$parameter_id').selectpicker({
        style: '$style',
        size: 8,
        noneSelectedText: '',
        liveSearch: $liveSearch,
        actionsBox: true,
        selectAllText: 'Označi sve',
        deselectAllText: 'Poništi',
        confirmSelectionText: 'Potvrdi odabir',
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary',
        dropupAuto: false
    });
    \$('#$parameter_id').on('loaded.bs.select', function(e) {
        var \$selectAllBtn = $(e.target).closest('.bootstrap-select').find('.actions-btn.bs-select-all');
        if (\$selectAllBtn.length) {
            \$selectAllBtn.remove();
        }
    });
JS;
        }

        return $markup_data;
    }

}
