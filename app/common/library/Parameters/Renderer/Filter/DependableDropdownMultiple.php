<?php

namespace Baseapp\Library\Parameters\Renderer\Filter;

use Baseapp\Library\Parameters\Renderer\Filter;
use Baseapp\Models\Dictionaries as Dictionary;

class DependableDropdownMultiple extends Filter
{

    public function getMarkup()
    {
        $markup_data = array(
            'html'        => '',
            'js'          => '',
            'filter'      => null,
            'email_agent' => false
        );

        if ($this->parameter_settings && count($this->parameter_settings)) {
            $curr_level = 1;
            $markup_data['js'] = <<<JS

    function parameter_{$this->parameter->id}_onChange(\$source, \$target) {
        \$target.find('option').remove();
        if ('undefined' != typeof \$source.val()) {
            var source_values = \$source.val();
            if (source_values) {
                if (source_values.length === 1 && '' != source_values[0]) {
                    \$.get(
                        '/ajax/dictionary/' + source_values[0] + '/values',
                        function(data) {
                            if (data.length) {
                                // workaround for safari mobile <select multiple> bugs
                                if (navigator.userAgent.match(/iPhone/i)) {
                                    \$target.append('<option value="" disabled="disabled"></option>');
                                }
                                \$.each(data, function(idx, data){
                                    \$target.append(
                                        \$('<option/>').attr('value', data.id).text(data.name)
                                    );
                                });
                            }
                            \$('#' + \$target.attr('id') + '_box').slideDown(function(){
                                \$target.prop('disabled', false);
                                \$target.selectpicker('refresh');
                            });
                        },
                        'json'
                    );
                } else {
                    \$('#' + \$target.attr('id') + '_box').slideUp(function(){
                        \$target.prop('disabled', true);
                        \$target.selectpicker('refresh');
                    });
                }
            } else {
                \$('#' + \$target.attr('id') + '_box').slideUp(function(){
                    \$target.prop('disabled', true);
                    \$target.selectpicker('refresh');
                });
            }
        } else {
            \$('#' + \$target.attr('id') + '_box').slideUp(function(){
                \$target.prop('disabled', true);
                \$target.selectpicker('refresh');
            });
        }
        \$target.prop('selectedIndex', 0).change();
    }
JS;

            $model_alias = 'ap_' . $this->parameter->id;
            $prev_level = null;
            $all_dropdowns = array();
            $curr_dictionary = $this->parameter->Dictionary;
            $markup_data['html'] .= '<div class="form-group onlyDeselect">' . PHP_EOL;
            foreach ($this->parameter_settings as $level) {
                if (!$level->is_searchable_options) {
                    break;
                }

                $parameter_level_id   = 'parameter_' . $this->parameter->id . '_level_' . $curr_level;
                $parameter_level_name = 'ad_params_' . $this->parameter->id . '_' . $curr_level;
                $all_dropdowns[]      = '#' . $parameter_level_id;

                if (isset($this->data) && isset($this->data[$parameter_level_name])) {
                    $level_selected_value = null;
                    if (is_array($this->data[$parameter_level_name]) && count($this->data[$parameter_level_name])) {
                        if (count($this->data[$parameter_level_name]) > 1) {
                            $level_selected_value = array();
                            foreach ($this->data[$parameter_level_name] as $val) {
                                if (intval($val)) {
                                    $level_selected_value[] = intval($val);
                                }
                            }
                            if (empty($level_selected_value)) {
                                $level_selected_value = null;
                            }
                        } else {
                            $level_selected_value = intval($this->data[$parameter_level_name][0]);
                        }
                    } elseif (intval($this->data[$parameter_level_name])) {
                        $level_selected_value = intval($this->data[$parameter_level_name]);
                    }
                } else {
                    $level_selected_value = null;
                }
                if ($level_selected_value) {
                    $markup_data['email_agent'] = true;
                }

                $filter_label = $level->label_text;
                if (isset($level->is_searchable_options->label_text) && !empty($level->is_searchable_options->label_text)) {
                    $filter_label = trim($level->is_searchable_options->label_text);
                }

                $placeholder_text = isset($level->placeholder_text) && trim($level->placeholder_text) ? trim($level->placeholder_text) : null;

                $markup_data['html'] .= '    <div id="' . $parameter_level_id . '_box" class="margin-bottom-sm"' . (!$curr_dictionary ? ' style="display:none"' : '') . '>' . PHP_EOL;
//                $markup_data['html'] .= '        <label class="control-label" for="' . $parameter_level_id . '">' . $filter_label . '</label>' . PHP_EOL;
                $markup_data['html'] .= '        <select class="form-control" id="' . $parameter_level_id . '" name="' . $parameter_level_name . '[]" data-type="select" data-default="" multiple="multiple" class="bs-select-hidden" data-bit-name="' . $filter_label . '"' . ($placeholder_text ? ' title="' . $placeholder_text . '"' : '') . '>' . PHP_EOL;
                // for loop begin
                if ($curr_dictionary) {
                    $dictionary_level_values = Dictionary::getCachedChildren($curr_dictionary->id);
                    if ($dictionary_level_values) {
                         foreach ($dictionary_level_values as $value) {
                            $selected_option = '';
                            if ($level_selected_value) {
                                if (is_array($level_selected_value)) {
                                    $selected_option = in_array(intval($value->id), $level_selected_value) ? ' selected="selected"' : '';
                                } else {
                                    $selected_option = intval($value->id) === intval($level_selected_value) ? ' selected="selected"' : '';
                                }
                            }
                            $markup_data['html'] .= '            <option value="' . ((int) $value->id) . '"' . $selected_option . ' data-bit-name="' . $value->name . '" data-bit-value="' . trim($value->id) . '">' . trim($value->name) . '</option>' . PHP_EOL;
                        }
                    }
                }
                // for loop end;
                $markup_data['html'] .= '        </select>' . PHP_EOL;
                $markup_data['html'] .= '    </div>' . PHP_EOL;

                if ($prev_level) {
                    $prev_parameter_level_id = 'parameter_' . $this->parameter->id . '_level_' . $prev_level;
                    $markup_data['js'] .= <<<JS

    \$('#$prev_parameter_level_id').change(function(){
        parameter_{$this->parameter->id}_onChange(\$(this), \$('#$parameter_level_id'));
    });
JS;
                }

                if ($level_selected_value) {
                    $markup_data['filter'] = array(
                        'model' => 'Baseapp\Models\AdsParameters',
                        'alias' => $model_alias
                    );

                    if (is_array($level_selected_value)) {
                        $markup_data['filter']['filter'] = array(
                            'where_conditions' => '(['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id: AND ['.$model_alias.'].level = :parameter_' . $this->parameter->id . '_level_' . $curr_level . ': AND ['.$model_alias.'].value IN (' . implode(',', $level_selected_value) . '))',
                            'where_params' => array(
                                'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                                'parameter_' . $this->parameter->id . '_level_' . $curr_level => $curr_level
                            ),
                            'where_types' => array(
                                'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT,
                                'parameter_' . $this->parameter->id . '_level_' . $curr_level => \PDO::PARAM_INT
                            )
                        );
                    } else {
                        $markup_data['filter']['filter'] = array(
                            'where_conditions' => '(['.$model_alias.'].parameter_id = :parameter_' . $this->parameter->id .'_id: AND ['.$model_alias.'].level = :parameter_' . $this->parameter->id . '_level_' . $curr_level . ': AND ['.$model_alias.'].value = :parameter_' . $this->parameter->id . '_level_' . $curr_level . '_value:)',
                            'where_params' => array(
                                'parameter_' . $this->parameter->id . '_id' => $this->parameter->id,
                                'parameter_' . $this->parameter->id . '_level_' . $curr_level => $curr_level,
                                'parameter_' . $this->parameter->id . '_level_' . $curr_level . '_value' => $level_selected_value
                            ),
                            'where_types' => array(
                                'parameter_' . $this->parameter->id . '_id' => \PDO::PARAM_INT,
                                'parameter_' . $this->parameter->id . '_level_' . $curr_level => \PDO::PARAM_INT,
                                'parameter_' . $this->parameter->id . '_level_' . $curr_level . '_value' => \PDO::PARAM_INT
                            )
                        );
                    }
                }

                $prev_level = $curr_level;

                // if we have something selected in current level, then, in next iteration we will fetch currently
                // selected level's value children
                $curr_dictionary = null;
                if ($level_selected_value && !is_array($level_selected_value) && (string) intval($level_selected_value) === (string) $level_selected_value) {
                    $curr_dictionary = Dictionary::findFirst($level_selected_value);
                }
                $curr_level++;
            }
            $markup_data['html'] .= '</div>' . PHP_EOL;

            if (count($all_dropdowns)) {
                $all_dropdowns = implode(', ', $all_dropdowns);
                $markup_data['js'] .= <<<JS

    \$('$all_dropdowns').selectpicker({
        style: 'btn btn-default',
        size: 8,
        noneSelectedText: '',
        liveSearch: true,
        actionsBox: true,
        selectAllText: 'Označi sve',
        deselectAllText: 'Poništi',
        confirmSelectionText: 'Potvrdi odabir',
        noneResultsText: '',
        mobile: OGL.is_mobile_browser,
        iconBase: 'fa',
        tickIcon: 'fa-check text-primary',
        dropupAuto: false
    });
    \$('$all_dropdowns').on('loaded.bs.select', function(e) {
        var \$selectAllBtn = $(e.target).closest('.bootstrap-select').find('.actions-btn.bs-select-all');
        if (\$selectAllBtn.length) {
            \$selectAllBtn.remove();
        }
    });
JS;
            }
        }

        return $markup_data;
    }

}
