<?php

namespace Baseapp\Library\Parameters\Renderer\View;

use Baseapp\Library\Parameters\Renderer\View;

class ClassicParameter extends View
{
    public function render()
    {
        $html           = '';
        $parameter_name = 'ad_params_' . $this->parameter->parameter_id;

        if (!$this->parameter_settings->is_hidden) {
            $parameter_option      = isset($this->data->$parameter_name) ? $this->data->$parameter_name : null;
            $selected_value        = null;
            $selected_value_string = '';

            if ('CHECKBOX' === $this->type) {
                $selected_value_string = '';
                if ($parameter_option) {
                    $selected_value        = 'Da';
                    $selected_value_string = $selected_value;
                } else {
                    $selected_value = 'Ne';
                }
            } elseif ('DATEINTERVAL' === $this->type && $parameter_option) {
                $from = '';
                if (isset($parameter_option->from)) {
                    if (isset($parameter_option->from->text_value) && trim($parameter_option->from->text_value)) {
                        $from = trim($parameter_option->from->text_value);
                    } elseif (isset($parameter_option->from->value)) {
                        $from = date('d.m.Y', strtotime(trim($parameter_option->from->value)));
                    }
                }
                $to = '';
                if (isset($parameter_option->to)) {
                    if (isset($parameter_option->to->text_value) && trim($parameter_option->to->text_value)) {
                        $to = trim($parameter_option->to->text_value);
                    } elseif (isset($parameter_option->to->value)) {
                        $to = date('d.m.Y', strtotime(trim($parameter_option->to->value)));
                    }
                }
                if ($from || $to) {
                    if ($from && $to) {
                        $selected_value = $from . ' - ' . $to;
                    } elseif ($from) {
                        $selected_value = 'od ' . $from;
                    } elseif ($to) {
                        $selected_value = 'do ' . $from;
                    }
                    $selected_value_string = $selected_value;
                }
            } elseif ('DATE' === $this->type && $parameter_option) {
                if (isset($parameter_option->text_value) && trim($parameter_option->text_value)) {
                    $selected_value = trim($parameter_option->text_value);
                } elseif (isset($parameter_option->value) && trim($parameter_option->value)) {
                    $tmp_time = strtotime(trim($parameter_option->value));
                    if ($tmp_time) {
                        $selected_value = date('d.m.Y', $tmp_time);
                    }
                }
                if ($selected_value) {
                    $selected_value_string = $selected_value;
                }
            } elseif ('MONTHYEAR' === $this->type && $parameter_option) {
                $datemonth_value = null;
                if (isset($parameter_option->text_value) && trim($parameter_option->text_value)) {
                    if (strpos(trim($parameter_option->text_value), '-') !== false) {
                        // MM-YYYY
                        $year_month = explode('-', trim($parameter_option->text_value));
                        if (count($year_month) == 2) {
                            $datemonth_value = strtotime($year_month[0] . '-' . $year_month[1] . '-01');
                        }
                    } elseif (strpos(trim($parameter_option->text_value), '/') !== false) {
                        // MM/YYYY
                        $year_month = explode('/', trim($parameter_option->text_value));
                        if (count($year_month) == 2) {
                            $datemonth_value = strtotime($year_month[1] . '-' . $year_month[0] . '-01');
                        }
                    }
                } elseif (isset($parameter_option->value) && trim($parameter_option->value)) {
                    $datemonth_value = strtotime(trim($parameter_option->value) . '-01');
                }
                if ($datemonth_value) {
                    $selected_value        = date('m/Y', $datemonth_value);
                    $selected_value_string = $selected_value;
                }
            } elseif ($parameter_option) {
                if (isset($parameter_option->text_value) && trim($parameter_option->text_value)) {
                    $selected_value = trim($parameter_option->text_value);
                } elseif (isset($parameter_option->value) && trim($parameter_option->value)) {
                    $selected_value = trim($parameter_option->value);
                }
                if ($selected_value) {
                    $selected_value_string = $selected_value;
                }
            }

            if ($selected_value_string) {
                $prefix = isset($this->parameter_settings->prefix) && trim($this->parameter_settings->prefix) ? trim($this->parameter_settings->prefix) . ' ' : '';
                $suffix = isset($this->parameter_settings->suffix) && trim($this->parameter_settings->suffix) ? ' ' . trim($this->parameter_settings->suffix) : '';

                if ('NUMBER' === $this->type && $parameter_option && isset($this->parameter_settings->number_decimals) && $this->parameter_settings->number_decimals) {
                    $selected_value_string = number_format($selected_value_string, $this->parameter_settings->number_decimals, ',', '.');
                }

                $html .= '<div class="col-sm-6">' . PHP_EOL;
                if ('group' === $this->fieldset_style) {
                    $html .= '    <span class="color-light">' . trim($this->parameter_settings->label_text) . '</span>' . PHP_EOL;
                    $html .= '    <ul>' . PHP_EOL;
                    $html .= '        <li>' . $prefix . $selected_value_string . $suffix . '</li>' . PHP_EOL;
                    $html .= '    </ul>' . PHP_EOL;
                } else {
                    $html .= '    <span class="color-light">' . trim($this->parameter_settings->label_text) . ':</span> ' . $prefix . $selected_value_string . $suffix . '<br>' . PHP_EOL;
                }
                $html .= '</div>' . PHP_EOL;

                return array(
                    'html' => trim($html)
                );
            }
        }

        return null;
    }

}
