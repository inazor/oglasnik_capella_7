<?php

namespace Baseapp\Library\Parameters;

use Baseapp\Library\Utils;

class CheckboxParameter extends BaseParameter
{
    protected $group_type = 'custom';
    protected $settings_location = 'data';
    protected $type = 'CHECKBOX';
    protected $accept_dictionary = false;
    protected $can_default = true;
    protected $can_default_type = 'checkbox';
    protected $can_other = false;
    protected $can_prefix_suffix = false;
    protected $is_dependable = false;
    protected $is_required = false;
    public $is_searchable = false;
    public $is_searchable_type = array('CHECKBOX');

    public function setValue($value)
    {
        if (intval($value) || 'on' == trim($value)) {
            $this->setPreparedData(array(
                'id' => (int) $this->parameter->id,
                'value' => 1
            ));

            $this->setPreparedDataJson(
                'ad_params_' . $this->parameter->id,
                array(
                    'value' => 1,
                    'text_value' => $this->parameter_settings->label_text
                )
            );
        }
    }

    public function process()
    {
        $this->validate_cannot_be_empty();
        if (isset($this->data['ad_params_' . $this->parameter->id])) {
            $this->setValue(trim(strip_tags($this->data['ad_params_' . $this->parameter->id])));
        }
    }

    public function render_input()
    {
        parent::render_input();

        $render_data = array(
            'row'  => false,
            'html' => '',
            'js'   => ''
        );

        if (!$this->is_hidden) {
            $parameter_id = 'parameter_' . $this->parameter->id;
            $parameter_name = 'ad_params_' . $this->parameter->id;

            if (isset($this->data)) {
                $checkbox_selected = isset($this->data[$parameter_name]);
            } else {
                $checkbox_selected = isset($this->parameter_settings->default_value) && intval($this->parameter_settings->default_value);
            }

            $required_html = ($this->parameter_settings->is_required ? ' <abbr title="' . $this->getValidationString('required_field') . '">*</abbr>' : '');

            $div_class = $this->render_as_full_row ? 'col-lg-12' : 'col-lg-3 col-md-3';
            $div_class .= ' solo-checkbox';
            $render_data['html'] .= '<div class="' . $div_class . '">' . PHP_EOL;

            // Forcing an empty label when shown within .col-md-3 so it aligns vertically with other fields around it hopefully
            if (!$this->render_as_full_row) {
                $render_data['html'] .= '<label class="spacer">&nbsp;</label>';
            }

            $render_data['html'] .= '    <div class="checkbox checkbox-primary' . (isset($this->errors) && $this->errors->filter($parameter_name) ? ' has-error' : '') . '">' . PHP_EOL;
            $render_data['html'] .= '        <input type="checkbox" name="' . $parameter_name . '" id="' . $parameter_id . '"' . ($checkbox_selected ? ' checked="checked"' : '') . ' />' . PHP_EOL;
            $render_data['html'] .= '        <label class="control-label" for="' . $parameter_id . '">' . trim($this->parameter_settings->label_text) . $required_html . '</label>' . PHP_EOL;

            if (isset($this->errors) && $this->errors->filter($parameter_name)) {
                $render_data['html'] .= '        <p class="help-block">' . current($this->errors->filter($parameter_name))->getMessage() . '</p>' . PHP_EOL;
            } elseif (isset($this->parameter_settings->help_text) && trim($this->parameter_settings->help_text)) {
                $render_data['html'] .= '        <p class="help-block">' . trim($this->parameter_settings->help_text) . '</p>' . PHP_EOL;
            }

            $render_data['html'] .= '    </div>' . PHP_EOL;
            $render_data['html'] .= '</div>' . PHP_EOL;
        }

        return $render_data;
    }

}
