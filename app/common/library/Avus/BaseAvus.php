<?php

namespace Baseapp\Library\Avus;

use Baseapp\Library\RawDB;

abstract class BaseAvus extends \Phalcon\Mvc\User\Component
{

    public $avus_db;
    public $export_table_name      = 'online2avus';
    public $paper_issue_table_name = 'paper_issues_future';

    public function __construct()
    {
        $db_config = array(
            'host'     => $this->config->avus->host,
            'username' => $this->config->avus->username,
            'password' => $this->config->avus->password,
            'dbname'   => $this->config->avus->dbname,
            'options'  => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $this->avus_db = new RawDB($db_config);

        if (!empty($this->config->avus->export_table)) {
            $this->export_table_name = trim($this->config->avus->export_table);
        }
        if (!empty($this->config->avus->paper_issue_table)) {
            $this->paper_issue_table_name = trim($this->config->avus->paper_issue_table);
        }
    }

    public function toAvusEncoding($str)
    {
        $str = str_replace('²', '2', $str);
        $str = str_replace('…', '...', $str);
        $str = str_replace('€', '&#8364;', $str);
        $str = iconv('UTF-8', 'ISO-8859-2//TRANSLIT//IGNORE', $str);
        $str = utf8_encode($str);

        return $str;
    }

    public function nextAppearanceDate()
    {
        $appearancedate = null;

        $row = $this->avus_db->findFirst(
            "SELECT * FROM {$this->paper_issue_table_name} WHERE `date` > :today ORDER BY `date` ASC LIMIT 0,1",
            array(
                ':today' => date('Y-m-d')
            )
        );
        if ($row) {
            $appearancedate = $row['id'];
        }

        return $appearancedate;
    }


}
