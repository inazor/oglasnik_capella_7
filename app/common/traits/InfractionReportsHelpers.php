<?php

namespace Baseapp\Traits;

use Phalcon\Di;
use Phalcon\Filter;
use Baseapp\Extension\Tag;
use Baseapp\Library\Email;
use Baseapp\Library\Validations\InfractionReports as InfractionReportValidations;
use Baseapp\Library\Utils;
use Baseapp\Models\InfractionReports;

trait InfractionReportsHelpers
{

/* Model related stuff
------------------------------------------------------------------------------*/

    /**
     * Handles infraction reporting
     *
     * @param  \Phalcon\Http\RequestInterface $request
     * @return \Phalcon\Mvc\Model\MessageInterface[]|bool
     */
    public function infractionReport($request)
    {
        $validation        = new InfractionReportValidations();
        $available_reasons = $this->getInfractionReportReasons();
        $validation->add('report_reason', new \Phalcon\Validation\Validator\InclusionIn(array(
            'message' => 'Odabrani razlog nije valjan!',
            'domain'  => $available_reasons
        )));
        $messages = $validation->validate($request->getPost());

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            $filter   = new Filter();
            $reporter = $this->getDI()->getShared('auth')->get_user();

            $report = new InfractionReports();
            $report->model_name     = get_class($this);
            $report->model_pk_val   = $this->id;
            $report->reporter_id    = $reporter->id;
            $report->report_reason  = $filter->sanitize($request->getPost('report_reason'), 'string');
            $report->report_message = $filter->sanitize($request->getPost('message'), 'string', null);
            $report->reported_at    = date('Y-m-d H:i:s', Utils::getRequestTime());
            $report->reported_ip    = $filter->sanitize($request->getClientAddress(true), 'string');

            $reported = $report->save();

            $saveModelChanges = false;
            if ($this instanceof \Baseapp\Models\Ads) { 
                $this->moderation = \Baseapp\Library\Moderation::MODERATION_STATUS_REPORTED;
                $saveModelChanges = true;
            }
            if ($saveModelChanges) {
                try {
                    $this->save();
                } catch (\PDOException $e) {
                    // maybe there is a better way of handling INSERT IGNORE (as
                    // Phalcon currently doesn't support it)?
                }
            }

            $this->sendInfractionReportEmail($request);

            return $reported;
        }
    }

    public function getInfractionReportReasons()
    {
        $reportReasons = array();

        if (isset($this->reportReasons) && is_array($this->reportReasons) && count($this->reportReasons)) {
            $reportReasons = $this->reportReasons;
        } else {
            $reportReasons = array('Ostalo');
        }

        // copy array values to keys..
        return array_combine($reportReasons, $reportReasons);
    }

    /**
     * Prepares and sends infraction report email message.
     *
     * @return bool
     * @throws \phpmailerException depending on Email class config
     */
    protected function sendInfractionReportEmail($request)
    {
        $filter                = new Filter();
        $reporter              = $this->getDI()->getShared('auth')->get_user();
        $InfractionReportEmail = Di::getDefault()->getShared('config')->app->InfractionReportEmail;

        $emailDetails = array(
            'reporter_name'    => $reporter->username . ' [ID: ' . $reporter->id . ']',
            'reporter_reason'  => $filter->sanitize($request->getPost('report_reason'), 'string'),
            'reported_message' => $filter->sanitize($request->getPost('message'), 'string')
        );
        if ($this instanceof \Baseapp\Models\Ads) {
            $emailSubject = 'Oglasnik.hr - prijava oglasa';
            $emailDetails = array_merge(
                $emailDetails,
                array(
                    'title'       => 'Prijava oglasa',
                    'entity_id'   => $this->id,
                    'entity_name' => $this->title,
                    'entity_url'  => 'admin/ads/edit/' . $this->id
                )
            );
        } elseif ($this instanceof \Baseapp\Models\Users) {
            $emailSubject = 'Oglasnik.hr - prijava korisnika';
            $emailDetails = array_merge(
                $emailDetails,
                array(
                    'title'       => 'Prijava korisnika',
                    'entity_id'   => $this->id,
                    'entity_name' => $this->username,
                    'entity_url'  => 'admin/users/edit/' . $this->id
                )
            );
        } elseif ($this instanceof \Baseapp\Models\UsersShops) {
            $emailSubject = 'Oglasnik.hr - prijava trgovine';
            $emailDetails = array_merge(
                $emailDetails,
                array(
                    'title'       => 'Prijava trgovine',
                    'entity_id'   => $this->id,
                    'entity_name' => $this->getName(),
                    'entity_url'  => 'admin/shops/edit/' . $this->id
                )
            );
        }

        $email = new Email();
        $email->prepare($emailSubject, $InfractionReportEmail, 'infractionreport', $emailDetails);
        return $email->Send();
    }

    public function getInfractionReports($groupStatuses = false)
    {
        return InfractionReports::getInfractionReports($this, $groupStatuses);
    }

    protected function getResolvedInfractionReports()
    {
        $infractionReports = $this->getInfractionReports(true);
        return $infractionReports['resolved'];
    }

    protected function getUnresolvedInfractionReports()
    {
        $infractionReports = $this->getInfractionReports(true);
        return $infractionReports['unresolved'];
    }

    protected function getIDsFromInfractionReports($infractionReports = array())
    {
        $ids = array();

        if (count($infractionReports)) {
            foreach ($infractionReports as $report) {
                $ids[] = (int) $report['id'];
            }
        }

        return array_unique($ids);
    }

/* Controller related stuff
------------------------------------------------------------------------------*/

    public function reportAction($identificator)
    {
        /* @var $model \Baseapp\Models\BaseModel */
        $model = $this->infractionReportModelClass;

        switch($model) {
            case 'Baseapp\Models\Ads':
                $entity              = $model::findFirst((int) $identificator);
                $flashSessionError   = 'Molimo vas da se prijavite u vaš račun kako bi mogli popuniti obrazac za prijavu oglasa...';
                $flashSessionSuccess = 'Naši administratori će pregledati oglas u najkraćem mogućem vremenu.';
                $signInRedirectURL   = 'user/signin?next=oglas/report/' . (int) $identificator;
                break;

            case 'Baseapp\Models\Users':
                $entity              = $model::findFirstByUsername($identificator);
                $flashSessionError   = 'Molimo vas da se prijavite u vaš račun kako bi mogli popuniti obrazac za prijavu korisnika...';
                $flashSessionSuccess = 'Naši administratori će pregledati korisnika u najkraćem mogućem vremenu.';
                $signInRedirectURL   = 'user/signin?next=korisnik/report/' . $identificator;
                break;

            case 'Baseapp\Models\UsersShops':
                $entity              = $model::findFirstBySlug($identificator);
                $flashSessionError   = 'Molimo vas da se prijavite u vaš račun kako bi mogli popuniti obrazac za prijavu trgovine...';
                $flashSessionSuccess = 'Naši administratori će pregledati trgovinu u najkraćem mogućem vremenu.';
                $signInRedirectURL   = 'user/signin?next=trgovina/report/' . $identificator;
                break;

            default:
                $entity = null;
        }

        if (!$entity) {
            return $this->trigger_404();
        }

        if (!$this->auth->logged_in()) {
            $this->flashSession->error('<strong>Ooops!</strong> ' . $flashSessionError);
            return $this->redirect_to($signInRedirectURL);
        }

        switch($model) {
            case 'Baseapp\Models\Ads':
                $redirectToURL               = $entity->get_frontend_view_link();
                $infractionReportTitle       = 'Prijava oglasa radi kršenja pravila oglašavanja';
                $infractionReportDescription = 'Ukoliko smatrate da ovaj oglas krši pravila oglašavanja, molimo vas da nam detaljnije objasnite što nije ispravno u oglasu, kako bi naši administratori mogli lakše uočiti probleme.';
                $entityArray                 = array(
                    'modelName'  => 'Oglas',
                    'title'      => $entity->title,
                    'modelPkVal' => $entity->id
                );
                break;
            case 'Baseapp\Models\Users':
                $redirectToURL               = $entity->get_ads_page();
                $infractionReportTitle       = 'Prijava korisnika radi kršenja pravila oglašavanja';
                $infractionReportDescription = 'Ukoliko smatrate da ovaj korisnik krši pravila oglašavanja, molimo vas da nam detaljnije objasnite što nije ispravno, kako bi naši administratori mogli lakše uočiti probleme.';
                $entityArray                 = array(
                    'modelName'  => 'Korisnik',
                    'title'      => $entity->username,
                    'modelPkVal' => $entity->id
                );
                break;
            case 'Baseapp\Models\UsersShops':
                $redirectToURL               = $entity->getUrl();
                $infractionReportTitle       = 'Prijava trgovine radi kršenja pravila oglašavanja';
                $infractionReportDescription = 'Ukoliko smatrate da ova trgovina krši pravila oglašavanja, molimo vas da nam detaljnije objasnite što nije ispravno, kako bi naši administratori mogli lakše uočiti probleme.';
                $entityArray                 = array(
                    'modelName'  => 'Trgovina',
                    'title'      => $entity->getName(),
                    'modelPkVal' => $entity->id
                );
                break;
        }

        $this->view->setVar('infractionReportTitle', $infractionReportTitle);
        $this->view->setVar('infractionReportDescription', $infractionReportDescription);
        $this->view->setVar('entity', $entityArray);
        $this->assets->addJs('assets/js/infraction-report.js');
        $reportReasons = $entity->getInfractionReportReasons();

        if ($this->request->isPost()) {
            $reported = $entity->infractionReport($this->request);
            if ($reported instanceof \Phalcon\Validation\Message\Group) {
                $this->flashSession->error('<strong>Ooops!</strong> Molimo ispravite uočene greške...');
                $this->view->setVar('errors', $reported);
                $this->infractionReportReasonDropdown($reportReasons, $this->request->getPost('report_reason'));
            } else {
                $this->flashSession->success('<strong>Prijava uspješna!</strong> ' . $flashSessionSuccess);
                return $this->redirect_to($redirectToURL);
            }
        } else {
            $this->infractionReportReasonDropdown($reportReasons);
        }
        $this->view->pick('chunks/infraction-report');
    }

    protected function infractionReportReasonDropdown($reportReasons, $selected = null)
    {
        if ($selected) {
            Tag::setDefault('report_reason', $selected);
        }

        $infractionReportReasonDropdown = Tag::select(array(
            'report_reason',
            $reportReasons,
            'useEmpty'   => true,
            'emptyText'  => '-- Odaberite razlog prijave --',
            'emptyValue' => '',
            'class'      => 'form-control'
        ));
        $this->view->setVar('report_reason_dropdown', $infractionReportReasonDropdown);
    }

}
