<?php

namespace Baseapp\Traits;

use Baseapp\Extension\Cache\Backend\MyMemcache;
use Baseapp\Extension\Crypt;
use Baseapp\Library\Assets\VersionedPathAssetsManager;
use Baseapp\Library\Debug;
use Baseapp\Library\MessagesCollector;
use Baseapp\Library\Queue\DummyServer;
use Baseapp\Library\ShutdownManager;
use Baseapp\Library\Uploader\Uploader;
use Baseapp\Library\Uploader\ValidatorCroatianMessages;
use Baseapp\Library\Utils as BaseappUtils;
use Baseapp\Library\Auth;
use Baseapp\Library\ViewCountsManager;
use Phalcon\Events\Event;

trait CommonServiceLoaders
{

    private $_di;
    private $_config;

    /**
     * Set the config service
     *
     * @return void
     */
    protected function config()
    {
        try {
            // Phalcon 3.0.1 contains our patch that allows specifying INI_SCANNER_NORMAL
            $config = new \Phalcon\Config\Adapter\Ini(ROOT_PATH . '/config/config.ini', INI_SCANNER_NORMAL);
        } catch (\BadMethodCallException $e) {
            // Earlier versions should end up here, which is broken in 3.0, but works fine in 2.0.x
            $config = new \Phalcon\Config\Adapter\Ini(ROOT_PATH . '/config/config.ini');
        }

        /**
         * Allows $_SERVER['HTTP_HOST'] to override the 'domain' and 'static_uri'
         * config settings (but only in a non-production env), so that we can run
         * and test from multiple addresses more easily.
         */
        if ($config->app->env !== 'production') {
            if ($config->app->override_base_uri_from_server_name) {
                if (isset($_SERVER['SERVER_NAME'])) {
                    $config->app->base_uri = BaseappUtils::detect_current_scheme() . '://' . BaseappUtils::detect_current_hostname() . '/';
                }
            }
            // Allows trusting client's Host header for easier ngrok integration etc.,
            // but only in non-prod environments, and only if specified in config
            if (
                isset($config->app->override_base_uri_from_host_header) &&
                $config->app->override_base_uri_from_host_header
            ) {
                $request_host = null;
                if (isset($_SERVER['HTTP_X_ORIGINAL_HOST'])) {
                    $request_host = $_SERVER['HTTP_X_ORIGINAL_HOST'];
                }
                if (!$request_host && isset($_SERVER['HTTP_HOST'])) {
                    $request_host = $_SERVER['HTTP_HOST'];
                }
                if ($request_host) {
                    $config->app->base_uri = BaseappUtils::detect_current_scheme() . '://' . $request_host . '/';
                }
            }
        }

        $this->_di->setShared('config', $config);
        $this->_config = $config;
    }

    /**
     * Register autoloaders with our custom namespaces
     *
     * @return void
     */
    protected function loader()
    {
        $loader = new \Phalcon\Loader();
        $loader->registerNamespaces(array(
            'Baseapp\Models' => APP_DIR . '/common/models/',
            'Baseapp\Library' => APP_DIR . '/common/library/',
            'Baseapp\Extension' => APP_DIR . '/common/extension/',
            'Baseapp\Controllers' => APP_DIR . '/common/controllers/',
            'Baseapp\Traits' => APP_DIR . '/common/traits/',
        ))->register();
    }

    /**
     * Set the database service
     *
     * @return void
     */
    protected function db()
    {
        $config = $this->_config;
        $this->_di->set('db', function() use ($config) {
            return new \Phalcon\Db\Adapter\Pdo\Mysql(array(
                'host'         => $config->database->host,
                'username'     => $config->database->username,
                'password'     => $config->database->password,
                'dbname'       => $config->database->dbname,
                'dialectClass' => '\Baseapp\Extension\Db\Dialect\MysqlExtended',
                'options'      => array(
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                    \PDO::ATTR_EMULATE_PREPARES   => false,
                    \PDO::ATTR_STRINGIFY_FETCHES  => false,
                )
            ));
        });
    }

    /**
     * If config specifies a certain Metadata adapter, use it, or
     * fallback to the default Memory() adapter
     * @link http://docs.phalconphp.com/en/latest/reference/models.html#caching-meta-data
     * @link http://docs.phalconphp.com/en/latest/api/Phalcon_Mvc_Model_MetaData.html
     */
    public function models_metadata()
    {
        $config = $this->_config;
        $this->_di->set(
            'modelsMetadata',
            function() use ($config) {
                if (isset($config->models->metadata)){
                    $metadata_adapter = $config->models->metadata;
                    $metadata_options = null;
                    if (isset($config->models->options)) {
                        $metadata_options = $config->models->options->toArray();
                    }
                    $adapter = '\Phalcon\Mvc\Model\Metadata\\' . $metadata_adapter;
                    return new $adapter($metadata_options);
                }

                // Fallback to default Memory adapter in case anything is missing
                return new \Phalcon\Mvc\Model\Metadata\Memory();
            }
        );
    }

    public function dispatcher()
    {
        $this->_di->set(
            'dispatcher',
            function() {
                if ('cli' === PHP_SAPI) {
                    $dispatcher = new \Phalcon\CLI\Dispatcher();
                } else {
                    $dispatcher = new \Phalcon\Mvc\Dispatcher();
                }
                return $dispatcher;
            },
            true
        );
    }

    /**
     * Setup the event manager and hook into whatever events we need in a single place
     *
     * @link https://gist.github.com/xboston/8116868
     * @return void
     */
    public function events_manager()
    {
        $config = $this->_config;

        $manager = new \Phalcon\Events\Manager();

        // Add extra response headers
        $manager->attach(
            'application:beforeSendResponse',
            function ($event, $app, $response) {
                $this->add_extra_headers($response);
            }
        );

        // attach database logging only if [database] debug is true in config
        if ($config->database->debug) {
            // models events logging
            $logger = new \Phalcon\Logger\Adapter\File(ROOT_PATH . '/logs/db-models-events.log');
            $manager->attach(
                'model',
                function($event, $model) use ($logger) {
                    /* @var $event Event */
                    $logger->log($event->getType() . ' in ' . get_class($model), \Phalcon\Logger::INFO);
                    $debug_details = false;
                    if ($debug_details) {
                        $source = $event->getSource();
                        $data = var_export($source->toArray(), true);
                        $logger->log("Event Trace:\n" . Debug::trace(), \Phalcon\Logger::DEBUG);
                        $logger->log("Event Source (" . get_class($source) . "):\n" . $data, \Phalcon\Logger::DEBUG);
                    }
                }
            );
            $this->_di->get('modelsManager')->setEventsManager($manager);

            // hook up sql query logging/profiling
            $profiler_logger = new \Phalcon\Logger\Adapter\File(ROOT_PATH . '/logs/db-profiling.log');
            $query_logger = new \Baseapp\Library\Profiler\QueryLogger($profiler_logger);
            $manager->attach('db', $query_logger);
            $this->_di->getShared('db')->setEventsManager($manager);
        }

        /**
         * FIXME: figure out why both of these seem to be required to get an identical eventsManager
         * across multiple modules?!
         */
        /*$this->_di->set(
            'eventsManager',
            function () use ($manager) {
                return $manager;
            },
            true
        );*/
        $this->setEventsManager($manager);

        // Attach our listener/error-handler to dispatch events
        $manager->attach(
            'dispatch',
            new \Baseapp\Library\ErrorHandler($this->_di)
        );
        $this->_di->get('dispatcher')->setEventsManager($manager);
    }

    /**
     * Set the security service
     *
     * @return void
     */
    protected function security()
    {
        $config = $this->_config;
        $this->_di->set('security', function() use ($config) {
            $security = new \Phalcon\Security();
            $security->setDefaultHash(\Phalcon\Security::CRYPT_BLOWFISH_Y);
            $security->setWorkFactor(12);
            return $security;
        });
    }

    /**
     * Set the crypt service
     *
     * @return void
     */
    protected function crypt()
    {
        $config = $this->_config;
        $this->_di->set(
            'crypt',
            function() use ($config) {
                $crypt = new Crypt();
                $crypt->setPadding(\Phalcon\Crypt::PADDING_ZERO);
                $crypt->setKey($config->crypt->salt);
                return $crypt;
            }
        );
    }

    /**
     * Set the filter service
     *
     * @return void
     */
    protected function filter()
    {
        $this->_di->set(
            'filter',
            function() {
                $filter = new \Phalcon\Filter();
                $filter->add('repeat', new \Baseapp\Extension\Repeat());
                // $filter->add('escape', new \Baseapp\Extension\Escape());
                return $filter;
            }
        );
    }

    /**
     * Set the flash service to use flashSession
     *
     * (there's a 'flash' service in Phalcon by default, but that
     * one works only for forwarded dispatch, it has no storage
     * like flashSession which persists across redirects)
     *
     * @return void
     */
    protected function flash()
    {
        $this->_di->set(
            'flashSession',
            function() {
                $flash = new \Phalcon\Flash\Session(array(
                    'warning' => 'status status-warning alert alert-warning',
                    'notice' => 'status status-info alert alert-info',
                    'success' => 'status status-success alert alert-success',
                    'error' => 'status status-error alert alert-danger',
                    'dismissible' => 'status status-dismissible',
                ));
                return $flash;
            }
        );
    }

    /**
     * Set the session service
     *
     * @return void
     */
    protected function session()
    {
        $config = $this->_config;
        $this->_di->set(
            'session',
            function() use ($config) {
                // FIXME: Backwards compat -- to be removed when not needed any more and everyone switches to new config.ini
                if (!isset($config->session->options['name'])) {
                    if (isset($config->session->name)) {
                        $config->session->options['name'] = $config->session->name;
                    }
                }
                $session = new \Baseapp\Extension\Session\Adapter\LazyExpirableFiles($config->session->options->toArray());
                $session->start_on_demand();
                return $session;
            }
        );
    }

    /**
     * Set the cookies service
     *
     * @return void
     */
    protected function cookies()
    {
        $this->_di->set(
            'cookies',
            function() {
                $cookies = new \Phalcon\Http\Response\Cookies();
                return $cookies;
            }
        );
    }

    /**
     * Set the assets service
     *
     * @return void
     */
    protected function assets()
    {
        $config = $this->_config;
        $this->_di->setShared(
            'assets',
            function() use ($config) {
                $options = array(
                    'version' => sha1($config->versions->app)
                );
                return new VersionedPathAssetsManager($options);
            }
        );
    }

    protected function shutdown_manager()
    {
        $this->_di->set(
            'shutdownManager',
            function() {
                return new ShutdownManager();
            }
        );
    }

    protected function messages_collector()
    {
        $this->_di->set(
            'messagesCollector',
            function() {
                return new MessagesCollector();
            }
        );
    }

    /**
     * Sets the viewCountsManager shared service
     */
    protected function view_counts_manager()
    {
        $this->_di->set(
            'viewCountsManager',
            function() {
                return new ViewCountsManager();
            },
            true // shared service
        );
    }

    /**
     * Initialize the error_reporting / error handling environment
     *
     * TODO: ideally, some of these things should be set outside of php, and we should
     * only be hooking into it, but fuck it for now, it's easier to set everything
     * up for ourselves even though ini_set() is not the most performant function
     */
    public function error_handling()
    {
        $config = $this->_config;

        if ($config->app->debug) {
            ini_set('display_errors', true);
            error_reporting(E_ALL);
        } else {
            ini_set('display_errors', false);
            // error_reporting(-1);
        }

        ini_set('error_log', ROOT_PATH . '/logs/php-errors.log');

        $error_logger = new \Baseapp\Library\ErrorHandler($this->_di);

        // In case a previous custom exception handler is already registered, restore it (don't set ours)
        $prev_exc_handler = set_exception_handler(array($error_logger, 'exception_handler'));
        if (null !== $prev_exc_handler) {
            restore_exception_handler();
        }

        // Do the same for error handler too...
        $prev_err_handler = set_error_handler(array($error_logger, 'error_handler'));
        if (null !== $prev_err_handler) {
            restore_error_handler();
        }

        // Using a shutdown manager we can now control whether the callbacks should really
        // execute or not (because there are scenarios in which when we don't want them to execute, i.e. PHPUnit tests)
        $sm = $this->_di->get('shutdownManager');
        if ($sm) {
            $sm->append(array($error_logger, 'shutdown_handler'));
        }
    }

    /**
     * Restores default error handling (used in tests/index.php bootstrap only for now)
     */
    public function restore_default_error_handling()
    {
        // Restore error and exception handlers...
        // TODO: these could've been set with more depth/nesting, we might need to check and loop to really unset everything
        restore_exception_handler();
        restore_error_handler();
    }

    /**
     * Set the logger service
     */
    protected function logger()
    {
        $config = $this->_config;
        $this->_di->set(
            'logger',
            function() use ($config) {
                if (class_exists($config->logger->adapter)) {
                    $adapter = $config->logger->adapter;
                } else {
                    $adapter = '\Phalcon\Logger\Adapter\\' . $config->logger->adapter;
                }
                /**
                 * @var $logger \Phalcon\Logger\AdapterInterface
                 */
                $logger = new $adapter($config->logger->target);
                if (class_exists($config->logger->formatter)) {
                    $formatter = $config->logger->formatter;
                } else {
                    $formatter = '\Phalcon\Logger\Formatter\\' . $config->logger->formatter;
                }
                $formatter = new $formatter($config->logger->format);
                $logger->setFormatter($formatter);
                return $logger;
            }
        );
    }

    /**
     * Set the cache service(s) defined in config.ini
     *
     * @return void
     */
    protected function cache()
    {
        $config = $this->_config;
        foreach ($config->cache->services as $service => $section) {
            $this->_di->set(
                $service,
                function() use ($config, $section) {
                    // Load settings for some section
                    $frontend = $config->$section;
                    $backend = $config->{$frontend->backend};

                    // Set adapters
                    $adapter_frontend = '\Phalcon\Cache\Frontend\\' . $frontend->adapter;

                    // Check for a fully namespaced custom backend adapter
                    if (false !== strpos($backend->adapter, '\\')) {
                        $adapter_backend = $backend->adapter;
                    } else {
                        $adapter_backend = '\Phalcon\Cache\Backend\\' . $backend->adapter;
                    }

                    // Set cache
                    $front_cache = new $adapter_frontend($frontend->options->toArray());
                    $cache = new $adapter_backend($front_cache, $backend->options->toArray());

                    // If this is an instance of our customized Memcache driver, attempt
                    // to connect when resolving the service for the first time.
                    // If we're unable to connect we'll probably not be able to use the cache
                    // properly during that request anyway, so return null instead of the useless driver instance,
                    // even though, in theory, the daemon could come back during the request at some point...
                    if ($cache instanceof MyMemcache) {
                        if (!$cache->isConnected()) {
                            $cache = null;
                        }
                    }

                    return $cache;
                }
            );
        }
    }

    /**
     * Set the url service
     *
     * @return void
     */
    protected function url()
    {
        $config = $this->_config;
        $this->_di->set(
            'url',
            function() use ($config) {
                $url = new \Phalcon\Mvc\Url();
                $url->setBaseUri($config->app->base_uri);
                $url->setStaticBaseUri($config->app->static_uri);
                return $url;
            }
        );
    }

    /**
     * Sets the \Uploader\Uploader service
     *
     * @return void
     */
    protected function uploader()
    {
        $this->_di->set(
            'uploader',
            function () {
                return new Uploader();
            }
        );
    }

    /**
     * Set the time zone
     *
     * @return void
     */
    protected function timezone()
    {
        date_default_timezone_set($this->_config->app->timezone);
    }

    /**
     * Set the tag service
     */
    protected function tag()
    {
        $this->_di->set(
            'tag',
            function() {
                return new \Baseapp\Extension\Tag();
            }
        );
    }

    /**
     * Setup the 'signer' service (used for CSRF protection)
     */
    protected function signer()
    {
        $this->_di->set(
            'signer',
            function(){
                $secret = $this->_config->crypt->salt;
                $signer = new \Baseapp\Library\SignatureGenerator($secret);
                $auth = $this->getDI()->getShared('auth');
                // if we have a logged_in user, link the generated signature to him specifically
                if ($auth->logged_in()) {
                    $signer->addKeyValue('user_id', $auth->get_user()->id);
                }
                return $signer;
            }
        );
    }

    /**
     * Set the auth service
     *
     * @return void
     */
    protected function auth()
    {
        $this->_di->set(
            'auth',
            function() {
                return Auth::instance();
            }
        );
    }

    /**
     * Configures and sets the hybridauth shared service
     */
    protected function hybridauth()
    {
        $config = $this->_config;

        $hybridauth_cfg = array(
            'base_url'  => $this->url->get('user/oauth'),
            'providers' => array(
                'Facebook' => array(
                    'enabled' => true,
                    'keys'    => array(
                        'id'     => $config->social->fb_app_id,
                        'secret' => $config->social->fb_app_secret,
                    ),
                    'scope'  => 'email',
                ),
                'Google' => array(
                    'enabled'           => true,
                    'keys'              => array(
                        'id'            => $config->social->google_client_id,
                        'secret'        => $config->social->google_secret,
                    ),
                    'scope'             => 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email',
                    'access_type'       => 'offline',
                    'approval_prompt'   => 'auto',
                ),
            ),
        );

        $this->_di->set(
            'hybridauth',
            function() use ($hybridauth_cfg) {
                $hybridauth = new \Hybrid_Auth($hybridauth_cfg);
                return $hybridauth;
            }
        );
    }

    /**
     * Does a very basic HMVC request in the application
     *
     * @param array $location
     * @return mixed
     */
    public function request($location)
    {
        $dispatcher = clone $this->getDI()->get('dispatcher');

        if (isset($location['controller'])) {
            $dispatcher->setControllerName($location['controller']);
        } else {
            $dispatcher->setControllerName('index');
        }

        if (isset($location['action'])) {
            $dispatcher->setActionName($location['action']);
        } else {
            $dispatcher->setActionName('index');
        }

        if (isset($location['params'])) {
            if (is_array($location['params'])) {
                $dispatcher->setParams($location['params']);
            } else {
                $dispatcher->setParams((array) $location['params']);
            }
        } else {
            $dispatcher->setParams(array());
        }

        $dispatcher->dispatch();

        $response = $dispatcher->getReturnedValue();
        if ($response instanceof \Phalcon\Http\ResponseInterface) {
            return $response->getContent();
        }

        return $response;
    }

    //Setup the beanstalk service
    protected function beanstalk()
    {
        $config = $this->_config;
        $this->_di->setShared(
            'beanstalk',
            function() use ($config) {
                // If beanstalk config is missing or it's disabled on purpose
                if (!isset($config->beanstalk) || (isset($config->beanstalk->disabled) && $config->beanstalk->disabled)) {
                    return new DummyServer();
                }

                return new \Phalcon\Queue\Beanstalk(array(
                    $config->beanstalk->host,
                    $config->beanstalk->port,
                ));
            }
        );
    }
}
