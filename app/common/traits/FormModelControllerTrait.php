<?php

namespace Baseapp\Traits;

trait FormModelControllerTrait
{

    /**
     * Populates model fields/attributes with $request's POST data
     *
     * @param \Phalcon\Http\RequestInterface|null $request
     */
    public function populate_data_with_post_values($request = null)
    {
        $protected_fields = array('id');

        if ($request && $request->isPost()) {
            $metaData = $this->getModelsMetaData();
            $fieldTypes = $metaData->getDataTypes($this);

            foreach ($fieldTypes as $field => $fieldType) {
                $has_post_field = $request->hasPost($field);
                $post_field = $has_post_field ? $request->getPost($field) : null;

                if (!in_array($field, $protected_fields)) {
                    if ($has_post_field) {
                        if (is_numeric($post_field)) {
                            $this->$field = (int) $post_field;
                        } elseif (is_string($post_field) && trim($post_field)) {
                            $this->$field = (string) $post_field;
                        } elseif (is_bool($post_field)) {
                            $this->$field = (bool) $post_field;
                        } else {
                            $this->$field = null;
                        }
                    } else {
                        if ('is_' == substr($field, 0, 3)) {
                            if ('is_deletable' == $field) {
                                $this->is_deletable = (int) $post_field;
                            } else {
                                $this->$field = $has_post_field ? 1 : 0;
                            }
                        } elseif ('active' === $field) {
                            $this->$field = $has_post_field ? 1 : 0;
                        } else {
                            // TODO: we should see if this will null magic setters/getters (relations between models) .. hopefully not :)
                        }
                    }
                }
            }
        }
    }

}
