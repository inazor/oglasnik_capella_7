<?php

namespace Baseapp\Traits;

use Baseapp\Models\CategoriesSettings;
use Baseapp\Models\Users;

trait AgeRestrictionHelpers
{
    /**
     * @param array $restricted_ids
     * @param callable $callback
     *
     * @return array
     */
    protected function filterRestrictedCategoryIds(array $restricted_ids, callable $callback)
    {
        return array_filter($restricted_ids, $callback);
    }

    /**
     * @param array $restricted
     *
     * @return array
     */
    protected function getNonConfirmedCategoryIdsFrom($restricted = array())
    {
        $restricted_ids = array_keys($restricted);

        return $this->filterRestrictedCategoryIds($restricted_ids, function($category_id){
            $confirmed = $this->session->get('category_' . $category_id . '_age_confirm', false);
            return !$confirmed;
        });
    }

    /**
     * @param array $restricted
     *
     * @return array
     */
    protected function getConfirmedCategoryIdsFrom($restricted = array())
    {
        $restricted_ids = array_keys($restricted);

        return $this->filterRestrictedCategoryIds($restricted_ids, function($category_id){
            $confirmed = $this->session->get('category_' . $category_id . '_age_confirm', false);
            return $confirmed;
        });
    }

    /**
     * @param array $restricted
     * @param $age
     *
     * @return array
     */
    protected function getCategoryIdsUserIsOldEnoughFor($restricted = array(), $age = 0)
    {
        if (!is_array($restricted)) {
            $restricted = (array) $restricted;
        }

        $old_enough_for = array();

        foreach ($restricted as $category_id => $min_age) {
            if ($age && $age >= $min_age) {
                $old_enough_for[] = $category_id;
            }
        }

        return $old_enough_for;
    }

    /**
     * @param Users|null $user
     *
     * @return array
     */
    protected function getSearchAgeRestrictionData(Users $user = null)
    {
        // Check if we're supposed to exclude age restricted categories (and which)
        $exclude_restricted      = false;
        $restricted_categories   = CategoriesSettings::getAgeRestrictedCategoryIdsList();
        $restricted_category_ids = array();
        if (!empty($restricted_categories)) {
            $exclude_restricted = true;
            $restricted_category_ids = array_keys($restricted_categories);
            // First check and remove categories user has confirmed age for in this session already
            $confirmed_category_ids = $this->getConfirmedCategoryIdsFrom($restricted_categories);
            $restricted_category_ids = array_diff($restricted_category_ids, $confirmed_category_ids);
            // Now reduce the restricted list even further if we have a logged in user that's "old enough"
            if (!empty($restricted_category_ids)) {
                if ($user) {
                    // Checking `$restricted_categories` because it contains age data!
                    $category_ids_old_enough = $this->getCategoryIdsUserIsOldEnoughFor($restricted_categories, $user->getAge());
                    $restricted_category_ids = array_diff($restricted_category_ids, $category_ids_old_enough);
                }
            }
            // Reset the exclusion flag if there's nothing to exclude after all the checks above
            if (empty($restricted_category_ids)) {
                $exclude_restricted = false;
            }
        }

        return array(
            'exclude'      => $exclude_restricted,
            'category_ids' => $restricted_category_ids
        );
    }
}
