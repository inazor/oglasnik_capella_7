<?php

namespace Baseapp\Cli;
use Phalcon\DiInterface;

/**
 * Cli Module
 */
class Module implements \Phalcon\Mvc\ModuleDefinitionInterface
{
    /**
     * Register specific autoloader(s) for the module
     *
     * @param DiInterface|null $di Dependency Injector
     *
     * @return void
     */
    public function registerAutoloaders(DiInterface $di = null)
    {
        $loader = new \Phalcon\Loader();

        $loader->registerNamespaces(array(
            'Baseapp\Cli\Tasks' => __DIR__ . '/tasks/',
			'Baseapp\Suva\Controllers' => APP_DIR. '/suva/controllers/',
			'Baseapp\Suva\Library' => APP_DIR. '/suva/library/',
			'Baseapp\Suva\Models' => APP_DIR. '/suva/models/',				
        ));

        $loader->register(); 
    }

    /**
     * Register specific services for the module
     *
     * @param DiInterface $di Dependency Injector
     *
     * @return void
     */
    public function registerServices(DiInterface $di)
    {
        // Modifying the default dispatcher for cli purposes slightly
        $di->get('dispatcher')->setDefaultNamespace('Baseapp\Cli\Tasks');

        // Registering the view component
        $di->set('view', function() use($di) {
            $view = new \Phalcon\Mvc\View();
            $view->setViewsDir(ROOT_PATH . '/app/frontend/views/');
            $view->registerEngines(\Baseapp\Library\Tool::registerEngines($view, $di));
            return $view;
        });
    }
}
