<?php

namespace Baseapp\Cli\Tasks;




use Baseapp\Suva\Library\Nava;


/**
 * Cron CLI Task with Actions
 */
class NavaTask extends MainTask{
	
    /**
     * Main Action
     */
    public function mainAction()
    {
        echo "navaTask/mainAction\n";
    }

    public function testAction()
    {
        echo "navaTask/testAction\n";
        print_r($this->router->getParams());
    }


	public function updateEveryMinuteAction(){
		\Baseapp\Suva\Library\libCron::updateEveryMinute();
	}
	public function firstSetupAction(){
		\Baseapp\Suva\Library\libFirstSetup::firstSetup();
	}

	
}
