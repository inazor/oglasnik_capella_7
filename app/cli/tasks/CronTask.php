<?php

namespace Baseapp\Cli\Tasks;

use Baseapp\Models\Ads;
use Baseapp\Models\AdsHomepage;
use Baseapp\Models\AdsOfflineExportHistory;
use Baseapp\Models\Orders;
use Baseapp\Models\UsersEmailAgents;
use Baseapp\Models\UsersEmailAgentsQueue;
use Baseapp\Library\Mapping\MigrationRawDb;
use Baseapp\Library\Mapping\ShopRefresher;
use Baseapp\Models\UsersShops;
use Baseapp\Models\UsersShopsFeatured;

/**
 * Cron CLI Task with Actions
 */
class CronTask extends MainTask
{
    /**
     * Main Action
     */
    public function mainAction()
    {
        echo "cronTask/mainAction\n";
    }

    public function testAction()
    {
        echo "cronTask/testAction\n";
        print_r($this->router->getParams());
    }

    public function markExpiredAdsAction()
    {
        ini_set('memory_limit', '256M');
        $ads = new Ads();
        $affected_rows = $ads->markExpired();
        if ($affected_rows > 0) {
            $this->saveAudit(sprintf('Marked %s ads(s) as expired', $affected_rows));
        }
    }

    public function markExpiringSoonAdsAction()
    {
        $ads = new Ads();
        $results = $ads->populateSoonExpiringNotificationsQueue();
        if ($results) {
            $count = count($results);
            $this->saveAudit(sprintf('Added %s ads(s) to "expires soon" queue', $count));
        }
    }

    public function markExpiredAdsHomepageAction()
    {
        $ads_homepage = new AdsHomepage();
        $affected_rows = $ads_homepage->markExpired();

        if ($affected_rows > 0) {
            $this->saveAudit(sprintf('Marked %s homepage ad(s) as expired', $affected_rows));
        }
    }

    public function markExpiredOrdersAction()
    {
        $orders = new Orders();
        $affected_rows = $orders->markExpired();
        if ($affected_rows > 0) {
            $this->saveAudit(sprintf('Marked %s order(s) as expired', $affected_rows));
        }
    }

    public function flushCachedViewCountsAction()
    {
        $di = $this->getDI();
        $vcm = $di->get('viewCountsManager');

        $vcm->flushCacheToDb();
    }

    public function addEmailAgentsToQueueAction()
    {
        UsersEmailAgents::process();
    }

    public function sendQueuedEmailAgentsAction()
    {
        $queue = new UsersEmailAgentsQueue();
        $queue->process();
    }

    public function importFromAvusAction()
    {
        $result = MigrationRawDb::importFromAvus();
        if (!empty($result)) {
            $this->saveAudit('AVUS import: [' . $result . ']');
        }
    }

    public function refreshShopAdsFromAvusAction()
    {
        $result = ShopRefresher::refreshShopsAds($this->router->getParams());
        if (!empty($result)) {
            $this->saveAudit('Refreshed shop\'s ads: [' . $result . ']');
        }
    }

    public function markExportedOfflineAdProductsAsProcessedAction()
    {
        $affected_rows = (new AdsOfflineExportHistory())->markProcessedRecords();
        if ($affected_rows > 0) {
            $this->saveAudit(sprintf('Marked %s ad offline product(s) as processed and exported to AVUS', $affected_rows));
        }
    }

    public function ensureShoppingWindowsHaveAdsAction()
    {
        $min_ads_per_window = 2;

        $results = UsersShops::queryShoppingWindowsForCategories();
        foreach ($results as $result) {

            /* @var UsersShops $shop */
            $shop = $result->shop;
            /* @var UsersShopsFeatured $window */
            $window = $result->izlog;

            // Check if the shopping window has the required amount of ads
            $currently_active_ad_ids = [];
            $ads = $window->getAds();
            $ads_count_active = count($ads);
            if ($ads_count_active) {
                $currently_active_ad_ids = array_map(function($ad){
                    return $ad['id'];
                }, $ads->toArray());
                if (null === $currently_active_ad_ids) {
                    $currently_active_ad_ids = [];
                }
            }

            // Find new ads if we don't have enough active ads
            if ($ads_count_active < $min_ads_per_window) {
                $missing = ($min_ads_per_window - $ads_count_active);
                if ($missing) {

                    // Grab latest ads with images excluding currently active ones
                    $latest_shop_ads = $shop->getLatestAdsWithImages($missing, $currently_active_ad_ids);
                    $latest_shop_ads_cnt = count($latest_shop_ads);

                    // TODO/FIXME: If/when we find less new ads than what we need, then what?
                    // (i.e., we need 2 ads, but only have one active?)
                    /*
                    if ($latest_shop_ads_cnt < $missing) {
                    }
                    */

                    if ($latest_shop_ads_cnt) {

                        $latest_shop_ad_ids = array_map(function($ad){
                            return $ad['id'];
                        }, $latest_shop_ads->toArray());

                        /**
                         * If we found some new/latest ads now, replace/append those that are no longer active
                         * with newly found ones. Note that there's still the possibility of not having found enough,
                         * but not really sure currently what to do in that case...
                         */

                        if (empty($currently_active_ad_ids)) {
                            // First, the simple case, we have no currently active ads, just overwrite/set the new ones
                            $ad_ids_to_save = $latest_shop_ad_ids;
                        } else {
                            // Keep existing active ads and append the newly found ones
                            $ad_ids_to_save = array_merge($currently_active_ad_ids, $latest_shop_ad_ids);
                        }

                        $ad_ids_to_save = $window->makeAdIdsListSafe($ad_ids_to_save);
                        $ad_ids_to_save_string = implode(',', $ad_ids_to_save);
                        $window->save(array('ads' => $ad_ids_to_save_string));
                        $audit_msg = sprintf(
                            'Updated shopping window ads for shop: %s (ShoppingWindowId: %s) with ads: %s',
                            $shop->ident(),
                            $window->id,
                            $ad_ids_to_save_string
                        );
                        // print "\n" . $audit_msg;
                        $this->saveAudit($audit_msg);

                    } else {
                        // TODO/FIXME:
                        // We're missing ads for an active shopping window, but couldn't find any new ads
                        // to use for this shopping window... What do we do here?
                    }
                }
            }
        }
    }
}
