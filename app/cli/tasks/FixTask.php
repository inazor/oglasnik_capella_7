<?php

namespace Baseapp\Cli\Tasks;

use Baseapp\Library\Utils;
use Baseapp\Library\RawDB;

/**
 * Initialize
 */
class FixTask extends MainTask
{
/**
 * Refresh all imported products with correct products and duration (avus user only!)
 */
    private $bezProductCategoryIds = array();

    public function refreshPaidAdsAction()
    {
        $in_params = $this->router->getParams();
        $options = array(
            'actionName' => 'refreshPaidAds'
        );
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        if (empty($options['last_id'])) {
            echo PHP_EOL;
            echo '========================================================' . PHP_EOL;
            echo '--  STARTING WITH REFRESHMENT OF PAID ADS\' PRODUCTS   --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
        }
        echo '-- Refreshing... ' . PHP_EOL;

        $results = $this->refreshPaidAds($options);
        echo 'Last refreshed ID: ' . $results['last_id'] . PHP_EOL;
        if ($results['last']) {
            echo PHP_EOL;
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- Total           : ' . $results['total'] . PHP_EOL;
            echo '-- Fixed           : ' . $results['ok'] . PHP_EOL;
            echo '-- Not fixed       : ' . $results['nok'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        } else {
            sleep(1);
            $options['counter'] = $results['counter'];
            $options['last_id'] = $results['last_id'];
            $run_argv = $this->get_run_argv($options);
            if ($run_argv) {
                \pcntl_exec($_SERVER['_'], $run_argv);
            }
        }
    }

    private function refreshPaidAds($options = array())
    {
        $last_id = isset($options['last_id']) && !empty($options['last_id']) ? $options['last_id'] : 0;
        $limit   = isset($options['limit']) && !empty($options['limit']) ? $options['limit'] : 1000;
        $count   = isset($options['counter']) && !empty($options['counter']) ? explode('_', $options['counter']) : array(0, 0, 0);
        $counter = array(
            'total' => isset($count[0]) && !empty($count[0]) ? $count[0] : 0,
            'ok'    => isset($count[1]) && !empty($count[1]) ? $count[1] : 0,
            'nok'   => isset($count[2]) && !empty($count[2]) ? $count[2] : 0
        );

        $db_config = array(
            'options' => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $db = new RawDB($db_config);
        $rows = $db->find(
            "SELECT id, category_id, import_id FROM ads WHERE user_id = 3 AND NOT(online_product_id = 'osnovni') AND import_id IS NOT NULL AND import_id > :last_id ORDER BY import_id ASC LIMIT 0, " . $limit,
            array(
                'last_id' => $last_id
            )
        );

        if ($counter['total'] == 0) {
            $counter['total'] = $db->findFirst(
                "SELECT COUNT(*) FROM ads WHERE user_id = 3 AND NOT(online_product_id = 'osnovni') AND import_id IS NOT NULL",
                array(),
                true
            );
        }

        if ($rows && count($rows)) {
            $this->initBezProductCategoryIds($db);

            $ads = array();
            $src_ids = array();

            foreach ($rows as $row) {
                $ads[$row['import_id']] = $row;
                $src_ids[] = $row['import_id'];
            }

            if (count($src_ids)) {
                $src_db_config = array(
                    'host'     => 'sql.oglasnik.hr',
                    'username' => 'npongrac',
                    'password' => 'fief7eej',
                    'dbname'   => 'oglasnik',
                    'options'  => array(
                        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES latin1',
                        \PDO::ATTR_EMULATE_PREPARES   => false,
                        \PDO::ATTR_STRINGIFY_FETCHES  => false,
                    )
                );
                $src_db = new RawDB($src_db_config);

                $src_rows = $src_db->find('SELECT id, inet_type_id, expires, pbl_date FROM ads WHERE expires IS NOT NULL AND pbl_date IS NOT NULL AND id IN (' . implode(', ', $src_ids) . ')');
                if ($src_rows && count($src_rows)) {
                    $user = \Baseapp\Models\Users::findFirst(3);

                    // we'll be saving final data here
                    $final_ads = array();

                    foreach ($src_rows as $src_row) {
                        $category = \Baseapp\Models\Categories::findFirst($ads[$src_row['id']]['category_id']);
                        if ($category) {
                            $ad_product = $this->getAdProducts($src_row, $user, $category);

                            if ($ad_product) {
                                $final_ads[] = array(
                                    'id'                => $ads[$src_row['id']]['id'],
                                    'published_at'      => strtotime($src_row['pbl_date']),
                                    'expires_at'        => strtotime($src_row['expires']),
                                    'product_sort'      => $ad_product['product_sort'],
                                    'online_product_id' => $ad_product['id'],
                                    'online_product'    => $ad_product['product'],
                                    'import_id'         => $src_row['id']
                                );
                            } else {
                                echo '---- No product for ad [' . $src_row['id'] . ']' . PHP_EOL;
                            }
                        } else {
                            echo '---- No category for ad [' . $src_row['id'] . ']' . PHP_EOL;
                        }
                    }

                    if (count($final_ads)) {
                        foreach ($final_ads as $final_ad) {
                            $last_id = $final_ad['import_id'];
                            if ($final_ad['online_product_id'] != 'osnovni') {
                                $updated = $db->update(
                                    "UPDATE ads SET published_at = :published_at, expires_at = :expires_at, product_sort = :product_sort, online_product_id = :online_product_id, online_product = :online_product WHERE id = :id",
                                    array(
                                        'published_at'      => $final_ad['published_at'],
                                        'expires_at'        => $final_ad['expires_at'],
                                        'product_sort'      => $final_ad['product_sort'],
                                        'online_product_id' => $final_ad['online_product_id'],
                                        'online_product'    => $final_ad['online_product'],
                                        'id'                => $final_ad['id']
                                    )
                                );

                                if ($updated) {
                                    $counter['ok']++;
                                    echo '---- fixed: ' . $last_id . PHP_EOL;
                                } else {
                                    $counter['nok']++;
                                    echo '---- error: ' . $last_id . PHP_EOL;
                                }
                            } else {
                                $counter['nok']++;
                                echo '---- better do not touch: ' . $last_id . PHP_EOL;
                            }
                        }
                    }
                }
            }
        }

        return array(
            'total'   => $counter['total'],
            'ok'      => $counter['ok'],
            'nok'     => $counter['nok'],
            'last'    => count($rows) < $limit,
            'last_id' => $last_id,
            'counter' => $counter['total'] . '_' . $counter['ok'] . '_' . $counter['nok']
        );
    }

    private function initBezProductCategoryIds($db)
    {
        $this->bezProductCategoryIds = array();

        $rows = $db->find("SELECT category_id, products_cfg_json FROM categories_settings WHERE products_cfg_json LIKE '%IstaknutiNoExtras%' OR products_cfg_json LIKE '%PremiumNoExtras%'");
        if ($rows && count($rows)) {
            foreach ($rows as $row) {
                if (trim($row['products_cfg_json'])) {
                    $products_json = json_decode($row['products_cfg_json']);

                    if ($products_json) {
                        foreach ($products_json as $group => $products) {
                            if ('online' === $group) {
                                foreach ($products as $product) {
                                    if (stripos($product->fqcn, 'NoExtras') !== false && $product->disabled == false) {
                                        $this->bezProductCategoryIds[] = (int) $row['category_id'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->bezProductCategoryIds = array_unique($this->bezProductCategoryIds);
    }

    private function isCategoryWithBezProducts($category = null)
    {
        // if we don't have any bezProductCategoryIds set, then we don't have bezProducts
        if (empty($this->bezProductCategoryIds)) {
            return false;
        }

        if (isset($category) && isset($category->id) && $category->id) {
            return in_array($category->id, $this->bezProductCategoryIds);
        }

        return false;
    }

    private function getAdProducts($src_ad, $user=null, $category=null)
    {
        $ad_products = null;

        if (isset($src_ad['inet_type_id'])) {
            // time in days
            $ad_timeperiod = (strtotime($src_ad['expires']) - strtotime($src_ad['pbl_date'])) / (3600 * 24);

            if ($ad_timeperiod >= 0 && $ad_timeperiod < 8) {
                $ad_selected_duration = '5 dana';
            } elseif ($ad_timeperiod >= 8 && $ad_timeperiod < 17) {
                $ad_selected_duration = '15 dana';
            } else {
                $ad_selected_duration = '30 dana';
            }

            $bezProduct = $this->isCategoryWithBezProducts($category);

            switch (intval($src_ad['inet_type_id'])) {
                case  6:
                case  7:
                    if ($bezProduct) {
                        $POST = array(
                            'products-online' => 'istaknuti-online-bez',
                            'options' => array(
                                'istaknuti-online-bez' => $ad_selected_duration
                            )
                        );
                        $online_product = new \Baseapp\Library\Products\Online\IstaknutiNoExtras($category, $user, $POST);
                    } else {
                        $POST = array(
                            'products-online' => 'istaknuti-online',
                            'options' => array(
                                'istaknuti-online' => $ad_selected_duration
                            )
                        );
                        $online_product = new \Baseapp\Library\Products\Online\IstaknutiOnline($category, $user, $POST);
                    }
                    break;
                case  8:
                case  9:
                case 10:
                case 11:
                    if ($bezProduct) {
                        $POST = array(
                            'products-online' => 'premium-bez',
                            'options' => array(
                                'premium-bez' => $ad_selected_duration
                            )
                        );
                        $online_product = new \Baseapp\Library\Products\Online\PremiumNoExtras($category, $user, $POST);
                    } else {
                        $POST = array(
                            'products-online' => 'premium',
                            'options' => array(
                                'premium' => $ad_selected_duration
                            )
                        );
                        $online_product = new \Baseapp\Library\Products\Online\Premium($category, $user, $POST);
                    }
                    break;
                case 117:
                    $POST = array(
                        'products-online' => 'pinky',
                        'options' => array(
                            'pinky' => '7 dana'
                        )
                    );
                    $online_product = new \Baseapp\Library\Products\Online\Pinky($category, $user, $POST);
                    break;
                default:
                    $POST = array(
                        'products-online' => 'osnovni',
                        'options' => array(
                            'osnovni' => '90 dana'
                        )
                    );
                    $online_product = new \Baseapp\Library\Products\Online\Osnovni($category, $user, $POST);
            }

            $ad_products = array(
                'id'           => $online_product->getId(),
                'product_sort' => $online_product->getSortIdx(),
                'product'      => serialize($online_product)
            );
        }

        return $ad_products;
    }


/**
 * Fix products expire dates according to their chosen products and published_at dates...
 */
    public function fixProductExpireDatesAction()
    {
        $in_params = $this->router->getParams();
        $options = array(
            'actionName' => 'fixProductExpireDates'
        );
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        if (empty($options['last_id'])) {
            echo PHP_EOL;
            echo '========================================================' . PHP_EOL;
            echo '--            STARTING WITH PRODUCT FIXING            --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
        }
        echo '-- Fixing... ';

        $results = $this->fixProductExpireDates($options);
        echo 'Last fixed ID: ' . $results['last_id'] . PHP_EOL;
        if ($results['last']) {
            echo PHP_EOL;
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- Total           : ' . $results['total'] . PHP_EOL;
            echo '-- Active (before) : ' . $results['active_bofore'] . PHP_EOL;
            echo '-- Active (now)    : ' . $results['active_now'] . PHP_EOL;
            echo '-- Were OK         : ' . $results['were_ok'] . PHP_EOL;
            echo '-- Fixed           : ' . $results['fixed'] . PHP_EOL;
            echo '-- Not fixed       : ' . $results['error'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        } else {
            sleep(1);
            $options['counter'] = $results['counter'];
            $options['last_id'] = $results['last_id'];
            $run_argv = $this->get_run_argv($options);
            if ($run_argv) {
                \pcntl_exec($_SERVER['_'], $run_argv);
            }
        }
    }

    private function fixProductExpireDates($options = array())
    {
        $last_id = isset($options['last_id']) && !empty($options['last_id']) ? $options['last_id'] : 0;
        $limit   = isset($options['limit']) && !empty($options['limit']) ? $options['limit'] : 1000;
        $count   = isset($options['counter']) && !empty($options['counter']) ? explode('_', $options['counter']) : array(0, 0, 0, 0, 0, 0);
        $counter = array(
            'active_bofore' => isset($count[0]) && !empty($count[0]) ? $count[0] : 0,
            'active_now'    => isset($count[1]) && !empty($count[1]) ? $count[1] : 0,
            'total'         => isset($count[2]) && !empty($count[2]) ? $count[2] : 0,
            'fixed'         => isset($count[3]) && !empty($count[3]) ? $count[3] : 0,
            'error'         => isset($count[4]) && !empty($count[4]) ? $count[4] : 0,
            'were_ok'       => isset($count[5]) && !empty($count[5]) ? $count[5] : 0
        );

        $db_config = array(
            'options' => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $db = new RawDB($db_config);

        $rows = $db->find(
            'SELECT id, category_id, product_sort, online_product_id, online_product, published_at, expires_at, active, moderation, import_id, is_spam, manual_deactivation, soft_delete FROM ads WHERE id > :last_id AND (import_id IS NULL OR modified_at > UNIX_TIMESTAMP(:start_timestamp)) ORDER BY id ASC LIMIT 0, ' . $limit,
            array(
                'last_id'         => $last_id,
                'start_timestamp' => '2015-09-06 00:00:00'
            )
        );

        if ($rows && count($rows)) {
            $counter['total'] += count($rows);
            foreach ($rows as $row) {
                $last_id = $row['id'];

                if ($row['active']) {
                    $counter['active_bofore']++;
                }

                $modified_data = $this->getModifiedDataBasedOnProductsExpireDate($row);

                // check if modified data has some different values that the original row
                $update_row = false;

                if ($modified_data['active'] != $row['active']) {
                    $update_row = true;
                }

                if ($modified_data['product_sort'] != $row['product_sort']) {
                    $update_row = true;
                }

                if ($modified_data['expires_at'] != $row['expires_at']) {
                    $update_row = true;
                }

                if ($modified_data['active']) {
                    $counter['active_now']++;
                }

                if ($update_row) {
                    $modified_data['id'] = $row['id'];
                    $updated = $db->update(
                        'UPDATE ads SET active = :active, expires_at = :expires_at, product_sort = :product_sort WHERE id = :id',
                        $modified_data
                    );

                    if ($updated) {
                        $counter['fixed']++;
                    } else {
                        $counter['error']++;
                    }
                } else {
                    $counter['were_ok']++;
                }
            }
        }

        return array(
            'active_bofore' => $counter['active_bofore'],
            'active_now'    => $counter['active_now'],
            'total'         => $counter['total'],
            'fixed'         => $counter['fixed'],
            'error'         => $counter['error'],
            'were_ok'       => $counter['were_ok'],
            'last'          => count($rows) < $limit,
            'last_id'       => $last_id,
            'counter'       => $counter['active_bofore'] . '_' . $counter['active_now'] . '_' . $counter['total'] . '_' . $counter['fixed'] . '_' . $counter['error'] . '_' . $counter['were_ok']
        );
    }

    private function getModifiedDataBasedOnProductsExpireDate($row)
    {
        $now                 = Utils::getRequestTime();
        $active              = intval($row['active']) == 1;
        $published_at        = !empty($row['published_at']) ? $row['published_at'] : $now;
        $expires_at          = $row['expires_at'];
        $product_sort        = intval($row['product_sort']) ? intval($row['product_sort']) : 100;
        $moderation          = $row['moderation'];
        $is_spam             = intval($row['is_spam']) == 1;
        $manual_deactivation = intval($row['manual_deactivation']) == 1;
        $soft_delteted       = intval($row['soft_deltete']) == 1;

        $modified_data = array(
            'product_sort' => $product_sort,
            'active'       => $active,
            'expires_at'   => $expires_at
        );

        if ($row['online_product']) {
            $product = @unserialize($row['online_product']);
            if ($product !== false) {
                // If the product has a duration, set the ad's expires_at date to now + $duration days
                $product_duration_days = $product->getDuration();
                if (null !== $product_duration_days && $product_duration_days > 0) {
                    $day_seconds = 24 * 3600;
                    $modified_data['expires_at'] = ($published_at + ($day_seconds * $product_duration_days));
                    $modified_data['product_sort'] = $product->getSortIdx();

                    // final checking of ad's data. For now, we'll assume that the ad can be active
                    // and later we'll check all possible reasons why ad should not be active
                    $modified_data['active'] = 1;

                    // If it's spam, it cannot be active
                    if ($is_spam) {
                        $modified_data['active'] = 0;
                    }

                    // If it's manually deactivated, it cannot be active
                    if ($manual_deactivation) {
                        $modified_data['active'] = 0;
                    }

                    // If it's soft_delteted, it cannot be active
                    if ($soft_delteted) {
                        $modified_data['active'] = 0;
                    }

                    // If it's declined on moderation with status 'nok', it cannot be active
                    if ($moderation === 'nok') {
                        $modified_data['active'] = 0;
                    } elseif ($moderation === 'waiting') {
                        // check if category is pre-moderation
                        $category_settings = \Baseapp\Models\CategoriesSettings::findFirst(array(
                            'conditions' => 'category_id = :category_id:',
                            'bind' => array(
                                'category_id' => $row['category_id']
                            )
                        ));

                        if ($category_settings) {
                            if ($category_settings->moderation_type === 'pre') {
                                $modified_data['active'] = 0;
                            }
                        }
                    }

                    // If it's expired even with new expire datetime, it cannot be active
                    if ($modified_data['expires_at'] < $now) {
                        $modified_data['active'] = 0;
                    }
                }
            }
        }

        return $modified_data;
    }


/**
 * Move import_id from ads to ads_additional_data table and also, connect to old
 * db (sql.oglasnik.hr) and get all avus_id's for import_id's and write them
 * down also
 */
    public function moveDataToAdsAdditionalDataAction()
    {
        $in_params = $this->router->getParams();
        $options = array(
            'actionName' => 'moveDataToAdsAdditionalData'
        );
        if (!empty($in_params)) {
            foreach ($in_params as $param) {
                if (strpos($param, '=')) {
                    $param_arr = explode('=', $param);
                    $options[(string)trim($param_arr[0])] = (string)trim($param_arr[1]);
                }
            }
        }

        if (empty($options['last_id'])) {
            echo PHP_EOL;
            echo '========================================================' . PHP_EOL;
            echo '--              STARTING WITH MIGRATION               --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
        }

        $results = $this->moveDataToAdsAdditionalData($options);
        if ($results['last']) {
            echo '========================================================' . PHP_EOL;
            echo '--                       STATS                        --' . PHP_EOL;
            echo '--------------------------------------------------------' . PHP_EOL;
            echo '-- OK  : ' . $results['ok'] . PHP_EOL;
            echo '-- NOK : ' . $results['nok'] . PHP_EOL;
            echo '========================================================' . PHP_EOL;
        } else {
            echo '-- Fixed so far: [OK: ' . $results['ok'] . ']' . ($results['nok'] ? '[NOK: ' . $results['nok'] . ']' : '') . ' - LastID: ' . $results['last_id'] . PHP_EOL;
            sleep(1);
            $options['counter'] = $results['counter'];
            $options['last_id'] = $results['last_id'];
            $run_argv = $this->get_run_argv($options);
            if ($run_argv) {
                \pcntl_exec($_SERVER['_'], $run_argv);
            }
        }
    }

    private function moveDataToAdsAdditionalData($options = array())
    {
        $last_id = isset($options['last_id']) && !empty($options['last_id']) ? $options['last_id'] : 0;
        $limit   = isset($options['limit']) && !empty($options['limit']) ? $options['limit'] : 1000;
        $count   = isset($options['counter']) && !empty($options['counter']) ? explode('_', $options['counter']) : array(0, 0);
        $counter = array(
            'ok'  => isset($count[0]) && !empty($count[0]) ? $count[0] : 0,
            'nok' => isset($count[1]) && !empty($count[1]) ? $count[1] : 0
        );

        $db_config = array(
            'options' => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $db = new RawDB($db_config);

        $src_db_config = array(
            'host'     => 'sql.oglasnik.hr',
            'username' => 'npongrac',
            'password' => 'fief7eej',
            'dbname'   => 'oglasnik',
            'options'  => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES latin1',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $src_db = new RawDB($src_db_config);

        $rows = $db->find(
            'SELECT id, import_id FROM ads WHERE import_id > :last_id ORDER BY import_id ASC LIMIT 0, ' . $limit,
            array(
                'last_id' => $last_id
            )
        );

        if ($rows) {
            $values  = array();
            $src_ids = array();
            foreach ($rows as $row) {
                $last_id = $row['import_id'];
                $values[$last_id] = array('id' => $row['id'], 'import_id' => $last_id, 'avus_id' => null);
                $src_ids[] = $last_id;
            }

            $src_rows = $src_db->find('SELECT id, avus_id FROM ads WHERE avus_id IS NOT NULL AND id IN (' . implode(', ', $src_ids) . ')');
            if ($src_rows && count($src_rows)) {
                foreach ($src_rows as $src_row) {
                    $values[$src_row['id']]['avus_id'] = $src_row['avus_id'];
                }
            }

            // format the final_array
            $final_array = array();
            foreach ($values as $value) {
                $final_array[] = '(' . $value['id'] . ', ' . $value['import_id'] . ', ' . ($value['avus_id'] ? $value['avus_id'] : 'NULL') . ')';
            }

            $sql = 'REPLACE INTO ads_additional_data (ad_id, import_id, avus_id) VALUES ' . implode(', ', $final_array);
            if ($db->insert($sql)) {
                $counter['ok'] += count($rows);
            } else {
                $counter['nok'] += count($rows);
            }
        }

        return array(
            'ok'      => $counter['ok'],
            'nok'     => $counter['nok'],
            'last'    => count($rows) < $limit,
            'last_id' => $last_id,
            'counter' => $counter['ok'] . '_' . $counter['nok']
        );
    }

    private function get_run_argv($options = array())
    {
        if (isset($options['actionName'])) {
            $run_argv = array(
                ROOT_PATH . '/private/index.php',
                'fix',
                $options['actionName']
            );

            if (isset($options['limit'])) {
                $run_argv[] = 'limit=' . $options['limit'];
            }
            if (isset($options['last_id'])) {
                $run_argv[] = 'last_id=' . $options['last_id'];
            }
            if (isset($options['counter'])) {
                $run_argv[] = 'counter=' . $options['counter'];
            }

            return $run_argv;
        }

        return null;
    }

    public function fixLongTitlesAction()
    {
        $db_config = array(
            'options' => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $db = new RawDB($db_config);

        $counters = array(
            'total' => 0,
            'fixed' => 0,
            'error' => 0
        );

        $ads = $db->find('SELECT id, title FROM ads WHERE LENGTH(title) > 81');
        if ($ads) {
            $counters['total'] = count($ads);
            echo PHP_EOL . 'Total: ' . count($ads) . PHP_EOL;
            foreach ($ads as $ad) {
                $title = Utils::str_truncate($ad['title'], 77, '...');
                $fixed = $db->update(
                    'UPDATE ads SET title = :title WHERE id = :id',
                    array(
                        'title' => $title,
                        'id'    => $ad['id']
                    )
                );
                if ($fixed) {
                    $counters['fixed']++;
                    echo 'Fixed: ' . $counters['fixed'] . PHP_EOL;
                } else {
                    $counters['error']++;
                    echo 'Error: ' . $counters['fixed'] . PHP_EOL;
                }
            }
        }

        var_dump($counters);
    }

    public function migrateAdReportsToInfractionReportsAction()
    {
        $lastInsertedId = null;
        $db_config = array(
            'options' => array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                \PDO::ATTR_EMULATE_PREPARES   => false,
                \PDO::ATTR_STRINGIFY_FETCHES  => false,
            )
        );
        $db = new RawDB($db_config);

        $ad_reports = $db->find('SELECT * FROM ads_reports');
        if ($ad_reports && count($ad_reports)) {
            $insert_values = array();
            $model_name    = '"Baseapp\\\Models\\\Ads"';
            $resolved_at   = '"' . date('Y-m-d H:i:s') . '"';

            foreach ($ad_reports as $ad_report) {
                $ad_id           = (int) $ad_report['ad_id'];
                $reporter_id     = (int) $ad_report['reporter_id'];
                $report_reason   = '"' . trim($ad_report['report_reason']) . '"';
                $report_message  = trim($ad_report['report_message']) ? '"' . trim($ad_report['report_message']) . '"' : 'NULL';
                $reported_at     = '"' . trim($ad_report['reported_at']) . '"';
                $reported_ip     = trim($ad_report['reported_ip']) ? '"' . trim($ad_report['reported_ip']) . '"' : 'NULL';

                $insert_values[] = "($model_name, $ad_id, $reporter_id, $report_reason, $report_message, $reported_at, $reported_ip, $resolved_at)";
            }

            $sql = "INSERT INTO infraction_reports (model_name, model_pk_val, reporter_id, report_reason, report_message, reported_at, reported_ip, resolved_at) VALUES " . implode(", ", $insert_values);

            $lastInsertedId = $db->insert($sql);
        }

        if ($lastInsertedId) {
            echo 'Migration successful!';
        } else {
            echo 'Nothing migrated...';
        }
        echo PHP_EOL;
    }

}
