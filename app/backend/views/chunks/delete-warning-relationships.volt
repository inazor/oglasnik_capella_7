{% if fk_warning is defined %}
    <p>{{ fk_warning['message'] }}</p>
    <table class="model-relationships table table-bordered table-condensed">
        <caption>Model relationships</caption>
        <thead>
            <tr class="active">
                <th>Referenced Model</th>
                <th>Referenced Fields</th>
                <th>Fields</th>
                <th>FK config</th>
            </tr>
        </thead>
        <tbody>
{% for data in fk_warning['relations'] %}
            <tr>
                <td><samp style="font-weight:bold;">{{ data['ref_model'] }}</samp></td>
                <td><samp>{{ data['ref_fields'] }}</samp></td>
                <td><samp>{{ data['fields'] }}</samp></td>
                <td><samp>{{ data['config'] }}</samp></td>
            </tr>
{% endfor %}
        </tbody>
    </table>
{% endif %}
