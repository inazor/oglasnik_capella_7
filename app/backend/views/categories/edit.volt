{# Admin Category Form View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Categories <small>{{ form_title_long }}</small></h1>
        {{ flashSession.output() }}

        {{ form(NULL, 'id' : 'frm_category', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('next') }}
            {{ hiddenField('_csrftoken') }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Category details</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="control-label" for="parent_id">Parent category</label>
                                {{ parent_id_dropdown }}
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="control-label" for="transaction_type_id">Transaction type</label>
                                {{ transaction_type_id_dropdown }}
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3{% if category.transaction_type_id == 0 %} hidden{% endif %}" id="ad_expire_time_div">
                            {% set field = 'default_ad_expire_time' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Default ad expire time</label>
                                <div class="input-group">
                                    <input class="form-control text-right" name="{{ field }}" id="{{ field }}" value="{{category.Settings.default_ad_expire_time | default(30)}}" />
                                    <span class="input-group-addon">days</span>
                                </div>
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-4">
                            {% set field = 'name' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Name</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{category.name | default('')}}" maxlength="64" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-4">
                            {% set field = 'url' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">URL</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{category.url | default('')}}" maxlength="512" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-4">
                            {% set field = 'fancy_name' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">FancyName</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{ category.json.fancy_name|default(category.name) }}" maxlength="64" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12">
                            {% set field = 'excerpt' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Excerpt</label>
                                <textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="5" cols="40">{{category.excerpt | default('')}}</textarea>
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% else %}
                                <p class="help-block">Category excerpt</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="control-label" for="moderation_type">Moderation type</label>
                                {{ moderation_type_dropdown }}
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="control-label" for="category_icon">Category icon</label>
                                {{ category_icon_dropdown }}
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Paid category</label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="paid_category"{% if category.paid_category %} checked="checked"{% endif %} value="1"> Add paid category mark (*)</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <div class="form-group">
                                <label class="control-label">Search by location</label>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="search_by_location"{% if category.json.search_by_location|default(0) %} checked="checked"{% endif %} value="1"> Enabled</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12">
                            <div class="checkbox">
                                <label><input type="checkbox" name="active"{% if category.active %} checked="checked"{% endif %} value="1"> Active</label>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <label class="control-label" for="category_fallback_image">NoImage icon</label>
                                {{ category_fallback_image_dropdown }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Category restrictions</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="checkbox">
                                <label><input type="checkbox" id="age_restriction" name="age_restriction"{% if category.Settings and category.Settings.age_restriction %} checked="checked"{% endif %} value="1"> Enable age restriction</label>
                            </div>
                        </div>
                        <div id="age_restriction_div"{{ category.Settings and category.Settings.age_restriction ? '' : ' style="display:none"' }}>
                            <div class="col-lg-2 col-md-3">
                                {% set field = 'min_age_allowed' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label" for="{{ field }}">Minimum age allowed</label>
                                    <div class="input-group">
                                        <input class="form-control text-right" name="{{ field }}" id="{{ field }}" value="{{category.Settings.min_age_allowed | default('')}}" maxlength="2" />
                                        <span class="input-group-addon">years</span>
                                    </div>
                                {% if errors is defined and errors.filter(field) %}
                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                                </div>
                            </div>
                            <div class="col-lg-10 col-md-9">
                                {% set field = 'min_age_html' %}
                                <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                    <label class="control-label" for="{{ field }}">HTML content</label>
                                    <textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="5" cols="40">{{category.Settings.min_age_html | default('')}}</textarea>
                                {% if errors is defined and errors.filter(field) %}
                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% else %}
                                    <p class="help-block">HTML content informing user about the age restriction</p>
                                {% endif %}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">SEO details</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            {% set field = 'meta_title' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">&lt;title&gt;</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{category.Settings.meta_title | default('')}}" maxlength="90" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            {% set field = 'meta_description' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">&lt;meta name="description" ... &gt;</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{category.Settings.meta_description | default('')}}" maxlength="160" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            {% set field = 'meta_keywords' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">&lt;meta name="keywords" ... &gt;</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{category.Settings.meta_keywords | default('')}}" maxlength="160" />
                            {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                            {% endif %}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button class="btn btn-primary" type="submit" name="save">Save</button>
                <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
                <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
            </div>
        {{ endForm() }}
    </div>
</div>
