{# Admin CMS category Form View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Categories <small>{{ form_title_long }}</small></h1>
        {{ flashSession.output() }}

        {{ form(NULL, 'id' : 'frm_category', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('next') }}
            {{ hiddenField('_csrftoken') }}
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Category details</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                <label class="control-label" for="parent_id">Parent category</label>
                                {{ parent_id_dropdown }}
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            {% set field = 'type' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Category type</label>
                                {{ type_dropdown }}
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            {% set field = 'template' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Category template</label>
                                {{ template_dropdown }}
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            {% set field = 'name' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Name</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{category.name | default('')}}" maxlength="64" />
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            {% set field = 'url' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">URL</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{category.url | default('')}}" maxlength="512" />
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-12">
                            {% set field = 'excerpt' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">Excerpt</label>
                                <textarea class="form-control" name="{{ field }}" id="{{ field }}" rows="5" cols="40">{{category.excerpt | default('')}}</textarea>
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% else %}
                                <p class="help-block">Category excerpt</p>
                                {% endif %}
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <div class="checkbox">
                                <label><input type="checkbox" name="active"{% if category.active %} checked="checked"{% endif %} value="1"> Active</label>
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3">
                            <div class="checkbox">
                                <label><input type="checkbox" name="zendesk"{% if category.Settings.zendesk|default(0) %} checked="checked"{% endif %} value="1"> Show ZenDesk</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">SEO details</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            {% set field = 'meta_title' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">&lt;title&gt;</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{category.Settings.meta_title | default('')}}" maxlength="90" />
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            {% set field = 'meta_description' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">&lt;meta name="description" ... &gt;</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{category.Settings.meta_description | default('')}}" maxlength="160" />
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            {% set field = 'meta_keywords' %}
                            <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                <label class="control-label" for="{{ field }}">&lt;meta name="keywords" ... &gt;</label>
                                <input class="form-control" name="{{ field }}" id="{{ field }}" value="{{category.Settings.meta_keywords | default('')}}" maxlength="160" />
                                {% if errors is defined and errors.filter(field) %}
                                <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                {% endif %}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button class="btn btn-primary" type="submit" name="save">Save</button>
                <button class="btn btn-primary" type="submit" name="saveexit">Save and exit</button>
                <button class="btn btn-default" type="submit" name="cancel">Cancel</button>
            </div>
        {{ endForm() }}
    </div>
</div>
