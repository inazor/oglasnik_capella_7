{# Admin Category Avus mapping #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                <button class="btn btn-primary" name="btnSaveCategoryMapping" id="btnSaveCategoryMapping" style="display:none;"><span class="fa fa-save"></span> Save changes</button>
            </div>
            <h1 class="page-header">Category Avus mapping</h1>
        </div>
        {{ flashSession.output() }}
        {{ form('admin/export2avus/save/' ~ category.id, 'id': 'frm_category_mapping', 'method': 'post') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('category_mapping') }}

            <div class="panel panel-default">
                {% set category_fieldsets = category.getFieldsets(['order': 'sort_order']) %}
                <div class="panel-heading">
                    <h3 class="panel-title">Mapping of '{{ category_path }}' category</h3>
                    <hr/>
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="map_type" id="map_type_entire" value="entire"{{ map_type == 'entire' ? ' checked="checked"' : '' }}>
                                    Entire category
                                </label>
                            </div>
                            <div id="avus_category_box" class="form-group"{{ map_type == 'parameter' ? ' style="display:none"' : '' }}>
                                <label for="avus_category" class="control-label">Avus category ID</label>
                                <input type="text" class="form-control" name="avus_category" id="avus_category" value="{{ avus_category|default('') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4">
                            <div class="radio">
                                <label>
                                    <input type="radio" name="map_type" id="map_type_parameter" value="parameter"{{ map_type == 'parameter' ? ' checked="checked"' : '' }}>
                                    Based on chosen dictionary parameter values
                                </label>
                            </div>
                            <div id="avus_parameter_box" class="form-group"{{ map_type == 'entire' ? ' style="display:none"' : '' }}>
                                <span class="btn btn-primary" name="btnAddAvusCategory" id="btnAddAvusCategory"><span class="fa fa-plus"></span> Add Avus category</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body" id="category_dictionary_parameters"{{ map_type == 'entire' ? ' style="display:none"' : '' }}>
                    <div class="row">
                        <div class="col-md-12 col-lg-12" id="rules_container">
                            {% if avus_rules is defined %}
                                {% for avus_rule in avus_rules %}
                            <div class="panel panel-default avus-rules" data-rule-index="{{ loop.index }}">
                                <div class="panel-heading">
                                    <div class="form-group" style="margin-bottom:0">
                                        <div class="input-group">
                                            <input type="text" class="form-control avus-category-id" name="nesto" id="nesto" value="{{ avus_rule.avus_id }}" placeholder="Avus category ID"/>
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary add-avus-rule" type="button"><span class="fa fa-plus fa-fw"></span> Add rule</button>
                                                <button class="btn btn-danger del-avus-rules" type="button"><span class="fa fa-trash fa-fw"></span></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body parameter-dict-values">
                                    {% for parameter_rule in avus_rule.rules %}
                                    {% set parameter_is_required = true %}
                                    {% if parameter_rule.required is defined %}
                                        {% set parameter_is_required = parameter_rule.required %}
                                    {% endif %}
                                    <div data-rule-item="{{ loop.index }}" data-parameter-rule='{{ parameter_rule|json_encode|escape_attr }}' class="panel panel-default">
                                        <div class="panel-heading">
                                            <div class="pull-right">
                                                <span class="btn btn-info btn-xs edit-rule"><span class="fa fa-edit"></span> Edit</span>
                                                <span class="btn btn-danger btn-xs del-rule"><span class="fa fa-trash"></span></span>
                                            </div>
                                            <div class="checkbox-inline parameter-required">
                                                <label class="no-margin">
                                                    <input data-required-parameter-id="{{ parameter_rule.id }}" type="checkbox"{{ parameter_is_required ? ' checked="checked"' : '' }}>
                                                    {{ parameter_is_required ? '<span class="text-success">Required</span>' : '<span class="text-muted">Optional</span>' }}
                                                </label>
                                            </div>
                                            <span>
                                                <span>{{ parameter_rule.text }}: </span>
                                                <strong class="text-success">{{ parameter_rule.value }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    {% endfor %}
                                </div>
                            </div>
                                {% endfor %}
                            {% endif %}
                        </div>
                    </div>
                </div>
            </div>
        {{ endForm() }}
    </div>
</div>

<!-- Add new Parameter Modal -->
<div class="modal fade" id="addParameterModal" tabindex="-1" role="dialog" aria-labelledby="Add parameter" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="addParameterModalLabel">Add parameter</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="control-label" for="parameter_id">Choose parameter</label>
                            <select class="form-control show-tick" name="parameter_id" id="parameter_id">
                                {% for parameter in dictionary_parameters %}
                                    {% set max_level = 0 %}
                                    {% set parameter_cfp = parameter.cfp %}
                                    {% if parameter_cfp.levels %}
                                        {% set parameter_cfp_levels = parameter_cfp.levels|json_decode %}
                                        {% set max_level = parameter_cfp_levels|length %}
                                    {% endif %}
                                <option value="{{ parameter.id }}"
                                    data-id="{{ parameter.id }}"
                                    data-icon="fa {{ parameter.class }} fa-fw"
                                    data-type-id="{{ parameter.type_id }}"
                                    {% if parameter.type_id == 'DEPENDABLE_DROPDOWN' %}
                                    data-subtext="<span class='fa fa-chain fa-fw' title='Dependable dropdown'><!-- IE --></span> Dependable dropdown"
                                    data-max-level="{{ max_level }}"
                                    {% elseif parameter.type_id == 'LOCATION' %}
                                    data-subtext="<span class='fa fa-chain fa-fw' title='Location'><!-- IE --></span> Location"
                                    data-max-level="{{ max_level }}"
                                    {% endif %}
                                >{{ parameter.name }}</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                </div>
                <div id="parameterOptionsModalBody">
                    <hr/>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            {% for parameter in dictionary_parameters %}
                            <div class="parameter-value" data-parameter-id="{{ parameter.id }}" style="display:none">
                                {{ parameter_markup['parameter_' ~ parameter.id] }}
                            </div>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" id="btnSaveParameterModal" data-type="add" data-rule-index="" data-rule-item="">Add</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
