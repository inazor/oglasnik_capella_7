{# Editing Latest Ads Exclusion Details #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">Latest Ads - Exclusions</h1>
        {{ flashSession.output() }}
        {{ form(NULL, 'id' : 'frm-tpleditor', 'method' : 'post', 'class' : 'form-horizontal') }}
        {{ hiddenField('_csrftoken') }}
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <label class="control-label" for="categories">Excluded categories</label>
                <div class="icon_dropdown">{{ categories_dropdown }}</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <label for="users" class="control-label">Excluded users</label>
                {{ excluded_users_json_script }}
                {{ textField(['users', 'class': 'form-control']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <br>
                <button class="btn btn-primary" type="submit" name="save">Save</button>
                <!--button class="btn btn-primary" type="submit" name="save-and-rebuild">Save and rebuild</button-->
                <span class="text-help text-muted"><small>(it may take a few seconds...)</small></span>
            </div>
        </div>
        <!--
        <h1 class="page-header">Actual Content</h1>
        <div class="row">
            <div class="col-md-12">
                Not used any more
            </div>
        </div>
        -->
        {{ endForm() }}
    </div>
</div>
