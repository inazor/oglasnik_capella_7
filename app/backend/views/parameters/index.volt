{# Admin Parameters Listing/View #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                {{ linkTo(['admin/parameters/create', '<span class="fa fa-plus fa-fw"></span> Add new', 'class': 'btn btn-info']) }}
            </div>
            <h1 class="page-header">Parameters</h1>
        </div>
        {{ flashSession.output() }}
        <form class="form-inline" role="form">
            {{ hiddenField('type') }}
            {{ hiddenField('mode') }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group search-panel">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_type">Specific / All Types</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#type" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search by parameter type</li>
                                {% for t in available_types %}
                                    <li{% if t['id'] === type %} class="active"{% endif %}><a href="#{{ t['id'] }}"><span class="fa {{ t['class'] }}"></span> {{ t['name'] }}</a></li>
                                {% endfor %}
                                <li class="divider"></li>
                                <li><a href="#ANY">Any type</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-right">
                    <select id="dir" name="dir" class="form-control" title="ASC/DESC">
                        {%- for value, title in dir_options -%}
                            <option {% if value == dir %} selected="selected" {% endif %}value="{{ value }}">{{ title }}</option>
                        {%- endfor -%}
                    </select>
                    <select id="limit" name="limit" class="form-control" title="Records per page">
                        {%- for value in limit_options -%}
                            <option {% if value == limit %} selected="selected" {% endif %}value="{{ value }}">{{ value }}</option>
                        {%- endfor -%}
                    </select>
                </div>
            </div>
        </form>
        <br>
        {% if page.items.valid() %}
        <table class="table table-striped table-hover table-condensed">
            <tr>
                <th>Name</th>
                <th width="240">Type</th>
                <th class="text-right">&nbsp;</th>
            </tr>
            {% for parameter in page.items %}
            <tr>
                <td>
                    {% if parameter.group_type == 'custom' %}
                        {{ linkTo(['admin/parameters/edit/' ~ parameter.id, parameter.name, 'title':'Edit parameter']) }}
                    {% else %}
                        {{ parameter.name }}
                    {% endif %}
                    {% if parameter.dictionary_name %}
                    <small class="text-muted"> <span class="fa fa-long-arrow-right fa-fw"><!--IE--></span> <span class="fa fa-book fa-fw"><!--IE--></span> {{ linkTo(['admin/dictionaries/manage/' ~ parameter.dictionary_id, parameter.dictionary_name, 'title': 'Manage dictionary values']) }}</small>
                    {% endif %}
                </td>
                <td>
                    {% if parameter.type_class %}<span class="fa fa-fw {{ parameter.type_class }}"></span>{% endif %}
                    {{ parameter.type_name | default(parameter.type_id) }}
                </td>
                <td class="text-right">
                {% if parameter.group_type == 'custom' %}
                    {{ linkTo(['admin/parameters/delete/' ~ parameter.id, '<span class="fa fa-trash-o text-danger fa-fw"></span>' ]) }}
                {% else %}&nbsp;{% endif %}
                </td>
            </tr>
            {% endfor %}
        </table>
        {% else %}
        <div class="no-results alert alert-success">
            <p class="text-center">Nothing matched your search query</p>
        </div>
        {% endif %}
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
