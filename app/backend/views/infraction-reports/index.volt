{# Audit Log Viewer #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <h1 class="page-header">{{ page_title }}</h1>
        {{ flashSession.output() }}
        <form class="form-inline" role="form">
            {{ hiddenField('model') }}
            {{ hiddenField('field') }}
            {{ hiddenField('mode') }}
            <div class="row">
                <div class="col-sm-6">
                    <div class="input-group search-panel" style="margin-bottom:1em">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="dropdown-label" id="model_name">Specific model / All models</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#model" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {% for model_value, model_name in model_options %}
                                    <li{% if model_value == model %} class="active"{% endif %}><a href="#{{ model_value }}">{{ model_name }}</a></li>
                                {% endfor %}
                                <li class="divider"></li>
                                <li><a href="#all">All models</a></li>
                            </ul>
                        </div>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default dropdown-toggle no-border-radius" data-toggle="dropdown">
                                <span class="dropdown-label" id="search_field">Specific field / Everything</span> <span class="caret"></span>
                            </button>
                            <ul data-param="#field" class="dropdown-menu" role="menu">
                                <li role="presentation" class="dropdown-header">Narrow down your search</li>
                                {% for f in field_options %}
                                    <li{% if f == field %} class="active"{% endif %}><a href="#{{ f }}">{{ f }}</a></li>
                                {% endfor %}
                                <li class="divider"></li>
                                <li><a href="#all">Everything</a></li>
                            </ul>
                        </div>
                        <input type="text" class="form-control" name="q" value="{{ q|escape_attr }}" placeholder="Search term(s)...">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul data-param="#mode" class="dropdown-menu dropdown-menu-right" role="menu">
                                <li role="presentation" class="dropdown-header">Search mode</li>
                                {% for m, m_desc in mode_options %}
                                    <li{% if m == mode %} class="active"{% endif %}><a href="#{{ m }}">{{ m_desc }}</a></li>
                                {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 text-right">
                    <select id="resolved" name="resolved" class="form-control" title="Solved status">
                        {% for resolved_value, resolved_text in resolved_options %}
                        <option {% if resolved_value == resolved %} selected="selected" {% endif %}value="{{ resolved_value }}">{{ resolved_text }}</option>
                        {% endfor %}
                    </select>
                </div>
            </div>
        </form>
        <table class="table table-striped table-hover master-detail">
            <tr>
                <th>Model <small class="text-muted">Details: <a id="toggle-all" href="javascript:void(0);">toggle all</a> / <a id="show-all" href="javascript:void(0);">show all</a> / <a id="hide-all" href="javascript:void(0);">hide all</a> / <span class="text-muted">(or click a certain row to reveal specifics)</span></small></th>
            </tr>
            {% if page.items %}
            {% for report in page.items %}
                <tr id="log-{{ report.id }}" title="Event Type: {{ row_title|default('') }}" class="master-row" data-pk-val="{{ report.model_pk_val }}">
                    <td>
                        {% set display = report.model_name %}
                        {% if report.model_pk_val %}
                            {% if models_controllers_map[display] is defined %}
                                {% set model_controller = models_controllers_map[display] %}
                                {% set edit_href = 'admin/' ~ model_controller ~ '/edit/' ~ report.model_pk_val ~ '?next=' ~ this.request.getURI() %}
                                {% set displayIcon = models_icons_map[display] ? models_icons_map[display] : '' %}
                                {% set display = linkTo([edit_href, displayIcon ~ report.model_title|default(display)]) ~ ' <small class="text-muted">' ~ display ~ '</small>' %}
                            {% endif %}
                        {% endif %}
                        <span class="badge">{{ report.count }}</span>
                        {{ display }}
                    </td>
                </tr>
                {% if report.details %}
                <tr data-master="log-{{ report.id }}" class="detail-row">
                    <td>
                        <table class="table table-condensed table-striped table-bordered" style="width:auto;">
                            <tr>
                                <th>Reported by</th>
                                <th>Reason</th>
                                <th>Message</th>
                                <th>Reported at</th>
                                <th>Reported IP</th>
                                <th>Resolved at</th>
                            </tr>
                            {% for detail in report.details %}
                                {% set reporter_href = 'admin/users/edit/' ~ detail['reporter_id'] %}
                                {% set reporter = detail['reporter_username'] %}
                            <tr>
                                <td><strong>{{ linkTo([reporter_href, reporter]) }}</strong></td>
                                <td><code>{{ detail['report_reason'] }}</code></td>
                                <td>{{ detail['report_message']|default('') ? '<code>' ~ detail['report_message'] ~ '</code>' : '&nbsp;' }}</td>
                                <td>{{ date('d.m.Y H:i:s', strtotime(detail['reported_at'])) }}</td>
                                <td>{{ detail['reported_ip'] }}</td>
                                <td class="{{ detail['resolved_at'] ? 'success' : 'danger' }}">{{ detail['resolved_at'] ? '<span class="text-success">' ~ date('d.m.Y H:i:s', strtotime(detail['resolved_at'])) ~ '</span>' : '<span class="text-danger">Unresolved</span>' }}</td>
                            </tr>
                            {% endfor %}
                        </table>
                    </td>
                </tr>
                {% endif %}
            {% endfor %}
            {% else %}
                <tr><td colspan="4"><p class="text-center">No matching infraction reports found!</p></td></tr>
            {% endif %}
        </table>
        {% if pagination_links %}{{ pagination_links }}{% endif %}
    </div>
</div>
