{# Ad moderation NOK email #}
<p>Poštovani,</p>
<p>
    Vaš oglas <a href="{{ ad.get_frontend_view_link() }}">{{ ad.title }}</a> <b>nije prošao</b> postupak autorizacije 
    kojime se osigurava kvaliteta oglasa i sigurnost korisnicima tiskanog i internetskog izdanja.
</p>
<p>Moderator koji je pogledao vaš oglas želi vam skrenuti pažnju na sljedeće:</p>
<p>{{ moderation_reason }}</p>
<p>
    Za daljnje upravljanje vašim oglasima, u Vašem kutku:
    <a href="{{ url.get('moj-kutak') }}">{{ url.get('moj-kutak') }}</a> možete
    produžiti trajanje ili ih nakon uspješne prodaje obrisati (deaktivirati).
</p>
