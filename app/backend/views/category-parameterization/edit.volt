{# Admin Category Parameterization Edit #}
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div>
            <div class="pull-right">
                <button class="btn btn-primary" name="btnSaveCategoryParameters" id="btnSaveCategoryParameters" style="display:none;"><i class="fa fa-save"></i> Save changes</button>
            </div>
            <h1 class="page-header">Category parameterization</h1>
        </div>
        {{ flashSession.output() }}
        {{ form('admin/category-parameterization/save/' ~ category.id, 'id': 'frm_category_parameterization', 'method': 'post') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('category_fieldsets_and_parameters') }}
            {{ hiddenField(['current_category_id', 'value': category.id]) }}
            {{ hiddenField(['chosen_categories', 'value': category.id]) }}

            <div class="panel panel-default">
                {% set category_fieldsets = category.getFieldsets(['order': 'sort_order']) %}
                <div class="panel-heading">
                    <div class="btn-group pull-right">
                        <div>
                            <span id="adDescTpl" class="btn btn-default btn-sm"><span class="fa fa-file-text-o fa-fw"></span> Ad Description Tpl</span>
                            <span id="offlineDescTpl" class="btn btn-default btn-sm"><span class="fa fa-newspaper-o fa-fw"></span> Offline Description Tpl</span>
                            <span id="reorderFilters" class="btn btn-default btn-sm"><span class="fa fa-binoculars fa-fw"></span> Reorder filters</span>
                            <span id="addNewParameter" class="btn btn-default btn-sm"><span class="fa fa-plus fa-fw"></span> Add new parameter</span>
                            <span id="addNewFieldset" class="btn btn-info btn-sm"><span class="fa fa-plus fa-fw"></span> Add new fieldset</span>
                        </div>
                    </div>
                    <h3 class="panel-title">Parameterization of '{{ category_path }}' category</h3>
                    <small class="text-muted"><strong>Notice:</strong> Fieldsets and parameters within them can be reordered by drag'n'drop!</small>
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="form-group">
                                <hr/>
                                <small class="text-muted">Choose additional categories to apply same structure</small>
                                {{ choosen_categories_id_dropdown }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-body" id="fieldset_container">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <ul class="sortable ui-sortable" id="fieldset_sorter">
                            {% for category_fieldset in category_fieldsets %}
                                {% set fieldset_settings = '' %}
                                {% if category_fieldset.settings %}
                                    {% set fieldset_settings = category_fieldset.getSettings() %}
                                {% endif %}
                                <li class="fieldset ui-sortable" data-fieldset-id="{{ loop.index }}" data-settings="{{ fieldset_settings | escape_attr }}">
                                    <div class="panel panel-info">
                                        <div class="panel-heading">
                                            <div class="row">
                                                <div class="col-md-12 col-lg-12">
                                                    <div class="panel-heading-handle"><span class="fa fa-arrows fa-fw"></span></div>
                                                    <div>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Fieldset label" value="{{ category_fieldset.name | default('') }}">
                                                            <span class="input-group-btn">
                                                                <button class="btn btn-primary btn-fieldset-settings" type="button">
                                                                    <span class="fa fa-cog fa-fw"></span> Settings
                                                                </button>
                                                                <button class="btn btn-danger btn-fieldset-delete disabled" type="button">
                                                                    <span class="fa fa-trash-o fa-fw"></span>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12 col-lg-12">
                                                    <div class="list-sortable">
                                                        <ul class="sortable ui-sortable">
                                                        {% for fieldset_parameter in category_fieldset.getParameters(['order': 'sort_order']) %}
                                                            {% set parameter_ok = false %}
                                                            {% set parameter_is_disabled = false %}
                                                            {% set parameter_is_required = '' %}
                                                            {% set parameter_is_searchable = '' %}
                                                            {% if fieldset_parameter.levels %}
                                                                {% set parameter_levels = fieldset_parameter.levels|json_decode %}
                                                                {% set parameter_label_text = '' %}
                                                                {% set parameter_label_html = '' %}
                                                                {% for level in parameter_levels %}
                                                                    {% if loop.first %}
                                                                        {% set parameter_label_text = level.label_text %}
                                                                        {% if level.is_hidden %}
                                                                            {% set parameter_is_disabled = true %}
                                                                            {% set parameter_label_html = '<span class="text-danger">' ~ level.label_text ~ '</span>' %}
                                                                        {% else %}
                                                                            {% set parameter_label_html = level.label_text %}
                                                                        {% endif %}
                                                                    {% else %}
                                                                        {% set parameter_label_text = parameter_label_text ~ ' › ' ~ level.label_text %}
                                                                        {% if level.is_hidden %}
                                                                            {% set parameter_is_disabled = true %}
                                                                            {% set parameter_label_html = parameter_label_html ~ ' <span class="fa fa-long-arrow-right fa-fw"><!--IE--></span> <span class="text-danger">' ~ level.label_text ~ '</span>' %}
                                                                        {% else %}
                                                                            {% set parameter_label_html = parameter_label_html ~ ' <span class="fa fa-long-arrow-right fa-fw"><!--IE--></span> ' ~ level.label_text %}
                                                                        {% endif %}
                                                                    {% endif %}
                                                                    {% if level.is_searchable %}
                                                                        {% set parameter_is_searchable = ' <span class="fa fa-search fa-fw text-muted" title="Searchable"><!--IE--></span>' %}
                                                                    {% endif %}
                                                                    {% if level.is_required %}
                                                                        {% set parameter_is_required = ' <span class="fa fa-asterisk fa-fw text-muted" title="Required"><!--IE--></span>' %}
                                                                    {% endif %}
                                                                {% endfor %}
                                                                {% set parameter_ok = true %}
                                                            {% elseif fieldset_parameter.data %}
                                                                {% set parameter_data = fieldset_parameter.data|json_decode %}
                                                                {% set parameter_label_text = parameter_data.label_text %}
                                                                {% set parameter_label_html = parameter_data.label_text %}
                                                                {% set parameter_ok = true %}
                                                                {% if parameter_data.is_hidden is defined and parameter_data.is_hidden %}
                                                                    {% set parameter_is_disabled = true %}
                                                                {% endif %}
                                                                {% if parameter_data.is_searchable is defined and parameter_data.is_searchable %}
                                                                    {% set parameter_is_searchable = ' <span class="fa fa-search fa-fw text-muted" title="Searchable"><!--IE--></span>' %}
                                                                {% endif %}
                                                                {% if parameter_data.is_required is defined and parameter_data.is_required %}
                                                                    {% set parameter_is_required = ' <span class="fa fa-asterisk fa-fw text-muted" title="Required"><!--IE--></span>' %}
                                                                {% endif %}
                                                            {% endif %}

                                                            {% if parameter_ok %}
                                                                {% if fieldset_parameter.price > 0 %}
                                                                    {% set parameter_label_html = parameter_label_html ~ ' <span class="fa fa-money fa-fw text-muted" title="Price"><!--IE--></span>' %}
                                                                {% endif %}
                                                                {% set fieldset_parameter_type = fieldset_parameter.getParameter().getParameterType() %}
                                                            <li class="sortable_li{% if parameter_is_disabled %} disabled{% endif %}"
                                                                id="item_{{ fieldset_parameter.parameter_id }}"
                                                                data-parameter-slug="{{ fieldset_parameter.parameter_slug }}"
                                                                data-parameter-name="{{ parameter_label_text }}"
                                                                data-parameter-id="{{ fieldset_parameter.parameter_id }}"
                                                                data-filters-sort-order="{{ fieldset_parameter.filters_sort_order|default('0') }}"
                                                                data-filters-sort-order-more="{{ fieldset_parameter.filters_sort_order_show_more|default(0) == 1 ? 'true' : 'false' }}"
                                                                data-parameter-price="{{ fieldset_parameter.price|default('0') }}"
                                                                data-parameter-attributes="{{ fieldset_parameter.getJSONattributes()|escape_attr }}">
                                                                <div class="sortable_div">
                                                                    <div class="sortable_row">
                                                                        <div class="drag-me">
                                                                            <span class="fa fa-arrows fa-fw"><!-- IE --></span>
                                                                        </div>
                                                                        <div class="name-cat">
                                                                            <span class="name">{{ parameter_label_html ~ parameter_is_required ~ parameter_is_searchable }}</span>
                                                                        </div>
                                                                        <div class="actions-cat labels">
                                                                            <span class="btn btn-info btn-xs" data-action="options">
                                                                                <span class="fa fa-cog fa-fw"><!-- IE --></span> Options
                                                                            </span>
                                                                            ·
                                                                            <span class="btn btn-danger btn-xs" data-action="delete">
                                                                                <span class="fa fa-trash-o fa-fw"><!-- IE --></span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            {% endif %}
                                                        {% endfor %}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            {% endfor %}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        {{ endForm() }}
    </div>
</div>

<!-- adDescTpl Modal -->
<div class="modal fade" id="adDescTplModal" tabindex="-1" role="dialog" aria-labelledby="adDescTpl" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Ad Description Template</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="form-group">
                            <textarea class="form-control" rows="4" cols="40"placeholder='Type @ to list available parameters'>
                            {{- category.getSettings().ad_desc_tpl -}}
                            </textarea>
                            <p class="help-block">
                                Format the description as it should appear on the ads listing (enter creates a new row). <br>
                                HTML code can be used to style additional links. In order to apply CSS tweaks to certain
                                part of the placeholder code, one should wrap it with <code>&#123;&#123;code to style&#125;&#125;</code>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="adDescTplModalSaveBtn">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- offlineDescTpl Modal -->
<div class="modal fade" id="offlineDescTplModal" tabindex="-1" role="dialog" aria-labelledby="offlineDescTpl" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Offline Description Template</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="form-group">
                            <textarea class="form-control" rows="4" cols="40"placeholder='Type @ to list available parameters'>
                            {{- category.getSettings().offline_desc_tpl -}}
                            </textarea>
                            <p class="help-block">Format the description as it should be prepared for offline (enter creates a new row)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="offlineDescTplModalSaveBtn">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Reorder filters Modal -->
<div class="modal fade" id="reorderFiltersModal" tabindex="-1" role="dialog" aria-labelledby="Reorder filters" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Reorder filters</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="list-sortable">
                            <ul class="sortable ui-sortable" id="filter_sorter">
                                <li class="sortable_li" id="filter_" data-parameter-id="">
                                    <div class="sortable_div">
                                        <div class="sortable_row">
                                            <div class="drag-me">
                                                <span class="fa fa-arrows fa-fw"><!-- IE --></span>
                                            </div>
                                            <div class="name-cat">
                                                <span class="name"></span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="reorderFiltersModalSaveBtn">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Fieldset settings Modal -->
<div class="modal fade" id="fieldsetSettingsModal" data-fieldset-id="" tabindex="-1" role="dialog" aria-labelledby="Fieldset settings" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Fieldset settings</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="radio">
                            <label><input type="radio" name="fieldset_settings_style" id="fieldset_settings_style_group" value="group"> Group parameters</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="radio">
                            <label><input type="radio" name="fieldset_settings_style" id="fieldset_settings_style_merge" value="merge"> Merge parameters</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="fieldsetSettingsModalSaveBtn" data-fieldset-id="">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Delete Fieldset Modal -->
<div class="modal fade" id="deleteFieldsetModal" tabindex="-1" role="dialog" aria-labelledby="Delete fieldset" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="deleteFieldsetModalLabel">Delete <span class="text-info"></span> fieldset?</h4>
            </div>
            <div class="modal-body" id="deleteFieldsetModalBody"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteFieldsetModalYesBtn" data-fieldset-id="">Yes, delete!</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Add new Parameter Modal -->
<div class="modal fade" id="addNewParameterModal" tabindex="-1" role="dialog" aria-labelledby="Add new parameter" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="addParameterModalLabel">Add new parameter</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <label class="control-label" for="parameter_id">Parameter</label>
                            <select class="form-control show-tick" name="parameter_id" id="parameter_id">
                                <optgroup label="Standard parameters" data-group="standard">
{%                              for parameter in standard_parameters %}
                                    <option value="{{ parameter.id }}"
                                        data-icon="fa {{ parameter.class }} fa-fw"
                                        data-type-id="{{ parameter.type_id }}"
                                        data-slug="{{ parameter.slug|default('parameter_slug') }}"
                                        data-can-default="{{ parameter.can_default | default(0) }}"
                                        data-can-default-type="{{ parameter.can_default_type | default('text') }}"
                                        data-can-prefix-suffix="{{ parameter.can_prefix_suffix | default(0) }}"
                                        data-is-required="{{ parameter.is_required | default(0) }}"
                                        data-is-searchable="{{ parameter.is_searchable | default(0) }}"
                                        data-is-searchable-type="{{ parameter.is_searchable_type | default('') }}"
{%                                      if parameter.is_dependable %}
                                        data-subtext="<span class='fa fa-chain fa-fw' title='Dependable parameter'><!-- IE --></span>"
{%                                      endif %}
                                        data-is-dependable="{{ parameter.is_dependable | default(0) }}"
{%                                      if parameter.type_id == 'LOCATION' %}
                                        data-max-level="4"
                                        data-geo-location="1"
{%                                      else %}
                                        data-max-level="0"
                                        data-geo-location="0"
{%                                      endif %}
                                    >{{ parameter.name }}</option>
{%                              endfor %}
                                </optgroup>
                                <optgroup label="Custom parameters" data-group="custom">
{%                              for parameter in custom_parameters %}
                                    <option value="{{ parameter.id }}"
                                        data-icon="fa {{ parameter.class }} fa-fw"
                                        data-type-id="{{ parameter.type_id }}"
                                        data-slug="{{ parameter.slug|default('parameter_slug') }}"
                                        data-accept-dictionary="{{ parameter.accept_dictionary | default(0) }}"
                                        data-dictionary-id="{{ parameter.dictionary_id | default('') }}"
{%                                  if parameter.accept_dictionary %}
                                        data-subtext="{% if parameter.is_dependable %}<span class='fa fa-chain fa-fw' title='Dependable parameter'><!-- IE --></span> {% endif %}<span class='fa fa-book fa-fw' title='Dictionary parameter'><!-- IE --></span>"
{%                                  endif %}
                                        data-can-default="{{ parameter.can_default | default(0) }}"
                                        data-can-default-type="{{ parameter.can_default_type | default('text') }}"
                                        data-can-prefix-suffix="{{ parameter.can_prefix_suffix | default(0) }}"
                                        data-is-dependable="{{ parameter.is_dependable | default(0) }}"
                                        data-is-searchable-type="{{ parameter.is_searchable_type | default('') }}"
                                        data-max-level="{% if parameter.is_dependable and parameter.max_level and parameter.max_level > 2 %}{{ (parameter.max_level - 1) }}{% else %}0{% endif %}"
                                    >{{ parameter.name }}</option>
{%                              endfor %}
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <label class="control-label" for="fieldset_id">Fieldset</label>
                            <select class="form-control show-tick" name="fieldset_id" id="fieldset_id"></select>
                        </div>
                    </div>
                </div>
                <div id="parameterOptionsModalBody">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <h5 class="text-info">Parameter options</h5>
                        </div>
                    </div>
                    <div id="classicParameter" class="parameter-options-panel hidden">
                        <div class="parameter_attributes">
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label" for="attribute_label_text">Label text</label>
                                        <input class="form-control" id="attribute_label_text" value="" maxlength="64" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4" id="attribute_placeholder_text_container">
                                    <div class="form-group">
                                        <label class="control-label" for="attribute_placeholder_text">Placeholder text</label>
                                        <input class="form-control" id="attribute_placeholder_text" value="" maxlength="64" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group default-value">
                                        <label class="control-label">Default value</label>
                                        <input class="form-control" name="default_value_text" id="default_value_text" value="" readonly />
                                        <select class="form-control show-tick" name="default_value_select" id="default_value_select"></select>
                                        <input class="form-control" name="default_value_date" id="default_value_date" value="" readonly />
                                        <div class="clearfix"><!--IE--></div>
                                        <input type="checkbox" class="hidden" id="default_value_checkbox">
                                    </div>
                                    <div class="form-group max-length hidden">
                                        <label class="control-label">Max length</label>
                                        <input class="form-control" name="default_max_length" id="default_max_length" value="" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4 help_text_div">
                                    <div class="form-group">
                                        <label class="control-label" for="attribute_help_text">Help text</label>
                                        <input class="form-control" id="attribute_help_text" value="" maxlength="256" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 prefix_div">
                                    <div class="form-group">
                                        <label class="control-label" for="attribute_prefix">Prefix</label>
                                        <input class="form-control" id="attribute_prefix" value="" maxlength="32" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 suffix_div">
                                    <div class="form-group">
                                        <label class="control-label" for="attribute_suffix">Suffix</label>
                                        <input class="form-control" id="attribute_suffix" value="" maxlength="32" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 number_decimals_div hidden">
                                    <div class="form-group">
                                        <label class="control-label" for="attribute_number_decimals">Decimal places</label>
                                        <input class="form-control text-right" id="attribute_number_decimals" value="0" maxlength="3" />
                                    </div>
                                </div>
                                <div class="col-md-6 number_min_value_div hidden">
                                    <div class="form-group">
                                        <label class="control-label" for="attribute_number_min_value">Minimal value</label>
                                        <input class="form-control text-right" id="attribute_number_min_value" value="0" />
                                    </div>
                                </div>
                                <div class="col-md-6 number_max_value_div hidden">
                                    <div class="form-group">
                                        <label class="control-label" for="attribute_number_max_value">Maximal value</label>
                                        <input class="form-control text-right" id="attribute_number_max_value" value="0" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 currency_div hidden">
                                    <div class="form-group">
                                        <label class="control-label" for="attribute_suffix">Currency</label>
                                        <select class="form-control show-tick" name="attribute_currency" id="attribute_currency">
{%                                          for currency in currencies %}
                                            <option value="{{ currency['id'] }}">{{ currency['short_name'] }}</option>
{%                                          endfor %}
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="attribute_is_searchable" class="is_searchable_toggle" data-container="is_searchable_container" data-protected="0"> Is searchable</label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="attribute_is_required" data-protected="0"> Is required</label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="attribute_is_hidden" data-protected="0"> Is hidden</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row hidden" id="is_searchable_container">
                                <div class="is_searchable_options">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" for="attribute_is_searchable_label_text">Search field label Text</label>
                                            <input class="form-control" id="attribute_is_searchable_label_text" value="" maxlength="32" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" for="attribute_is_searchable_type">Search field type</label>
                                            <select class="form-control show-tick filter-select-type" id="attribute_is_searchable_type">
                                            {% for filter_type in filter_types %}
                                                <option value="{{ filter_type['id'] }}">{{ filter_type['name'] }}</option>
                                            {% endfor %}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="dependableParameter" data-dictionary-id="" data-max-level="" class="parameter-options-panel hidden">
                        <ul class="nav nav-tabs deletable dependableParameter" role="tablist">
                            <li class="active"><a href="#dp_level_1">Level #1</a></li>
                            <li><a href="#dp_level_2">Level #2</a></li>
                            <li><a href="#" class="add-level" id="add_level_btn"><span class="fa fa-plus"><!--IE--></span> Add level</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="dp_level_1" data-level="1">
                                <div class="row">
                                    <div class="col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label" for="dp_level_1_attribute_label_text">Label text</label>
                                            <input class="form-control" id="dp_level_1_attribute_label_text" maxlength="64" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-lg-8 dict-values">
                                        <div class="form-group">
                                            <label class="control-label text-muted">Dictionary values</label>
                                            <p class="form-control-static text-muted level_dictionary_values" id="dp_level_1_dictionary_values">- - -</p>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4 default-value hidden">
                                        <div class="form-group">
                                            <label for="location_default_value_select" class="control-label">Default value</label>
                                            <select class="form-control show-tick" name="location_default_value_select" id="location_default_value_select">
                                                <option value=""></option>
{%                                              for country in locations_countries %}
                                                <option value="{{ country.id }}">{{ country.name }}</option>
{%                                              endfor %}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="dp_level_1_attribute_placeholder_text">Placeholder text</label>
                                            <input class="form-control" id="dp_level_1_attribute_placeholder_text" value="" maxlength="64" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="dp_level_1_attribute_help_text">Help text</label>
                                            <input class="form-control" id="dp_level_1_attribute_help_text" value="" maxlength="256" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="dp_level_1_attribute_is_searchable" class="is_searchable_toggle" data-container="dp_level_1_is_searchable_container"> Is searchable</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="dp_level_1_attribute_is_required"> Is required</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="dp_level_1_attribute_is_hidden"> Is hidden</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden" id="dp_level_1_is_searchable_container">
                                    <div class="is_searchable_options">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="dp_level_1_attribute_is_searchable_label_text">Search field label Text</label>
                                                <input class="form-control" id="dp_level_1_attribute_is_searchable_label_text" value="" maxlength="32" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="dp_level_1_attribute_is_searchable_type">Search field type</label>
                                                <select class="form-control show-tick filter-select-type" id="dp_level_1_attribute_is_searchable_type"></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="dp_level_2" data-level="2">
                                <div class="row">
                                    <div class="col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label" for="dp_level_2_attribute_label_text">Label text</label>
                                            <input class="form-control" id="dp_level_2_attribute_label_text" maxlength="64" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-lg-8">
                                        <div class="form-group">
                                            <label class="control-label text-muted">Dictionary values</label>
                                            <p class="form-control-static text-muted level_dictionary_values" id="dp_level_2_dictionary_values">- - -</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="dp_level_2_attribute_placeholder_text">Placeholder text</label>
                                            <input class="form-control" id="dp_level_2_attribute_placeholder_text" value="" maxlength="64" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label" for="dp_level_2_attribute_help_text">Help text</label>
                                            <input class="form-control" id="dp_level_2_attribute_help_text" value="" maxlength="256" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="dp_level_2_attribute_is_searchable" class="is_searchable_toggle" data-container="dp_level_2_is_searchable_container"> Is searchable</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="dp_level_2_attribute_is_required"> Is required</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        <div class="checkbox">
                                            <label><input type="checkbox" id="dp_level_2_attribute_is_hidden"> Is hidden</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden" id="dp_level_2_is_searchable_container">
                                    <div class="is_searchable_options">
                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="dp_level_2_attribute_is_searchable_label_text">Search field label Text</label>
                                                <input class="form-control" id="dp_level_2_attribute_is_searchable_label_text" value="" maxlength="32" />
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="dp_level_2_attribute_is_searchable_type">Search field type</label>
                                                <select class="form-control show-tick filter-select-type" id="dp_level_2_attribute_is_searchable_type"></select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="uploadableParameter" class="parameter-options-panel hidden">
                        <div class="parameter_attributes">
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label" for="uploadable_label_text">Label text</label>
                                        <input class="form-control" id="uploadable_label_text" value="" maxlength="64" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label" for="uploadable_button_text">Button text</label>
                                        <input class="form-control" id="uploadable_button_text" value="" maxlength="64" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label" for="uploadable_attribute_max_items">Max items</label>
                                        <input class="form-control" id="uploadable_attribute_max_items" value="20" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-lg-12">
                                    <div class="form-group">
                                        <label class="control-label" for="uploadable_attribute_help_text">Help text</label>
                                        <input class="form-control" id="uploadable_attribute_help_text" value="" maxlength="255" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="uploadable_attribute_is_searchable" class="is_searchable_toggle" data-container="uploadable_attribute_is_searchable_container" data-protected="0"> Is searchable</label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="uploadable_attribute_is_required" data-protected="0"> Is required</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row hidden" id="uploadable_attribute_is_searchable_container">
                                <div class="is_searchable_options">
                                    <div class="col-md-8 col-lg-8">
                                        <div class="form-group">
                                            <label class="control-label" for="uploadable_attribute_is_searchable_label_text">Search field label Text</label>
                                            <input class="form-control" id="uploadable_attribute_is_searchable_label_text" value="" maxlength="64" />
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-lg-4">
                                        <div class="form-group">
                                            <label class="control-label">Default value</label>
                                            <div class="clearfix"><!--IE--></div>
                                            <input type="checkbox" id="uploadable_attribute_is_searchable_default_value">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="intervalParameter" class="parameter-options-panel hidden">
                        <div class="parameter_attributes">
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label" for="interval_label_text">Label text</label>
                                        <input class="form-control" id="interval_label_text" value="" maxlength="64" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4" id="interval_placeholder_text_container">
                                    <div class="form-group">
                                        <label class="control-label" for="interval_placeholder_text">Placeholder text</label>
                                        <input class="form-control" id="interval_placeholder_text" value="" maxlength="64" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4 help_text_div">
                                    <div class="form-group">
                                        <label class="control-label" for="interval_help_text">Help text</label>
                                        <input class="form-control" id="interval_help_text" value="" maxlength="256" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-lg-3">
                                    <div class="form-group">
                                        <label class="control-label" for="interval_begin">Interval begin</label>
                                        <div class="input-group">
                                            <input class="form-control text-right" id="interval_begin" value="" maxlength="5" data-type="Year" />
                                            <div class="input-group-btn">
                                                <button type="button" id="interval_begin_btn" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="name">Y</span> <span class="caret"></span></button>
                                                <ul class="dropdown-menu dropdown-menu-right" id="interval_begin_menu" role="menu">
                                                    <li data-type="Year"><a href="#">Year</a></li>
                                                    <li data-type="Month"><a href="#">Month</a></li>
                                                    <li data-type="Number" class="hidden"><a href="#">Number</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-lg-3">
                                    <div class="form-group">
                                        <label class="control-label" for="interval_end">Interval end</label>
                                        <div class="input-group">
                                            <input class="form-control text-right" id="interval_end" value="" maxlength="5" data-type="Year" />
                                            <div class="input-group-btn">
                                                <button type="button" id="interval_end_btn" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="name">Y</span> <span class="caret"></span></button>
                                                <ul class="dropdown-menu dropdown-menu-right" id="interval_end_menu" role="menu">
                                                    <li data-type="Year"><a href="#">Year</a></li>
                                                    <li data-type="Month"><a href="#">Month</a></li>
                                                    <li data-type="Number" class="hidden"><a href="#">Number</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-lg-2">
                                    <div class="form-group">
                                        <label class="control-label" for="interval_is_reverse">Reverse</label>
                                        <div class="clearfix"><!--IE--></div>
                                        <input type="checkbox" id="interval_is_reverse">
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="form-group">
                                        <label class="control-label" for="interval_default_value">Default value</label>
                                        <select class="form-control show-tick" name="interval_default_value" id="interval_default_value"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-lg-4">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="interval_is_searchable" class="is_searchable_toggle" data-container="interval_is_searchable_container" data-protected="0"> Is searchable</label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="interval_is_required" data-protected="0"> Is required</label>
                                    </div>
                                </div>
                                <div class="col-md-4 col-lg-4">
                                    <div class="checkbox">
                                        <label><input type="checkbox" id="interval_is_hidden" data-protected="0"> Is hidden</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row hidden" id="interval_is_searchable_container">
                                <div class="is_searchable_options">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" for="interval_is_searchable_label_text">Search field label Text</label>
                                            <input class="form-control" id="interval_is_searchable_label_text" value="" maxlength="32" />
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label" for="interval_is_searchable_type">Search field type</label>
                                            <select class="form-control show-tick filter-select-type" id="interval_is_searchable_type"></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="has_price_container">
                        <div class="col-md-12 col-lg-12">
                            <div class="checkbox">
                                <label><input type="checkbox" id="attribute_has_price"> Enable 'Price' for this parameter</label>
                            </div>
                        </div>
                        <div class="has_price_options hidden">
                            <div class="col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label class="control-label" for="attribute_price">Price</label>
                                    <div class="input-group">
                                        <input class="form-control text-right" id="attribute_price" value="0" data-currency-id="{{ default_currency['id'] }}" />
                                        <span class="input-group-addon">{{ default_currency['short_name'] }}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="pull-left">
                    <div class="form-group has-feedback">
                        <div class="input-group">
                            <input class="form-control" id="parameter_slug" placeholder="Parameter's slug" />
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button" id="parameter_slug_refresh"><span class="fa fa-refresh"></span></button>
                            </span>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-info" id="btnSaveParameterModal" data-type="add">Add</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
