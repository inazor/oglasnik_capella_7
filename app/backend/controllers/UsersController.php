<?php

namespace Baseapp\Backend\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Models\Roles;
use Baseapp\Models\Users;
use Baseapp\Models\Locations;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\ParametrizatorHelpers;
use Phalcon\Paginator\Adapter\QueryBuilder;

/**
 * Backend Users Controller
 */
class UsersController extends IndexController
{
    use ParametrizatorHelpers;
    use CrudActions;
    use CrudHelpers;

    /* @var \Baseapp\Models\Users */
    public $crud_model_class = 'Baseapp\Models\Users';

    protected $allowed_roles = array('admin', 'supersupport', 'support', 'finance', 'moderator', 'sales');

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('Users');

        $page = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode = $this->processSearchMode();
        $dir = $this->processAscDesc('desc');
        $order_by = $this->processOrderBy('id');
        $limit = $this->processLimit();
        $user_role = $this->processFilterUserRole();

        Tag::setDefaults(array(
            'q'         => $search_for,
            'mode'      => $mode,
            'dir'       => $dir,
            'order_by'  => $order_by,
            'limit'     => $limit,
            'user_role' => $user_role
        ), true);

        // Available search fields
        $available_fields = array(
            'id',
            'type',
            'username',
            'first_name',
            'last_name',
            'company_name',
            'email',
            'newsletter',
            'phone1',
            'phone2',
            'oib',
            'active',
            'banned',
            'ban_reason',
            'comment',
            'created_at',
            'modified_at',
            'import_id'
        );
        $date_fields = array(
            'created_at',
            'modified_at'
        );
        $field = $this->processOptions('field', $available_fields, null);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                'u.id',
                'u.type',
                'u.email',
                'u.username',
                'u.company_name',
                'u.first_name',
                'u.last_name',
                'u.banned',
                'u.remark',
                'u.active',
                'us.id AS user_shop_id',
                'ulr.role_id AS login_role',
                '(SELECT COUNT(*) FROM Baseapp\Models\InfractionReports WHERE model_name LIKE \'%Users\' AND model_pk_val = u.id AND resolved_at IS NULL) AS infraction_reports_count'
            ))
            ->addFrom('Baseapp\Models\Users', 'u')
            ->leftJoin('Baseapp\Models\UsersShops', 'u.id = us.user_id', 'us')
            ->leftJoin('Baseapp\Models\RolesUsers', 'u.id = ulr.user_id AND ulr.role_id = 1', 'ulr');

        if ('all' !== $user_role) {
            $user_role_id = Roles::getRoleIdByRoleName($user_role);
            if ($user_role_id) {
                $builder->innerJoin('Baseapp\Models\RolesUsers', 'u.id = ur.user_id AND ur.role_id = ' . $user_role_id, 'ur');
            }
        }

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                if (in_array($fld, $date_fields)) {
                    $conditions[] = 'DATE(FROM_UNIXTIME(u.' . $fld . ')) LIKE ' . $field_bind_name;
                } else {
                    $conditions[] = 'u.' . $fld . ' LIKE ' . $field_bind_name;
                }
                $binds[$fld] = $val;
            }
            $conditions = $this->changeSearchParamsLikeIntoEqualsWhenExactMode($conditions, $mode);
            $builder->andWhere(implode(' OR ', $conditions), $binds);
        }

        $builder->groupBy('u.id');
        $builder->orderBy('u.' . $order_by . ' ' . strtoupper($dir));

        $paginator = new QueryBuilder(array('builder' => $builder, 'limit' => $limit, 'page' => $page));
        $current_page = $paginator->getPaginate();

        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/users',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
    }

    /**
     * Helper method to process user type filter dropdown
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterUserRole($default = 'all') {
        $user_role_option = $this->processOptions(
            'user_role',
            array(
                'all'          => 'Display all users',
                'admin'        => 'Display admins only',
                'supersupport' => 'Display supersupport users only',
                'support'      => 'Display support users only',
                'moderator'    => 'Display moderators only',
                'content'      => 'Display content creators only',
                'finance'      => 'Display finance users only',
                'sales'        => 'Display sales users only'
            ),
            (string) $default,
            'string'
        );

        if (trim($user_role_option)) {
            return (string)$user_role_option;
        }

        return null;
    }

    protected function getPossibleOrderByOptions() {
        $order_bys = array(
            'id'          => 'ID',
            'created_at'  => 'Date created',
            'modified_at' => 'Date modified',
            'type'        => 'User type',
            'username'    => 'Username',
            'email'       => 'Email'
        );

        return $order_bys;
    }

    public function createAction()
    {
        // Only 'admin' and 'support' roles allowed to create new users
        if (!$this->auth->logged_in(array('admin', 'supersupport', 'support', 'sales'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('admin/users');
        }

        $this->tag->setTitle('Create user');
        $this->add_common_crud_assets();

        // Use the same form/view as editAction
        $this->view->pick('users/edit');

        // New users can't be impersonated od activated at this point
        $this->view->setVar('show_impersonate_link', false);
        $this->view->setVar('show_activate_btn', false);

        $form_title_long = 'Add user';
        $form_action = 'add';
        $send_email = $this->request->hasPost('send_email') ? 1 : 0;

        $user = new Users();
        if ($this->request->isPost()) {
            $backend_signup = $user->backend_signup($this->request);
            if ($backend_signup instanceof Users) {
                $email_notice = '';
                if ($this->request->getPost('action') == 'add' && $this->request->hasPost('send_email')) {
                    $email_notice = 'Activation link has been sent.';
                }
                $this->flashSession->success('<strong>User successfully created!</strong> ' . $email_notice);
                $save_action = $this->get_save_action();
                $next_url = $this->get_next_url();
                // Override next url to go to entity edit in save button case
                if ('save' === $save_action) {
                    $next_url = 'admin/users/edit/' . $user->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('errors', $backend_signup);
            }
        } else {
            $user->assignDefaults(array('newsletter' => 1));
        }

        $this->country_dropdown($user);
        $this->county_dropdown($user);
        $this->setup_roles_checkboxes($user);

        $this->view->setVar('user', $user);
        $this->view->setVar('send_email', $send_email);
        $this->view->setVar('form_title_long', $form_title_long);
        $this->view->setVar('form_action', $form_action);
    }

    protected function county_dropdown($user)
    {
        if (isset($user->county_id)) {
            Tag::setDefault('county_id', $user->county_id);
        }
        $counties = Locations::findCounties($user->country_id);
        $county_dropdown = Tag::select(array(
            'county_id',
            $counties,
            'using'      => array('id', 'name'),
            'useEmpty'   => true,
            'emptyText'  => 'Odaberite županiju...',
            'emptyValue' => '',
            'class'      => 'form-control'
        ));
        $this->view->setVar('county_dropdown', $county_dropdown);
        $this->view->setVar('county_dropdown_hidden', count($counties) === 0);
    }

    protected function country_dropdown($user)
    {
        if (isset($user->country_id)) {
            Tag::setDefault('country_id', $user->country_id);
        }
        $country_dropdown = Tag::select(array(
            'country_id',
            Locations::findCountries(),
            'using'      => array('id', 'name'),
            'useEmpty'   => true,
            'emptyText'  => 'Odaberite državu...',
            'emptyValue' => '',
            'class'      => 'form-control'
        ));
        $this->view->setVar('country_dropdown', $country_dropdown);
    }

    /**
     * @param \Baseapp\Models\Users $user
     */
    protected function setup_roles_checkboxes($user)
    {
        $current_roles = $user->get_roles_joined();

        $has_dev_role = false;
        $selected_role_names = array();
        foreach ($current_roles as $current_role) {
            $selected_role_names[] = $current_role['name'];
            if ('developer' === $current_role['name']) {
                $has_dev_role = true;
            }
        }

        // Maintain posted roles in case of errors
        if (isset($_POST['roles'])) {
            $selected_role_names = $_POST['roles'];
        }

        $all_roles = Roles::find()->toArray();

        // Remove 'developer' role from the list of roles for those that shouldn't see it
        if (!$has_dev_role) {
            $all_roles = Utils::array_where($all_roles, function($k, $role){
                return ('developer' !== $role['name']);
            });
        }

        $all_roles_names = array();
        foreach ($all_roles as $role) {
            $all_roles_names[$role['name']] = $role['description'];
        }

        $this->view->setVar('selected_roles', $selected_role_names);
        $this->view->setVar('roles', $all_roles_names);
    }

    public function add_common_crud_assets()
    {
        $this->assets->addJs('assets/vendor/jquery.mask.min.js');
        $this->assets->addJs('assets/backend/js/users-edit.js');
    }

    /**
     * Edit Action
     */
    public function editAction($entity_id = null)
    {
        $entity_id = (int) $entity_id;

        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Models\Users */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        $this->tag->setTitle('Edit user');
        $this->add_common_crud_assets();

        $form_title_long = 'Edit user';
        $form_action = 'edit';

        if ($this->request->isPost()) {
            if ($this->auth->logged_in(array('admin', 'supersupport', 'support', 'sales'))) {
                $saved = $entity->backend_save($this->request);
                if ($saved instanceof Users) {
                    if ($this->request->hasPost('activate_user')) {
                        if (true === $entity->insert_login_role()) {
                            $this->flashSession->success('<strong>User saved and activated successfully!</strong>');
                        } else {
                            $this->flashSession->error('<strong>User saved successfully BUT NOT activated!</strong>');
                        }
                    } else {
                        $this->flashSession->success('<strong>User saved successfully!</strong>');
                    }
                    // Determine which save button was used
                    $save_action = $this->get_save_action();
                    if ('save' === $save_action) {
                        return $this->redirect_self();
                    } else {
                        $next_url = $this->get_next_url();
                        return $this->redirect_to($next_url);
                    }
                } else {
                    $this->view->setVar('errors', $saved);
                    $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                }
            } else {
                $this->flashSession->error($this->insufficient_privileges);
            }
        }

        // Check if impersonation link should be visible
        $show_impersonate_link = false;
        if ($this->auth->logged_in(array('admin', 'supersupport', 'support', 'sales'))) {
            if ($entity && $entity->canBeImpersonated()) {
                $show_impersonate_link = true;
            }
        }
        $this->view->setVar('show_impersonate_link', $show_impersonate_link);

        $this->country_dropdown($entity);
        $this->county_dropdown($entity);
        $this->setup_roles_checkboxes($entity);

        $this->view->setVar('user', $entity);
        $this->view->setVar('infraction_reports', $entity->getInfractionReports($groupStatuses = true));
        $this->view->setVar('form_title_long', $form_title_long);
        $this->view->setVar('form_action', $form_action);
        $this->view->setVar('show_activate_btn', !$entity->hasRole('login'));
    }

    /**
     * banToggle Action
     */
    public function banToggleAction()
    {
        $params = $this->router->getParams();
        if ($user_id = (int) $params[0]) {
            if ($user = Users::findFirst(array('id=:user_id:', 'bind' => array('user_id' => $user_id)))) {
                if ($user->banToggle()) {
                    $this->flashSession->success('Ban status for user <strong>' . $user->username . '</strong> changed successfully.');
                } else {
                    $this->flashSession->error('Failed changing ban status for user <strong>'.$user->username.'</strong>.');
                }
            } else {
                $this->flashSession->error('User with ID: <strong>'.$user_id.'</strong> not found.');
            }
        }

        return $this->redirect_back();
    }

    /**
     * Switches the currently logged in admin to another user's account.
     *
     * First we check if the current user should be able to do it.
     * Then we check if the target user allows it.
     *
     * If everything checks out then we create some audit log records,
     * logout the current user, log in as the target user, and redirect to the site homepage.
     */
    public function impersonateAction($user_id = null)
    {
        if (null === $user_id) {
            $this->flashSession->error('Missing required parameter for user switch.');
            return $this->redirect_back();
        }

        // Check if currently logged-in user has 'admin' or 'support' role
        if (!$this->auth->logged_in(array('admin', 'supersupport', 'support', 'sales'))) {
            $this->flashSession->error('Insufficient privileges.');
            return $this->redirect_back();
        }

        // Check target user existence and roles
        /** @var Users $target_user */
        $target_user = Users::findFirst($user_id);
        if (!$target_user) {
            $this->flashSession->error('Invalid User specified.');
            return $this->redirect_back();
        }

        // Target user must have login role
        if (!$target_user->hasRole('login')) {
            $this->flashSession->error('Target user has no "login" role and cannot be switched to!');
            return $this->redirect_back();
        }

        // Target user must not have admin role (impersonating other admins sounds dangerous)
        if ($target_user->hasRole(array('admin', 'supersupport', 'support', 'sales'))) {
            $this->flashSession->error('Target user has "admin" or "supersupport" or "support" od "sales" role and cannot be switched to!');
            return $this->redirect_back();
        }

        // We're all good, proceed with user switching

        $current_user = $this->auth->get_user();
        $current_user_ident = $current_user->ident();
        $current_user_role = $current_user->hasRole('admin') ? 'Admin' : 'Support';

        // Logout the current user
        $this->saveAudit(sprintf($current_user_role . ' user switch: logging out %s', $current_user_ident));
        if ($this->hybridauth) {
            $this->hybridauth->logoutAllProviders();
        }
        $logged_out = $this->auth->logout();
        // Login as the target user
        if ($logged_out) {
            $this->auth->login_impersonate($target_user);
            $this->saveAudit(sprintf($current_user_role . ' user switch: logged in %s as %s.', $current_user_ident, $target_user->ident()));
        }

        // Redirect to homepage
        return $this->redirect_home();
    }

    /**
     * Ajax users search for auto-suggest and similar.
     *
     * @return \Phalcon\Http\Response|\Phalcon\HTTP\ResponseInterface
     */
    public function searchAction()
    {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            return $this->response;
        }

        $params = $this->router->getParams();
        if (!isset($params[0]) || empty($params[0])) {
            $this->response->setStatusCode(400, 'Bad Request');
            return $this->response;
        }

        $search_string = $params[0];
        $found_users = Users::get_suggest_values($search_string);

        if ($found_users && count($found_users)) {
            $results = array();
            foreach ($found_users as $user) {
                $data        = new \stdClass();
                $data->text  = $user->username;
                $data->value = (string) $user->id;
                $results[]   = $data;
            }
            $this->response->setJsonContent($results);
        }

        return $this->response;
    }
}
