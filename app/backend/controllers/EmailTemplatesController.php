<?php

namespace Baseapp\Backend\Controllers;

use Baseapp\Extension\Tag;
use Baseapp\Library\Utils;

class EmailTemplatesController extends IndexController
{
    protected $allowed_roles = array('admin');

    public function indexAction()
    {
        $this->tag->setTitle('Email Templates Editor');

        $this->assets->addJs('assets/vendor/ace-builds-master/src-min-noconflict/ace.js');
        $this->assets->addJs('assets/backend/js/email-templates.js');

        // Grab a list of all the templates
        $templates = $this->get_templates_list();

        // Decide which file we're handling
        $template_id = $this->request->hasPost('template_id') ? $this->request->getPost('template_id') : key($templates);
        if ($this->request->isPost()) {
            if ($this->request->hasPost('tpl_id')) {
                $template_id = $this->request->getPost('tpl_id');
            }
        }

        // Make sure tpl_id is valid
        if (null !== $template_id) {
            if (!isset($templates[$template_id])) {
                $template_id = null;
            }
        }

        // Process save submission
        if ($this->request->isPost() && $this->request->hasPost('save')) {
            if ($this->request->hasPost('code')) {
                $new_code = $this->request->getPost('code');
                $compiler = new \Phalcon\Mvc\View\Engine\Volt\Compiler();
                try {
                    // Try compiling the edited .volt contents to see if it's at least somewhat valid...
                    $compiled = $compiler->compileString($new_code);
                    if ($compiled) {
                        $bytes = file_put_contents(ROOT_PATH . $templates[$template_id], $new_code);
                        if ($bytes !== false) {
                            $this->flashSession->success(sprintf('File <strong>%s</strong> successfully modified (%s bytes written).', $templates[$template_id], $bytes));
                        } else {
                            $this->flashSession->error('An error occurred while trying to save the file. Please try again.');
                        }
                    }
                } catch (\Exception $e) {
                    $this->flashSession->error($e->getMessage());
                }
            }
        }

        // load the specified template
        $template_contents = '';
        if (null !== $template_id) {
            Tag::setDefault('template_id', $template_id);
            // if new contents have been posted, display them instead, so that
            // in case of errors you have the previous state available, otherwise it's lost
            if (isset($new_code)) {
                $template_string = $new_code;
            } else {
                $template_string = file_get_contents(ROOT_PATH . $templates[$template_id]);
            }
            $template_contents = ltrim(Utils::esc_html($template_string));
        }
        $this->view->setVar('tpl_id', $template_id);
        $this->view->setVar('template_contents', $template_contents);

        $templates_dropdown = Tag::select(array(
            'template_id',
            $templates,
            'using' => array('id', 'name'),
            'class' => 'form-control show-tick',
            'useEmpty' => false
        ));
        $this->view->setVar('templates_dropdown', $templates_dropdown);
    }

    public function get_templates_list()
    {
        // Collect all the email view files
        $dir   = ROOT_PATH . '/app/frontend/views/email';
        $files = array();

        foreach (new \DirectoryIterator($dir) as $file) {
            if ($file->isDot()) {
                continue;
            }
            $filename         = $file->getBasename('.volt');
            $pathname         = str_replace(ROOT_PATH, '', $file->getPathname());
            $files[$filename] = $pathname;
        }

        // Add master layout file to the end of the list
        $files['email'] = '/app/frontend/views/layouts/email.volt';

        return $files;
    }
}
