<?php
namespace Baseapp\Backend\Controllers;

use Baseapp\Library\Debug;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Library\Moderation;
use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Library\Avus\ExportToAvus;
use Baseapp\Models\Ads;
use Baseapp\Models\AdsAdditionalData;
use Baseapp\Models\AdsHomepage;
use Baseapp\Models\Categories;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\ParametrizatorHelpers;
use Baseapp\Library\Products\BackendProductsSelector;
use Phalcon\Mvc\Model;
use Phalcon\Paginator\Adapter\QueryBuilder;

/**
 * Backend Ads Controller
 */
class AdsController extends IndexController
{
    use ParametrizatorHelpers;
    use CrudActions;
    use CrudHelpers;

    public $crud_model_class = 'Baseapp\Models\Ads';

    // Overriding the default list of backend-allowed roles to exclude 'content' here
    protected $allowed_roles = array('admin', 'supersupport', 'support', 'finance', 'moderator', 'sales');

    /**
     * Index Action
     */
    public function indexAction() {
        $this->tag->setTitle('Ads');

        $page = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode = $this->processSearchMode('exact');
        $dir = $this->processAscDesc('desc');
        $order_by = $this->processOrderBy('id');
        $limit = $this->processLimit();
        $moderation = $this->processFilterModeration();
        $product = $this->processFilterProducts();
        $payment_state = $this->processFilterPaymentState();

        Tag::setDefaults(array(
            'q'        => $search_for,
            'product'  => $product,
            'mode'     => $mode,
            'dir'      => $dir,
            'order_by' => $order_by,
            'limit'    => $limit
        ), true);

        $filter_category_id = null;
        $filter_category = null;
        if ($this->request->has('filter_category_id')) {
            $filter_category_id = $this->request->get('filter_category_id', 'int', null);
            $filter_category = Categories::findFirst($filter_category_id);
        }

        // Filter category tree (view variable) + category dropdown generation
        $this->category_id_dropdown(
            'filter_category_id',
            $filter_category_id,
            array(
                'emptyText' => 'Any category',
                'subtext'   => false
            )
        );

        // Populates the hidden dropdown for the 'create new ad' modal
        $this->category_id_dropdown(
            'category_id',
            $filter_category_id,
            array(
                'disabled' => Categories::getCategoriesThatDontAcceptAds()
            )
        );

        // Available search fields
        $available_fields = array(
            'id',
            'title',
            'description',
            'active',
            'user_id',
            'username',
            'online_product_id',
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date',
            'is_spam',
            'soft_delete',
            'import_id',
            'avus_id',
            'remark',
            'phone1',
            'phone2',
        );
        $date_fields = array(
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date'
        );

        $valid_fields   = $available_fields;
        $valid_fields[] = 'all';
        $field = $this->processOptions('field', $valid_fields, 'id');
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        // Keeps track whether changes to default filters/conditions occurred
        $default_listing = true;

        // Build the queryr
        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                'ad.*',
                'user.type AS user_type',
                'user.username AS user_username',
                'user.remark AS user_remark',
                'shop.title AS user_shop_title'
            ))
            ->addFrom('Baseapp\Models\Ads', 'ad')
            ->innerJoin('Baseapp\Models\Users', 'ad.user_id = user.id', 'user')
            ->leftJoin('Baseapp\Models\UsersShops', 'ad.user_id = shop.user_id', 'shop')
            ->leftJoin('Baseapp\Models\AdsAdditionalData', 'ad.id = aad.ad_id', 'aad');

        // only if there is a filtering by a specific category, inner join categories model
        if (null !== $filter_category) {
            $default_listing = false;
            $builder->innerJoin('Baseapp\Models\Categories', 'ad.category_id = category.id', 'category');
            // If a chosen node is a leaf node, we can get better performance for certain types of queries
            // if/when this condition reduces the search space drastically
            if ($filter_category->transaction_type_id) {
                $builder->andWhere('category.id = ' . $filter_category->id);
            }
            // This is a general search for any results in any descendant categories too, but it's not too performant,
            // and its not even needed in certain cases
            // TODO/FIXME: what happens if we change al this into a list of category ids with IN() clause?
            $builder->andWhere(
                'category.lft >= :category_left: AND category.rght <= :category_right:',
                array(
                    'category_left'  => $filter_category->lft,
                    'category_right' => $filter_category->rght
                )
            );
        }

        if (null !== $moderation) {
            $default_listing = false;
            $builder->andWhere('ad.moderation = :moderation:', array('moderation' => $moderation));
        }

        if (null !== $product) {
            $default_listing = false;
            $product_details = explode('_', $product);
            $builder->andWhere('ad.' . strtolower($product_details[0]) . '_product_id = :product:', array('product' => $product_details[1]));
        }

        if ($payment_state) {
            $builder->andWhere('ad.latest_payment_state = :payment_state:', array('payment_state' => $payment_state));
        }

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $default_listing = false;
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                if (in_array($fld, $date_fields)) {
                    $conditions[] = 'DATE(FROM_UNIXTIME(ad.' . $fld . ')) LIKE ' . $field_bind_name;
                } else {
                    if ('username' === $fld) {
                        $conditions[] = 'user.' . $fld . ' LIKE ' . $field_bind_name;
                    } elseif (in_array($fld, array('import_id', 'avus_id', 'remark'))) {
                        $conditions[] = 'aad.' . $fld . ' LIKE ' . $field_bind_name;
                    } else {
                        $conditions[] = 'ad.' . $fld . ' LIKE ' . $field_bind_name;
                    }
                }
                $binds[$fld] = $val;
            }
            $conditions = $this->changeSearchParamsLikeIntoEqualsWhenExactMode($conditions, $mode);
            $builder->andWhere(implode(' OR ', $conditions), $binds);
        }

        // For default listing we need some kind of WHERE sql condition or the production
        // server losses its shit for some reason. This forces the PRIMARY index to be used in the query execution plan.
        if ($default_listing) {
            $builder->andWhere('ad.id > 0');
        }

        $builder->orderBy('ad.' . $order_by . ' ' . strtoupper($dir));

        $paginator = new QueryBuilder(array('builder' => $builder, 'limit' => $limit, 'page' => $page));
        $current_page = $paginator->getPaginate();

        // Process the resulting page and extend with some additional data (ad image if it exists)
        $results_array = Ads::buildResultsArray($current_page->items);
        $current_page->items = $results_array;

        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/ads',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);

        $this->assets->addJs('assets/backend/js/classifieds-index.js');
    }

    protected function getPossibleOrderByOptions() {
        $order_bys = array(
            'id'                 => 'ID',
            'created_at'         => 'Date created',
            'modified_at'        => 'Date modified',
            'first_published_at' => 'First Publish date',
            'published_at'       => 'Last Publish date',
            'sort_date'          => 'Virtual publish date',
            'expires_at'         => 'Date expired',
            'title'              => 'Title'
        );

        return $order_bys;
    }

    protected function processSmsOnly($default = false)
    {
        $sms = null;

        if ($this->request->hasQuery('sms')) {
            $sms = (bool) $this->request->getQuery('sms', 'int', $default);
        }

        $this->view->setVar('sms', $sms);

        return $sms;
    }

    /**
     * Helper method to process moderation filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterModeration($default = null) {
        $moderation = new Moderation();

        $moderation_option = $this->processOptions(
            'moderation',
            array_merge(
                array('' => 'Any moderation'),
                $moderation->getPossibleModerationStatuses()
            ),
            (string) $default,
            'string'
        );

        if (trim($moderation_option)) {
            return (string)$moderation_option;
        }

        return null;
    }

    /**
     * Helper method to process products filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterProducts($default = null) {
        $selector              = new BackendProductsSelector();

        $product_option = $this->processOptions(
            'product',
            array_merge(
                array('' => 'Any product'),
                $selector->getPossibleIdsList()
            ),
            (string) $default,
            'string'
        );

        if (trim($product_option)) {
            return (string)$product_option;
        }

        return null;
    }

    /**
     * Helper method to process ad's payment state filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterPaymentState($default = null) {
        $payment_state_option = $this->processOptions(
            'payment_state',
            array_merge(
                array('0' => 'Any payment state'),
                (new Ads())->getAllPaymentStates()
            ),
            (string) $default,
            'string'
        );

        if (trim($payment_state_option)) {
            return (string)$payment_state_option;
        }

        return null;
    }

    /**
     * Helper method to process ad's payment type filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterPaymentType($default = null) {
        $payment_type_option = $this->processOptions(
            'payment_type',
            array_merge(
                array('0'       => 'Any payment type'),
                array('paid'    => 'Paid ads'),
                array('ordered' => 'Ordered paid ads'),
                array('free'    => 'Free ads')
            ),
            (string) $default,
            'string'
        );

        if (trim($payment_type_option)) {
            return (string)$payment_type_option;
        }

        return null;
    }

    /**
     * Helper method to generate moderation dropdowns for ad editing..
     *
     * @param string $status Default status
     * @param string $reason Default reason
     */
    protected function processModerationReasons($status, $reason)
    {
        $moderation = new Moderation();
        $moderation_statuses = $moderation->getPossibleModerationStatuses();
        $moderation_reasons = $moderation->getModerationReasonsData();
        $moderation_statuses_final = array();
        $moderation_reasons_final = array();

        foreach ($moderation_statuses as $k => $title) {
            if (isset($moderation_reasons[$k])) {
                $moderation_statuses_final[$k] = array(
                    'text' => $title,
                    'attributes' => array(
                        'data-has-reasons' => 1
                    )
                );

                $curr_reason = $reason;

                $available_reasons = array();
                foreach ($moderation_reasons[$k] as $row_key => $row_data) {
                    $available_reasons[$row_key] = $row_data['title'];
                }


                // Prevent invalid choices from being set as default
                if (!array_key_exists($curr_reason, $available_reasons)) {
                    $curr_reason = null;
                }

                // Preselected value, if there's a valid one
                if ($curr_reason) {
                    Tag::setDefault('moderation_reason_' . $k, $curr_reason);
                }

                // Default options
                $moderation_reason_options = array(
                    'moderation_reason_' . $k,
                    $available_reasons,
                    'useEmpty'   => false,
                    'class'      => 'form-control show-tick moderation-reasons'
                );
                // Build the dropdown
                $moderation_reasons_final[$k] = Tag::select($moderation_reason_options);
            } else {
                $moderation_statuses_final[$k] = array(
                    'text' => $title,
                    'attributes' => array(
                        'data-has-reasons' => 0
                    )
                );
            }
        }

        // Prevent invalid choices from being set as default
        if (!array_key_exists($status, $moderation_statuses_final)) {
            $status = null;
        }

        // Preselected value, if there's a valid one
        if ($status) {
            Tag::setDefault('moderation', $status);
        }

        // Default options
        $moderation_select_options = array(
            'moderation',
            $moderation_statuses_final,
            'useEmpty'   => false,
            'class'      => 'form-control show-tick'
        );

        // Build the dropdown
        $moderation_dropdown = Tag::select($moderation_select_options);
        $this->view->setVar('moderation_dropdown', $moderation_dropdown);
        $this->view->setVar('moderation_reasons', $moderation_reasons_final);
    }

    /**
     * @param string $dropdown_field_name Name/ID of the dropdown
     * @param int|null $selected Value to preselect
     * @param array|null $options Tag::select() option overrides
     *
     * @return array|null
     */
    protected function category_id_dropdown($dropdown_field_name, $selected = null, $options = array()) {

        // Category tree is used so we don't have to call getPath for every node,
        // (and instead just get already queried data) + to populate the categories dropdown
        $model = new Categories();
        $tree = $this->getDI()->get(Categories::MEMCACHED_KEY);
        $selectables = $model->getSelectablesFromTreeArray($tree, '', $with_root = false, $depth = null, $breadcrumbs = true);

        // Prevent invalid choices from being set as default
        if (!array_key_exists($selected, $selectables)) {
            $selected = null;
        }

        // Preselected value, if there's a valid one
        if ($selected) {
            Tag::setDefault($dropdown_field_name, $selected);
        }

        // Process $selectables before using them in Tag::select() in order to
        // display a more useful dropdown (for when it's not being handled by selectpicker())
        if (isset($options['subtext']) && !$options['subtext']) {
            foreach ($selectables as $k => $data) {
                if (isset($data['attributes'])) {
                    if (isset($data['attributes']['data-subtext'])) {
                        $subtext = $data['attributes']['data-subtext'];
                        $selectables[$k]['text'] = $subtext . ' › ' . $selectables[$k]['text'];
                        unset($selectables[$k]['attributes']['data-subtext']);
                    }
                }
            }
            unset($options['subtext']);
        }

        // Default options
        $defaults = array(
            $dropdown_field_name,
            $selectables,
            'useEmpty'   => true,
            'emptyText'  => 'Choose category...',
            'emptyValue' => '',
            'class'      => 'form-control'
        );

        // Merge passed in $options
        $options = array_merge($defaults, $options);

        // Build the dropdown
        $category_id_dropdown = Tag::select($options);
        $this->view->setVar($dropdown_field_name . '_dropdown', $category_id_dropdown);

        // Set the entire tree as a view variable (for displaying an article's category path or similar)
        $this->view->setVar('tree', $tree);

        return $tree;
    }

    // protected function columnExists($model, $name)
    protected function columnExists($schema, $table, $name)
    {
        $result = false;

        /*
        $cols = $this->getDI()->get('db')->describeColumns($model->getSource());
        foreach ($cols as $col) {
            if ($name == $col->getName()) {
                $result = true;
                break;
            }
        }
        */

        /**
         * @var $db \Phalcon\Db\AdapterInterface
         */
        $db  = $this->getDI()->getShared('db');
        $sql = "SELECT column_name FROM information_schema.columns WHERE table_schema = :schema AND table_name = :table AND column_name = :name";
        $params = array(
            ':schema' => $schema,
            ':table'  => $table,
            ':name'   => $name
        );

        $result = $db->fetchOne($sql, \Phalcon\Db::FETCH_ASSOC, $params);

        // If we get an array back, it means the column exists (otherwise we get bool false from fetchOne it seems)
        if (is_array($result) && count($result)) {
            $result = true;
        }

        return $result;
    }

    protected function modifySqlBuilderForSmsOnly(Model\Query\BuilderInterface $builder)
    {
        // TODO/FIXME: we can remove this check after the db change has been deployed
        $sms_col_exists = $this->columnExists($this->config->database->dbname, AdsAdditionalData::TABLE_NAME, 'via_sms');
        if ($sms_col_exists) {
            $builder->leftJoin('Baseapp\Models\AdsAdditionalData', 'ad.id = aad.ad_id', 'aad');
            $builder->andWhere(
                'aad.via_sms = 1'
            );
        }

        return $builder;
    }

    /**
     * AwaitingModeration Action
     */
    public function awaitingModerationAction() {
        $this->tag->setTitle('Ads awaiting moderation');

        $page = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode = $this->processSearchMode();
        $dir = $this->processAscDesc('asc');
        $order_by = $this->processOrderBy('modified_at');
        $payment_type = $this->processFilterPaymentType();
        $product = $this->processFilterProducts();
        $limit = $this->processLimit();
        $sms_only = $this->processSmsOnly();

        Tag::setDefaults(array(
            'q'            => $search_for,
            'mode'         => $mode,
            'product'      => $product,
            'dir'          => $dir,
            'order_by'     => $order_by,
            'payment_type' => $payment_type,
            'limit'        => $limit
        ), true);

        $filter_category_id = null;
        $filter_category = null;
        if ($this->request->has('filter_category_id')) {
            $filter_category_id = $this->request->get('filter_category_id', 'int', null);
            $filter_category = Categories::findFirst($filter_category_id);
        }

        // Filter category tree (view variable) + category dropdown generation
        $this->category_id_dropdown(
            'filter_category_id',
            $filter_category_id,
            array(
                'emptyText' => 'Any category',
                'subtext'   => false
            )
        );

        // Populates the hidden dropdown for the 'create new ad' modal
        $this->category_id_dropdown(
            'category_id',
            $filter_category_id,
            array(
                'disabled' => Categories::getCategoriesThatDontAcceptAds()
            )
        );

        // Available search fields
        $available_fields = array(
            'id',
            'title',
            'description',
            'active',
            'user_id',
            'username',
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date',
            'import_id'
        );
        $date_fields = array(
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date'
        );
        $field = $this->processOptions('field', $available_fields, null);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        $acceptable_payment_states = array(
            Ads::PAYMENT_STATE_FREE,
            Ads::PAYMENT_STATE_ORDERED,
            Ads::PAYMENT_STATE_PAID
        );

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                'ad.*',
                'user.type AS user_type',
                'user.username AS user_username',
                'user.remark AS user_remark',
                'shop.title AS user_shop_title'
            ))
            ->addFrom('Baseapp\Models\Ads', 'ad')
            ->innerJoin('Baseapp\Models\Users', 'ad.user_id = user.id', 'user')
            ->leftJoin('Baseapp\Models\UsersShops', 'ad.user_id = shop.user_id', 'shop')
            ->where('ad.soft_delete = 0')
            ->andWhere('ad.online_product_id IS NOT NULL')
            ->andWhere(
                'ad.moderation = :moderation_status:',
                array(
                    'moderation_status' => 'waiting'
                )
            )
            ->andWhere('ad.latest_payment_state IN (' . implode(',', $acceptable_payment_states) . ')');

        // Filter sms_only if required
        if ($sms_only) {
            $builder = $this->modifySqlBuilderForSmsOnly($builder);
        }

        // only if there is a filtering by a specific category, inner join categories model
        if (null !== $filter_category) {
            $builder->innerJoin('Baseapp\Models\Categories', 'ad.category_id = category.id', 'category');
            // If a chosen node is a leaf node, we can get better performance for certain types of queries
            // if/when this condition reduces the search space drastically
            if ($filter_category->transaction_type_id) {
                $builder->andWhere('category.id = ' . $filter_category->id);
            }
            // This is a general search for any results in any descendant categories too, but it's not too performant,
            // and its not even needed in certain cases
            // TODO/FIXME: what happens if we change al this into a list of category ids with IN() clause?
            $builder->andWhere(
                'category.lft >= :category_left: AND category.rght <= :category_right:',
                array(
                    'category_left'  => $filter_category->lft,
                    'category_right' => $filter_category->rght
                )
            );
        }

        if (null !== $product) {
            $product_details = explode('_', $product);
            $builder->andWhere('ad.' . strtolower($product_details[0]) . '_product_id = :product:', array('product' => $product_details[1]));
        }

        if (null !== $payment_type) {
            if ('paid' == $payment_type) {
                $builder->andWhere('ad.latest_payment_state = :latest_payment_state:', array('latest_payment_state' => Ads::PAYMENT_STATE_PAID));
            } else if ('ordered' == $payment_type) {
                $builder->andWhere('ad.latest_payment_state = :latest_payment_state:', array('latest_payment_state' => Ads::PAYMENT_STATE_ORDERED));
            } else if ('free' == $payment_type) {
                $builder->andWhere('ad.latest_payment_state = :latest_payment_state:', array('latest_payment_state' => Ads::PAYMENT_STATE_FREE));
            }
        }

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                if (in_array($fld, $date_fields)) {
                    $conditions[] = 'DATE(FROM_UNIXTIME(ad.' . $fld . ')) LIKE ' . $field_bind_name;
                } else {
                    if ('username' === $fld) {
                        $conditions[] = 'user.' . $fld . ' LIKE ' . $field_bind_name;
                    } else {
                        $conditions[] = 'ad.' . $fld . ' LIKE ' . $field_bind_name;
                    }
                }
                $binds[$fld] = $val;
            }
            $conditions = $this->changeSearchParamsLikeIntoEqualsWhenExactMode($conditions, $mode);
            $builder->andWhere(implode(' OR ', $conditions), $binds);
        }

        $builder->orderBy('ad.active ASC, ad.product_sort ASC, ad.' . $order_by . ' ' . strtoupper($dir));

        $paginator = new QueryBuilder(array('builder' => $builder, 'limit' => $limit, 'page' => $page));
        $current_page = $paginator->getPaginate();

        // Process the resulting page and extend with some additional data (ad image if it exists)
        $results_array = Ads::buildResultsArray($current_page->items);
        $current_page->items = $results_array;

        $pagination_links = Tool::paginationAdmin(
            $current_page,
            'admin/ads/awaiting-moderation',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);

        $this->assets->addJs('assets/backend/js/classifieds-index.js');
    }


    public function add_common_crud_assets() {
        $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');

        $this->assets->addCss('assets/vendor/magicsuggest/magicsuggest-min.css');
        $this->assets->addJs('assets/vendor/magicsuggest/magicsuggest-min.js');

        $this->assets->addCss('assets/vendor/datepicker3.css');
        $this->assets->addJs('assets/vendor/bootstrap-datepicker.js');

        $this->assets->addJs('assets/backend/js/classifieds-edit.js');

        $this->assets->addCss('assets/vendor/awesome-bootstrap-checkbox.css');
        $this->assets->addJs('assets/js/classified-submit-choose-ad-type.js');
    }

    public function createAction() {
        // Only 'admin' and 'support' roles are allowed to create ads from the backend
        if (!$this->auth->logged_in(array('admin', 'supersupport', 'sales'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('admin/ads');
        }

        if (!$this->request->has('category_id')) {
            $this->flashSession->error('Missing required category ID for create action!');
            return $this->redirect_to('admin/ads');
        }
        $selected_category_id = (int)$this->request->get('category_id');

        if (!$selected_category_id) {
            $this->flashSession->error('Invalid category ID!');
            return $this->redirect_to('admin/ads');
        }

        $selected_category = Categories::findFirst($selected_category_id);

        if (!$selected_category) {
            $this->flashSession->error(sprintf('Category not found [ID: %s]', $selected_category));
            return $this->redirect_back();
        } else {
            if (0 == count($selected_category->FieldsetParameters)) {
                $this->flashSession->error('Selected category does not have parameters set up!');
                return $this->redirect_back();
            }

            $this->tag->setTitle('Create new ad');
            $this->view->setVar('form_title_long', 'Create new ad');
            $this->view->setVar('form_action', 'add');
            $this->add_common_crud_assets();
            // Use the same form/view as editAction
            $this->view->pick('ads/edit');

            $ad = new Ads();
            $ad->created_by_user_id = $this->auth->get_user()->id;
            $parametrizator = new Parametrizator();
            $parametrizator->setModule('backend');
            $parametrizator->setCategory($selected_category);
            $parametrizator->setAd($ad);

            if ($this->request->isPost()) {
                $created = $ad->backend_add_new($this->request);
                if ($created instanceof Ads) {
                    $this->flashSession->success('<strong>Ad created!</strong> ' . $ad->get_frontend_view_link('html'));
                    $save_action = $this->get_save_action();
                    $next_url = $this->get_next_url();

                    // Override next url to go to entity edit in save button case
                    if ('save' === $save_action) {
                        $next_url = 'admin/ads/edit/' . $ad->id;
                    }
                    return $this->redirect_to($next_url);
                } else {
                    $this->view->setVar('errors', $created);
                    $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                    $parametrizator->setRequest($this->request);
                    $parametrizator->setErrors($created);
                }
            }
            $this->setup_products_chooser($ad, $selected_category);
            $this->setup_rendered_parameters($parametrizator->render_input());
            $this->view->setVar('category', $selected_category);
            $this->view->setVar('ad', $ad);
            $this->view->setVar('show_save_buttons', true);
        }
    }

    /**
     * Returns true if the currently logged in user has the proper role to give
     * Ads a free PushUp product and if the Ad ($entity) in question satisfies
     * certain business rules. False otherwise.
     *
     * @param Ads $entity
     *
     * @return bool
     */
    protected function isPushupAllowed(Ads $entity)
    {
        $allowed = false;

        if ($this->auth->logged_in(array('admin', 'supersupport'))) {
            $allowed = true;
        }

        // Applying pushup logic to soft-deleted, expired or inactive ads makes no sense to me, so...
        if ($allowed) {
            if ($entity->isSoftDeleted() || $entity->isExpired() || !$entity->is_active()) {
                $allowed = false;
            }
        }

        if ($allowed) {
            /**
             * TODO/FIXME:
             * Do we want to check if the current entity's online product allows the pushup at all?
             * Or we don't care and we actually wanto to allow bumping dates on ads from the backend for whatever reason?
             *
             * Pushups also make little sense (or none at all) for ads that are not yet active or those that
             * are in the process of being submitted etc... But that hasn't been fully specified either, and it could
             * conflict with the decision from the previous paragraph...
             *
             * Also, category configuration might have to allow disabling the Pushup product on a category level,
             * so we might have to account for that too in the future...
             *
             * We currently only show the pushup purchase option for active ads whose online product returns true
             * from hasPushUp()... (on the frontend, see Ads::get_possible_listing_options())
             */
        }

        return $allowed;
    }

    /**
     * Calls $entity->pushUp() and sets session success/error messages.
     * Stores an audit log record in case pushUp was applied successfully.
     *
     * @param Ads $entity
     *
     * @return bool Result of calling $entity->pushUp()
     */
    protected function doPushUp(Ads $entity)
    {
        $pushed_up = $entity->pushUp(false); // false here means it's a "free" backend-applied pushUp
        if ($pushed_up) {
            $this->flashSession->success('Ad pushed up successfully!');
            $this->saveAudit(sprintf('Backend PushUp applied to Ad: %s', $entity->ident()));
        } else {
            $errors = $this->modelMessagesTransformer($entity);
            $this->flashSession->error('Failed to apply PushUp (error(s): ' . $errors . ')');
        }

        return $pushed_up;
    }

    /**
     * @param Model $entity
     *
     * @return string Comma separated list of messages from the Model
     */
    protected function modelMessagesTransformer(Model $entity)
    {
        // TODO: extend Phalcon\Validation\Message\Group to encapsulate this via __toString() or something...
        $msgs = array();
        foreach ($entity->getMessages() as $msg) {
            $msgs[] = $msg->getMessage();
        }
        $msg_string = implode(', ', $msgs);

        return $msg_string;
    }

    /**
     * Edit Action
     */
    public function editAction($entity_id) {
        if (!$this->auth->logged_in(array('admin', 'supersupport', 'support', 'moderator', 'sales'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('admin/ads');
        }

        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for edit action!');
            return $this->redirect_back();
        }

        $title = 'Edit ad';
        $this->tag->setTitle($title);
        $this->view->setVar('form_title_long', $title);
        $this->add_common_crud_assets();

        /* @var $model \Baseapp\Models\Ads */
        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Models\Ads */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        // set $resolveUnresolvedInfractionReports to true in case inital ads moderation status is 'reported'.
        // later, in case admin pressed 'savemoderation' button, we'll check if the moderation status changed and if it
        // did, we assume that admin reviewed the ad, and infraction reports are considered as resolved so we have to 
        // mark them as resolved!
        $resolveUnresolvedInfractionReports = $entity->moderation == Moderation::MODERATION_STATUS_REPORTED;

        $this->category_id_dropdown(
            'new_category_id',
            null,
            array(
                'disabled' => Categories::getCategoriesThatDontAcceptAds()
            )
        );

        $ad_category = $entity->getCategory();
        if ($this->request->hasPost('category_id') && intval($this->request->getPost('category_id')) !== intval($ad_category->id)) {
            $ad_category = Categories::findFirst(intval($this->request->getPost('category_id')));
        }

        // Check backend pushup stuff
        $pushup_allowed = $this->isPushupAllowed($entity);
        $this->view->setVar('show_pushup_button', $pushup_allowed);
        // Check if pushup logic itself should be applied (and if so, do it and redirect back to self)
        if ($pushup_allowed && $this->request->isPost() && $this->request->hasPost('pushup')) {
            $this->doPushUp($entity);
            return $this->redirect_self();
        }

        $parametrizator = new Parametrizator();
        $parametrizator->setModule('backend');
        $parametrizator->setCategory($ad_category);
        $parametrizator->setAd($entity);

        if ($this->request->isPost() && $this->request->hasPost('category_id')) {
            $next_url = $this->get_next_url();

            if ($entity->isSoftDeleted()) {
                $this->flashSession->error('<strong>Error!</strong> SoftDeleted ad cannot be modified!');
                return $this->redirect_to($next_url);
            }

            // detect which button submitted the form.. in case the detected button is 'savemoderation' then we
            // handle only saving of 'Ads moderation part', otherwise, save all submitted data.
            if ($this->request->hasPost('savemoderation')) {
                // check if $resolveUnresolvedInfractionReports was set as true (meaning, ad was reported) stayed the 
                // same. if it did, we assume that admin decided to still leave the ad as 'reported' and we'll leave
                // infraction reports as unresolved...
                if ($resolveUnresolvedInfractionReports && $this->request->getPost('moderation', 'string', null) == Moderation::MODERATION_STATUS_REPORTED) {
                    $resolveUnresolvedInfractionReports = false;
                }

                $saved = $entity->save_moderation($this->request, $resolveUnresolvedInfractionReports);
                $success_msg = '<strong>Ad moderated!</strong>';
                $save_action = 'savemoderation';
            } else {
                $saved = $entity->backend_save_changes($this->request);
                $success_msg = '<strong>Ad updated!</strong>';
                $save_action = $this->get_save_action();
            }

            if ($saved instanceof Ads) {
                $this->flashSession->success($success_msg . ' ' . $saved->get_frontend_view_link('html'));

                if ('save' === $save_action) {
                    $next_url = 'admin/ads/edit/' . $saved->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $parametrizator->setRequest($this->request);
                $parametrizator->setErrors($saved);
                $this->processModerationReasons($this->request->getPost('moderation', 'string', null), $this->request->getPost('moderation_reason', 'string', null));
            }

        } else {
            $parametrizator->setData($entity->getData());
            $this->processModerationReasons($entity->moderation, $entity->moderation_reason);
        }
        if ($this->auth->logged_in(array('support','moderator'))) {
            $this->print_chosen_products($entity);
        } else {
            $this->setup_products_chooser($entity, $ad_category);
        }
        $this->setup_rendered_parameters($parametrizator->render_input());
        $this->view->setVar('category', $ad_category);
        $this->view->setVar('ad', $entity);

        $can_be_moderated = true;
        $moderation_warning = null;

        $ad_status = $entity->get_status();
        if ($ad_status == 0) {
            $can_be_moderated = false;
            $moderation_warning = 'This ad has not yet finished with <b>ad submission</b> process and cannot be moderated!';
        }
        if ($ad_status == 20) {
            $can_be_moderated = false;
            $moderation_warning = 'This ad has not yet finished with <b>ad republish</b> process (<b>waiting for payment</b>) and cannot be moderated!';
        }
        if ($entity->soft_delete === 1) {
            $can_be_moderated = false;
            $moderation_warning = 'This ad is <b>soft deleted</b> and cannot be moderated or edited!';
        }

        $this->view->setVar('can_be_moderated', $can_be_moderated);
        $this->view->setVar('moderation_warning', $moderation_warning);
        $this->view->setVar('infraction_reports', $entity->getInfractionReports($groupStatuses = true));
        // get ad's latest (last 10) related orders
        $this->view->setVar('ads_related_orders', $entity->getAllRelatedOrders(10));
        $this->view->setVar('is_offline_exportable', $entity->is_offline_exportable());

        $show_save_buttons = true;
        if ($entity->isSoftDeleted()) {
            $show_save_buttons = false;
        }
        $this->view->setVar('ads_remark', (isset($entity->AdditionalData->remark) ? $entity->AdditionalData->remark : ''));
        $this->view->setVar('show_save_buttons', $show_save_buttons);
    }

    public function saveRemarkAction()
    {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['msg'] = 'Bad Request';
        } else {
            $ad = Ads::findFirst(array(
                'conditions' => 'id = :id:',
                'bind'       => array(
                    'id' => $_POST['ad_id']
                )
            ));
            if ($ad) {
                if ($ad->save_remark($_POST['remark'])) {
                    $response_array['status'] = true;
                } else {
                    $response_array['msg'] = 'Error saving remark for this ad!';
                }
            } else {
                $response_array['msg'] = 'Ad could not be found!?!';
            }
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    public function softDeleteAction($entity_id = null)
    {
        // Soft delete allowed only for 'admin' and 'support' roles
        if (!$this->auth->logged_in(array('admin', 'supersupport'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('admin/ads');
        }

        $this->view->pick('chunks/delete');

        $title = 'SoftDelete ad';
        $this->tag->setTitle($title);
        $this->view->setVar('form_title_long', $title);
        $this->view->setVar('is_soft_delete', true);
        $this->add_common_crud_assets();

        // Check if multiple resources are to be handled
        if (false !== strpos($entity_id, ',')) {
            $entity_ids = explode(',', $entity_id);
        } else {
            $entity_id = (int) $entity_id;
            if (!$entity_id) {
                $this->flashSession->error('Missing required ID parameter for SoftDelete action');

                return $this->redirect_back();
            }
            $entity_ids = array($entity_id);
        }

        /* @var $model \Baseapp\Models\BaseModel */
        $model = $this->crud_model_class;

        $ids = implode(',', array_map(function($id){
            return (int) trim($id);
        }, $entity_ids));

        /* @var $entities \Baseapp\Models\BaseModel[]|\Phalcon\Mvc\Model\Resultset */
        $condition = 'id IN (' . $ids . ')';
        $entities = $model::find($condition);

        // Do actual deletion if needed
        if ($this->request->isPost()) {
            foreach ($entities as $entity) {
                $ident = $entity->ident();
                $result = $entity->soft_delete();
                if ($result) {
                    // When SoftDeleting an ad, delete the potential AdsHomepage record too
                    AdsHomepage::deleteByAdId($entity->id);
                    $this->flashSession->success('Resource <strong>' . $ident . '</strong> has been SoftDeleted.');
                } else {
                    $this->flashSession->error('Failed SoftDeleting <strong>' . $ident . '</strong>!');
                }
            }
            return $this->redirect_to($this->get_next_url());
        }

        // Setup view vars
        $this->tag->setTitle('SoftDelete Confirmation');
        Tag::setDefault('entity_id', $entity_id);
        $this->view->setVar('entity_id', $entity_id);
        $this->view->setVar('entities', $entities);
        $this->view->setVar('model', $model);
    }

    public function refreshOfflineDescriptionAction()
    {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['msg'] = 'Bad Request';
        } else {
            $ad = new Ads();
            // clear the offline description!
            $_POST['ad_description_offline'] = null;
            $parametrizator = new Parametrizator();
            $parametrizator->setValidation((new \Baseapp\Library\Validations\AdsBackend())->get());
            $parametrizator->setModule('backend');
            $parametrizator->setMode('edit');
            $parametrizator->setCategoryById($this->request->getPost('category_id'));
            $parametrizator->setAd($ad);
            $parametrizator->setRequest($this->request);
            $parametrizator->prepare_and_validate();
            $parametrizator->parametrize();

            if (!empty( $ad->description_offline)) {
                $export2avus = new ExportToAvus($ad);
                $ads_avus_category = $export2avus->get_offline_category_id($ad);

				//20.12.2016 - nema više gradela, samo u Issues.php
				//$gradele = strpos( $ad->description_offline >= 0 ) ? ' ' : ' ###';
				$gradele = '';
				
                $response_array['status'] = true;
                $response_array['data']   = array(
                    'avus'    => '[AVUS ID: ' . ($ads_avus_category ? '<span class="text-success">' . $ads_avus_category . '</span>' : '<span class="text-danger">unknown</span>') . ']',
                    'content' => $ad->description_offline.$gradele.'<Tel>'.$ad->phone1.'</Tel>'.($ad->phone2 ? ", <Tel>$ad->phone2</Tel>": "")
                );
            } else {
                $response_array['msg'] = 'Not enough parameters were filled!';
            }
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    /**
     * @param Ads $entity
     * @param Categories $category
     */
    protected function setup_products_chooser(Ads $entity, Categories $category) {
        // only certain roles have ability to attach/update ad's product
        if ($this->auth->logged_in(array('admin', 'supersupport', 'sales'))) {
            $chooser = $entity->getProductsChooser($this->request, 'backend', $category);
            $this->view->setVar('products_chooser_markup', $chooser->getMarkup());
        }
    }

    protected function print_chosen_products(Ads $entity) {
        $chosen_products = array();

        $ads_products = $entity->getProducts();
        if (isset($ads_products['online'])) {
            $chosen_products[] = '<h5>Online</h5><div class="radio radio-primary radio-big"><input id="chosen-online-product" type="radio" disabled checked><label for="chosen-online-product"><span class="text-primary text-bold">' . $ads_products['online']->getBillingTitleWithSelectedOption() . '</span></label></div>';
        }
        if (isset($ads_products['offline'])) {
            $chosen_products[] = '<h5>Tisak</h5><div class="radio radio-primary radio-big"><input id="chosen-offline-product" type="radio" disabled checked><label for="chosen-offline-product"><span class="text-primary text-bold">' . $ads_products['offline']->getBillingTitleWithSelectedOption() . '</span></label></div>';
        }

        if (count($chosen_products)) {
            $this->view->setVar('products_chooser_markup', implode(PHP_EOL, $chosen_products));
        }
    }

    /**
     * spamToggle Action
     */
    public function spamToggleAction($entity_id) {
        $entity_id = (int)$entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for this action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Models\Ads */
        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Models\Ads */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($entity->spamToggle()) {
            $this->flashSession->success('Spam status for ad <strong>' . $entity->ident() . '</strong> changed successfully.');
        } else {
            $this->flashSession->error('Failed changing spam status for ad <strong>' . $entity->ident() . '</strong>.');
        }

        return $this->redirect_back();
    }

    /**
     * activeToggle Action
     */
    public function activeToggleAction($entity_id) {
        $entity_id = (int)$entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for this action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Models\Ads */
        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Models\Ads */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($entity->activeToggle()) {
            $this->flashSession->success('Active status for ad <strong>' . $entity->ident() . '</strong> changed successfully.');
        } else {
            $this->flashSession->error('Failed changing active status for ad <strong>' . $entity->ident() . '</strong>.');
        }

        return $this->redirect_back();
    }

}
