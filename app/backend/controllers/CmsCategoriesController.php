<?php

namespace Baseapp\Backend\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Models\CmsCategories;
use Baseapp\Traits\CrudActions;

/**
 * Backend CmsCategories Controller
 */
class CmsCategoriesController extends IndexController
{
    use CrudActions;

    public $crud_model_class = 'Baseapp\Models\CmsCategories';

    protected $allowed_roles = array('admin', 'content');

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->tag->setTitle('CMS categories');

        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/vendor/jquery.nestedSortable.js');
        $this->assets->addJs('assets/backend/js/nested-items-sortable.js');
        $this->assets->addJs('assets/backend/js/cms-categories-index.js');

        $model = new CmsCategories();
        $tree = $model->getTree();
        // TODO: since categories are using a single root tree, everything
        // is wrapped within a master root node, which has id=1 currently,
        // so this grabs those children. But this feels very hackish.
        // Need to try to come up with a better api for this...
        $tree_root_id = 1;
        Tag::setDefault('tree_root_id', $tree_root_id);
        $categories = $tree[$tree_root_id]->children;
        $this->view->setVar('categories', $categories);

        $this->view->setVar('available_item_actions', array(
            array(
                'path' => 'admin/cms-categories/edit/',
                'name'   => '<i class="fa fa-edit fa-fw"></i>Edit'
            ),
            array(
                'path' => 'admin/cms-categories/create/',
                'name'   => '<i class="fa fa-code-fork fa-fw"></i>New sub'
            ),
            array(
                'path' => 'admin/cms-categories/delete/',
                'name'   => '<i class="fa fa-trash-o text-danger fa-fw"></i>'
            )/*,
            array(
                'path' => 'admin/categories/active-toggle/',
                'name'   => '<i class="fa fa-trash-o text-danger fa-fw"></i>'
            )*/
        ));
    }

    /**
     * Create Action
     *
     * @param int|string|null $parent_id
     *
     * @return void|\Phalcon\Http\ResponseInterface
     */
    public function createAction($parent_id = null)
    {
        if ($this->request->isPost() && $this->request->hasPost('parent_id')) {
            $parent_id = (int) $this->request->getPost('parent_id');
        } elseif (is_numeric($parent_id)) {
            $parent_id = (int) $parent_id;
        } else {
            $parent_id = 1;
        }

        $type = 'magazine';
        if ($this->request->isPost() && $this->request->hasPost('type')) {
            $type = (string) $this->request->getPost('type');
        }

        $template = CmsCategories::TEMPLATE_INHERIT;
        if ($this->request->isPost() && $this->request->hasPost('template')) {
            $template = $this->request->getPost('template');
        }

        /* @var $model \Baseapp\Models\CmsCategories */
        /* @var $parent \Baseapp\Models\CmsCategories */
        $model = $this->crud_model_class;
        $parent = $model::findFirst($parent_id);

        if (!$parent) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $parent_id));
            return $this->redirect_back();
        }

        $this->tag->setTitle('Add new category');
        $this->view->setVar('form_title_long', 'Add category');

        $this->add_common_crud_assets();

        // Use the same form/view as editAction
        $this->view->pick('cms-categories/edit');

        $entity = new CmsCategories();
        if ($this->request->isPost()) {
            $save_action = $this->get_save_action();
            $next_url = $this->get_next_url();
            $created = $entity->add_new($this->request, $parent);
            if ($created instanceof CmsCategories) {
                $this->flashSession->success('<strong>Successfully created new category!</strong> ');
                if ('save' === $save_action) {
                    $next_url = 'admin/cms-categories/edit/' . $entity->id;
                }
                return $this->redirect_to($next_url);
            } else {
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
                $this->view->setVar('errors', $created);
            }
        } else {
            $entity->active = 1;
        }

        $this->type_dropdown($type);
        $this->template_dropdown($template);
        $this->parent_id_dropdown($parent_id);

        $this->view->setVar('category', $entity);
    }

    /**
     * Edit Action
     *
     * @param $entity_id
     *
     * @return void|\Phalcon\Http\ResponseInterface
     */
    public function editAction($entity_id)
    {
        $this->tag->setTitle('Edit category');
        $this->view->setVar('form_title_long', 'Edit category');

        $this->add_common_crud_assets();

        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for edit action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Models\CmsCategories */
        /* @var $entity \Baseapp\Models\CmsCategories */
        $model = $this->crud_model_class;
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        $parent_id = $this->request->isPost() ? $this->request->getPost('parent_id') : $entity->parent()->id;
        $type      = $this->request->isPost() ? $this->request->getPost('type') : $entity->type;
        $template  = $this->request->isPost() ? $this->request->getPost('template') : ($entity->template ? $entity->template : CmsCategories::TEMPLATE_INHERIT);

        if ($this->request->isPost()) {
            $save_action = $this->get_save_action();
            $next_url = $this->get_next_url();
            $saved = $entity->save_changes($this->request, CmsCategories::findFirst($parent_id));
            if ($saved instanceof CmsCategories) {
                $this->flashSession->success('<strong>Category saved successfully!</strong>');
                if ('save' === $save_action) {
                    return $this->redirect_self();
                } else {
                    return $this->redirect_to($next_url);
                }
            } else {
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
            }
        }

        $disabled_categories = $entity->getChildrenIDs(true);
        $this->type_dropdown($type);
        $this->template_dropdown($template);
        $this->parent_id_dropdown($parent_id, $disabled_categories);

        $this->view->setVar('category', $entity);
    }

    /**
     * Save categories order Action
     */
    public function saveOrderAction()
    {
        if ($this->request->isPost()) {

            $model = new CmsCategories();
            $root = $model->getRoot();
            $tree = $root->getTree();

            if (!$root) {
                $this->flashSession->error('Tree root not found');
                return $this->redirect_back();
            }

            $items = $this->request->getPost('categories_order');
            $items = json_decode($items, true);

            if ($root->updateTreeOrder($items, $tree)) {
                $this->flashSession->success('<strong>Categories reordered successfully!</strong>');
            } else {
                $this->flashSession->error('<strong>Error!</strong> Categories were not reordered successfully!');
            }
            return $this->redirect_to('admin/cms-categories');
        } else {
            $this->flashSession->error('<strong>Error!</strong> Method not allowed!');
            return $this->redirect_back();
        }
    }

    /**
     * activeToggle Action
     */
    public function activeToggleAction($category_id = null)
    {
        if ($category = CmsCategories::findFirst(array('id=:category_id:', 'bind' => array('category_id' => $category_id)))) {
            if ($category->activeToggle()) {
                $this->flashSession->success('Active status for category <strong>' . $category->name . '</strong> changed successfully.');
            } else {
                $this->flashSession->error('Failed changing active status for category <strong>'.$category->name.'</strong>.');
            }
        } else {
            $this->flashSession->error('Category with ID: <strong>'.$category_id.'</strong> not found.');
        }

        return $this->redirect_back();
    }

    /**
     * Helper method to generate parent_id dropdown
     *
     * @param integer $preselected_category_value   Value to preselect
     * @param array                                 Array of values to disable in dropdown
     */
    public function parent_id_dropdown($preselected_category_value = 1, $disabled = array())
    {
        Tag::setDefault('parent_id', $preselected_category_value);

        $parent_id_dropdown = Tag::select(array(
            'parent_id',
            CmsCategories::findFirst(1)->getSelectValues(),
            'disabled' => $disabled,
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('parent_id_dropdown', $parent_id_dropdown);
    }

    /**
     * Helper method to generate type dropdown
     *
     * @param integer $preselected_type_value   Value to preselect
     */
    public function type_dropdown($preselected_type_value = 'magazine')
    {
        Tag::setDefault('type', $preselected_type_value);

        $type_dropdown = Tag::select(array(
            'type',
            CmsCategories::getTypes(),
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('type_dropdown', $type_dropdown);
    }

    /**
     * Helper method to generate type dropdown
     *
     * @param integer $preselected_type_value   Value to preselect
     */
    public function template_dropdown($preselected_template_value = CmsCategories::TEMPLATE_DEFAULT)
    {
        Tag::setDefault('template', $preselected_template_value);

        $template_dropdown = Tag::select(array(
            'template',
            CmsCategories::getTemplates($withInherit = true),
            'class' => 'form-control show-tick'
        ));

        $this->view->setVar('template_dropdown', $template_dropdown);
    }

    public function add_common_crud_assets()
    {
        $this->assets->addJs('assets/backend/js/cms-categories-edit.js');
    }
}
