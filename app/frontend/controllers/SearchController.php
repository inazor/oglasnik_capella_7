<?php

namespace Baseapp\Frontend\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\AdsFiltering;
use Baseapp\Library\GPSHandler;
use Baseapp\Library\MapSearch;
use Baseapp\Models\Ads;
use Baseapp\Traits\RandomSpecialProductsHelpers;
use Baseapp\Traits\MapSearchTrait;

/**
 * Frontend Search Controller
 */
class SearchController extends IndexController
{
    use RandomSpecialProductsHelpers;
    use MapSearchTrait;

    protected $sort_data;
    protected $maintain_sort_param = false;

    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        $this->add_default_sidebar_banner_zones();

        $this->add_banner_zone('mobile_top', '2285080', '<div id="zone2285080" class="goAdverticum"></div>');
        $this->add_banner_zone('mobile_pagination', '2285057', '<div id="zone2285057" class="goAdverticum"></div>');

        $this->add_banner_zone('listing_7', '2285047', '<div id="zone2285047" class="goAdverticum"></div>');
        $this->add_banner_zone('listing_14', '4084895', '<div id="zone4084895" class="goAdverticum"></div>');

        // Removing certain banners for searchByLocation action
        if ('map' == $dispatcher->getActionName()) {
            $this->remove_banner_zone('billboard');
            $this->remove_banner_zone('background');
        }

        $this->view->setVar('banner_zone_ids', $this->get_active_zone_ids());
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        $this->setLayoutFeature('mobile_search_bar_toggler', false);
        $this->setLayoutFeature('mobile_search_bar_visible_initially', true);

        $this->view->setVar('form_action', $this->requested_uri);

        // set the appropriate ads listing viewType (grid/list) variable
        $this->view->setVar('viewType', $this->getViewTypeData());

        $searchTerm = $this->request->has('q') ? trim($this->request->get('q')) : null;

        $searchCategoryId = '';
        if ($this->request->has('category_id')) {
            $searchCategoryId = intval($this->request->get('category_id'));
            if ($searchCategoryId <= 0) {
                $searchCategoryId = '';
            }
        }

        Tag::setDefault('q', $searchTerm);
        $this->view->setVar('main_search_term', $searchTerm);
        $this->view->setVar('main_search_category', $searchCategoryId);

        // Bail early with error if no search term
         if (!$searchTerm) {
            $this->flashSession->error('Upišite pojam za pretragu');
            return;
        }

        // Bail early with error if search term is not long enough
        $len = mb_strlen($searchTerm, 'UTF-8');
        if ($len < 2) {
            $this->flashSession->error('Upišite pojam od barem dva znaka!');
            return;
        }

        // Keeping sort-related data in a single place as its used all over the controller
        $this->sort_data = $this->getSortParamData();
        if ($this->sort_data['current'] !== $this->sort_data['default']) {
            $this->maintain_sort_param = true;
        }

        // Set page title
        $page_title = 'Oglasnik pretraga';
        if ($searchTerm) {
            $page_title = $searchTerm . ' - ' . $page_title;
        }
        $this->tag->setTitle($page_title); // setTitle() does escaping...
        $og_meta = array(
            'og:title' => $page_title,
            'og:url' => $this->url->get($this->requested_uri), // contains scheme, host, etc
        );
        $this->setOpenGraphMeta($og_meta);

        // Quickly check if searched string is an ad id (all digits), if so search for
        // such an Ad and redirect to it if found
        if (ctype_digit($searchTerm)) {
            $ad = Ads::findFirst($searchTerm);
            if ($ad) {
                return $this->redirect_to($ad->get_frontend_view_link());
            }
        }

        $adsFiltering = new AdsFiltering();
        $adsFiltering->cacheResults(true);
        $adsFiltering->cacheLifeTime(120);
        $adsFiltering->cachePrefix('search');
        $adsFiltering->setLoggedInUser($this->auth->get_user());
        $adsFiltering->setSortParamData($this->getSortParamData());
        $adsFiltering->setSearchTerm($searchTerm);
        $adsFiltering->setBaseURL('search');
        $adsFiltering->allowSpecialProducts(false);
        $adsFiltering->showOnlyRootCategoriesCounts(false);
        if ($searchCategoryId) {
            $adsFiltering->setAdCategoryByID($searchCategoryId);
            $adsFiltering->alwaysShowCompleteCategoriesTree(false);
        } else {
            $adsFiltering->alwaysShowCompleteCategoriesTree(true);
        }

        $ads = $adsFiltering->getAds();

        // Reset all `isPushable` keys back to false in order to prevent showing pushup upsell on search listings
        // https://trello.com/c/m2CTguAD/576
        if (!empty($ads)) {
            $ads = array_map(function ($item) {
                $item['isPushable'] = false;

                return $item;
            }, $ads);
        }

        if ($flashSession = $adsFiltering->getFlashSession()) {
            $this->flashSession->$flashSession['type']($flashSession['text']);
        }
        if ($adsFiltering->shouldTrigger404()) {
            return $this->trigger_404();
        }
        $specialProductsArray = $adsFiltering->getSpecialProducts($hydrate = true, $limit = 3, $random = true);
        if (!$adsFiltering->hasError()) {
            $this->buildSortingDropdown();
            if ($specialProductsArray) {
                if (is_array($ads) && !empty($ads)) {
                    $ads = array_merge($specialProductsArray, $ads);
                }
            }
            $this->view->setVar('ads', $ads);
            $totalAdsCount = $adsFiltering->getTotalAdsCount();
            $this->view->setVar('total_items', $totalAdsCount);
            if ($pagination = $adsFiltering->getPagination()) {
                $this->view->setVar('pagination_links_top', $pagination['top']);
                $this->view->setVar('pagination_links', $pagination['bottom']);
            }
            if ($generatedViewVars = $adsFiltering->getGeneratedViewVars()) {
                foreach ($generatedViewVars as $varTitle => $varValue) {
                    $this->view->setVar($varTitle, $varValue);
                }
            }
            $this->assets->addJs('assets/js/categories-view.js');
            $this->assets->addJs('assets/js/search-index.js');
            $this->assets->addCss('assets/vendor/slick/slick.css');
            $this->assets->addJs('assets/vendor/slick/slick.min.js');
        }
    }


    /**
     * Search by GPS location
     *
     * 
     * Some helping URI's
     * https://developers.google.com/maps/documentation/javascript/places-autocomplete
     * https://developers.google.com/maps/documentation/javascript/examples/places-autocomplete-addressform
     */
    public function mapAction()
    {
        //--------------------------------------------------------------------------------------------------------------
        // TODO: This whole block should be handled much better, but for now, it's working ...
        $resultingAds   = null;
        $data           = $this->request->getQuery();
        $data['_url']   = null; unset($data['_url']);
        $mapSearch      = new MapSearch($data);
        $mapSearch->setLoggedInUser($this->auth->get_user());
        $mapSearch->setSortParamData($this->getSortParamData());
        if ($searchCategoryId = $this->request->has('category_id') ? intval($this->request->get('category_id')) : null) {
            $mapSearch->setCategoryID($searchCategoryId);
        }

        $moreFilters    = $mapSearch->getSpecificFilters();
        $moreFiltersSQL = isset($moreFilters['filters']) && count($moreFilters['filters']) ? $moreFilters['filters'] : null;

        $mapBounds        = $this->request->has('location_bounds') ? explode(',', $this->request->get('location_bounds')) : null;
        $mapZoomLevel     = $this->request->has('location_zoom') ? (int) $this->request->get('location_zoom') : null;
        $mapCenter        = $this->request->has('location_center') ? explode(',', $this->request->get('location_center')) : null;
        $locationText     = $this->request->has('location_text') ? $this->request->get('location_text') : null;
        $gpsHandler       = new GPSHandler($mapBounds);
        $gpsHandler->setZoomLevel($mapZoomLevel);
        $gpsHandler->setPoint($mapCenter);
        //--------------------------------------------------------------------------------------------------------------

//        $itemsPerPage = $this->request->has('itemsPerPage') && intval($this->request->get('itemsPerPage')) ? intval($this->request->get('itemsPerPage')) : 100;
        $itemsPerPage = 100;

        if ($bounds = $gpsHandler->getBounds()) {
            $resultingAds = $mapSearch->searchAds($bounds, $moreFiltersSQL, $itemsPerPage);
        }

        if ($this->request->isAjax()) {
            $this->disable_view();
            $this->response->setContentType('application/json', 'UTF-8');
            $ajaxResponse = array('status' => false);

            if ($resultingAds) {
                if (!$resultingAds['trigger404']) {
                    $ajaxResponse['status'] = true;
                    $ajaxResponse['md5']    = $resultingAds['md5'];
                    if (isset($resultingAds['flashSession'])) {
                        $this->flashSession->$resultingAds['flashSession']['type']($resultingAds['flashSession']['text']);
                    }
                    $renderArray = array();
                    if (isset($resultingAds['ads'])) {
                        $renderArray['ads'] = $resultingAds['ads'];
                        if (isset($resultingAds['pagination'])) {
                            $renderArray['pagination_links'] = $resultingAds['pagination']['bottom'];
                        }
                    }
                    $ajaxResponse['data'] = $this->simpleView->render('chunks/ads-map', $renderArray);
                }
            }
            $this->response->setJsonContent($ajaxResponse);
            return $this->response;
        } else {
            /**
             * Set some basic layout features
             */
            // disable search bar in header
            $this->setLayoutFeature('search_bar', false);
            // take all possible screen real estate
            $this->setLayoutFeature('full_page_width', true);
            // disable site's footer as we don't need it
            $this->setLayoutFeature('footer', false);

            /**
             * Set some basic assets we need
             */
            $this->tag->setTitle('Pretraga po lokaciji | Oglasnik.hr');
            $this->view->pick('search/map');
            $this->assets->addCss('assets/css/search-map.css');
            $this->assets->addJs('//maps.googleapis.com/maps/api/js?language=hr&key=' . $this->config->google->maps->browserKey . '&libraries=places');
            $this->assets->addJs('assets/vendor/markerclusterer.js');
            $this->assets->addJs('assets/vendor/oms.min.js');
            $this->assets->addJs('assets/js/search-map-widget.js');
            $this->assets->addJs('assets/js/search-map.js');

            /**
             * For now, we hardcode the root category we'll be working with. 
             * In case we set this to null, then we'll look and return all root
             * categories that have a check for 'Search by location' in backend!
             */
            $rootCategoryNodeID = 24;   // nekretnine
            // selected parent category id (on frontend represented as real estate type)
            $selectedCategoryID                = $this->request->has('parent_category_id') ? (int) $this->request->get('parent_category_id') : null;
            // selected category id (on frontend represented as transaction type)
            $selectedTransactionTypeCategoryID = $this->request->has('category_id') ? (int) $this->request->get('category_id') : null;

            if ($selectedTransactionTypeCategoryID && !$selectedCategoryID) {
                $selectedCategoryID = $this->getParentCategoryByTransactionTypeCategoryID($selectedTransactionTypeCategoryID);
            }

            $selectables = $this->getMapSearchableSelectables($rootCategoryNodeID);
            $this->generateMapSearchableCategoryDropdown($selectables['categories'], $selectedCategoryID);
            $this->generateMapSearchableTransactionTypesDropdown($selectables['transactionTypes'], $selectedTransactionTypeCategoryID);

            // continue with generating specific js and css for this page
            if (isset($moreFilters['js']) && count($moreFilters['js'])) {
                $pageSpecificJS = '';
                foreach ($moreFilters['js'] as $js) {
                    $pageSpecificJS .= $js;
                }
                $this->scripts['map_search_more_filters_js'] = $pageSpecificJS;
            }
            if (isset($moreFilters['assets'])) {
                if (isset($moreFilters['assets']['css']) && count($moreFilters['assets']['css'])) {
                    $uniqueCSS = array_unique($moreFilters['assets']['css']);
                    foreach ($uniqueCSS as $css) {
                        $this->assets->addCss($css);
                    }
                }
                if (isset($moreFilters['assets']['js']) && count($moreFilters['assets']['js'])) {
                    $uniqueJS = array_unique($moreFilters['assets']['js']);
                    foreach ($uniqueJS as $js) {
                        $this->assets->addJs($js);
                    }
                }
            }

            $this->buildSortingDropdown();
            $this->view->setVar('moreFiltersBtn', ($this->request->has('more-filters') ? (int) $this->request->get('more-filters', 'int', 0) : 0));
            $this->view->setVar('moreFilters', $moreFilters['html']);
            $this->view->setVar('locationBounds', $gpsHandler->getGoogleMapsBoundsString());
            $this->view->setVar('locationZoom', $mapZoomLevel);
            $this->view->setVar('locationCenter', ($mapCenter ? implode(',', $mapCenter) : ''));
            $this->view->setVar('locationText', $locationText);
            $this->view->setVar('adPriceFrom', ($this->request->has('ad_price_from') && (int) $this->request->get('ad_price_from', 'int', 0) ? (int) $this->request->get('ad_price_from', 'int', 0) : null));
            $this->view->setVar('adPriceTo', ($this->request->has('ad_price_to') && (int) $this->request->get('ad_price_to', 'int', 0) ? (int) $this->request->get('ad_price_to', 'int', 0) : null));
            $this->view->setVar('adPriceCurrency', ($this->request->has('ad_price_code') && $this->request->get('ad_price_code', 'string', 'EUR') ? $this->request->get('ad_price_code', 'string', 'EUR') : 'EUR'));

            $mapMD5 = md5('initial');
            if ($resultingAds) {
                $mapMD5 = $resultingAds['md5'];
                if ($resultingAds['trigger404']) {
                    return $this->trigger_404();
                }
                if (isset($resultingAds['flashSession'])) {
                    $this->flashSession->$resultingAds['flashSession']['type']($resultingAds['flashSession']['text']);
                }
                if (isset($resultingAds['ads'])) {
                    $this->view->setVar('ads', $resultingAds['ads']);

                    if (isset($resultingAds['pagination'])) {
                        //$this->view->setVar('pagination_links_top', $resultingAds['pagination']['top']);
                        $this->view->setVar('pagination_links', $resultingAds['pagination']['bottom']);
                    }
                }
            }
            $this->view->setVar('mapMD5', $mapMD5);
        }
    }
}
