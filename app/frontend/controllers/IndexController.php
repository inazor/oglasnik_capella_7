<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Library\StatsFetcher;
use Baseapp\Models\MiscSettings;
use Phalcon\Dispatcher;
use Baseapp\Controllers\BaseFrontendController;
use Baseapp\Models\AdsHomepage;
use Baseapp\Models\Sections;

/**
 * Frontend Index Controller
 */
class IndexController extends BaseFrontendController
{

    private $full_with_homepage_sections = false;

    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        $this->add_default_banner_zones();

        $this->add_banner_zone('home_mobile', '2285087', '<div id="zone2285087" class="goAdverticum"></div>');
        $this->add_banner_zone('megabanner', '4447721', '<div id="zone4447721" class="goAdverticum"></div>');

        if (!$this->full_with_homepage_sections) {
            $this->add_banner_zone('home_300x250_s1', '4447618', '<div id="zone4447618" class="goAdverticum"></div>');
            $this->add_banner_zone('home_300x250_s2', '4447626', '<div id="zone4447626" class="goAdverticum"></div>');
            $this->add_banner_zone('home_300x250_s3', '4447630', '<div id="zone4447630" class="goAdverticum"></div>');
            $this->add_banner_zone('home_300x250_s4', '4447635', '<div id="zone4447635" class="goAdverticum"></div>');
            $this->add_banner_zone('home_300x250_s5', '4447717', '<div id="zone4447717" class="goAdverticum"></div>');
        }
/*
        $this->add_banner_zone('home_300x250', '2285021', '<div id="zone2285021" class="goAdverticum"></div>');
        $this->add_banner_zone('home_300xwhatever', '2285020', '<div id="zone2285020" class="goAdverticum"></div>');
*/
        $this->view->setVar('banner_zone_ids', $this->get_active_zone_ids());
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        $title = 'Plavi oglasnik: besplatni mali oglasi';
        $desc = '250.000 oglasa garantira da ćete pronaći apsolutno sve što Vam treba! Jedino mjesto za Vašu ponudu, potražnju, iznajmljivanje, unajmljivanje i zamjenu.';

        $this->tag->setTitle($title);
        $this->site_desc = $desc;

        // Build and set opengraph meta tags for the homepage
        $og_meta = array(
            'og:title' => $title,
            'og:type' => 'website',
            'og:url' => $this->url->get(),
            'og:description' => $desc,
            'og:image' => $this->url->get('assets/img/fb-homepage-og-image-01.jpg')
        );
        $this->setOpenGraphMeta($og_meta);

        // Sitelinks search box url
        $this->view->setVar('slsb_url', $this->url->get());

        $this->view->setVar('fb_pixel', true);

        $this->full_with_homepage_sections = (1 === MiscSettings::getSectionLocationFullWidthValue('homepage'));
        $this->view->setVar('full_with_homepage_sections', $this->full_with_homepage_sections);
        $homepage_sections = Sections::getTreeStructured('homepage', true);
        if ($homepage_sections) {
            $this->view->setVar('homepage_sections', $homepage_sections);
        }

        // $latest_magazine_articles = \Baseapp\Models\CmsArticlesMagazine::getLatest(8);
        $latest_magazine_articles = false;
        if ($latest_magazine_articles) {
            $this->view->setVar('latest_magazine_articles', $latest_magazine_articles);
            $this->assets->addJs('assets/vendor/imagesloaded.pkgd.min.js');
            $this->assets->addJs('assets/vendor/masonry.pkgd.min.js');
        }

        $posaoHr = new \Baseapp\Library\PosaoHR();
        if ($posaoHrClassifieds = $posaoHr->getClassifieds()) {
            $this->view->setVar('posaoHr', $posaoHr);
            $this->view->setVar('posaoHrClassifieds', $posaoHrClassifieds);
        }

        if ($homepageFeaturedShopIds = \Baseapp\Models\UsersShopsFeaturedHomepage::getRandomlyOrderedUsersShopsFeaturedIds()) {
            $cacheResults  = true;
            $cacheLifeTime = 120;
            $this->view->setVar('homepageFeaturedShops', \Baseapp\Models\UsersShopsFeatured::buildShoppingWindowsMarkupByIDs($homepageFeaturedShopIds, $cacheResults, $cacheLifeTime));
        }

        $this->view->setVar('homepage_stats_boxes', StatsFetcher::getHomepageStatsBoxes());

        if (method_exists($this, 'setLayoutFeature')) {
            // Homepage must have search box shown and no toggler present at all
            $this->setLayoutFeature('full_page_width', true);
            $this->setLayoutFeature('search_bar', true);
            $this->setLayoutFeature('mobile_search_bar_toggler', false);
            $this->setLayoutFeature('mobile_search_bar_visible_initially', true);
        }

        //$this->view->setVar('aktualno_contents', MiscSettings::getHomepageAktualnoContents());

        $this->assets->addCss('assets/vendor/slick/slick.css');
        $this->assets->addJs('assets/vendor/slick/slick.min.js');
        $this->assets->addJs('assets/js/index.js');

        // Allow inhouse devs to override stuff on the homepage via js that is under their control
        $this->assets->addJs('http://www.posao.hr/files/download/sponsorship-logo-oglasnik/sponsorship.js');
    }
}
