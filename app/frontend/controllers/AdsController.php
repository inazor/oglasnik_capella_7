<?php

namespace Baseapp\Frontend\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\Utils;
use Baseapp\Library\Validations\ContactForm;
use Baseapp\Models\Ads;
use Baseapp\Traits\ParametrizatorHelpers;
use Phalcon\Mvc\View;
use Phalcon\Validation;
use Baseapp\Traits\InfractionReportsHelpers;

/**
 * Frontend Ads Controller
 */
class AdsController extends IndexController
{
    use ParametrizatorHelpers;
    use InfractionReportsHelpers;

    public $infractionReportModelClass = 'Baseapp\Models\Ads';

    protected $recaptcha_for_all = true;

    /**
     * Helper method to load entity or display Error404
     *
     * @param int $entity_id
     * @param Ads &$entity Ads instance reference
     *
     * @return mixed
     */
    protected function load_or_404($entity_id, &$entity)
    {
        $do_404 = false;

        $entity_id = (int) $entity_id;
        if (!$entity_id) {
            $do_404 = true;
        }

        /* @var $entity Ads */
        $entity = Ads::findFirst($entity_id);

        if (!$entity) {
            $do_404 = true;
        }

        if ($do_404) {
            return $this->trigger_404();
        }
    }

    private function redirect_if_wrong_url(Ads $ad)
    {
        $our_url_full       = $ad->get_frontend_view_link();
        $requested_url_full = $this->url->get($this->requested_uri_no_qs);

        if ($our_url_full !== $requested_url_full) {
            return $this->redirect_to($our_url_full, 301);
        }
    }

    protected function isCaptchaRequired()
    {
        $required = false;

        $auth_user = null;
        if ($this->auth->logged_in()) {
            $auth_user = $this->auth->get_user();
        }

        // Anon users need a captcha (or a global override)
        if (empty($auth_user) || $this->recaptcha_for_all) {
            $required = true;
        }

        return $required;
    }

    /**
     * View Action
     */
    public function viewAction($entity_id, $seo_slug = null)
    {
        $this->add_banner_zone('mobile_top', '2285084', '<div id="zone2285084" class="goAdverticum"></div>');
        $this->add_banner_zone('300x250', '2285081', '<div id="zone2285081" class="goAdverticum"></div>');
        $this->add_banner_zone('300x600', '2285082', '<div id="zone2285082" class="goAdverticum"></div>');

        $using_captcha = $this->isCaptchaRequired();

        /* @var $entity Ads */
        $this->load_or_404($entity_id, $entity);
        if ($entity) {
            // Load breadcrumbs
            // TODO/FIXME: we should be able to dump generated markup from cached categoryTree
            $this->view->setVar('breadcrumbs', $entity->getCategory()->getBreadcrumbs());

            // first test if this as was soft_deleted. if yes, then we should inform the visitor that the ad that was
            // on this URL was either sold of was deleted..
            if ($entity->isSoftDeleted()) {
                $this->tag->setTitle('Oglas koji tražite više ne postoji');
                $this->view->pick('ads/soft-deleted');
                $this->view->setVar('ad', $entity);
                // TODO: do we need to show ads simmilar to this one? Maybe it isn't a bad idea...
                $this->view->setVar('similar_ads', $this->get_similar_ads($entity));
                $this->response->setStatusCode(410, 'Gone');
            } else {
                if ($this->request->isPost()) {
                    $this->check_csrf();

                    $abort               = false;
                    $validation_messages = null;

                    if ($this->auth->logged_in()) {
                        $auth_user = $this->auth->get_user();
                    }

                    // Anon users need a verified captcha response
                    if ($using_captcha) {
                        $g_response = $this->request->getPost('g-recaptcha-response');
                        $validation_messages = new Validation\Message\Group();
                        if (empty($g_response)) {
                            $abort = true;
                            $validation_messages->appendMessage(new Validation\Message('Molimo potvrdite da niste robot', 'captcha'));
                        }
                        if (!$abort) {
                            $recaptcha = new \ReCaptcha\ReCaptcha($this->recaptcha_secret);
                            $response  = $recaptcha->verify($g_response, $this->request->getClientAddress(true));
                            if (!$response->isSuccess()) {
                                $abort = true;
                                foreach ($response->getErrorCodes() as $code) {
                                    $validation_messages->appendMessage(new Validation\Message('Captcha greška: ' . $code, 'captcha'));
                                }
                            }
                        }
                    }

                    // Don't even bother doing our validations if recaptcha failed
                    if (!$abort) {
                        $validation          = new ContactForm();
                        $validation_messages = $validation->validate($this->request->getPost());
                    }

                    if ($this->request->isAjax()) {
                        $this->disable_view();
                        $this->response->setContentType('application/json', 'UTF-8');
                        $response_array = array('status' => false);

                        if (!count($validation_messages)) {
                            if ($entity->send_contact_email($this->request)) {
                                $response_array['status'] = true;
                                $response_array['msg'] = 'Poruka uspješno poslana oglašivaču';
                            } else {
                                $response_array['msg'] = 'Slanje poruke nije uspjelo (serverska greška)!';
                            }
                        } else {
                            // Show errors
                            $errors = array();
                            foreach ($validation_messages as $message) {
                                $errors[] = array(
                                    'field' => $message->getField(),
                                    'value' => $message->getMessage()
                                );
                            }
                            $response_array['msg']    = '<h2>Oops!</h2><p>Molimo ispravite uočene greške.</p>';
                            $response_array['errors'] = $errors;
                        }
                        $this->response->setJsonContent($response_array);
                        return $this->response;
                    } else {
                        if (!count($validation_messages)) {
                            // Sending only if form validation passes
                            if ($entity->send_contact_email($this->request)) {
                                $this->flashSession->success('Poruka uspješno poslana oglašivaču');
                            } else {
                                $this->flashSession->error('Slanje poruke nije uspjelo (serverska greška)!');
                            }

                            return $this->redirect_self();
                        } else {
                            // Show errors
                            $this->view->setVar('errors', $validation_messages);
                            $this->flashSession->error('<h2>Oops!</h2><p>Molimo ispravite uočene greške.</p>');
                        }
                    }
                }

                $this->redirect_if_wrong_url($entity);

                // Setup page title and description
                $meta_desc = Utils::str_truncate($entity->description, 120);
                $this->tag->setTitle($entity->title);
                $this->site_desc = $meta_desc;

                $rendered_ad = $entity->getRenderedAd();
                $this->view->setVar('ad', $entity);
                $this->view->setVar('ad_render', $rendered_ad);

                // Build Open Graph data
                $meta = array(
                    'og:title' => $entity->title,
                    'og:type' => 'product',
                    'og:url' => $entity->get_frontend_view_link(),
                    'og:description' => $this->site_desc,
                );
                if (!empty($rendered_ad['ad_media'])) {
                    $first = isset($rendered_ad['ad_media'][0]) ? $rendered_ad['ad_media'][0] : false;
                    if ($first) {
                        $thumb = $first->get_thumb('GalleryBig', false, $entity->category_id);
                        $meta['og:image'] = $thumb->getFullSrcForEmail();
                    }
                }
                $this->setOpenGraphMeta($meta);

                // Make sure our age checking is finished before doing anything else (unless we're
                // forwarded here, which means age restriction fulfillment has completed successfully
                if (!$this->dispatcher->wasForwarded()) {
                    $restriction        = $entity->getCategory()->getAgeRestriction();
                    if ($restriction) {
                        // Modify the destination url to point back here
                        $restriction['url'] = $entity->get_frontend_view_link();
                    }

                    $restricted = $this->ageRestrictionCheckRequest($restriction, $meta);
                    if ($restricted) {
                        // Just return since ageRestrictionAction() should've created/disabled the output
                        return;
                    }
                }

                $entity_user = $entity->getUserDetails();
                // display users data only if it's not an 'avus' user - imported ads...
                if ($entity_user && $entity_user->username != 'avus') {
                    // check if the ad itself has custom phone numbers on it... if yes,
                    // then we have to remove phone numbers from $entity_user and replace them
                    // with the ones found in the ad
                    if ((isset($entity->phone1) && trim($entity->phone1)) || (isset($entity->phone2) && trim($entity->phone2))) {
                        $entity_user->phone1 = $entity->phone1;
                        $entity_user->phone2 = $entity->phone2;
                    }

                    // set ad_user to template..
                    $this->view->setVar('ad_user', $entity_user);
                }

                $is_print_request = ('print' === $this->request->get('layout_type'));

                if ($is_print_request) {
                    $this->assets->addCss('assets/css/print.css');
                    $this->view->pick('ads/print-preview');
                    $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_LAYOUT);
                } else {
                    // set custom assets if present
                    if (isset($rendered_ad['assets'])) {
                        $this->setup_assets($rendered_ad['assets']);
                    }
                    $this->assets->addJs('assets/js/classified-view.js');

                    // see if current user can favorite this ad?
                    $show_contact_form = true;
                    $can_report_ad     = true;
                    $can_favorite_ad   = true;
                    $loggedin_users_ad = false;
                    if ($this->auth->logged_in()) {
                        $auth_user = $this->auth->get_user();

                        if ($auth_user->id == $entity->user_id) {
                            $loggedin_users_ad = true;
                            $show_contact_form = false;
                            $can_report_ad     = false;
                            $can_favorite_ad   = false;
                        } elseif ($auth_user->get_favorite($entity_id)) {
                            $can_favorite_ad = false;
                        }
                    }

                    // show special warning on ads that have active=0 or that are expired
                    $special_warning = null;
                    if ($entity->active == 0) {
                        $show_contact_form = false;
                        $can_report_ad = false;
                        $special_warning = 'Ovaj oglas trenutno nije aktivan!';
                        if ($entity->sold) {
                            $special_warning = 'Ovaj oglas je prodan!';
                        } elseif ($entity->expires_at < Utils::getRequestTime()) {
                            $special_warning = 'Ovaj oglas je istekao!';
                        }
                    }

                    // Setup re-captcha for anon users (or for everyone if configured as such due to spammers)
                    if ($using_captcha) {
                        $this->assets->addJs('https://www.google.com/recaptcha/api.js?onload=handleMultipleReCaptcha&render=explicit&hl=hr', null, false, array('async' => 'async', 'defer' => 'defer'));
                        $this->view->setVar('recaptcha_sitekey', $this->recaptcha_sitekey);
                    }
                    $this->view->setVar('using_captcha', $using_captcha);

                    $this->view->setVar('special_warning', $special_warning);
                    $this->view->setVar('show_contact_form', $show_contact_form);
                    $this->view->setVar('can_report_ad', $can_report_ad);
                    $this->view->setVar('can_favorite_ad', $can_favorite_ad);

                    //$this->view->setVar('similar_ads', $this->get_similar_ads($entity));
                    $this->view->setVar('similar_ads', null);

                    // Invoke the viewCountsManager service and do the counting
                    $di = $this->getDI();
                    $vcm = $di->get('viewCountsManager');
                    // TODO: when hooked like this, cookie setting doesn't work because it's too late
                    // In order to support that, we'd have to refactor the ViewCountsManager probably
                    // $vcm->registerCheckRequestForShutdown($entity_id);
                    $vcm->checkRequest($entity_id);

                    // Fetches the view count stored in the database. If/when Memcache is used and/or the counting
                    // is done in the shutdown phase, this will show "stale" data (if not using memcache and doing the check
                    // on shutdown it shows data without the "current" request, since shutdown happens after this query on
                    // the database is performed)
                    $view_count = $vcm->getTotalCount($entity_id);
                    $inflated_view_count = Utils::inflate_view_count_numbers($view_count);
                    $this->view->setVar('view_count', $inflated_view_count);
                }
            }
        }
    }

    protected function get_similar_ads(Ads $entity, $limit = 8)
    {
        $similar_ads = $entity::find(array(
            'conditions' => 'category_id = :category_id: AND NOT(id = :id:) AND is_spam=0 AND active=1',
            'bind'       => array(
                'id'          => $entity->id,
                'category_id' => $entity->category_id
            ),
            'order'      => 'sort_date DESC',
            'limit'      => $limit
        ));
        $similar_ads = Ads::getEnrichedFrontendBasicResultsArray($similar_ads);

        return $similar_ads;
    }

    public function makeFavoriteAction($entity_id)
    {
        $this->load_or_404($entity_id, $entity);
        if ($entity) {  // ad is found
            if (!$this->auth->logged_in()) {
                $this->flashSession->error('<strong>Ooops!</strong> Molimo vas da se prijavite u vaš račun kako bi mogli spremiti ovaj oglas u svoj račun...');
                return $this->redirect_to('user/signin?next=oglas/makeFavorite/' . $entity_id);
            }

            $user = $this->auth->get_user();
            if ($user->make_favorite($entity_id)) {

                $di = $this->getDI();
                if ($di->has('url')) {
                    $url = $di->get('url')->get('moj-kutak/spremljeni-oglasi');
                    $success_msg = '<strong>Oglas uspješno dodan u <a href="' . $url . '">spremljene oglase</a>!</strong>';
                } else {
                    $success_msg = '<strong>Oglas uspješno dodan u spremljene oglase!</strong>';
                }

                $this->flashSession->success($success_msg);
                return $this->redirect_to($entity->get_frontend_view_link());
            }
        }
    }

    public function bannerAction()
    {
        $this->view->setRenderLevel(View::LEVEL_AFTER_TEMPLATE);
        $this->view->setLayout('banner-iframed');

        // Reduce the number of queries done
        $this->setLayoutFeature('megamenu', false);

        // Prevent prophiler toolbar rendering if it's turned on
        $this->app->setRenderProphiler(false);

        $mobile = $this->request->get('mobile', 'int', 0);

        if ($mobile) {
            $this->view->setVar('banner_code', '<div id="zone2285087" class="goAdverticum"></div>');
        } else {
            $this->view->setVar('banner_code', '<div id="zone2285610" class="goAdverticum"></div>');
        }
    }

    public function latestAction()
    {
        $page_title = 'Novi oglasi';
        $meta_desc = '';
        $this->tag->setTitle($page_title);
        $this->site_desc = $meta_desc;

        // Build Open Graph data
        $meta = array(
            'og:title' => $page_title,
            'og:type' => 'website',
            'og:url' => $this->url->get($this->requested_uri),
            'og:description' => $this->site_desc,
            // 'og:image' => ''
        );
        $this->setOpenGraphMeta($meta);

        $results = Ads::getFresh(48);
        if (count($results)) {
            $results = Ads::getEnrichedFrontendBasicResultsArray($results);
            $group_size = 8;
            $ads_grouped = array_chunk($results, $group_size, true);
            $this->view->setVar('ads', $ads_grouped);
        }

        $this->assets->addJs('assets/js/classifieds-loader.js');

        $this->view->setVar('ad_iframe_url', $this->url->get('ads/banner'));
        $this->view->setVar('ad_iframe_url_mobile', $this->url->get('ads/banner', array('mobile' => 1)));
    }

    public function popularAction()
    {
        $page_title = 'Popularni oglasi dana';
        $meta_desc = '';
        $this->tag->setTitle($page_title);
        $this->site_desc = $meta_desc;

        // Build Open Graph data
        $meta = array(
            'og:title' => $page_title,
            'og:type' => 'website',
            'og:url' => $this->url->get($this->requested_uri),
            'og:description' => $this->site_desc,
            // 'og:image' => ''
        );
        $this->setOpenGraphMeta($meta);

        $results = Ads::getRecentlyPopular(48);
        if (count($results)) {
            $group_size = 8;
            $ads_grouped = array_chunk($results, $group_size, true);
            $this->view->setVar('ads', $ads_grouped);
        }

        $this->assets->addJs('assets/js/classifieds-loader.js');

        $this->view->setVar('ad_iframe_url', $this->url->get('ads/banner'));
        $this->view->setVar('ad_iframe_url_mobile', $this->url->get('ads/banner', array('mobile' => 1)));
    }
}
