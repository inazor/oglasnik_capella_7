<?php

namespace Baseapp\Frontend\Controllers;

use Baseapp\Bootstrap;
use Baseapp\Library\Products\GroupedProductsChooser;
use Baseapp\Library\Products\OrderableEnabledProductsSelectorManual;
use Baseapp\Library\Utils;
use Baseapp\Library\WspayHelper;
use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Models\Ads;
use Baseapp\Models\Categories;
use Baseapp\Models\Orders;
use Baseapp\Models\Users;
use Baseapp\Traits\BarcodeControllerMethods;
use Baseapp\Traits\ParametrizatorHelpers;
use Baseapp\Traits\AdSubmissionUserValidations;
use Phalcon\Escaper;
use Phalcon\Http\ResponseInterface;
use Phalcon\Dispatcher;
use Phalcon\Mvc\Model;
use Phalcon\Mvc\View;
use Phalcon\Validation;

use Baseapp\Suva\Library\Nava;

class PredajaOglasaController extends IndexController
{
    use ParametrizatorHelpers;
    use BarcodeControllerMethods;
    use AdSubmissionUserValidations;

    const SESSION_TOKEN_NAME = 'tokens';

    // Load zendesk for the entire controller as requested
    protected $load_zendesk_widget_script = true;

    protected function addNoCacheHeaders(ResponseInterface $response = null)
    {
        if (null === $response) {
            $response = $this->response;
        }

        $response->setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $response->setHeader('Expires', 'Mon, 20 Dec 1998 01:00:00 GMT');
        $response->setHeader('Pragma', 'no-cache');

        return $response;
    }

    /**
     * Overriding beforeExecuteRoute in order to provide some global view variables and secure access.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::beforeExecuteRoute($dispatcher);

        // turn off the search bar
        if (method_exists($this, 'setLayoutFeature')) {
            $this->setLayoutFeature('search_bar', false);
        }
    }

    /**
     * Overriding afterExecuteRoute in order to provide some global view variables.
     *
     * @param Dispatcher $dispatcher
     *
     * @return bool
     */
    public function afterExecuteRoute(Dispatcher $dispatcher)
    {
        // Making sure the parent is called too!
        parent::afterExecuteRoute($dispatcher);

        // turn off all banner zones for this controller
        $this->view->setVar('banners', false);
    }

    public function indexAction()
    {
        // QUICKHACK until we get user signup done/handled fully:
        // - only allow logged in users to submit ads for now
        if (!$this->auth->logged_in()) {
            $register_href = $this->url->get('user/signup');
            $msg = 'Predaja oglasa dozvoljena je samo registriranim korisnicima. Molimo prijavite se.<br>' .
                   'Niste registrirani korisnik? Molimo <a href="' . $register_href . '">registrirajte se</a>.';
            $this->flashSession->notice($msg);
            return $this->redirect_signin();
        }
        $this->auth->refresh_user();

        // Make sure our oib/fullName checking is finished before doing anything else (unless we're
        // forwarded here, which means oib fulfillment has completed successfully
        if (!$this->dispatcher->wasForwarded()) {
            $missingUserData = $this->missingUserDataCheckRequest();

            if ($missingUserData) {
                // Just return since missingUserDataCheckRequest should have called required action which should have created/disabled the output
                return;
            }
        }

        // Perform sanity checks for requested step and token vars, and if something is not quite right
        // redirect back to the beginning
        $sanity_checks_ok = $this->isRequestedStepAndTokenValid();
        if (!$sanity_checks_ok) {
            // Bail back to the beginning if anything is weird
            return $this->redirect_to('predaja-oglasa');
        }

        // Prep some variables
        $steps             = $this->getWizardSteps();
        $current_step      = $this->getCurrentStep();
        $current_step_data = $steps[$current_step];

        // Build wizard view markup
        $this->view->setVar('steps_wizard_markup', $this->buildStepsMarkup($current_step));

        // Set appropriate page title
        $page_title = sprintf('Predaja oglasa - Korak %s - %s', $current_step, $current_step_data['title']);
        $this->tag->setTitle($page_title);

        $this->assets->addJs('assets/js/ie-11-nav-cache-bust.js');

        // Grab/set common vars
        $token      = $this->request->get('token', 'string', null);
        $token_data = $this->getSessionTokenData($token);

        switch ($current_step)
        {
            // Category chooser
            case 1:
            default:
                $result = $this->processStep1($token);
                break;

            // Ad submit form
            case 2:
                $result = $this->processStep2($token, $token_data);
                break;

            // Products chooser, potentially creating an order upon submit (if not a free ad)
            case 3:
                // TODO: Should we allow choosing the payment method here already?
                $result = $this->processStep3($token, $token_data);
                break;

            // Payment/Order recap
            case 4:
                // TODO: decouple payment handling into a separate self-contained PaymentController and forward to it
                $result = $this->processStep4($token, $token_data);
                break;
        }

        // If any of the step $result above is already a ResponseInterface instance, return that
        if ($result instanceof ResponseInterface) {
            $result = $this->addNoCacheHeaders($result);
            return $result;
        }

        $this->addNoCacheHeaders();

        // Always pick the "current" view
        $this->view->pick($current_step_data['view']);
        $this->view->setVar('stepType', 'new');
    }

    /**
     * Handles showing and submitting the category chooser
     *
     * @param string|null $token
     *
     * @return mixed|ResponseInterface
     */
    protected function processStep1($token = null)
    {
        // Allows continuation of ad submit process (if there is an ad with this id)
        $ad_id = $this->request->get('ad_id', 'int', $this->getSessionTokenData($token, 'ad_id'));

        /** @var Ads $ad */
        if ($ad_id && $ad = Ads::findFirst($ad_id)) {
            if ($ad->canContinueSubmission($this->auth->get_user()->id)) {
                // remove ad_id parameter from the querystring, we don't need it anymore!
                $_GET['ad_id'] = null;
                unset($_GET['ad_id']);

                // proceed with step 2
                $next_step = 2;
                $token     = $this->createRandomToken();
                $this->storeSessionTokenData(
                    $token,
                    array(
                        'ad_id'       => $ad->id,
                        'category_id' => $ad->category_id,
                        'step'        => $next_step
                    )
                );
                $this->saveAudit('Continue with ad submission process [ID: ' . $ad_id . ']');
                return $this->redirect_to($this->buildStepHref($next_step, $token));
            }
        }

        // Allows pre-defining the chosen/selected category (either via GET or from the session, if there's data there)
        $category_id = $this->request->get('category_id', 'int', $this->getSessionTokenData($token, 'category_id'));

        // Make sure the potentially specified category actually exists
        if ($category_id) {
            $category = $this->getCategoryIfExists($category_id);
            if (!$category) {
                return $this->trigger_404();
            }
        }

        if ($this->request->hasPost('next')) {
            // Submitted, redirect to next step
            $next_step = 2;
            $token     = $this->createRandomToken();
            $this->storeSessionTokenData($token, array('category_id' => $category_id, 'step' => $next_step));

            return $this->redirect_to($this->buildStepHref($next_step, $token));
        } else {
            // No submit (meaning no category was chosen), show a list of categories
            $tree       = $this->getDI()->get(Categories::MEMCACHED_KEY);
            $categories = $tree[1]->children;

            $this->view->setVar('category_id', $category_id);
            $this->view->setVar('current_category_level', (!empty($category) ? $category->level - 1 : 1));
            $this->view->setVar('categories', $categories);
            $this->assets->addJs('assets/js/classified-submit-choose-category.js');
        }
    }

    /**
     * Handles showing/saving the parametrized Ad form
     *
     * @param string $token
     * @param array|null $token_data
     *
     * @return mixed|ResponseInterface
     */
    protected function processStep2($token, $token_data = array())
    {
		$this->flashSession->success("tu sam");
        // We must have token data in the session if we want to be here
        if (empty($token_data)) {
            return $this->redirect_to($this->buildStepHref(1));
        }

        // Check that the specified category actually exists
        $category_id = isset($token_data['category_id']) ? $token_data['category_id'] : null;
        $category    = $this->getCategoryIfExists($category_id);
        if (!$category) {
            // This should not happen under normal circumstances (since
            // we shouldn't even be storing token data for a non-existing category)
            return $this->trigger_404();
        }

        $this->view->setVar('category', $category);

        // Load the ad form page, allowing submit/edit, potentially even logging in

        // TODO: parametrizator will need a rewrite, I feel it's doing a lot of things that could be simplified
        $parametrizator = new Parametrizator();
        $parametrizator->setModule('frontend');

        $ad_id = isset($token_data['ad_id']) ? $token_data['ad_id'] : null;
        if ($ad_id) {
            /** @var Ads $ad */
            $ad = Ads::findFirst($ad_id);

            // Bail back to step 1 in case the ad should not be allowed to proceed
            if (!($this->allowFurtherProcessing($ad))) {
                return $this->redirect_to($this->buildStepHref(1));
            }

            $parametrizator->setCategory($ad->getCategory());
            $parametrizator->setAd($ad);
            $parametrizator->setData($ad->getData());
/*
        } elseif (isset($token_data['ad_preview']) && !empty($token_data['ad_preview'])) {
            var_dump($token_data['ad_preview']->toArray());
            exit;
            $ad = $token_data['ad_preview'];
            $parametrizator->setCategory($category);
            $parametrizator->setAd($ad);
            $parametrizator->setData($ad->getData());
            $parametrizator->setRequest($this->request);
            $parametrizator->parametrize();
            var_dump($parametrizator->getRenderedAd());
            var_dump($ad->toArray());
            exit;
//*/
        } else {
            $ad = new Ads();
            $parametrizator->setCategory($category);
            $parametrizator->setAd($ad);
        }

        // TODO: how to handle user registration?
        // TODO: how the fuck do we do it completely including potentially oauth crap?

        // TODO: If we'll have to implement previewing without any validations (as is the case on Njuskalo),
        // we could do it here maybe - check for preview button submit and handle it (rename the button in the view!)
        if ($this->request->hasPost('preview')) {
            // TODO: Build an Ads object from raw $_POST or something, so that we can store it in session and
            // use it in pregledAction()... That way we don't have to rely on the million validations and/or
            // other tight couplings in Ads model or the Parametrizator classes... But have it build in a
            // way that we can reuse across everything.

            // data using something...
            // Parametrizator
            // ($_POST);
            // var_dump($ad);
            // exit;
            // $this->storeSessionTokenData($token, array('ad_preview' => $ad));
            // return $this->redirect_to($this->buildStepHref(2, $token, 'predaja-oglasa/pregled'));
        }

        // Now checking if submit was pressed and if we should create a new or maybe even update an existing ad
        // Either way, we're either done with this step and moving to the next one or showing errors
        if ($this->request->hasPost('next')) {
            $signed_in = $this->attemptSigninFromRequestData();
            if ($signed_in) {
                // TODO: this is really not the way this should be done...
                if ($this->auth->logged_in()) {
                    $_POST['user_id'] = $this->auth->get_user()->id;
                }
            }

            if ($ad_id) {
                // Updating an existing ad...
                $saved = $ad->frontend_save_changes($this->request);
            } else {
                // Creating a new one
                $saved = $ad->frontend_add_new($this->request);
            }

            if ($saved instanceof Ads) {
                // For now, previewing requires all validations to pass
                if ('preview' === $this->request->getPost('next')) {
                    $next_step = 2;
                    $this->storeSessionTokenData($token, array('ad_id' => $saved->id, 'step' => $next_step));
                    return $this->redirect_to($this->buildStepHref($next_step, $token, 'predaja-oglasa/pregled'));
                } else {
                    $next_step = 3;
                    $this->storeSessionTokenData($token, array('ad_id' => $saved->id, 'step' => $next_step));
                    // comment-out flashMessage for now... they don't want it here :)
                    //$this->flashSession->success('<strong>Super!</strong> Vaš oglas je uspješno spremljen.');
                    return $this->redirect_to($this->buildStepHref($next_step, $token));
                }
            } else {
				// $this->flashSession->success("<pre>".print_r($saved, true)."</pre>");
				
                $this->view->setVar('errors', $saved);
                $this->flashSession->error('<strong>Greška!</strong> Ispravite sva crvena polja.');
                $parametrizator->setRequest($this->request);
                $parametrizator->setErrors($saved);
            }
        }

        $this->view->setVar('ad', $ad);
        $this->addParametrizationAssets();

        $currUser = $this->auth->get_user();
        $phoneNumberType = $currUser && $currUser->country_id ? ($currUser->country_id == 1 ? 'domestic' : 'foreign') : 'auto';
        $this->view->setVar('phoneNumberType', $phoneNumberType);

        $this->assets->addJs('assets/js/classified-submit-classified-edit.js');

        $this->setup_rendered_parameters($parametrizator->render_input());
		

		
    }

    /**
     * Handles the Products chooser, creating/updating the potential Order upon submit
     *
     * @param string $token
     * @param array|null $token_data
     *
     * @return void|ResponseInterface
     */
    protected function processStep3($token = null, $token_data = array())
    {
        // We need token data in order to do anything
        if (empty($token_data)) {
            return $this->redirect_to($this->buildStepHref(1));
        }

        // We shouldn't be here without an ad
        if (!isset($token_data['ad_id']) || empty($token_data['ad_id'])) {
            // Return to step 2, which might even again redirect to step 1, but who cares...
            return $this->redirect_to($this->buildStepHref(2, $token));
        }

        // We could have an order created earlier on step 4 and then have gone back to change it completely
        $order = null;
        if (isset($token_data['order_id']) && !empty($token_data['order_id'])) {
            $order = Orders::findFirst($token_data['order_id']);
        }

        // TODO: handle required $token_data earlier and remove this needless if nesting
        $ad_id = $token_data['ad_id'];
        /**
         * @var $ad Ads
         */
        $ad = Ads::findFirst($ad_id);
        if ($ad) {

            // Bail back to step 1 in case the ad should not be allowed to proceed
            if (!($this->allowFurtherProcessing($ad))) {
                return $this->redirect_to($this->buildStepHref(1));
            }

            $show_offline_phone_warning = false;
            if (!$ad->phone1 && !$ad->phone2) {
                $show_offline_phone_warning = true;
            }
            $this->view->setVar('show_offline_phone_warning', $show_offline_phone_warning);

            $this->setupProductsChooser($ad);

            if ($this->request->hasPost('next')) {
                $result = $this->processStep3Submit($ad, $order, $token);
                if ($result instanceof ResponseInterface) {
                    return $result;
                } elseif ($result instanceof Validation\Message\Group) {
                    $this->view->setVar('errors', $result);
                }
            }
        }
    }

    /**
     * Handles saving chosen products and creating an Order if needed and redirecting to step 4.
     * Also deals with saving/publishing a free Ad and redirecting to "/moj-kutak"
     *
     * @param Ads $ad
     * @param Orders|null $order
     * @param string|null $token
     * @param string $payment_method Optional, defaults to 'offline'
     *
     * @return ResponseInterface|Validation\Message\Group
     */
    protected function processStep3Submit(Ads $ad, Orders $order = null, $token = null, $payment_method = 'offline')
    {
        $ad->handleProductsChooserSaving($this->request, 'frontend');
        $updated = $ad->update();
        if (true === $updated) {
            // If chosen products require any kind of payment, create an order, store it
            // in session token data storage and forward to the payment page/controller (which is a redirect
            // to step 4 currently, but it will probably change in the future)
            if ($ad->hasAnyProductsRequiringPaymentBeforePublication() ) {
                // Creates a new or updates an existing order (depending on value of $order)
                $order = Orders::saveForAd(
                    $ad,
                    $order,
                    null,
                    array(
                        'payment_method' => $payment_method,
                        /**
                         * This makes sure that in a complicated case involving:
                         *
                         * - 1. create an order
                         * - 2. go back, change it to free stuff (which marks the order as STATUS_CANCELLED)
                         * - 3. go back again somehow skipping finishing-up ad-submission with free stuff (which
                         *      should not happen, but, things break, yes?) and changing the products to something
                         *      paid-for (again, somehow)
                         *
                         * we don't end up with an Order with a STATUS_CANCELED state which should really not
                         * be marked as cancelled (well, it was, once, but it was updated again and it now again
                         * requires fulfillment due to having paid-for stuff assigned to this ad).
                         *
                         * So we make sure here that order status always gets set appropriately.
                         *
                         * We could in theory add another STATUS_ flag for this special case if we're
                         * interested in how many people actually do this kind of thing at all...
                         */
                        'status'         => Orders::STATUS_NEW
                    )
                );

                // If successful, go to step 4, otherwise stay and show errors
                if ( $order instanceof Orders ){
                    $next_step = 4;
                    $this->storeSessionTokenData(
                        $token,
                        array(
                            'ad_id'    => $ad->id,
                            'step'     => $next_step,
                            // 'order'    => $order,
                            'order_id' => $order->id
                        )
                    );

                    // change the 'latest_payment_state' to 'ordered' ... in case user will pay by creditcard, then
                    // this state will be changed to paid, otherwise, it will stay like this until it's paid
                    $ad->latest_payment_state = Ads::PAYMENT_STATE_ORDERED;
                    $ad->update();
					
					//IN: updateanje ad-a - dodavanje Suvinih podataka
					// \Baseapp\Suva\Library\libFrontend::fUpdateAdForSuva($ad, true);

                    return $this->redirect_to($this->buildStepHref($next_step, $token));
                } 
				else {
                    $this->flashSession->error('<strong>Greška!</strong> Došlo je do greške prilikom spremanja narudžbe..');

                    $errors         = new Validation\Message\Group();
                    $errors->appendMessage(new Validation\Message($this->modelMessagesTransformer($order)));

                    return $errors;
                }
            } 
			else {
				//IN: i u slučaju da je sve free kreira se order i order itemi 
				
                $order = Orders::saveForAd(
                    $ad,
                    $order,
                    null,
                    array(
                        'status'         => Orders::STATUS_COMPLETED
                    )
                );				
				
                if ( $order instanceof Orders ){
					//IN: ako su free itemi dodani u order dalje ide kao i da nije bilo ordera
					
					// No payments required of any kind, set success and clear current token's
					// stored session data and redirect the user somewhere?
					$this->deleteSessionTokenData($token);
					$ad->latest_payment_state = Ads::PAYMENT_STATE_FREE;
					$this->saveAudit('Ad with free product saved.');
					$ad->verify_and_publish_ad('frontend');
					$saved = $ad->save();
					
					// //IN: updateanje ad-a dodavanje Suvinih podataka
					// if ($saved){
						// \Baseapp\Suva\Library\libFrontend::fUpdateAdForSuva($ad, true);
					// }
 
					// TODO: There are probably various different messages we need to show based on
					// different moderation statuses and moderation requirements, no?
					if ('ok' !== $ad->moderation) {
						// Moderation different than 'ok'
						// Now, we have to check if this category is 'post' or 'pre' moderation type

						if ('pre' === $ad->getCategory()->getModerationType()) {
							$this->flashSession->success('<strong>Super!</strong> Vaš oglas je uspješno predan i biti će objavljen nakon moderacije!');
							return $this->redirect_to('moj-kutak/svi-oglasi');
						} else {
							$this->flashSession->success('<strong>Super!</strong> Vaš oglas je uspješno predan!');
							$link = $ad->get_frontend_view_link();
							return $this->redirect_to($link);
						}
					} else {
						// Moderation 'ok' -> we're golden...
						$this->flashSession->success('<strong>Super!</strong> Vaš oglas je uspješno objavljen!');
						$link = $ad->get_frontend_view_link();
						return $this->redirect_to($link);
					}
                } 
				else {
					//IN: ako je bilo greške kod snimanja free order itema, obrada greške ista kao i kod paid ordera
                    $this->flashSession->error('<strong>Greška!</strong> Došlo je do greške prilikom spremanja narudžbe...');
					// foreach($order->getMessages as $msg)
					$this->flashSession->error(get_class($order));
					return;
                    $errors         = new Validation\Message\Group();
                    $errors->appendMessage(new Validation\Message($this->modelMessagesTransformer($order)));

					
					
                    return $errors;
                }
            }
        } 
		else {
            $this->flashSession->error('<strong>Greška!</strong> Greška prilikom odabira proizvoda.');
        }
    }

    /**
     * Handles payment/order recap + payment method chooser + cc payment provider handling + finish up when done
     *
     * @param null $token
     * @param array|null $token_data
     *
     * @return ResponseInterface
     */
    protected function processStep4($token = null, $token_data = array())
    {
        if (!$this->hasRequiredTokenData($token_data, array('ad_id', 'order_id'))) {
            return $this->redirectToLastKnownStepWithMessage($token, $token_data);
        }

        $order_id = $token_data['order_id'];
        $order    = Orders::findFirst($order_id);
        /** @var Ads $ad */
        $ad = Ads::findFirst($token_data['ad_id']);

        if (!$order) {
            $message = '<strong>Ooops!</strong> Nedostaju podaci o vašoj narudžbi!';
            return $this->redirectToLastKnownStepWithMessage($token, $token_data, $message);
        }

        if (!$ad) {
            $message = '<strong>Ooops!</strong> Nedostaju podaci o vašem oglasu!';
            return $this->redirectToLastKnownStepWithMessage($token, $token_data, $message);
        }

        if (!$this->allowFurtherProcessing($ad)) {
            $message = '<strong>Ooops!</strong> Nastavak predaje tog oglasa nije moguć!';
            return $this->redirectToLastKnownStepWithMessage($token, $token_data, $message);
        }

        /**
         * zyt: 23.09.2016.
         * The Order we're currently working with could've been cancelled either via /moj-kutak/otkazi-narudzbu or
         * by some other means in between the time it was created and now that we landed on this url again...
         * In case we're here and it's a cancelled order, handle all the things that are done
         * on processStep3Submit in order to avoid duplicating code (and so that we don't have
         * ads with latest_payment_state=cancelled when we're about to re-new the now-cancelled
         * order that still appears to be existing in the session since step4 was never fully
         * submitted).
         */
        if ($order->status == Orders::STATUS_CANCELLED) {
            // $order->save(array('status' => Orders::STATUS_NEW));
            $result = $this->processStep3Submit($ad, $order, $token);
            if ($result instanceof ResponseInterface) {
                return $result;
            }/* elseif ($result instanceof Validation\Message\Group) {
                $this->view->setVar('errors', $result);
            }*/
        }

        // Checking 'offline' payment submit confirmation and redirecting
        if ($this->request->isPost('next')) {
            // we should mark the ad as waiting for paiment!
            $ad->active = 0;
            // TODO: do we need this here? We already set it on processStep3Submit()
            $ad->latest_payment_state = Ads::PAYMENT_STATE_ORDERED;
            $ad->save();

            // We're done here, the order should be fulfilled offline
            $this->deleteSessionTokenData($token);
            $this->flashSession->success('<strong>Super!</strong> Vaša narudžba je zaprimljena i čeka plaćanje!');
            return $this->redirect_to('moj-kutak');
        }

        // Build payment thing
        $url_args = array(
            'step'  => 4,
            'token' => $token
        );
        $payment_data = array(
            'ShoppingCartID' => $order->getPbo(),
            'TotalAmount'    => $order->getTotal(),
            'ReturnURL'      => $this->url->get('predaja-oglasa', $url_args + ['return' => 1]),
            'CancelURL'      => $this->url->get('predaja-oglasa', $url_args + ['cancel' => 1]),
            'ReturnErrorURL' => $this->url->get('predaja-oglasa', $url_args + ['error' => 1])
        );
        $helper = new WspayHelper($payment_data, $this->auth->get_user());

        // TODO: check incoming IPG data if present and handle accordingly
        // Example error request we get:
        if ($this->request->getQuery('error')) {
            // predaja-oglasa?step=4&token=83b6fef8f17b772c417f4d8587631b87aafb1032&error=1&CustomerFirstname=sda
            // &CustomerSurname=asd&CustomerAddress=neka+tamo&CustomerCountry=Hrvatska&CustomerZIP=da+nebi
            // &CustomerCity=ma+je&CustomerEmail=asd@asdf.com&CustomerPhone=123456&ShoppingCartID=900000164
            // &Lang=HR&DateTime=20150621022730&Amount=350&ECI=&PaymentType=VISA&PaymentPlan=0000
            // &ShopPostedPaymentPlan=0000&Success=0&ApprovalCode=&ErrorMessage=ODBIJENO
            $escaper = new Escaper();
            $cart_id = $this->request->getQuery('ShoppingCartID', 'string');
            $remote_message = $escaper->escapeHtml($this->request->getQuery('ErrorMessage', 'string'));
            $data = json_encode($this->request->getQuery());

            $log_message = 'WSPay returned an error for Order #' . $escaper->escapeHtml($cart_id) . ': ' . $remote_message;
            $log_message .= "\n" . $data;
            $this->saveAudit($log_message);

            $this->flashSession->error('<strong>Greška</strong> WSPay vratio: ' . $remote_message . '. Molimo pokušajte ponovo ili odaberite drugi način plaćanja.');
        } elseif ($this->request->getQuery('cancel')) {
            // TODO: Do we even have to bother with anything in case of cancel? Why?
        } elseif ($this->request->getQuery('return') && $this->request->getQuery('Success')) {
            // Verifying received data from WSpay
            $query_vars      = $this->request->getQuery();
            $valid_signature = $helper->verifyReturnedSignature($query_vars);
            if (true === $valid_signature) {
                // Great success!
                $save_data = array(
                    'payment_method' => strtolower($this->request->getQuery('PaymentType', 'string')),
                    'approval_code'  => $this->request->getQuery('ApprovalCode')
                );
                $finalized = $order->finalizePurchase($save_data);
                if ($finalized) {
					// error_log("placeno borna");
					//IN: syncanje podataka za suvu
					\Baseapp\Library\libSuva::PredajaOglasaController_UpdateOrderStatusForOnlinePayment( $order );
                    $this->deleteSessionTokenData($token);
                    $this->flashSession->success('<strong>Odlično!</strong> Vaša narudžba je uspješno obrađena!');
                    return $this->redirect_to('moj-kutak');
                } else {
                    $this->flashSession->error('<strong>Greška!</strong> Izvršenje plaćene narudžbe nije uspjelo. Molimo kontaktirajte korisničku podršku!');
                    Bootstrap::log($order->getMessages());
                }
            }
        }

        // Display payment method choices
        $this->view->setVar('order_total_formatted', Utils::format_money($order->getTotal(), true));
        $this->view->setVar('order_pbo', $order->getPbo());
        $this->view->setVar('order_table_markup', $order->getPaymentRecapTableMarkup());
        $this->view->setVar('rendered_form',  $helper->getFormMarkup());

        // $barcode_src = $this->url->get('predaja-oglasa/barcode', array('token' => $token));
        $barcode_src = $order->get2DBarcodeInlineSvg();
        $this->view->setVar('order_id', $order->id);
        $this->view->setVar('payment_data_table_markup', Orders::buildPaymentDataTableMarkup($order, $barcode_src));
        $this->view->setVar('offline_button_markup', Orders::buildOfflineConfirmButtonMarkup('predaja-oglasa', 'Predaj oglas'));

        $this->assets->addJs('assets/js/payment-method-toggle.js');
    }

    /**
     * Previews the Ad before submitting it
     *
     * @param string|null $token
     *
     * @return ResponseInterface
     */
    public function pregledAction($token = null)
    {
        // Simplify the main layout to remove distractions
        $this->setLayoutFeature('full_page_width', true);
        $this->setLayoutFeature('main_nav', false);
        $this->setLayoutFeature('footer_links', false);

        $error   = false;
        $message = null;

        $token = $this->request->getQuery('token', 'string', $token);
        if (!$token) {
            $message = 'Nedostaju podaci o predaji oglasa';
            $error   = true;
        }

        // We need token data in order to do anything
        $token_data = $this->getSessionTokenData($token);
        if (empty($token_data) || !isset($token_data['ad_id']) || empty($token_data['ad_id'])) {
            $message = 'Nedostaju podaci o pregledu/predaji oglasa';
            $error   = true;
        }

        unset($token_data['ad_preview']);
        $this->storeSessionTokenData($token, $token_data);

        // Check if we have any kind of Ad for the token
        $ad = Ads::findFirst($token_data['ad_id']);
        if (!$ad) {
            // Check if we have a preview version that has no validations etc
            $ad = isset($token_data['ad_preview']) ? $token_data['ad_preview'] : null;
            if (!$ad) {
                $message = 'Nedostaju podaci o oglasu';
                $error   = true;
            }
        }

        // TODO: do we care/handle if the ad is fully submitted/published already?

        if ($error) {
            $this->flashSession->error('<h2>Greška!</strong> ' . $message . '');
            return $this->redirect_to($this->buildStepHref(2, $token));
        }

        $back_href   = $this->buildStepHref(2, $token);
        $back_markup = '<section><a href="' . $back_href . '" class="btn btn-primary"><span class="fa fa-fw fa-arrow-circle-left"></span> Povratak na predaju oglasa</a></section>';

        $extra_view_data = array(
            'back_markup' => $back_markup
        );

        $this->setupViewPreviewData($ad, $extra_view_data);
    }

    /**
     * @param Ads $ad
     * @param array $extras Additional view vars/data if needed
     */
    protected function setupViewPreviewData(Ads $ad, $extras = array())
    {
        $page_title = sprintf('Pregled oglasa prije predaje');
        $this->tag->setTitle($page_title . ' - ' . $ad->title);

        $rendered_markup = $ad->getRenderedAd();

        // Load custom assets if needed
        if (isset($rendered_markup['assets'])) {
            $this->setup_assets($rendered_markup['assets']);
        }
        // Load regular assets for the ad view
        $this->assets->addJs('assets/js/classified-view.js');

        $entity_user = $ad->getUserDetails();
        // Check if the ad itself has custom phone numbers on it... if so,
        // then we have to remove phone numbers from $entity_user and replace them
        // with the ones found in the ad
        if ((isset($ad->phone1) && trim($ad->phone1)) || (isset($ad->phone2) && trim($ad->phone2))) {
            $entity_user->phone1 = $ad->phone1;
            $entity_user->phone2 = $ad->phone2;
        }
        $this->view->setVar('ad_user', $entity_user);

        // Quick hack to allow passing additional view data/vars from the action
        if (!empty($extras)) {
            foreach ($extras as $k => $v) {
                $this->view->setVar($k, $v);
            }
        }

        $this->view->pick('chunks/ads-submission-preview');
        $this->view->setVar('ad', $ad);
        $this->view->setVar('ad_render', $rendered_markup);
    }

    /**
     * @return array
     */
    protected function getWizardSteps()
    {
        static $steps = array(
            1 => array(
                'view'  => 'chunks/ads-submission-category-chooser',
                'title' => 'Odabir kategorije'
            ),
            2 => array(
                'view'  => 'chunks/ads-submission-edit',
                'title' => 'Opis oglasa',
            ),
            3 => array(
                'view'  => 'chunks/ads-submission-products-chooser',
                'title' => 'Vrsta oglasa',
            ),
            4 => array(
                'view'  => 'chunks/ads-submission-payment',
                'title' => 'Plaćanje'
            )
        );

        return $steps;
    }

    /**
     * @param int $step
     *
     * @return int
     */
    protected function getCurrentStep($step = 1)
    {
        $checks_ok = $this->isRequestedStepAndTokenValid($step);

        if ($checks_ok) {
            $step = $this->request->get('step', 'int', $step);
        }

        $steps = $this->getWizardSteps();
        if (!isset($steps[$step])) {
            $step = 1;
        }

        return (int) $step;
    }

    protected function buildStepHref($step = 1, $token = null, $uri = 'predaja-oglasa')
    {
        $query = $this->request->getQuery();
        unset($query['_url']);

        if ($step >= 1) {
            $query['step'] = $step;
        }

        if (null !== $token) {
            $query['token'] = $token;
        }

        // having ?step=1 is not needed
        if (1 === $step) {
            unset($query['step']);
        }

        $href = $this->url->get($uri, $query);
        return $href;
    }

    /**
     * Builds and returns the "steps wizard" markup.
     *
     * @param int $active The step considered 'active' or 'current'
     *
     * @return string
     */
    protected function buildStepsMarkup($active = 1)
    {
        $steps = $this->getWizardSteps();

        // Check if we're given a token via GET
        $token = $this->request->get('token', 'string', null);

        $markup = '<div class="row wizard-steps">';

        foreach ($steps as $k => $step_data) {
            $is_active    = ($k == $active);
            $is_done      = ($k < $active);

            $classnames = array();
            if ($is_active) {
                $classnames[] = 'active';
            }
            if ($is_done) {
                $classnames[] = 'done';
            }

            $step_markup = '<span class="wizard-step">' . $step_data['title'] . '</span>';
            if (!$is_active) {
                // Current step not active, but not everything should be clickable, depending on where
                // we are in the process

                // TODO: determine if something else needs to be clickable (and when, if so)
                $do_link = false;

                // If we have a token, check how far we've come according to the
                // token info from the session and do not build links for stuff we haven't
                // already reached
                if ($token) {
                    $token_step = $this->getStepForToken($token);
                    if ($k <= $token_step) {
                        $do_link = true;
                    }
                }

                if ($do_link) {
                    $step_link   = $this->buildStepHref($k, $token);
                    $step_markup = sprintf('<a href="%s">%s</a>', $step_link, $step_markup);
                }
            }

            $classnames_string = '';
            if (!empty($classnames)) {
                $classnames_string = ' ' . implode(' ', $classnames);
            }

            $markup .= '<div class="col-sm-3 col-xs-6' . $classnames_string . '">';
            $markup .= $step_markup;
            $markup .= '</div>';
        }

        $markup .= '</div>';

        return $markup;
    }

    /**
     * @param int $default
     *
     * @return bool True if all checks pass, false otherwise
     */
    protected function isRequestedStepAndTokenValid($default = 1)
    {
        $error = false;

        // Check for GET `step` parameter, falling back to possibly provided $default
        $specified_step = (int) $this->request->get('step', 'int', $default);
        if (!$specified_step || $specified_step < 0) {
            $error = true;
        }

        if (!$error && $specified_step > 1) {
            // First make sure we're within allowed upper bounds
            $steps       = $this->getWizardSteps();
            $steps_total = count($steps);
            if ($specified_step > $steps_total) {
                $error = true;
            }

            // Now make sure we have a token to work with
            $token = null;
            if (!$error) {
                $token = $this->request->get('token', 'string', null);
            }

            // Make sure session-stored step of the token is ok compared
            // to the one specified in the GET parameters
            if (!$error && $token) {
                // Requested step has to be lower or equal to what's stored for it on the server-side
                $stored_step = $this->getStepForToken($token);
                if (!($specified_step <= $stored_step)) {
                    $error = true;
                }

                // Also make sure the requested step actually exists among our defined steps
                if (!$error) {
                    if (!isset($steps[$specified_step])) {
                        $error = true;
                    }
                }
            }
        }

        return !$error;
    }

    /**
     * @param string $token Ad submission token
     * @param array $data Data to store (or merge if the token already exists)
     */
    protected function storeSessionTokenData($token, $data = array())
    {
        $tokens = $this->session->get(self::SESSION_TOKEN_NAME, array());

        if (!isset($tokens[$token])) {
            $tokens[$token] = $data;
        } else {
            $tokens[$token] = array_merge($tokens[$token], $data);
        }

        $this->session->set(self::SESSION_TOKEN_NAME, $tokens);
    }

    /**
     * @param $token string Ad submission token
     * @param null $key Optional, specific key of data stored under the specified $token bucket. Returns null if key does not exist
     *
     * @return array|mixed|null
     */
    protected function getSessionTokenData($token, $key = null)
    {
        $data = null;

        $tokens = $this->session->get(self::SESSION_TOKEN_NAME, array());

        if (isset($tokens[$token])) {
            $data = $tokens[$token];
            // Allows fetching a single key (if specified) from the store if it exists. Returns null if it doesn't.
            if (null !== $key) {
                if (isset($data[$key])) {
                    $data = $data[$key];
                } else {
                    $data = null;
                }
            }
        }

        return $data;
    }

    /**
     * @param $token
     *
     * @return bool
     */
    protected function deleteSessionTokenData($token)
    {
        $tokens = $this->session->get(self::SESSION_TOKEN_NAME, array());
        if (isset($tokens[$token])) {
            unset($tokens[$token]);
            $this->session->set(self::SESSION_TOKEN_NAME, $tokens);
            return true;
        }

        return false;
    }

    /**
     * @param string|null $token
     *
     * @return int
     */
    protected function getStepForToken($token = null)
    {
        $step = 1;

        $stored_step = $this->getSessionTokenData($token, 'step');
        if (null !== $stored_step) {
            $step = $stored_step;
        }

        return $step;
    }

    protected function setupProductsChooser(Ads $ad)
    {
        $post_data = $this->request->getPost();

        $ad_products = $ad->getProducts();

        $selection = new OrderableEnabledProductsSelectorManual($ad->getCategory(), $ad->getUser(), $post_data, $ad_products);
        $selection->process();

        $chooser = new GroupedProductsChooser($selection);

        $chosen_products = $chooser->getChosenProducts('grouped');
        $billing_info = array();
        foreach ($chosen_products as $group => $group_products) {
            foreach ($group_products as &$product) {
                $billing_info[$group][] = $product->getShoppingCartInfo();
            }
        }

        // $this->assets->addCss('assets/vendor/awesome-bootstrap-checkbox.css');
        $this->assets->addJs('assets/js/classified-submit-choose-ad-type.js');

        // Fake the offline group checkbox if an offline product is preselected
        $post_data = $this->request->getPost();
        if (!empty($ad_products['offline'])) {
            $post_data['offline-products'] = true;
        }

        // If there is no post data yet and there aren't any products saved yet,
        // make the offline-products checkbox checked (as requested by Oglasnik)
        if (empty($post_data) && empty($ad_products)) {
            $post_data['offline-products'] = true;
        }

        $this->view->setVar('products_chooser_markup', $chooser->getMarkup($post_data));
        $this->view->setVar('products_cost_total', $chooser->getChosenProductsTotalCostDisplay());
        $this->view->setVar('billing_info', $billing_info);
    }

    public function addParametrizationAssets()
    {
        $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        $this->assets->addCss('assets/vendor/datepicker3.css');
        $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');
        $this->assets->addJs('assets/vendor/bootstrap-datepicker.js');
        $this->assets->addJs('assets/vendor/jquery.autoNumeric.js');
        $this->assets->addCss('assets/vendor/fancyBox/source/jquery.fancybox.css');
        $this->assets->addJs('assets/vendor/fancyBox/source/jquery.fancybox.pack.js');
    }

    /**
     * @param string|null $token
     * @param array|null $token_data
     * @param string|null $message
     *
     * @return \Phalcon\Http\ResponseInterface
     */
    protected function redirectToLastKnownStepWithMessage($token, $token_data, $message = null)
    {
        if (null !== $message) {
            $this->flashSession->error($message);
        }

        if (null !== $token_data && is_array($token_data) && !empty($token_data)) {
            $last_reached_step = $token_data['step'];
        } else {
            $last_reached_step = 1;
        }

        return $this->redirect_to($this->buildStepHref($last_reached_step, $token));
    }

    /**
     * Helper to encapsulate and reduce code duplication when checking required
     * session token data at various stages of the ad submission process
     *
     * @param array|null $data Session token data
     * @param array $keys List of key names in $data which have to exist and be non-empty
     *
     * @return bool False if any data is missing, true if everything's ok
     */
    protected function hasRequiredTokenData(array $data = null, $keys = array())
    {
        $errors = false;

        if (empty($data)) {
            $errors = true;
        }

        if (!is_array($keys) && is_string($keys)) {
            $keys = (array) $keys;
        }

        if (!$errors) {
            foreach ($keys as $key) {
                if (!isset($data[$key]) || empty($data[$key])) {
                    $errors = true;
                    break;
                }
            }
        }

        return !$errors;
    }

    /**
     * Creates a hexadecimal representation of a random token. Resembles
     * an SHA1 hash, but it's not. And it's better (or so they say).
     *
     * @link http://stackoverflow.com/a/14869745
     *
     * The resulting string will be twice as long as the random bytes we generate;
     * Each byte encoded to hex is 2 characters. 20 bytes will be 40 characters of hex.
     * Using 20 bytes, we have 256^20 or 1,461,501,637,330,902,918,203,684,832,716,283,019,655,932,542,976 unique
     * output values. This is identical to SHA1's 160-bit (20-byte) possible outputs.
     *
     * @return string
     */
    protected function createRandomToken()
    {
        $token = bin2hex(openssl_random_pseudo_bytes(20));

        return $token;
    }

    protected function modelMessagesTransformer(Model $model)
    {
        // TODO: extend Phalcon\Validation\Message\Group to encapsulate this via __toString() or something...
        $msgs = array();
        foreach ($model->getMessages() as $msg) {
            $msgs[] = $msg->getMessage();
        }
        $msg_string = implode(', ', $msgs);

        return $msg_string;
    }

    protected function attemptSigninFromRequestData()
    {
        $success = false;

        if ($this->auth->logged_in()) {
            $success = true;
            return $success;
        }

        if ($this->request->hasPost('username') && $this->request->hasPost('password')) {
            $login = $this->auth->login(
                $this->request->getPost('username'),
                $this->request->getPost('password'),
                $this->request->getPost('remember') ? TRUE : FALSE
            );
            if (!$login) {
                // Log the bad signin attempt along with some data
                $log_data = array(
                    'u' => $this->request->getPost('username'),
                    'p' => $this->request->getPost('password'),
                    'remember' => $this->request->getPost('remember') ? 1 : 0
                );
                $this->saveAudit(sprintf('Failed login attempt during ad submission: %s', var_export($log_data, true)));
                // Prepare and set view errors
                $errors = new \Phalcon\Validation\Message\Group();
                $errors->appendMessage(new \Phalcon\Validation\Message('Korisničko ime i/ili lozinka nisu ispravni'));
                $this->view->setVar('errors', $errors);
            } else {
                $success = true;
                // Log the successful signin event
                $this->saveAudit(sprintf('User %s signed in during ad submission', $this->auth->get_user()->ident()));
            }
        }

        return $success;
    }

    /**
     * @param $category_id
     *
     * @return Categories|false
     */
    protected function getCategoryIfExists($category_id)
    {
        $category = false;
        if ($category_id) {
            $category = Categories::findFirst($category_id);
        }

        return $category;
    }

    /**
     * @param Ads $ad
     *
     * @return bool
     */
    protected function allowFurtherProcessing(Ads $ad)
    {
        $allow = true;

        if ($ad->isSoftDeleted()) {
            $allow = false;
        }

        return $allow;
    }
}
