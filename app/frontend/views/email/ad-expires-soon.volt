{# Ad expires soon email tpl #}
<p>Poštovani {{ username }},<br><br>Vaš oglas <b><a href="{{ ad_link }}">{{ ad_title }}</a></b> u rubrici „{{ ad_category_name }}“ istječe za 2 dana.</p>
<p>Podsjećamo vas da, ako još niste kupili, prodali, unajmlili, iznajmili ili poklonili ono što oglašavate, možete dodatno unaprijediti svoj oglas.</p>
<p>U <a href="{{ moj_kutak_link }}">Mom Kutku</a> možete svoj oglas dodatno promovirati uslugom „Skok na vrh“ ili isticanjem oglasa.</p>
<p>Za sva dodatna pitanja, obratite se našoj korisničkoj podršci na broj telefona: <a href="tel:+38516102885">01/6102-885</a> ili putem e-mail adrese <a href="mailto:podrska@oglasnik.hr">podrska@oglasnik.hr.</a></p>
<p>Vaš Oglasnik</p>
