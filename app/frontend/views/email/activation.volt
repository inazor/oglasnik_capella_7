{# Activation email #}
<p>Pozdrav <b>{{ username }}</b>,</p>
<p>Molimo aktivirajte Vaš korisnički račun.</p>
<p>Za aktivaciju korisničkog računa slijedite ovaj link: <a href="{{ url.get('user/activation/' ~ username ~ '/' ~ hash) }}">Aktivacija</a>.</p>
