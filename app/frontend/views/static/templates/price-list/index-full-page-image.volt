{% set inArticle_media = false %}

{% set main_pic_thumb = 'http://placehold.it/1920x520' %}
{% if not (content_media is empty) and content_media.valid() %}
    {% set main_pic = content_media[0] %}
    {% set main_pic_thumb = main_pic.get_thumb('CMS-1500x400').getSrc() %}
{% endif %}

<div class="page-header page-header-user-dashboard">
    <div class="position-relative fixed-height">
        <div class="profile-picture overflow-hidden" style="background-image:url({{ main_pic_thumb }});"></div>
    </div>
</div>

{% if virtualRoot is defined and virtualRoot %}
<div class="user-cp-nav">
    <div class="container">
        <div class="row">
            <div class="col-md-3"><strong>{{ virtualRoot.name }}</strong></div>
            <div class="col-md-9 user-header-nav-wrapper">
                {% set virtualDirectChildren = virtualRoot.getDirectChildren() %}
                {% if virtualDirectChildren|length %}
                <ul>
                    {% for virtualChild in virtualDirectChildren %}
                    <li{{ currentCategorySlug == virtualChild.url ? ' class="active"' : '' }}>
                        {{ currentCategorySlug == virtualChild.url ? '<span>' ~ virtualChild.name ~ '</span>' : linkTo([virtualChild.url, virtualChild.name]) }}
                        {% set virtualChildArticles = virtualChild.getCategoryArticles() %}
                        {% if virtualChildArticles and virtualChildArticles|length %}
                            <ul>
                            {% for virtualChildArticle in virtualChildArticles %}
                                <li{{ (currentArticleSlug|default(null) == virtualChildArticle.slug ? ' class="active"' : '') }}>
                                    <a href="{{ virtualChildArticle.getURI() }}">{{ virtualChildArticle.title }}</a>
                                </li>
                            {% endfor %}
                            </ul>
                        {% endif %}
                    </li>
                    {% endfor %}
                </ul>
                {% endif %}
                <div class="clearfix"><!--IE--></div>
            </div>
        </div>
    </div>
</div>
{% endif %}

<div class="container user-cp">
    <div class="row">
        <div class="side-col col-md-3 margin-top-lg hidden-xs">
            {% set articleRows = category.getCategoryArticles() %}
            {% if articleRows|length %}
            <ul class="user-cp-menu">
                {% for articleRow in articleRows %}
                <li{{ (currentArticleSlug|default(null) == articleRow.slug ? ' class="active"' : '') }}>
                    {{ currentArticleSlug|default(null) == articleRow.slug ? '<span>' ~ articleRow.title ~ '</span>' : '<a href="' ~ articleRow.getURI() ~ '">' ~ articleRow.title ~ '</a>' }}
                </li>
                {% endfor %}
            </ul>
            {% endif %}
        </div>
        <div class="col-md-9 margin-top-lg">
            {% if content is defined and content %}{% include('static/templates/price-list/content') %}{% endif %}
        </div>
    </div>
</div>
