{{ form(section.get_url(), 'id':'section_filters', 'method':'get') }}
    <h1 class="h2-like section-title">{{ section.name|category_name_markup }}</h1>

    <p class="total-results visible-xs hidden-sm hidden-md hidden-lg pad-xs-only-lr">Pronađenih oglasa: {{ total_items|money }}</p>

    {{ shopping_windows_markup }}

    <a name="classifieds"></a>

    {% if ads is defined %}
    <div class="row no-gutters-xs pad-xs-only-lr margin-top-lg">
        <div class="col-md-3 side-col">
            <a class="hidden-md hidden-lg btn btn-md display-block filters-anchor-link" href="#categories-tree-anchor">
                Kategorije
            </a>
        </div>
        <div class="listings-main-col col-md-6">
            <div class="grid-sort-options margin-bottom-sm row no-gutters-xs">
                <div class="col-xs-6 col-sm-7 upper-pagination">{% if pagination_links_top is defined and pagination_links_top %}{{ pagination_links_top }}{% endif %}</div>
                <div class="col-xs-6 col-sm-5 sort-dropdown">{{ sort_dropdown }}</div>
            </div>
        </div>
    </div>
<div class="reordered-on-mobile">
    <div class="row no-gutters-xs">
        <div id="categories-tree-anchor" class="listings-side-col side-col col-md-3">
            {% if searchHasCategories is defined and searchHasCategories %}
            <h2 class="blue"><img class="margin-right-sm" src="{{ url('assets/img/icn_filter_category_02.svg') }}" alt="">Kategorije oglasa</h2>
            <div class="classifieds-categories-listing-sidebar">
                {{ categoryTreeCountsMarkup }}
                {% if needsShowAll %}
                <div class="hidden-filters-more"><a class="all-cats-toggle more-btn" href="{{ showAllToggleHref }}">{{ showAllToggleText }}</a></div>
                {% endif %}
            </div>
            {% endif %}
            {% include 'chunks/banners-sidebar-left.volt' %}
        </div>
        <div class="listings-main-col col-md-6">
            {{ flashSession.output() }}
            {% set narrowGridMode = true %}
            {% include 'chunks/ads-mixed.volt' %}
        </div>
        <div class="col-md-3 listings-banner-col-right hidden-xs hidden-sm">
            {% include 'chunks/banners-sidebar-right.volt' %}
        </div>
    </div>
</div>
    {% else %}
    <div class="alert alert-warning text-center">Trenutno nema aktivnih oglasa</div>
    {% endif %}
{{ endForm() }}
