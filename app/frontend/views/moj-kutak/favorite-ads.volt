{# MyAccount favorite ads listing #}
{% include 'chunks/moj-kutak-profile-header.volt' %}

<div class="container saved-ads">
    <h2 class="margin-top-lg hidden-sm hidden-xs">Spremljeni oglasi</h2>

    {{ flashSession.output() }}

    {% if ads is defined and ads is iterable and ads|length > 0 %}
    <div class="list margin-top-md">
        {% for ad in ads %}
        <div class="row position-relative" data-favorite-classified-id="{{ ad['id'] }}">
            <div class="col-md-9">
                {{ partial('moj-kutak/adslisting-detailed-chunk', ['ad': ad]) }}
            </div>
            <div class="col-md-3 border-dark border-radius saved-ad-note hidden-sm hidden-xs">
                <textarea placeholder="Upišite bilješku..." class="note-content">{{ ad['favorited_ad_note'] }}</textarea>
                <a href="javascript:void(0);" class="save save-note"><span class="fa fa-circle-o-notch fa-spin fa-fw hidden"></span>spremi bilješku</a>
            </div>
        </div>
        {% endfor %}
    </div>

    <div class="margin-top-sm">        
        <div class="saved-ads-select-all bulk-actions margin-top-md position-relative">
            <input class="select-all checkbox" type="checkbox" id="select-all" name="select-all"><label for="select-all" title="Označi sve"></label>
            <button disabled class="delete-btn bulk-delete" data-toggle="modal" data-target="#deleteModal" data-formurl="{{ url('moj-kutak/obrisi-spremljeni-oglas/') }}" data-classified-ids=""><span class="fa fa-trash"></span>&nbsp; Obriši označene</button>
        </div>
    </div>
    {% else %}
    <div class="alert alert-info margin-top-md">Trenutno nemate spremljenih oglasa.</div>
    {% endif %}
</div>

{% include 'chunks/moj-kutak-profile-footer.volt' %}

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Zatvori"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel"><span class="text-danger">Upozorenje</span></h4>
            </div>
            {{ form(NULL, 'id': 'frm_ads_delete', 'method': 'post', 'autocomplete': 'off') }}
                {{ hiddenField(['next', 'value': next_url]) }}
                <div class="modal-body">
                    <p>
                        Jeste li sigurni da želite obrisati ovaj oglas iz popisa spremljenih oglasa?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Ne, odustani</button>
                    <button type="submit" class="btn btn-primary">Da, obriši oglas</button>
                </div>
            {{ endForm() }}
        </div>
    </div>
</div>
