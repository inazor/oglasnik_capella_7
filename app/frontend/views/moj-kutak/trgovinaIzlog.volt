{# MyAccount Featured shop form #}
{% if tree is defined and form_action == 'create' %}
<script>var category_tree_items = {{ tree|json_encode }};</script>
{% endif %}

{{ form(NULL, 'id':'frm_shop', 'method':'post', 'autocomplete':'off') }}
    {{ hiddenField(['action', 'value':form_action]) }}
    {{ hiddenField(['shop_id', 'value':shop.id]) }}
    {{ hiddenField(['ads', 'value':featured_shop.readAttribute('ads')]) }}

    <div class="border-radius border-dark margin-bottom-sm">
        <div class="box-header">
            <h2 class="margin-0">Trgovina › {{ featured_shop_action_title }}</h2>
        </div>
        <div class="box-body">

            {% if errors_markup is defined %}
                {{ errors_markup }}
            {% endif %}

            <div class="row">
                {% if form_action == 'create' %}
                <div class="col-md-6">
                    {% set field = 'main_category_id' %}
                    <div class="form-group{{ errors is defined and errors.filter('categories') ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label">Glavna kategorija</label>
                        <div class="icon_dropdown">{{ main_category_id_dropdown }}</div>
                        {% if errors is defined and errors.filter('categories') %}
                        <p class="help-block">{{ current(errors.filter('categories')).getMessage() }}</p>
                        {% endif %}
                    </div>
                </div>
                <div class="col-md-6" id="additional_cat_div"{{ additional_category_ids ? '' : ' style="display:none"' }}>
                    {% set field = 'additional_category_ids' %}
                    <div class="form-group">
                        <label for="{{ field }}" class="control-label">Dodatne kategorije (max 2 kategorije)</label>
                        <div class="icon_dropdown">{{ additional_category_ids_dropdown }}</div>
                    </div>
                </div>
                {% else %}
                <div class="col-xs-12">
                    Odabrane kategorije
                    {% set cat_data = featured_shop.getCategoriesDisplayData() %}
                    {% if not(cat_data is empty) %}
                        {{ cat_data['markup'] }}
                    {% endif %}
                </div>
                {% endif %}

                <div class="col-xs-12">
                    <h4>Preview izloga</h4>
                    <div class="row featured-shops-box featured-shops-box-preview">
                        <div class="col-md-6">
                            {{ featured_shop.buildShoppingWindowMarkup(shop, true, true) }}
                        </div>
                        <div class="col-md-6">
                            <div>
                                <button class="btn btn-primary" type="submit" name="save">{{ form_action == 'create' ? 'Plaćanje <span class="fa fa-arrow-circle-right fa-fw"></span>' : 'Spremi promjene' }}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-8"><h4 class="margin-top-5px">Trenutno aktivni oglasi</h4></div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    {{ textField(['filter-classifieds', 'class':'form-control form-control-md', 'placeholder':'ID ili naslov oglasa']) }}
                                    <div class="input-group-btn">
                                        <button id="filter-classifieds-btn" class="btn btn-primary btn-md" type="button"><span class="fa fa-search"></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="selectable-classifieds" id="all_classifieds_container">
                        {% if ads is defined and ads is iterable %}
                            {% for ad in ads %}
                                {{ partial('moj-kutak/adslisting-simple-chunk', ['ad': ad]) }}
                            {% endfor %}
                            {% if pagination_links %}{{ pagination_links }}{% endif %}
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
    </div>
{{ endForm() }}

