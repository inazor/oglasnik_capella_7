<div class="ad-box ad-box-wide no-checkbox" data-classified-id="{{ ad['id'] }}">
    <h3 class="hidden-sm hidden-md hidden-lg classified-title" data-href="{{ ad['frontend_url'] }}">{{ ad['title']|escape }}</h3>

    <div class="image-wrapper">
        {{ ad['thumb_pic'] }}
    </div>
    <div class="info-wrapper">
        <h3 class="hidden-xs" data-href="{{ ad['frontend_url'] }}">{{ ad['title']|escape }}</h3>
        
        <div class="meta clearfix">
            {% set ad_price = ad['price'] %}
            <div class="fr">
                {% if ad_price['other'] is defined %}<span class="price-euro">{{ ad_price['other'] }}</span><br>{% endif %}
                <span class="price-kn">{{ ad_price['main'] }}</span>
            </div>

            <b>{{ ad['status'] == 0 ? 'Stvoren' : 'Predan' }}:</b> {{ ad['status'] == 0 ? ad['created_at'] : ad['published_at'] }}<br />
            {% if ad['status'] == 1 OR ad['status'] == 14 OR ad['status'] == 15 %}
            <b>Aktivan do:</b> {{ ad['expires_at'] }}<br />
            {% endif %}
            <b>Kategorija:</b> {{ ad['category_path'] }}<br>

            {% if not (ad['online_product'] is empty) %}
            <b>Proizvod na webu:</b> {{ ad['online_product'] }}<br />
            {% endif %}
            {% if ad['offline_product'] %}<b>Naručeni proizvod u tisku:</b> {{ ad['offline_product'] }}<br />{% endif %}

            {% set ad_status_class = ad['status_class'] %}
            {% set ad_status_text = ad['status_text'] %}
            {% if current_action|default('') == 'spremljeniOglasi' %}
                {% if ad['active'] == '1' %}
                    {% set ad_status_class = 'success' %}
                    {% set ad_status_text = 'Aktivan' %}
                {% else %}
                    {% set ad_status_class = 'danger' %}
                    {% set ad_status_text = 'Neaktivan' %}
                    {% set show_link = false %}
                {% endif %}
            {% endif %}
            {% if ad_status_text %}
            <b>Status oglasa:</b> <span class="text-{{ ad_status_class }}">{{ ad_status_text }}</span><br />
            {% endif %}
        </div>
    </div>

    <div class="actions margin-top-sm width-full text-right">
        <div>
            <button class="btn btn-primary btn-sm add-to-shopping-window hidden"><span class="fa fa-fw fa-plus"></span> Dodaj u izlog</button>
            <button class="btn btn-danger btn-sm remove-from-shopping-window hidden"><span class="fa fa-fw fa-times"></span> Ukloni iz izloga</button>
        </div>
        <span class="no-image-warning text-small text-danger hidden">* U izlog je moguće dodati samo oglase s fotografijom</span>
    </div>
</div>
