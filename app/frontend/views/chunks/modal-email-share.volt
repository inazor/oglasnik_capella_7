{% set sender       = null %}
{% set senderAvatar = null %}
{% set senderName   = '' %}
{% set senderEmail  = '' %}
{% set showCaptcha  = true %}

{% if auth.logged_in() %}
    {% set sender       = auth.get_user() %}
    {% set senderAvatar = sender.getAvatar().src %}
    {% set senderName   = sender.getDisplayName() %}
    {% set senderEmail  = sender.email %}
    {% set showCaptcha  = false %}
{% endif %}

<div class="modal fade modal-email-share modal-send-message" id="modal-email-share" tabindex="-1" role="dialog" aria-labelledby="modal-email-share-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ form(url('ajax/share-via-email'), 'id':'frm-modal-email-share', 'method':'post', 'autocomplete':'off', 'class':'ajax-form') }}

                {{ hiddenField('_csrftoken') }}

                {{ hiddenField([messageEntityField, 'value':messageEntityID]) }}

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Zatvori"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modal-send-message-label">Podijelite putem emaila</h4>
                </div>
                <div class="destination">
                    {% set field = 'sender_email' %}
                    {% if sender %}
                    Pošiljatelj<br>
                    <input type="hidden" name="sender_id" value="{{ sender.id }}"/>
                    <input type="hidden" name="{{ field }}" value="{{ senderEmail }}"/>
                    <div class="border-radius overflow-hidden display-inline-block vertical-align-middle"><img src="{{ senderAvatar }}" alt="{{ senderName }}" class="avatar-header" height="30" width="30" /></div>
                    {{ senderName }}
                    {% else %}
                        <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                            <label for="{{ field }}" class="control-label">Vaša e-mail adresa<abbr title="Obavezno polje">*</abbr></label>
                            {{- textField([field, 'class':'form-control', 'placeholder':'Email adresa', 'value':senderEmail]) }}

                            <p class="help-block">{{ errors is defined and errors.filter(field) ? current(errors.filter(field)).getMessage() : '' }}</p>
                        </div>
                    {% endif %}
                </div>
                <div class="alert text-center hidden"></div>
                <div class="modal-body">
                    {% set field = 'recipients' %}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <label for="{{ field }}" class="control-label">Primatelji<abbr title="Obavezno polje">*</abbr></label>
                        {{ textArea([field, 'class':'form-control', 'rows':'8', 'value':'']) }}

                        <p class="help-block text-small">{{ errors is defined and errors.filter(field) ? current(errors.filter(field)).getMessage() : 'Možete dodati više email adresa (svaka email adresa mora biti u novom redu!)' }}</p>
                    </div>

                    {% if showCaptcha and not(recaptcha_sitekey is empty) -%}
                    {%- set field = 'captcha-email-share' -%}
                    <div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                        <div id="{{ field }}" class="g-recaptcha" data-sitekey="{{ recaptcha_sitekey }}"></div>
                        <p class="help-block">{{ errors is defined and errors.filter(field) ? current(errors.filter(field)).getMessage() : '' }}</p>
                    </div>
                    {%- endif -%}

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary width-full modal-btn-send text-center light-blue">Podijeli</button>
                </div>
                {{ endForm() }}

        </div>
    </div>
</div>
