<ul data-level="{{ level }}" class="category-level-{{level}}{{ class ? ' ' ~ class : '' }}"{{ parent_category_id ? ' data-parent-id="' ~ parent_category_id ~ '"' : '' }}>
{{ partial('chunks/nested-li', ['items': items, 'mark_paid_categories': (mark_paid_categories ? mark_paid_categories : false), 'level': level, 'class': class]) }}
</ul>
