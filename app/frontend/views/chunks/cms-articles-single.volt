<div class="col-md-3 col-sm-6 col-xs-12{{ itemClass|default('') ? ' ' ~ itemClass : ''}}">
    <div class="post-box">
        <a class="width-full text-center" href="{{ article['frontend_url'] }}">
            {% set thumb = article['thumb'] %}
            {{ thumb.setAlt(article['title']|striptags|truncate(80)|escape_attr).setHeight(270).setWidth(180).getTag() }}
            <h3>{{ article['title'] }}</h3>
        </a>
        {% if article['excerpt'] %}<p>{{ article['excerpt'] }}</p>{% endif %}
        <div class="meta">
            <span class="date">{{ article['publish_date'] }}</span><br>
        </div>
    </div>
</div>
