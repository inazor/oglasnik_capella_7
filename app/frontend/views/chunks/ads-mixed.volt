{% set separateAdsWithHeaders = true %}
{% set showListingHeaderText = true %}
{% set lastListingHeaderText = null %}
{% set lastListingHeaderClass = null %}
{% set loggedInUserId = auth.logged_in() ? auth.get_user().id : null %}

<svg class="svgs hidden">
    <filter id="blur">
        <feGaussianBlur stdDeviation="50"></feGaussianBlur>
    </filter>
    <filter id="blur-100">
        <feGaussianBlur stdDeviation="100"></feGaussianBlur>
    </filter>
</svg>

{% if ads is defined and ads|length %}
    {% if viewType|default(default_viewType) == 'grid' %}
    <div class="category-grid-layout">
        {% set adsGridSingleClass = 'col-md-4 col-xs-6' %}
        {% include 'chunks/ads-grid.volt' %}
    </div>
    {% endif %}

    {% if viewType|default(default_viewType) == 'list' %}
        {% include 'chunks/ads-list.volt' %}
    {% endif %}

    {% if pagination_links is defined and pagination_links %}
        {{ pagination_links }}
    {% endif %}
{% endif %}

{% if banners and banner_mobile_pagination is defined %}
<div class="text-center banner banner-mobile-pagination margin-bottom-sm margin-top-sm visible-xs">{{ banner_mobile_pagination }}</div>
{% endif %}
