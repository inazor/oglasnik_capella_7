<h1>Ups! Stranica je privremeno nedostupna!</h1>
<h2>Ne brini, nije ništa strašno...</h2>
<p>U tijeku je planirana nadogradnja naših servera te je naša stranica Oglasnik.hr privremeno nedostupna. Uspostavljanje normalnog rada stranice očekuje se uskoro.</p>
<p>Zahvaljujemo na razumijevanju,<br>Vaš oglasnik</p>
