<div class="underline margin-bottom-2em">
    <h1 class="underline">Ups! Stranica koju tražiš ne postoji.</h1>
</div>
<p class="text-center"><img src="{{ url('assets/img/404.jpg') }}"></p>
<h2><strong>Ne brini, nije ništa strašno...</strong></h2>
<p>Evo što možeš napraviti:</p>
<ul class="margin-bottom">
    <li>Ako si na ovu stranicu došao upisivanjem adrese, provjeri ispravnost adrese.</li>
    <li>Ako si na ovu stranicu došao preko linka na <a href="{{ url() }}">www.oglasnik.hr</a>, vjerojatno je link neispravan - molimo prijavite grešku na <a href="mailto:webmaster@oglasnik.hr">webmaster@oglasnik.hr</a>.</li>
    <li>Stranica je možda postojala ranije, no sada je više nema.</li>
    <li>Vrati se na <a href="{{ url() }}">početnu stranicu</a> ili upiši u tražilicu pojam kojeg želiš pronaći.</li>
</ul>
