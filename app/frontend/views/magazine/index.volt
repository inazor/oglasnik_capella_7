{% if articles_categories is defined and articles_categories is iterable %}
    {% for catArticles in articles_categories %}
        {% set category = catArticles['category'] %}
        {% set articles = catArticles['articles'] %}
        <div class="position-relative">
            <h2 class="section-title">{{ category.name|category_name_markup }}</h2>
            <a href="{{ category.get_frontend_view_link() }}" class="position-absolute position-absolute-right">Otvori sve u {{ category.name }}<span class="fa fa-angle-right fa-fw"></span></a>
        </div>
        {% if articles is defined and articles and articles is iterable %}
            {{ partial('chunks/cms-articles', ['articles':articles, 'rowClass':'masonry-articles', 'itemClass':'masonry-item']) }}
        {% endif %}
        <div class="clearfix"></div>
    {% endfor %}
{% endif %}
