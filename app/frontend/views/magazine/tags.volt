{# Magazine tags listing #}
<section>
    <div class="underline margin-bottom">
        <h1 class="underline">Članci označeni tagom: <span class="upperCase">{{ entity.raw }}</span></h1>
    </div>
    <div class="ad-listing-wrapper no-sidebar">
        <div class="ad-listing category-articles">
            {% for article in articles.items %}
            {% set article_permalink = article.get_frontend_view_link() %}
            <div class="post-box">
                <div class="img-wrapper">
                    {%- set thumb = article.get_thumb('Oglas-140x140') -%}
                    {%- if thumb -%}
                        <a href="{{ article_permalink }}"><img width="{{ thumb.getWidth() }}" height="{{ thumb.getHeight() }}" src="{{ thumb.getSrc() }}" alt="{{ article.title|escape_attr }}"></a>
                    {%- endif -%}
                </div>
                <h3><a href="{{ article_permalink }}">{{ article.title }}</a></h3>
                <p>{{ article.excerpt }}</p>
                <div class="toolbar">
                    <span class="publish-date">Objavljeno: {{ article.created_at|day_date }}</span>
                </div>
            </div>
            {% endfor %}
            {% if pagination_links %}{{ pagination_links }}{% endif %}
        </div>
    </div>
</section>
