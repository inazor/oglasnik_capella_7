{% include 'chunks/breadcrumbs.volt' %}

<div class="position-relative">
    <h2 class="section-title">{{ category.name|category_name_markup }}</h2>
</div>
{% if articles is defined and articles and articles is iterable %}
{{ partial('chunks/cms-articles', ['articles':articles, 'rowClass':'masonry-articles', 'itemClass':'masonry-item']) }}

{% if pagination_links %}{{ pagination_links }}{% endif %}
{% endif %}
