{% include 'chunks/breadcrumbs.volt' %}

<div class="alert alert-danger text-center">Ovaj oglas je obrisan!</div>

{% if banners and banner_728x90 is defined %}
    <div class="row"><div class="banner w728 center banner-728x90 hidden-xs margin-top-sm">{{ banner_728x90 }}</div></div>
{% endif %}

{% if similar_ads is defined and similar_ads and similar_ads is iterable %}
    <h2 class="section-title">Slični <span>oglasi</span></h2>
    {{ partial('chunks/ads-grid', ['ads':similar_ads] )}}
{% endif %}
