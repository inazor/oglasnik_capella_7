{{ form(form_action, 'id':'search_filters', 'method':'get') }}
    {{ hiddenField('q') }}
    {% if main_search_category is defined %}{{ hiddenField(['category_id', 'value':main_search_category]) }}{% endif %}
    {{ hiddenField(['all', 'data-type':'text', 'data-default':'0']) }}

<a name="classifieds"></a>

<h1 class="h2-like section-title text-center">
    Rezultati <b>pretrage</b>
    {% if main_search_term %}<small class="search-term">"{{ main_search_term|escape }}"</small>{% endif %}
</h1>

<div class="row no-gutters-xs margin-top-lg">
    <div class="col-md-3 side-col">
        <p style="margin-top:10px !important;">
            {% if searchHasCategories is defined and searchHasCategories %}
            <a class="hidden-md hidden-lg btn btn-md display-block filters-anchor-link margin-bottom-1em" href="#filters-anchor">
                Kategorije
            </a>
            {% endif %}
            Pronađenih oglasa: {{ total_items|default(0)|money }}
        </p>
    </div>
    <div class="listings-main-col col-md-6">
        <div class="grid-sort-options margin-bottom-sm row no-gutters-xs">
            {% if ads is defined and ads|length %}
            <div class="col-xs-6 col-sm-7 upper-pagination">{% if pagination_links_top is defined and pagination_links_top %}{{ pagination_links_top }}{% endif %}</div>
            {% if sort_dropdown is defined %}
            <div class="col-xs-6 col-sm-5 sort-dropdown">{{ sort_dropdown }}</div>
            {% endif %}
            {% endif %}
        </div>
    </div>
</div>

<div class="reordered-on-mobile">
    <div class="row no-gutters-xs">
        <div id="filters-anchor" class="listings-side-col side-col col-md-3">
            {% if searchHasCategories is defined and searchHasCategories %}
            <h2 class="blue"><img class="margin-right-sm" src="{{ url('assets/img/icn_filter_category_02.svg') }}" alt="">Kategorije oglasa</h2>
            <div class="classifieds-categories-listing-sidebar">
                {{ categoryTreeCountsMarkup }}
                {% if needsShowAll %}
                <div class="hidden-filters-more"><a class="all-cats-toggle more-btn" href="{{ showAllToggleHref }}">{{ showAllToggleText }}</a></div>
                {% endif %}
                {% if linkToLeafCategory %}
                <a href="{{ linkToLeafCategoryHref }}" class="btn btn-primary width-full auto-height">Detaljnije pretraži<br/>odabranu kategoriju<span class="fa fa-chevron-right fa-fw"></span></a>
                {% endif %}
            </div>
            {% endif %}
            {% include 'chunks/banners-sidebar-left.volt' %}
        </div>
        <div class="listings-main-col col-md-6">
            {{ flashSession.output() }}

            {% set narrowGridMode = true %}
            {% include 'chunks/ads-mixed.volt' %}
        </div>
        <div class="col-md-3 listings-banner-col-right hidden-xs hidden-sm">
            {% include 'chunks/banners-sidebar-right.volt' %}
        </div>
    </div>
</div>

{{ endForm() }}
