{# CONTEXT<pre>{{ print_r( _context, true ) }}</pre>#}
<style>
	/*da se buttoni ispisuju u više redova*/
	.btn{
		white-space : normal;
	}
</style>

{% set is_new = true %}
{% if entity.id is defined %}	
	{% set is_new = false %}
{% endif %}

{% set publication = _model.Publication(_context['_default_publication_id']) %}

<div class="row">
	<div class="col-lg-12 col-md-12">
		{{ partial("partials/blkFlashSession") }}
		<div>
			<h1 class="page-header">
				{{ ( is_new ? 'New ' : '' ) }}
				
				{% if publication is defined %}
					Category for <i>{{ publication.name }}</i> 
				{% else %}
					Category for Publication
				{% endif %}
				
			</h1>
		</div>
		
			{% if ! is_new %}
				<div class="row">
					<div class="col-lg-4 col-md-6">
						{% set countPcm = _model.getMany('PublicationsCategoriesMappings', "category_mapping_id = " ~ entity.id ).count() %}
						<a  href = "/suva/publications-categories-mappings?_context[default_filter][category_mapping_id]={{ entity.id }}" target="_blank">
							<span class="btn btn-primary  btn-m">Mappings for category  <i>{{ entity.n_path }}</i> ( {{ countPcm }} ) </span>&nbsp;
						</a>
						<br/><br/>
					</div>
					<div class="col-lg-4 col-md-6">
						<a 
							href = "/suva/categories-mappings/createDefaultPublicationsCategoriesMapping/{{ entity.id }}" 
							>
							<span class="btn btn-warning btn-m">Create blank Mapping for category <i>{{ entity.name }}</i></span>
						</a>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<a  href = "/suva/ads2/adsInCategoryMapping/{{ entity.id }}" target="_blank">
							<span class="btn btn-primary  btn-m">Ads in category  <i>{{ entity.n_path }}</i> </span>&nbsp;
						</a>
						<br/><br/>
					</div>
				</div>

			{% endif %}

			
		
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }} 
				<input type="hidden" name="_gfid" value="{{_gfid}}"/>
				{% if entity.id is defined %}
					<input type="hidden" name="id" value="{{ entity.id }}"></input>
				{% endif %}
				{% if entity.n_publications_id is defined %}
					<input type="hidden" name="n_publications_id" value="{{entity.n_publications_id}}"></input>
				{% endif %}
	
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title"></h3>
							</div>
							<div class="panel-body">
								<div class="row">
								
									{{ partial("partials/ctlText",[
										'value':( 
													is_new 
													?  ( 
														publication 
														? publication.name 
														: null 
														) 
													: entity.Publication().name 
												), 
										'title':'Publication', 
										'field' : '',
										'width':3, 
										'readonly': ( publication ? true : false )
									] )}} 
									
									{{ partial("partials/ctlTextTypeV3",[
										'title' : 'Parent Category'
										, 'field' : 'parent_id'
										, 'width' : 6
										, 'value' : entity.parent_id
										, '_context' : _context 
										, 'value_show' :  entity.ParentCategory().n_path
										, 'model_name' : 'CategoriesMappings' 
										, 'expr_search' : 'name'
										, 'expr_value' : 'id' 
										, 'expr_show_input' : 'n_path'
										, 'expr_show_list' : 'n_path'
										, 'additional_where' : ' and active = 1 ' ~ ( publication ? ' and n_publications_id = ' ~ publication.id  : '') ~ ' order by name '
										, 'nr_returned_rows' : 30
									]) }}
									
									{{ partial("partials/ctlDropdown",[
										'value': entity.transaction_type_id, 
										'title': 'Transaction Type', 
										'list' : _model.TransactionTypes(), 
										'field': 'transaction_type_id', 
										'width': 2, 
										'option_fields' : ['id','name'],
										'errors' : errors				
									] )}}	
									
								</div>
								
								<div class="row">
									{{ partial("partials/ctlCheckbox", [
										'value' : entity.paid_category, 
										'title' : 'Paid Category',
										'width' : 2,
										'field' : 'paid_category',
										'errors' : errors
									] ) }} 
									
									{{ partial("partials/ctlText",[
										'value' : entity.name, 
										'title' : 'Name', 
										'width' : 5, 
										'field' : 'name',
										'_context' : _context
									] )}} 
								</div>
								
								<div class="row">									
									{{ partial("partials/ctlText",[
										'value' : entity.url, 
										'title' : 'URL', 
										'width' : 4, 
										'field' : 'url',
										'_context' : _context
									] )}} 
									
									{{ partial("partials/ctlText",[
										'value' : entity.eps_file_path, 
										'title' : 'EPS File path', 
										'width' : 4, 
										'field' : 'eps_file_path',
										'_context' : _context
									] )}} 

									{{ partial("partials/ctlNumeric",['value':entity.sort_order, 'title':'Sort Order', 'width':2, 'field':'sort_order', 'decimals':0, '_context': _context] ) }}
									
									
									{{ partial("partials/ctlDropdown",[
										'value': entity.layout_id, 
										'title': 'DTP Layout', 
										'list': _model.Layouts(), 
										'field': 'layout_id', 
										'width': 4, 
										'option_fields':['id','name'],
										'_context': _context				
									] )}}										
									
									{{ partial("partials/ctlDropdown",[
										'value': entity.dtp_group_id,
										'title': 'DTP Group',
										'list': _model.DtpGroups(), 
										'field': 'dtp_group_id', 
										'width': 2, 
										'option_fields':['name'],
										'_context': _context
									] ) }}	

									{{ partial("partials/ctlText",[
										'value' : entity.level, 
										'title' : 'Level', 
										'field' : 'level',
										'width' : 1, 
										'readonly': true
									] )}} 									
								
									{{ partial("partials/ctlCheckbox", [
										'value' : entity.active, 
										'title' : 'Active',
										'width' : 2,
										'field' : 'active',
										'errors' : errors
									] ) }} 

								</div>

							
							</div>
						</div>
					</div>
				</div>
				<!-- include "partials/btnSaveCancel.volt" -->
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}

		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>
