{#
	ctlTextType parametri:  
	
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	search_field - ime polja po kojem se pretražuje tablica
	return_field - vrijednost polja koja se vraća
	table_name - ime tablice u bazi koja se čita
	additional_where - dodatni kriteriji u tablici, na kraju where clause, moraju sadržavati .. and kriterij..
	readonly 
	value_show - vrijednost koju će pokazati
	keep_typed_text - ako je true onda ostaje utipkani tekst  u kontroli
	do_not_fill_clicked_option_value - ako je true, kod klika na opciju se ništa ne odabire (služi u kombinaciji sa botunom na opcijama, npr. Choose na listi korisnika)
	list_item_button_string 
	list_item_button_href
	input_field_class - dodatna klasa input fielda
	input_field_style - style input polja
	hidden_field_id - ID skrivenog polja
	lose_focus - Sakriva listu rezultata kada se klikne na neki drugi element a ne na botun choose

	primjer; {{ partial("partials/ctlTextType",['field':'n_category_id', 'search_field':'name', 'return_field':'name', 'table_name':'category', 'value_show':category.n_path, 'value_id':category.id, 'width':3] ) }}
	
					{{ partial("partials/ctlTextType",[
						'field':'_context[selected_category_id]'
						,'value':category.id
						,'value_show':category.n_path
						,'search_field':'n_path'
						, 'return_field':'n_path'
						, 'table_name':'categories'
						, 'readonly' : false
						, 'lose_focus' : true
						,'js_onclick_value' : "alert('alo');"
						,'js_beforecreate_value' : "$(this).append('ss');"
						,'do_not_fill_clicked_option_value': false
						, 'additional_where' : ' and transaction_type_id is not null order by n_path'
						, 'width':3] 
					) }}
									
	TODO:: kada se return_field šalje "id" to ga zbunjuje jer mu se tako zove i id u <div id=' + txtFieldName + '__' + obj.id + '>' + obj.id ...
	
	TODO:: Šta ako se ukuca nešto bezveze i po  tome se ništa ne pronađe? Onda to ostane u inputu i treba nekako javit da to nije valjan unos
#}
	
	{% set _js_onclick_value = ( js_onclick_value is defined ) ? js_onclick_value : '' %}
	{% set _js_beforecreate_value = ( js_beforecreate_value is defined ) ? js_beforecreate_value : '' %}
	
	{% set _do_not_fill_clicked_option_value = 'false' %}
	{% if do_not_fill_clicked_option_value is defined %}
		{% if do_not_fill_clicked_option_value === true %}
			{% set _do_not_fill_clicked_option_value = 'true' %}
		{% endif %}
	{% endif %}

	<!-- TODO maknit ovo greske idu preko contexta -->
	{#{% if errors is defined %}
		{% set _errors = errors %}
	{% endif %}#}
	{% if _context is defined %}
		{% if _context['errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
	{% endif %}
	
	

 
		{% set l_readonly = false %}{% if readonly is defined %}{% if readonly === true %}{% set l_readonly = true %}{% endif %}{% endif %}
		{% if lose_focus is defined %}
			{% if lose_focus === true %}
		<script>
			$(document).on('click', function () {
    			$(".dropdown_list").hide();
			});
			$('a').on('click', function (event) {
    			event.stopPropagation();
			});
		</script>
			{% endif %}
		{% endif %}
		
		{% if field is defined %}
			{% set escField = str_replace(']','_',str_replace('[','_',field)) %}
		{% else %}
			{% set escField = '' %}
		{% endif %}
		{% if additional_where is not defined %}
			{% set additional_where = '' %}
		{% endif %}
		{% if value_show is not defined %}{% set value_show = '' %}{% endif %}
		{% if value is not defined %}{% set value = '' %}{% endif %}
		

<script>
//testCtlPartial();

</script>
		
		{% if width is defined %}<div class="col-lg-{{width}} col-md-{{width}}"> {% endif %}
			{% if  field is defined and _errors is defined %}<div class="form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
				{% if  title  is defined %}
					<label {% if  field  is defined %}class="control-label" for="{{  field  }}" {% endif %} >
						{{  title  }}
					</label>
				{% endif %}
				
				{% if field is defined %} 
					<input type="hidden" id="{{escField}}" name ="{{field}}" value="{{value}}">
					</input> 
				{% endif %}
				<div>
					<input 
							 type = "text" 
							 class = "form-control" 
							 id = "{{ escField }}-input" 
							 value = "{{value_show}}"
							 {% if l_readonly === false %}
								 onkeydown = " 
ctlTextType_onkeydown( 
	'{{escField}}'
	, '{{table_name}}'
	, '{{search_field}}'
	, '{{return_field}}'
	, '{{additional_where}}'
	, '{{value}}'
	, '{{list_item_button_string}}'
	, '{{list_item_button_href}}' 
	, null
	, null
	,{% if keep_typed_text === true %}true{% else %}false{% endif %}
	,{{ _do_not_fill_clicked_option_value }}
);
"
							{% else %}
								readonly
							{% endif %}
					></input> 
					<div 
						style = "position:absolute; cursor:pointer; overflow:scroll; background-color:#EEEEEE; z-index:1000;" 
						id = "{{ str_replace(']','_',str_replace('[','_',field)) }}_value_list">
					</div>
					{% if _errors is defined %}
						{% if _errors.filter(field) %}
							<p class="help-block">{{ current(_errors.filter(field)).getMessage() }}</p>
						{% endif %}
					{% endif %}
				</div>
			{% if  field is defined and _errors is defined %}</div>{% endif %}	
		{% if width is defined %}</div> {% endif %}