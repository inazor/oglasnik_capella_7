
{#
Korištenje:

		{{ partial("partials/wraSubFormAjax",[
			'_form_id' : _form_id,
			'_subform_id' : _subform_id,
			'_url': 'users-contacts/index/' ~ ad.user_id, 
			'_model': _model, 
			'_context': _context, 
			'p': ['ids' : _ids ]
		] ) }} 
#}

{#% set _form_id = '' %}
{% if _context is defined  %}
	{% if _context['_global_form_id'] is defined  %}
		{% if _context['_global_form_id'] > 0  %}
			{% set _context['_global_form_id'] += 1 %}
			{% set _form_id = _context['_global_form_id'] %}
		{% endif %}
	{% endif %}
{% endif %#}

{% set _err = false %}
{% if _form_id is not defined %}
	<br/>ERROR: Wrapper called without form_id
	{% set _err = true %}
{% else %}
	{% if strlen(_form_id) == 0 %}
		<br/>ERROR: Wrapper called with form_id length 0
		{% set _err = true %}
	{% endif %}
{% endif %}

{% if _url is not defined %}
	<br/>ERROR: Wrapper called without url
	{% set _err = true %}
{% else %}
	{% if strlen(_url) == 0 %}
		<br/>ERROR: Wrapper called with url length 0
		{% set _err = true %}
	{% endif %}
{% endif %}


{% if _err == false %}
	<script>
		//alert("FROM WRAPPER: URL {{_url}}, FORM_ID {{_form_id}}");
	</script>
	{% set _wrapper_id = _form_id ~ '_' ~ _subform_id %}

	{% set _wrapper_context  = _context %}
	{% set _wrapper_context['_is_ajax_submit'] = true %}
	{% set _wrapper_context['_form_id'] = _wrapper_id %}


	<div class="row">
		<div class="col-lg-12 col-md-12" id="_wrapper_{{ _wrapper_id }}">
			<script>
				//alert("wrapper prije {{ _url }}   wrapper: {{ _wrapper_id }}");
				ajaxSubmit( "{{ _url }}", "{{ _wrapper_id }}" , null  );
				//alert("wrapper poslije");
			</script>
		</div>
	</div>
{% endif %}