{#
	parametri:  
	field - 'ime_polja'  - ako je postavljen dodaje klasu inputa (postaje form control)
	value - vrijednost polja
	width - ako je postavljen dodaje div class="col-mg-width-..width".....
	title - ako je naveden dodaje Label
	show_time - ako je postavljen prikazuje i vrijeme
	readonly - ako je postavljen (1) dodaje READONLY na input
	placeholder - defaultni tekst (( Unesite vrijednost.. ))
	
	ajax_edit - ako je definiran onda se izmjene odmah šalju ajaxom. Potrebne su još varijable ajax_id, ajax_table
	ajax_id - vidi ajax_edit
	ajax_table - vidi ajax_edit
	

	primjer; ctlDate(['value':entity.name, 'title':'Name', 'width':3, 'field':'name', 'readonly':1 ] ) 
			 ctlDate( ['value':'2015-12-31' , 'title':'Datum', 'width':3, 'field':'date'] )
 			{{ partial("partials/ctlDate",[
				'title':'Datum', 
				'value':'2015-12-31' , 
				'field':'date',
				'width':3, 
				'readonly' : false,
				'show_time' : true,
				'_context' : _context
			] ) }}
			
	doc:
	http://bootstrap-datepicker.readthedocs.io/en/latest/options.html#autoclose
#}

	
	{% set _show_time = false %}{% if show_time is defined %}{% if show_time === true %}{% set _show_time = true %}{% endif %}{% endif %}
	{% set _readonly = false %}{% if readonly is defined %}{% if readonly === true %}{% set _readonly = true %}{% endif %}{% endif %}
 

	{% set is_ajax = false %}
	
	{% if errors is defined %}
		{% set _errors = errors %}
	{% endif %}
	
	{% if value is not defined %}
		{% set value = '' %}
	
	{% endif %}
	
	{% if field is  not defined %}
		{% set field = '' %}
	{% endif %}

	{% if _context is defined %}
		{% if _context['ajax_edit'] is defined and _context['current_entity_id'] is defined and _context['current_entity_table_name'] is defined and field is defined %}
			{% if _context['ajax_edit'] === true %}
				{% set is_ajax = true %}
				{% set ajax_table = _context['current_entity_table_name'] %}
				{% set ajax_id = _context['current_entity_id'] %}
			{% endif %}
		{% endif %}
		
		{% if _context['_errors'] is defined %}
			{% set _errors = _context['errors'] %}
		{% endif %}
		
	{% endif %}

	
	{% if is_ajax %}
		{% set id_string = 'id__' ~ ajax_id ~ '__field__' ~ field %}
	{% else %}
		{% set id_string = field %}
	{% endif %}
	
	
	{% if field is defined %}
		{% if _show_time %}
			{% set _jsUpdateInput = "
				$('#" ~ id_string ~ "').val(
					$('#" ~ id_string ~ "_date').val() 
					+ ' ' + $('#" ~ id_string ~ "_hours').val() 
					+ ':' + $('#" ~ id_string ~ "_minutes').val() 
					+ ':' + $('#" ~ id_string ~ "_seconds').val() 
				); 
				//alert ($('#" ~ id_string ~ "').val());
			" %}			
		{% else %}
			{% set _jsUpdateInput = "$('#" ~ id_string ~ "').val($('#" ~ id_string ~ "_date').val()); 
							//alert ($('#" ~ id_string ~ "').val());" %}	
		{% endif %}
	{% endif %}
	
	{% if width is defined %}<div class="col-lg-{{ width }} col-md-{{ width }}">{% endif %} 
		{% if  field  is defined %}<div class="form-group{{ _errors is defined and _errors.filter( field ) ? ' has-error' : '' }}">{% endif %}
			{% if title is defined %}<label {% if field is defined %} class="control-label" for="{{ field }}" {% endif %} >{{ title }}</label>{% endif %}
			
			
			
			
			{% if field is defined %} 
				<input 
					type = "hidden" 
					name = "{{ field }}" 
					id="{{ id_string }}"
					value = "{{ value }}" 
					/>
			{% endif %}
			
			<span class="form-control" >
				<input 
					{% if placeholder is defined %} placeholder = "{{ placeholder}}"{% endif %}
					{% if _readonly %} readonly {% endif %}
					
					
					style ="
						border:0px;
						{% if _show_time %}
							float:left; width:40%;"
						{% endif %}
					"
					
					class = "
						{% if is_ajax %} ajax_edit {% endif %}
					"
					
					{% if field is defined %} 
						
						name = "{{ id_string }}_date" 
						id = "{{ id_string }}_date" 
					{% endif %} 

					value = "{{ (value ? date('d.m.Y', strtotime(value)) : '') }}" 

					{% if _show_time %}
						maxlength = "10"
					{%  else %}
						maxlength = "19"
					{% endif %}
						
					{% if ! _readonly %}
						onmouseover="$(this).datepicker({
									 format: 'dd.mm.yyyy',
									 weekStart: 1,
									 language: 'hr',
									 autoclose: true,
									 todayHighlight: true,
									clearBtn: false,
									autoclose: true

									 });" 
						{% if field is defined %} 
							onchange = "
								{{ _jsUpdateInput }}
								
								{% if is_ajax %}
									ctlDateUpdateField( '{{ id_string }}', '{{ ajax_table }}', '{{ ajax_id }}', '{{ field }}' );
								{% endif %}
							"
						{% endif %}
					{% endif %}
					
					/>
				{% if _show_time %}
					<span>&nbsp;</span>
					<input 
						{% if _readonly %} readonly {% endif %}
						style= "width:10%; border:0px;"
						type = "number" 
						maxlength = "2" 
						value = "{{ (value ? date('H', strtotime(value)) : '00') }}" 
						min = "00"
						max = "23"
						
						{% if field is defined %} 
							
							name = "{{ id_string }}_hours" 
							id = "{{ id_string }}_hours" 
							onchange = "
								{{ _jsUpdateInput }}
								
								{% if is_ajax %}
									ctlDateUpdateField( '{{ id_string }}', '{{ ajax_table }}', '{{ ajax_id }}', '{{ field }}' );
								{% endif %}
							"
						{% endif %}
						/>
					<span>:</span>
					<input 
						{% if _readonly %} readonly {% endif %}
						style= "width:10%; border:0px;"
						type = "number" 
						maxlength = "2" 
						value = "{{ (value ? date('i', strtotime(value)) : '00') }}" 
						min = "00"
						max = "59"
						{% if field is defined %} 
							
							name = "{{ id_string }}_minutes" 
							id = "{{ id_string }}_minutes" 
							onchange = "
								{{ _jsUpdateInput }}
								
								{% if is_ajax %}
									ctlDateUpdateField( '{{ id_string }}', '{{ ajax_table }}', '{{ ajax_id }}', '{{ field }}' );
								{% endif %}
							"
						{% endif %}
						/>
					<span>:</span>
					<input 
						{% if _readonly %} readonly {% endif %}
						style= "width:10%; border:0px;"
						type = "number" 
						maxlength = "2" 
						value = "{{ (value ? date('s', strtotime(value)) : '00') }}" 
						min = "00"
						max = "59"
						{% if field is defined %} 
							
							name = "{{ id_string }}_seconds" 
							id = "{{ id_string }}_seconds" 
							onchange = "
								{{ _jsUpdateInput }}
								
								{% if is_ajax %}
									ctlDateUpdateField( '{{ id_string }}', '{{ ajax_table }}', '{{ ajax_id }}', '{{ field }}' );
								{% endif %}
							"
						{% endif %}
						/>
				{% endif %}
			</span>
				
		{% if field is defined %}</div>{% endif %}
	{% if width is defined %}</div>{% endif %}