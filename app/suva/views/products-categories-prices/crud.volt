{% if errors is not defined %}
	{% set errors = null %}
{% endif %}

{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div><h1 class="page-header">{% if entity.id is not defined %}New {% endif %}Products Categories Prices</h1></div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">Products details</h3>
							</div>
							<div class="panel-body">
								<div class="row">		
									{#{ partial("partials/ctlDropdown",[
										'value':entity.categories_id, 
										'title':'Category', 
										'list':_model.Categories(), 
										'field':'categories_id', 
										'width':5, 
										'option_fields':['n_path','id'],
										'_context': _context
									] ) }#}		

									{{ partial("partials/ctlDropdown",[
										'value' : entity.categories_mappings_id, 
										'title' : 'Publication Category', 
										'list' : _model.CategoriesMappings(), 
										'field' : 'categories_mappings_id', 
										'width' : 5, 
										'option_fields' : ['n_path','id'],
										'_context': _context
									] ) }}	
								</div>
								<div class="row">		
								
									{{ partial("partials/ctlDropdown",['value':entity.products_id, 'title':'Product', 'list':_model.Products(), 'width':3, 'field':'products_id', 'errors': errors] ) }}
									{{ partial("partials/ctlNumeric",['value':_model.Product(entity.products_id).unit_price, 'title':'Default price', 'width':2, 'field':'unit_price', 'decimals':2, 'readonly':true, '_context': _context] ) }}									
									{{ partial("partials/ctlNumeric",['value':entity.unit_price, 'title':'Unit price', 'width':2, 'field':'unit_price', 'decimals':2, '_context': _context] ) }}
									{{ partial("partials/ctlCheckbox",['value':entity.is_displayed, 'title':'Displayed on list of products', 'width':2, 'field':'is_displayed',  '_context': _context] ) }}	
									{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title':'Active', 'width':2, 'field':'is_active',  '_context': _context] ) }}	
								</div>
								<div class="row">
									
							
								</div>

								
								<div class="row">
								
																					

								</div>
						
					
								
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>