{# CONTEXT<pre>{{ print_r( _context, true ) }}</pre>#}


<!-- <style>
td {
border: 1px solid black;

}

</style> -->


<div class="row">
    <div class="col-lg-12 col-md-12">
       
		<div>
			
			<h1 class="page-header">
				Insertions Avus
			</h1>
			
        </div>
		
        <br>
        {{ partial("partials/blkFlashSession") }}
        <table style="width:1150 !important;" class="table table-striped table-hover table-condensed">

		{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				 ['title':'Options'													,'width': 150	,'show_navigation' : true ] 
				,['title':'ID'						,'name':'id'					,'width': 100		,'search_ctl':'ctlText'		] 
				,['title':'Ad Id'					,'name':'ad_id'					,'width': 300	,'search_ctl':'ctlText' ]  
				,['title':'Issue'					,'name':'issue_id'				,'width': 300	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list' : _model.Issues( ) , 'option_fields':['id', 'date_published', 'status', 'publications_id'] ]]
				,['title':'Is paid'					,'name':'is_paid'				,'width': 100	,'search_ctl':'ctlText']  
				,['title':'Date published'			,'name':'date_published'					,'width': 100	,'search_ctl':'ctlDate'	] ]
				]) }}

			
			{% for id in ids %}
				{% set entity = _model.getOne('InsertionsAvus',id) %}
				
				 
				<tr>
					<td>{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'insertions-avus'] ) }}</td>
					<td>{{ entity.id }}</td>
					<td>{{ entity.ad_id }}</td>
					<td>
						{% set issue = entity.Issue() %}
						{{ issue.id ~ ' - ' ~ issue.status ~ ' - ' ~ issue.date_published }}
					</td>
					<td>{{ entity.is_paid }}</td>
					<td>{{ entity.date_published }}</td>

				</tr>
            {% else %}
				<tr><td colspan="6">Currently, there are no Insertions!</td></tr>
            {% endfor %}
        </table>
    </div>
</div>
