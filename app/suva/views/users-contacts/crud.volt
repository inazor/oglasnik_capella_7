{# AJAX BLOK - NA SVAKU FORMU #}
{% if _window_id is not defined %}{% set _window_id = '1' %}{% endif %}
{% if _is_ajax_submit is not defined %}{% set _is_ajax_submit = false %}{% endif %}


{% set _subform_id = 0 %}


{% if errors is not defined %}{% if _context['errors'] is defined %}{% set errors = _context['errors'] %}{% endif %}{% endif %}

{% if entity is not defined and p is defined %}{% if p['entity'] is defined %}{% set entity = p['entity'] %}{% endif %}{% endif %}

{#{ var_dump(entity.toArray(), true) }#}

{% set _url = '/suva/users-contacts/crud' %}
{% if entity.user_id > 0 %}{% set _url = _url ~ '/' ~ entity.user_id %}{% else  %}{% set _url = _url ~ '/0' %}{% endif %}
{% if entity.id > 0 %}{% set _url = _url ~ '/' ~ entity.id %}{% endif %}





<div class="row">
	<div class="col-lg-12 col-md-12">
		
		{% if _is_ajax_submit === false %} 
			{{ flashSession.output() }}
		{% endif %}
		
		{% if entity is defined %}
			{% set _form_id = '_form_' ~ _window_id %}
			{{ form(NULL, 'id' :  _form_id , 'method' : 'post', 'autocomplete' : 'off') }}
				
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type = "hidden" name="_window_id" value ="{{_window_id}}">
				<input type="hidden" name="_caller_url" value="{{ _caller_url }}"></input>				
				<input type ="hidden" name="_context[{{ _form_id }}]" value = "{{ _form_id }}"></input>
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				<input type="hidden" name="user_id" value="{{ entity.user_id }}"></input>
				<input type="hidden" name="sub_type" value=""></input>
				<input type="hidden" name="is_hidden" value="0"></input>
				<input type="hidden" name="type" value="phone"></input>
				<div class="panel-body">
					<div class="tab-content">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										{% if entity.id is defined %}
											<div class="pull-right">[ID: <b>{{ entity.id }}</b> ]</div>
										{% endif %}
										<h3 class="panel-title">{% if entity.id is not defined %}New {% endif %} Contact Info</h3>
									</div>
									<div class="panel-body">
										<div class = "row">
											<div class= "col-lg-12 col-md-12">
												<input type="radio" 
													{% if entity.type is not defined or entity.type =='phone' %}checked{% endif %}
													name="type" value="phone">Phone</input>
												<input type="radio" {% if entity.type == 'email' %}checked{% endif %} name="type" value="email">E-mail</input>
												<input type="radio" {% if entity.type == 'www' %}checked{% endif %} name="type" value="www">www</input>
											</div>
										</div>
										<div class="row">
											{{ partial("partials/ctlText",['value':entity.name, 'width':12, 'field':'name'] ) }} 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>				
				{{ partial ("partials/btnSaveAndExit") }}
				{#% include "partials/btnSaveCancel.volt" %#}
			{{ endForm() }}
		{% else %}
			ENTITY NIJE DEFINIRAN
		{% endif %}
	</div>
</div>