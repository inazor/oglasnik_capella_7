{% if _is_ajax_submit is not defined %}
	{% set _is_ajax_submit = '' %}
{% endif %}
{% set _is_ajax_submit = '' %}

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div>
			<h1 class="page-header">
				{% if entity.id is defined %}
					{{ _m.t( 'Issue No. %s',  entity.id ) }}
				{% else %}
					{{ _m.t('New Issue') }}
				{% endif %}
			</h1>
		</div>
		{{ partial("partials/blkFlashSession") }}
		{% if entity is defined %}
			{% set _context['ajax_edit'] = false %}
		
			{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
				{{ hiddenField('_csrftoken') }}
				{{ hiddenField('next') }}
				<input type="hidden" name="id" value="{{entity.id}}"></input>
				
				
				{% if entity.id is defined %}
					<div class="row ">
						<div class="col-lg-4 col-md-6">

							{% set _countInsertions = _model.Insertions(" issues_id = " ~ entity.id).count() %}
							<a  href = "/suva/insertions?_context[default_filter][issues_id]={{ entity.id }}" target="_blank">
								<span class="btn btn-primary  btn-m">{{ _m.t('Insertions for this issue ') }} (&nbsp;{{ _countInsertions }}&nbsp;) </span>&nbsp;
							</a>
							<br/><br/>

						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-md-6">
							<a  href = "/suva/ads2/adsInIssue/{{ entity.id }}" target="_blank">
								<span class="btn btn-primary  btn-m">{{ _m.t('Ads in Issue') }}  <i>{{ entity.n_path }}</i> </span>&nbsp;
							</a>
							<br/><br/>
						</div>
					</div>
				{% endif %}
				
				
				
				
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								{% if entity.id is defined %}
									<div class="pull-right">[{{ _m.t('ID:') }} <b>{{ entity.id }}</b> ]</div>
								{% endif %}
								<h3 class="panel-title">{{ _m.t('Issue details') }}</h3>
							</div>
							<div class="panel-body">
								<div class="row">
																											
									{{ partial("partials/ctlDate",[
										'value':entity.date_published, 
										'title':_m.t('Date published'), 
										'width':3, 
										'field':'date_published', 
										'ajax_edit':1,
										'ajax_table':'n_issues',
										'ajax_id':entity.id,
										'_context': _context
									] ) }}
									{{ partial("partials/ctlDropdown",[
										'value':entity.status, 
										'title':_m.t('Status'), 
										'list':['prima','deadline', 'popunjeno','izdano', 'nije_izdano'], 
										'width':3, 
										'field':'status', 
										'no_ids':1, 
										'ajax_edit':1,
										'ajax_table':'n_issues',
										'ajax_id':entity.id,

										'_context': _context
									] ) }}
									{{ partial("partials/ctlDropdown",[
										'value':entity.publications_id, 
										'title':_m.t('Publication'), 
										'list':_model.Publications(), 
										'width':3, 
										'field':'publications_id', 
										'ajax_edit':1,
										'ajax_table':'n_issues',
										'ajax_id':entity.id,
										'_context': _context
									] ) }}
									{{ partial("partials/ctlDate",[
										'value':entity.deadline, 
										'title':_m.t('Deadline'), 
										'width':3, 
										'field':'deadline', 
										'show_time': true,
										'ajax_edit':0,
										'ajax_table':'n_issues',
										'ajax_id':entity.id,
										'_context': _context
									] ) }}
								</div>
							</div>
						</div>
					</div>
				</div>
				{% include "partials/btnSaveCancel.volt" %}
			{{ endForm() }}
		{% else %}
			{{ _m.t('ENTITY NIJE DEFINIRAN') }}
		{% endif %}
	</div>
</div>

