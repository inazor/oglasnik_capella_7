<div class="row">

	
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">{{_model.t("App Settings")}}</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'app-settings'] ) }}
			{{ partial("partials/blkFlashSession") }}
		<table class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",['fields':[ 
				['title': _model.t("Options")				,'width':150	,'show_navigation' : true ]
				,['title':'ID'			,'name':'id'				,'width':100	,'search_ctl':'ctlNumeric'	]
				,['title':_model.t("Name")		,'name':'name'			,'width':250	,'search_ctl':'ctlText'	] 
				,['title':_model.t("Type")		,'name':'type'			,'width':150	,'search_ctl' : 'ctlDropdown'	,'search_ctl_params': [ 'list': [ 'boolean','integer','decimal','text' ], 'no_ids' : true ]]
				,['title': _model.t("Value Decimal"), 'name':'value_decimal','width':100	,'search_ctl':'ctlNumeric'		,'search_ctl_params': [ 'decimals':2 ] ]
				,['title': _model.t("Value Text"),'name':'value_text'	,'width':250	,'search_ctl':'ctlText'	]				
			] ]) }}
			

		
			{% for id in ids %}
			
				{% set entity = _model.getOne('AppSettings',id) %}
				
				<tr>
					<td class="text-right">{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'app-settings'] ) }}</td>
					<td>{{ entity.id }}</td>
					<td>{{ entity.name }}</td>
					<td>{{ entity.type }}</td>
					<td>{{ entity.value_decimal }}</td>
					<td>{{ entity.value_text }}</td>						
				</tr>
				{% else %}
				<tr><td colspan="6">Currently, there are no app settings here!</td></tr>
				
			{% endfor %}
		
        </table>

    </div>
</div>