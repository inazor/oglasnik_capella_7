<div class="row">
	
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">Permissions by user Roles</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'roles-permissions'] ) }}
			{{ partial("partials/blkFlashSession") }}

			<table class="table table-striped table-hover table-condensed">

			{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				['title':'Options'														,'width':150	,'show_navigation' : true ] 
				,['title':'ID'								,'name':'id'				,'width':100	,'search_ctl':'ctlNumeric'	]
				,['title':'Name'							,'name':'name'				,'width':150	,'search_ctl':'ctlText'	] 
				,['title':'Role'							,'name':'role_id'			,'width':150	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Roles() ]] 
				,['title':'Order status'					,'name':'order_status_id'	,'width':150	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.OrdersStatuses() ]] 
				,['title':'Action'							,'name':'action_id'			,'width':200	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list':_model.Actions() ]] 
				,['title':'Role name Filter'				,'name':'role_name_filter'	,'width':150	,'search_ctl':'ctlText'	] 
				,['title':'Action name Filter'				,'name':'action_name_filter','width':150	,'search_ctl':'ctlText'	] 
				,['title':'Permission(1) Restriction(0)'	,'name':'is_permission'		,'width':100	,'search_ctl':'ctlNumeric'	]
				,['title':'Is active'						,'name':'is_active'			,'width':100	,'search_ctl':'ctlNumeric'	]

			] ]) }}
			
    
		{% for id in ids %}
			{% set entity = _model.getOne('RolesPermissions',id) %}			
			{% set roles = _model.getOne('Roles', entity.role_id) %}
			{% set ordersStatuses = _model.getOne('OrdersStatuses', entity.order_status_id) %}
			{% set actions = _model.getOne('Actions', entity.action_id) %}
			{% set roles_name = roles ? roles.name : '' %}
			{% set ordersStatuses_name = ordersStatuses ? ordersStatuses.name : '' %}
			{% set actions_name = actions ? actions.name : '' %}
			
			<tr>
				<td style="white-space:nowrap;">{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'roles-permissions'] ) }}</td>
				<td>{{ entity.id }}</td>
				<td>{{ entity.name }}</td>
				<td>{{ roles_name }}</td>
				<td>{{ ordersStatuses_name }}</td>
				<td>{{ actions_name }}</td>
				<td>{{entity.role_name_filter}}</td>
				<td>{{entity.action_name_filter}}</td>
				<td>{{ partial("partials/ctlCheckbox", ['value':entity.is_permission, 'readonly':1] )}}</td>
				<td>{{ partial("partials/ctlCheckbox", ['value':entity.is_active, 'readonly':1] )}}</td>
				
			</tr>
            {% else %}
            <tr><td colspan="6">Currently, there are none here!</td></tr>
        {% endfor %}
        </table>
        
    </div>
</div>