{% set _form_url = '/admin/discounts' %}
{% if _form_id is not defined %}{% set _form_id = '1' %}{% endif %}
{% set _subform_id = 0 %}
{% if errors is not defined and _context is defined %}{% if _context['errors'] is defined %}{% set errors = _context['errors'] %}{% endif %}{% endif %}

{% if ids is not defined and p is defined %}{% if p['ids'] is defined %}{% set ids = p['ids'] %}{% endif %}{% endif %}


{# Admin User Listing/View #}

	{#{ print_r(_context,true)}#}
	
{% set is_ajax = false %}
{% if _context is defined %}{% if _context['_is_ajax_submit'] is defined %}{% if _context['_is_ajax_submit'] === true %}{% set is_ajax = true %}{% endif %}{% endif %}{% endif %}
	

<div class="row">
	<div class="col-lg-12 col-md-12">
		<div>
			
			<h1 class="page-header">Discounts</h1>
		</div>
		<br/>
		{{ partial("partials/btnAddNew", ['controller':'discounts'] ) }}
		{{ partial("partials/blkFlashSession") }}
		<table class="table table-striped table-hover table-condensed">
			{{ partial("partials/blkTableSort",[
			'_context' : _context,
			'fields':[ 
				['title':'Options'								,'width':150	,'show_navigation' : true ]
				,['title':'ID'			,'name':'id'			,'width':100		,'search_ctl':'ctlNumeric'	]
				,['title':'Name'		,'name':'name'			,'width':230	,'search_ctl':'ctlText'	] 
				,['title':'Amount'		,'name':'amount'		,'width':180	,'search_ctl':'ctlNumeric'	,'search_ctl_params': [ 'decimals':2 ] ] 
				,['title':'Tag'			,'name':'tag'			,'width':230	,'search_ctl':'ctlText'	] 
				,['title':'Category'	,'name':'category_id'	,'width':100	,'search_ctl':'ctlDropdown'	,'search_ctl_params': [ 'list': _model.Categories(), 'option_fields':['n_path'] ]] 
				,['title':'Is active'	,'name':'is_active'		,'width':100		,'search_ctl':'ctlNumeric'	]
			] ]) }}
			
			{% for id in ids %}
				{% set entity = _model.getOne('Discounts', id) %}
				
				{% set _context['current_entity_id'] = entity.id %}
				{% set _context['ajax_edit'] = true %}
				
					{#{ print_r(_context,true)}#}

				<tr>
					<td>{{ partial("partials/btnEditDelete", ['value':entity.id , 'controller':'discounts'] ) }}</td>
					<td>{{ entity.id }}</td>
					<td>
						{{ partial("partials/ctlText",[
							'value':entity.name, 
							'field':'name',
							'_context':_context
						] )}} 

						
					</td>
					<td>
						{{ partial("partials/ctlNumeric",[
							'value':entity.amount, 
							'field':'amount', 
							'decimals':0,
							'_context':_context
						] ) }}
					
					</td>
					<td>{{ partial("partials/ctlText",[
							'value':entity.tag, 
							'field':'tag',
							'_context':_context
						] )}} </td>
					<td>{#{ _model.Category(entity.category_id).n_path }#} 
					
					{{ partial("partials/ctlDropdown",[
						'value':entity.category_id, 
						'list':_model.Categories(), 
						'field':'category_id', 
						'option_fields':['n_path'],
						'_context':_context
					] ) }}	
					
					</td>
					<td>{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'readonly':1 ] ) }}</td>
					
				</tr>
			{% else %}
				<tr><td colspan="7">Currently, there are no discounts here!</td></tr>
			{% endfor %}
		</table>
	</div>
	
	
	<div class = "col-lg-12 col-md-12" style = "border:1px solid black">
		
		{#% set _subform_id += 1 %}
		{{ partial("partials/wraSubFormAjax",[
			'_form_id' : _form_id,
			'_subform_id' : _subform_id,
			'_form_name': 'discounts/crud', 
			'_model': _model, 
			'_context': _context, 
			'p': ['entity' : _model.Discount(10) ]
		] ) }#} 
	
	</div>
	
	
</div>