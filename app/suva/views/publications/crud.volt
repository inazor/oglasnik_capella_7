{# CONTEXT<pre>{{ print_r( _context, true ) }}</pre>#}
<style>
	/*da se buttoni ispisuju u više redova*/
	.btn{
		white-space : normal;
	}
</style>


{% if errors is not defined%}
	{% set errors = null %}
{% endif %}

{% if _is_ajax_submit is not defined%}
	{% set _is_ajax_submit = false %}
{% endif %}
{% set _count = 0 %}

<div class="row"> 
	<div class="col-lg-12 col-md-12">
		{{ partial("partials/blkFlashSession") }}
	
		<h1 class="page-header">
			{% if entity.id is not defined %}
				New Publication
			{% else %}
				Publication <i>{{ entity.name }}</i>
			{% endif %}
		</h1>
	</div>
	{% if entity is defined %}
		
		<div class="col-lg-12 col-md-12">
		
			{% if entity.id is defined %}
				<div class="row ">
					<div class="col-lg-4 col-md-6">
						{% set _countProducts = _model.Products("publication_id = " ~ entity.id).count() %}
						<a  href = "/suva/products?_context[default_filter][publication_id]={{ entity.id }}" target="_blank">
							<span class="btn btn-primary  btn-m">Products for {{ entity.name }} (&nbsp;{{_countProducts}}&nbsp;) </span>&nbsp;
						</a>
					</div>
					<br/><br/>
				</div>
				<div class = "row ">
					<div class="col-lg-4 col-md-6 ">
						{% set _countCategoriesMappings = _model.CategoriesMappings("n_publications_id = " ~ entity.id).count() %}
					
						<a  href = "/suva/categories-mappings?_context[default_filter][n_publications_id]={{ entity.id }}" target="_blank">
							<span class="btn btn-primary  btn-m">Categories for {{ entity.name }} (&nbsp;{{_countCategoriesMappings}}&nbsp;) </span>&nbsp;
						</a>
						
						<br/><br/>
					</div>
					<div class="col-lg-4 col-md-6 ">
						<a   href = "/suva/publications/createDefaultCategoryTree/{{ entity.id }}">
							<span class="btn btn-danger  btn-m">Create default Categories tree</span>&nbsp;
						</a>
						<a   href = "/suva/publications/createBlankCategoryMapping/{{ entity.id }}">
							<span class="btn btn-warning  btn-m">Create blank Category for {{entity.name}}</span>&nbsp;
						</a>
					</div>
				</div>

				<div class="row ">
					<div class="col-lg-4 col-md-6">

						{% set _countPCP = _model.ProductsCategoriesPrices(" publication_id = " ~ entity.id).count() %}
						<a  href = "/suva/products-categories-prices?_context[default_filter][publication_id]={{ entity.id }}" target="_blank">
							<span class="btn btn-primary  btn-m">Products-Categories-prices list for {{ entity.name }}  (&nbsp;{{_countPCP}}&nbsp;) </span>&nbsp;
						</a>
						<br/><br/>

					</div>
					<div class="col-lg-4 col-md-6 ">
						<a  href = "/suva/publications/createDefaultProductsCategoriesPrices/{{ entity.id }}" >
							<span class="btn btn-danger  btn-m">Create default Products-Categories-prices list for {{ entity.name }} </span>&nbsp;
						</a>

					</div>
				</div>
				<div class = "row ">
					<div class="col-lg-4 col-md-6 ">
						{% set _countPublicationsCategoriesMappings = entity.PublicationsCategoriesMappings().count() %}
						<a  href = "/suva/publications-categories-mappings/index?_context[default_filter][publication_id]={{ entity.id }}" target="_blank">
							<span class="btn btn-primary  btn-m">Mappings for {{ entity.name }} (&nbsp;{{ _countPublicationsCategoriesMappings }}&nbsp;)</span>&nbsp;
						</a>
						<br/><br/>
					</div>
					<div class="col-lg-4 col-md-6 ">
						<a  href = "/suva/publications/createDefaultCategoryMappings/{{ entity.id }}">
							<span class="btn btn-danger  btn-m">Create default Categories mappings</span>&nbsp;
						</a>
					</div>
				</div>
				<div class = "row ">
					<div class="col-lg-4 col-md-6 ">
					</div>
					<div class="col-lg-4 col-md-6 ">
						<a  href = "/suva/publications/refreshAdsSearchWords/{{ entity.id }}">
							<span class="btn btn-danger  btn-m">Re-generate ads search database</span>&nbsp;
						</a>
					</div>
				</div>
			{% endif %}				
		</div>
		<div class="col-lg-12 col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					{% if entity.id is defined %}

						<div class="pull-right">&nbsp;&nbsp;&nbsp;[ID: <b>{{ entity.id }}</b> ]</div>
						  
					{% endif %}
					<h3 class="panel-title">Publication details</h3>
				</div>
				<div class="panel-body">
					{{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
						{{ hiddenField('_csrftoken') }}
						{{ hiddenField('next') }}
						<input type="hidden" name="_gfId" value="{{_gfId}}"/>
						<input type="hidden" name="id" value="{{entity.id}}"></input>
						<div class="row">
							{{ partial("partials/ctlText",['value':entity.name, 'title':'Name', 'width':4, 'field':'name', '_context': _context]) }}
							{{ partial("partials/ctlText",['value':entity.slug, 'title':'Slug', 'width':2, 'field':'slug', '_context': _context]) }}
						</div>

						<div class="row">
							{{ partial("partials/ctlText",['value':entity.cycle_day_of_week, 'title':'Cycle day of week', 'width':3, 'field':'cycle_day_of_week'] ) }}                   
							{{ partial("partials/ctlText",['value':entity.cycle_day_of_month, 'title':'Cycle day of month', 'width':3, 'field':'cycle_day_of_month'] ) }}
							{{ partial("partials/ctlText",['value':entity.cycle_week_of_month, 'title':'Cycle week of month', 'width':3, 'field':'cycle_week_of_month'] ) }}
							{{ partial("partials/ctlText",['value':entity.cycle_month_of_year, 'title':'Cycle month of year', 'width':3, 'field':'cycle_month_of_year'] ) }}
						</div>
						<div class="row">
							{{ partial("partials/ctlDate",['value':entity.start_date, 'title':'Start date', 'width':3, 'field':'start_date'] ) }}  
							{{ partial("partials/ctlText",['value':entity.deadline_day_of_week, 'title':'Deadline day of the week', 'width':3, 'field':'deadline_day_of_week'] ) }}
							{#{ partial("partials/ctlDate",['value':entity.deadline_hour, 'title':'Deadline hour', 'width':3, 'field':'deadline_hour'] ) }#}    
							{{ partial("partials/ctlText",['value':entity.deadline_hour, 'title':'Deadline hour', 'width':3, 'field':'deadline_hour'] ) }}    
							{{ partial("partials/ctlCheckbox",['value':entity.is_online, 'title':'Online', 'width':1, 'field':'is_online'] ) }}							
							{{ partial("partials/ctlCheckbox",['value':entity.is_active, 'title':'active', 'width':1, 'field':'is_active'] ) }}	
						</div>
						<div class="row">
							{{ partial("partials/ctlTextArea",['value':entity.layout|default(''), 'title':'Layout', 'width':12, 'field':'layout'] ) }}	
						</div>
						<div class = "row">
							{{ partial("partials/ctlNumeric",['value':entity.nav_object_id, 'title':'Navision Object ID', 'width':2, 'field':'nav_object_id', 'decimals':0, '_context': _context] ) }}
						</div>
						{% include "partials/btnSaveCancel.volt" %}
					{{ endForm() }}								
				</div>
			</div>
		</div>
	
	{% else %}
		ENTITY NIJE DEFINIRAN
	{% endif %}
</div>
