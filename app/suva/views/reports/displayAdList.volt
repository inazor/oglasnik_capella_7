<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous"/>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"/>



<div class="page" style="font:12px arial, sans-serif;  margin:0;" >

{#CONTEXT<pre>{{ print_r( _context, true ) }}</pre> #}

<h1> DisplayAd List Report</h1>

{% set issue = _model.Issue(_context['xml_report_issue_id']) %}
<h4>Issue: {{ issue.id }}</h4>
<h4>Date: {{ issue.date_published }}</h4>

{#<table class="table table-bordered">
	<tr>
		<td>Publications</td>
		<td>Issue</td>
		<td>Ad Taker</td>
		<td>Layouts Name</td>
		<td>ID</td>
		<td>File Name</td>
		<td>File Format</td>
		<td>Width(column)</td>
		<td>Height(mm)</td>
	</tr>
#}	
	Publication;Issue;Category;Ad Taker;Layout;Product;Ad ID;File Name;Width;Height
	<br/>
	!!!!!!!!!!!!!!!!!!!!!!!
{% for insertion in issue.Insertions() %}

		{% set order_item = insertion.OrderItem() %}
		{% if order_item %}
			{% set ad_taker = order_item.AdTaker() %}
			{% set ad = order_item.Ad() %}
			{% set product = order_item.Product() %}
			{% set publication = product.Publication() %}
			{% set category = ad.Category() %}
			{% set layout = product.Layout()%}
			
			{% if ad.n_is_display_ad %}
				
					{{ publication.name }};
					{{ issue.id }};
					{{ category.n_path }};
					{{ ad_taker.username }};
					{{ layout.name }};
					{{ product.name }};
					{{ ad.id }};
					{{ ad.user_id }}_{{ ad.id }}.eps;
					{{ product.display_ad_width }};
					{{ product.display_ad_height }}
			{% endif %}
		{% endif %}	
		<br/>
{% endfor %}





	


</div>
