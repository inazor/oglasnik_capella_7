{# Admin User Edit View #}
<div class="row">
	<div class="col-lg-12 col-md-12">
		<div>
			<a href='#' onclick="window.history.back();" class="btn btn-primary">&lt;&lt; Back</a>
            
			<h1 class="page-header">Orders Items <small>{{ form_title_long }}</small></h1>
		</div>

        {{ partial("partials/blkFlashSession") }}

        {% if auth.logged_in(['admin', 'support']) %}
        {{ form(NULL, 'id' : 'frm_register', 'method' : 'post', 'autocomplete' : 'off') }}
            {{ hiddenField('_csrftoken') }}
            {{ hiddenField('next') }}
		
		<input type="hidden" name="action" value="{{form_action}}">
		
		{% if order_item.ad_id is defined %}
			<input type="hidden" name="ad_id" id="ad_id" value="{{ order_item.ad_id}}">
		{% endif %}
			
		{% if order_item.id is defined %}
			<input type="hidden" name="order_item_id" id="order_item_id" value="{{ order_item.id}}">
		{% endif %}
			
        {% endif %}
<!-- 			{ { print_r(products,true) } } -->
			
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<div class="panel panel-default">
						<div class="panel-heading">
                {% if order_item.id is defined %}
                        <div class="pull-right">[ID: <b>{{ order_item.id }}</b> ]</div>
                        {% endif %}
                 {% if order_item.ad_id is defined %}
                        <div class="pull-right">[Ad ID: <b>{{ order_item.ad_id }}</b> ]</div>
                        {% endif %}
                        <h3 class="panel-title">Order item</h3>         
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-lg-8 col-md-8">
									<div class="row">
										<div class="col-lg-6 col-md-6">
			                                {% set field = 'n_publications_id' %}

												<label class="control-label" for="{{ field }}">Publication</label>
												<select disabled="disabled" id="offline-publications" name="{{field}}" class="form-control" title="Publication">

						                             {%- for pub in pubs -%}
																<option 
																{% if order_item.n_products_id is defined %}
																{% if pub['id'] == order_item.n_publications_id %} selected="selected" {% endif %}
																{% endif %}
																 value="{{ pub['id'] }}">{{ pub['name'] }}</option>
						                             {%- endfor -%}
												</select>
										</div>
										<div class="col-lg-6 col-md-6">
											<label class="control-label">Issues</label>
											<div id="sel-spot-issues">
											{% if !(issues is empty) %}
												{%- for issue in issues -%}	
		                                    		<input disabled="disabled" type="checkbox" {% if issue['checked'] == '1' %} checked {% endif %} name="offline-issues[]" value="{{ issue['issues_id'] }}"><label for="offline-issues"><span class="text-primary"> {{ date('d.m.Y', strtotime(issue['date_published'])) }} {{ issue['day_of_week'] }}</span></label><br>
		                                    	{%- endfor -%}
		                                    {%- endif -%}
											</div>
										</div>
									</div>
									<hr>
									<div class="row">
										<div class="col-lg-6 col-md-6">
												<label class="control-label">Category: </label> {{ category_name  }}
												<br/>
												<label class="control-label">Product</label>
												<div id='select-products' class='products-group-offline'>
														<div class='icon_dropdown pull_right'>
															<select disabled="disabled" id='offline-products-option' name='n_products_id' class='select-product-options n_sel'>
												 {% if order_item.n_products_id is defined %}
														{%- for product in products -%}
															<option  {% if product['id'] == order_item.n_products_id %} selected="selected" {% endif %} value="{{ product['id'] }}">{{ product['name'] }} - {{ product['pcp_unit_price'] }} kn</option>
														{%- endfor -%}
												 {% else %}
																<option value="0">-- Choose publication first --</option>
												 {% endif %}
															</select>
														</div>
												</div> 
										</div>
										<div class="col-lg-6 col-md-6">
		                            		<label class="control-label">Discounts</label>

											<div id="sel-spot-discounts">
												<div id='select-discounts'>
												{% if !(discounts is empty) %}
			                                		{%- for discount in discounts -%}	
			                                    		<input disabled="disabled" type="checkbox" {% if discount['checked'] == '1' %} checked {% endif %} name="offline-discounts[]" value="{{ discount['id'] }}"><label for="offline-discounts"><span class="text-primary">{{ discount['name'] }}</span></label><br>
			                                    	{%- endfor -%}
		                                    	{%- endif -%}

		                                    	</div>
		                                    </div>
										</div>
										
									</div>
											
											
									<div class="row">
										<div class="col-lg-12 col-md-12">
										{% set field = 'n_remark' %}
											<div class="form-group{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
												<label class="control-label" for="{{ field }}">Remark</label>
												<input disabled="disabled" class="form-control" name="{{ field }}" id="{{ field }}" value="{{order_item.n_remark | default('')}}" maxlength="24" />
												{% if errors is defined and errors.filter(field) %}
													<p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
												{% endif %}
											</div>
										</div>
									</div>
									  {% set field = 'n_eps_filename' %}
                                            <div class="col-lg-6 col-md-6 companyField{{ errors is defined and errors.filter(field) ? ' has-error' : '' }}">
                                                <div class="form-group">
                                                    <label class="control-label">Eps Filename</label>
                                                    <input disabled="disabled" placeholder="Eps filename" class="form-control" name="{{ field }}" id="{{ field }}" value="{{order_item.n_eps_filename | default('')}}" maxlength="128">
                                                {% if errors is defined and errors.filter(field) %}
                                                    <p class="help-block">{{ current(errors.filter(field)).getMessage() }}</p>
                                                {% endif %}
                                                </div>
                                            </div>		
											
								</div>
								<div class="col-lg-4 col-md-4">
									<table class="table table-condensed table-striped table-bordered">
									 	{% if ad.user_id is defined %}
					                    <tr>
					                        <td>Ad taker</td>
					                        <td><span class="fa fa-user"></span> {{ ad.user_id }}</td>
					                    </tr>
					                    {% endif %}
					                    
					                    <tr>
					                        <td>Order id</td>
					                        <td>{{ order_item.order_id}}</td>
					                    </tr>
					                    
					                    {% if order_item.n_first_published_at is defined %}
					                    <tr>
					                        <td>Created</td>
					                        <td><span class="fa fa-clock-o"></span> {{ date('Y-m-d H:i:s', order_item.n_first_published_at) }}</td>
					                    </tr>
					                    {% endif %}
					                    {% if order_item.n_expires_at is defined %}
					                    <tr>
					                        <td>Expires at</td>
					                        <td><span class="fa fa-clock-o"></span> {{ date('Y-m-d H:i:s', order_item.n_expires_at) }}</td>
					                    </tr>
					                    {% endif %}
					                    
					                    {% if order_item.qty is defined %}
											<tr>
												<td>Qty</td>
												<td><span class="fa fa-money"></span> {{ order_item.qty }}</td>
											</tr>

											<tr>
												<td>Unit Price</td>
												<td><span class="fa fa-money"></span> {{ order_item.price/100}}</td>
											</tr>

											<tr>
												<td>Total without discount</td>
												<td><span class="fa fa-money"></span> {{ order_item.price * order_item.qty/100 }}</td>
											</tr>

											<tr>
												<td>Discount</td>
												<td><span class="fa fa-money"></span> {{ (order_item.price * order_item.qty - order_item.total)/100  }}</td>
											</tr>

											{% if order_item.qty * order_item.price  >0  %}
												<tr>
													<td>Cumulative discount %</td>
													<td><span class="fa fa-money"></span> {{ ( (order_item.price * order_item.qty) - order_item.total ) / (order_item.price * order_item.qty) * 100 }} %</td>
												</tr>
											{% endif %}

											<tr>
												<td><b>Line Total</b></td>
												<td><span class="fa fa-money"> {{ order_item.total/100 }}</td>
											</tr>
												
											<tr>
												<td>Tax {{ order_item.tax_rate * 100 }} % </td>
												<td><span class="fa fa-money"> {{ order_item.tax_amount }}</td>
											</tr>
																							
											<tr>
												<td><b>Line Total with Tax</b></td>
												<td><span class="fa fa-money"> {{ order_item.total/100 + order_item.tax_amount }}</td>
											</tr>
					                    {% endif %}
					                </table>
								</div>
						</div>
					</div>
				</div>
			</div>


         {% if auth.logged_in(['admin', 'support']) %}
			<div class="col-lg-12 col-md-12">				
				<button class="btn btn-default" type="submit" name="cancel">Cancel</button>
				<button class="btn btn-default" onclick="history.go(-1);">Back</button>
			</div>
        {{ endForm() }}
        {% else %}
			<div>
				<button class="btn btn-default" onclick="history.go(-1);">Back</button>
			</div>
        {% endif %}
		</div>
	</div>
	