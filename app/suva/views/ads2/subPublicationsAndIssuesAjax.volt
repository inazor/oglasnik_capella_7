<!-- subPublicationsAndIssuesAjax.volt -->
{% set _is_ajax_submit = true %}

{%  set _form_id = 'form_subPublicationsAndIssuesAjax' %}
{%  set _window_id = 'subPublicationsAndIssuesAjax' %}
{%  set _url = '/suva/ads2/subPublicationsAndIssuesAjax/' ~ ad.id %}
{%  set _function_to_execute_after_load = 'ctlFileUploadAjax' %}
{%  set _active_tab = (  array_key_exists('_active_tab', _context) ? _context['_active_tab'] : null ) %}


{% if ad.user_id > 0 %}
	{% set _additional_js_onclick_save = 
		"ajaxSubmit ( '/suva/users-contacts/index/" ~ ad.user_id ~ "', 'subUsersContactsDisplay' );
		ajaxSubmit( '/suva/ads2/subInsertions/" ~ ad.id ~ "' , 'subInsertionsDisplay'  );
		" %}
{% else %}
	{% set _additional_js_onclick_save = ''%}
{% endif %}

{% if ! _is_ajax_submit %}
	{{ partial("partials/blkFlashSession") }}
{% endif %}

					<!-- FORMA -->
					{{ form(NULL, 'id' :  _form_id , 'enctype' : "multipart/form-data", 'method' : 'post', 'autocomplete' : 'off') }}
						{{ hiddenField('next') }}
						{{ hiddenField('_csrftoken') }}
						{% if ad is defined and ad.id is defined %}
							{{ hiddenField(['ad_id', 'value': ad.id]) }}
						{% endif %}
						{{ hiddenField(['user_id', 'value': ad.user_id]) }}
					
						<input type="hidden" name="_context[selected_user_id]" value = "{{ad.user_id}}" ></input>
						<input type="hidden" name="_context[clicked_action]" id="_clicked_action_subPublicationsAndIssuesAjax" value = "" ></input>

						<input type="hidden" name="n_first_uploaded_photo_id" value="{{ad.n_first_uploaded_photo_id}}"/> 
						<input type="hidden" name="moderation" value="ok"/> 
						<input type="hidden" id="_context_active_tab_subPublicationsAndIssues" name = "_context[_active_tab]" value="{{ _active_tab }}"></input>
						<input type="hidden" name="_context[clicked_action_params][logged_user_id]" value="{{ auth.get_user().id }}"/> 
			
						<!-- Publications & Issues -->
						{{ partial("ads2/subPublicationsAndIssues" ) }}
						
					
						<!-- Moderation  -->
						{#% if _user.isAllowed('frmAdsEdit_secModeration') %}
							{{ partial("ads2/subModeration" ) }}
						{% endif %#}
							
						
					{{ endForm() }}	
					
					<!-- Informacije o datumima -->
					{% if ad is defined and ad.id is defined %} 
						<div class="col-md-12 col-lg-12">
							<table class="table table-condensed table-striped table-bordered">
								<tr{{ ad.latest_payment_state == constant('Baseapp\Models\Ads::PAYMENT_STATE_CANCELED_ORDER') ? ' class="danger"' : '' }}>
									<td>Last payment state</td>
									<td><span class="fa fa-money"></span> {{ Models_Ads__getPaymentStateString(ad.latest_payment_state) }}</td>
								</tr>
								<tr>
									<td>Created</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.created_at) }}</td>
								</tr>
								<tr>
									<td>Last modified</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.modified_at) }}</td>
								</tr>
								<tr>
									<td>Virtual publish date (sort)</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.sort_date) }}</td> 
								</tr>
								<tr>
									<td>First published at</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.first_published_at) }}</td>
								</tr>
								<tr>
									<td>Last published at</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.published_at) }}</td>
								</tr>
								{% set classname = ad.isExpired() ? 'danger' : 'success' %}
								<tr class="{{ classname }}">
									<td>Expire{{ ad.isExpired() ? 'd' : 's' }}</td>
									<td><span class="fa fa-clock-o"></span> {{ date('d.m.Y H:i:s', ad.expires_at) }}</td>
								</tr>
								{% set classname = ad.active ? 'success' : 'danger' %}
								<tr class="{{ classname }}">
									<td><b>Currently active</b></td>
									<td><span class="fa fa-{{ ad.active ? 'check' : 'close' }}"></span> {{ ad.active ? 'YES' : 'NO' }}</td>
								</tr>
								<tr>
									<td>Currently active order item</td>
									<td><span class="fa fa-clock-o"></span>
										<span
											ondblclick = "window.open ('/suva/orders-items/edit/{{ad.n_active_online_order_item_id}}','_blank');
												">
											{{ ad.n_active_online_order_item_id }}
										</span>
									</td>
								</tr>
								<tr>
								<td>Last active Pushup</td>
									<td><span class="fa fa-clock-o"></span>
										<span
											ondblclick = "window.open ('/suva/orders-items/edit/{{ad.n_last_active_pushup_order_item_id}}','_blank');
												">
											{{ ad.n_last_active_pushup_order_item_id }}
										</span>
									</td>
								</tr>								
								
							</table>
						</div>
						
					{% endif %}
					