{#  
ulaz: 
 ad  - može biti null
 user - mora bit



#}

{#{  var_dump(ad) }#}

{{ partial("ads2/subEditCss" ) }}



{% if array_key_exists('_active_tab' , _context ) %}
	{% set _active_tab = _context['_active_tab'] %}
{% else %}
	{% set  _active_tab = 'users_ads' %}<br/>
{% endif %}
{% set  _context['_keep_active_tab_settings'] = true %}{# čemu ovo služi? #}


{#  nakon flashsessina se gubi sadrža _context['_active_tab'] #}

<script>
	var zadnjiKliknutiOrder = 0;
</script>

<span id = "var_current_ad_id" value = "{{ad.id}}" style = "display:none;"></span>
<span id = "var_current_ad_is_display" value="0"  style = "display:none;"></span>
<span id = "var_current_user_id" value = {{ _context['selected_user_id'] }} style = "display:none;"></span>

	<div class="row">
		<!-- FlashSession -->
		<div class="col-lg-12 col-md-12">
			{{ partial("partials/blkFlashSession") }}
		</div>
	</div>
	{% if ad.user_id > 0  and  ad.category_id > 0 %}
		
		<div class="row">
			<!-- LIJEVA POLOVINA EKRANA -->
			<div class="col-md-12 col-lg-12">
				<div class="panel panel-default">

					{% set field = 'user_id' %}
					
					<!-- OVO TREBA RIJEŠIT PREKO JAVASCRIPTA -->
					{% set count_ads = user.Ads(['order': 'id desc']).count() %}
					{% set count_offers = user.Orders(['n_status = 3', 'order':'id desc']).count() %}
					{% set count_invoices = user.Orders(['n_status = 4', 'order':'id desc']).count() %}
					{% set count_paid = user.Orders(['n_status = 5', 'order':'id desc']).count() %}
					{% set count_cancelled = user.Orders(['n_status = 6', 'order':'id desc']).count() %}
					{% set count_late = user.Orders(['n_status = 9', 'order':'id desc']).count() %}
					{% set count_deactivated = user.Orders(['n_status = 10', 'order':'id desc']).count() %}

					{% set count_cart = 0 %}
					{% for ao in user.Orders("n_status = 1")  %}
						{% for oi in ao.OrdersItems(['order': 'id desc']) %}
							{% set count_cart +=1 %}
						{% endfor %}
					{% endfor %}

					{% set count_free = 0 %}
					{% for ao in user.Orders(['n_status = 2', 'order':'id desc'])  %}
						{% for oi in ao.OrdersItems(['order': 'id desc']) %}
							{% set count_free +=1 %}
						{% endfor %}
					{% endfor %}
					
					{# varijabla za pamćenje koji je zadnji tab bio aktivan, defaultno je 'basic' #}
					{#{ var_dump(_context)}#}

					<div class="panel-body">

						<!--Naslovi Tab-ova-->
						<ul id="top_tab_bar" class="nav nav-tabs" role="tablist">

							
							
								<li
									id = "tabUsersAds"
									role="presentation"
									{% if _active_tab == 'users_ads' %} class = "active" {% endif %}
									>
									<a
										href="#users_ads"
										aria-controls="users_ads"
										role="tab"
										data-toggle="tab"
										
										onclick = "
											/*
											$('#_context_active_tab').val('users_ads');
											$('#_context_active_tab_subUsersAds').val('users_ads');
											*/
											$('#_context_active_tab').val('users_ads');
											$('#_context_active_tab_subUsersAds').val('users_ads');
											{%  if count_ads < 1000 %}
												//ajaxSubmit( '/suva/ads2/subUsersAds/{{ad.user_id}}' , 'subUsersAds'  );
												ajaxSubmitP ({ 
													url: '/suva/ads2/subUsersAds/{{ad.user_id}}' 
													,windowId: 'subUsersAds'
													,isDebug: false
												});
											{% endif %}
											"
										>
										
										<b>A</b>ds 
											<span id="count_items_users_ads">
												{% if count_ads > 0 %} ({{ count_ads }}) {% endif %}
											</span>
									</a>
								</li>
							

							
							<!-- Single AD -->
							<li
								id = "tabSingleAd"
								role="presentation"
								{% if _active_tab === 'single_ad' %} class="active" {% endif %} 
								style = "{% if ad  %}display:block;{% else %}display:none;{% endif %}"
								>
								<a
									id="single_ad_link"
									href="#single_ad"
									aria-controls="single_ad"
									role="tab"
									data-toggle="tab"
									onclick = "
										//alert ( $('#var_current_ad_id').attr('value') );
										ads2RefreshAd({
											 adId : $('#var_current_ad_id').attr('value')
											, userId : $('#var_current_user_id').attr('value')
											, isDisplayAd : $('#var_current_ad_is_display').attr('value')
											, isSubmitForm : false
											, isRefreshAd : true
											, isRefreshUsersContacts : true
											, isRefreshModeration : false
											, isRefreshPublicationsAndIssues : false
											});
										"
									>
									{% if ad.n_is_display_ad %}Display{% endif %} Ad {{ad.id}} 
								</a>
							</li>

							<!-- Cart -->
							<li
								role="presentation"
								{% if _active_tab === 'users_orders_items' %} class="active" {% endif %}
								>
								<a
									href="#users_orders_items"
									aria-controls="users_ads"
									role="tab"
									data-toggle="tab"
									onclick = "
										$('#_context_active_tab').val('users_orders_items');
										$('#_context_active_tab_subUsersCart').val('users_orders_items');
										ajaxSubmit( '/suva/ads2/subUsersCart/{{ad.user_id}}' , 'subUsersCart'  );
										"
									>
									<B>C</B>art 
										<span id="count_items_users_cart">
											{% if count_cart > 0 %} ({{ count_cart }}) {% endif %}
										</span>
								</a>
							</li>

							<!-- Offers -->
							<li
								role="presentation"
								{% if _active_tab === 'users_offers' %} class="active" {% endif %}
								>
								<a
									href="#users_offers"
									aria-controls="users_ads"
									role="tab"
									data-toggle="tab"
									onclick = "
										zadnjiKliknutiOrder = 0;
										$('#_context_active_tab').val('users_offers');
										$('#_context_active_tab_subOrdersWithItems_3').val('users_offers');
										ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/3' , 'subOrdersWithItems_3'  );
										"
									>
									<B>O</B>ffers 
										<span id="count_items_order_status_3">
											{% if count_offers > 0 %} ({{ count_offers }}) {% endif %}
										</span>
								</a>
							</li>

							<!-- Free -->
							<li
								role="presentation"
								{% if _active_tab === 'users_free_ads' %} class="active" {% endif %}
								>
								<a
									href="#users_free_ads"
									aria-controls="users_ads"
									role="tab"
									data-toggle="tab"
									onclick = "
										/*
										$('#_context_active_tab').val('users_free_ads');
										$('#_context_active_tab_subUsersFreeAds').val('users_free_ads');
										*/
										$('#_context_active_tab').val('users_free_ads');
										$('#_context_active_tab_subUsersFreeAds').val('users_free_ads');
										ajaxSubmit( '/suva/ads2/subUsersFreeAds/{{ad.user_id}}' , 'subUsersFreeAds'  );
										"
										
									>
									<B>F</B>ree 
										<span id="count_items_users_free_ads">
											{% if count_free > 0 %} ({{ count_free }}) {% endif %}
										</span>
								</a>
							</li>

							<!-- Invoices -->
							<li
								role="presentation"
								{% if _active_tab === 'users_invoices' %} class="active" {% endif %}
								>
								<a
									href="#users_invoices"
									aria-controls="users_ads"
									role="tab"
									data-toggle="tab"
									onclick = "
										zadnjiKliknutiOrder = 0;									
										$('#_context_active_tab').val('users_invoices');
										$('#_context_active_tab_subOrdersWithItems_4').val('users_invoices');
										ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/4' , 'subOrdersWithItems_4'  );
										"
									>
									Unpaid invoices 
										<span id="count_items_order_status_4">
											{% if count_invoices > 0 %} ({{ count_invoices }}) {% endif %}
										</span>
								</a>
							</li>

							<!-- Paid -->
							<li
								role="presentation"
								{% if _active_tab === 'users_paid' %} class="active" {% endif %}
								>
								<a
									href="#users_paid"
									aria-controls="users_ads"
									role="tab"
									data-toggle="tab"
									onclick = "
										zadnjiKliknutiOrder = 0;									
										$('#_context_active_tab').val('users_paid');
										$('#_context_active_tab_subOrdersWithItems_5').val('users_paid');
										ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/5' , 'subOrdersWithItems_5'  );
										"
									>
									Paid invoices 
										<span id="count_items_order_status_5">
											{% if count_paid > 0 %} ({{ count_paid }}) {% endif %}
										</span>
								</a>
							</li>

							<!-- Cancelled -->
							<li
								role="presentation"
								{% if _active_tab === 'users_cancelled' %} class="active" {% endif %}
								>
								<a
									href="#users_cancelled"
									aria-controls="users_ads"
									role="tab"
									data-toggle="tab"
									onclick = "
										zadnjiKliknutiOrder = 0;									
										$('#_context_active_tab').val('users_cancelled');
										$('#_context_active_tab_subOrdersWithItems_6').val('users_cancelled');
										ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/6' , 'subOrdersWithItems_6'  );
										"
									>
									Cancelled 
										<span id="count_items_order_status_6">
											{% if count_cancelled > 0 %} ({{ count_cancelled }}) {% endif %}
										</span>
								</a>
							</li>

							{#
							<!-- Late -->
							<li
								role="presentation"
								{% if _active_tab === 'users_late' %} class="active" {% endif %}
								>
								<a
									href="#users_late"
									aria-controls="users_ads"
									role="tab"
									data-toggle="tab"
									onclick = "
										zadnjiKliknutiOrder = 0;							
										$('#_context_active_tab').val('users_late');
										$('#_context_active_tab_subOrdersWithItems_9').val('users_late');
										ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/9' , 'subOrdersWithItems_9'  );
										"									>
									Late 
										<span id="count_items_order_status_9">
											{% if count_late > 0 %} ({{ count_late }}) {% endif %}
										</span>
								</a>
							</li>
							#}
							<!-- Deactivated -->
							<li
								role="presentation"
								{% if _active_tab === 'users_deactivated' %} class="active" {% endif %}
								>
								<a
									href="#users_deactivated"
									aria-controls="users_ads"
									role="tab"
									data-toggle="tab"
									onclick = "
										zadnjiKliknutiOrder = 0;									
										$('#_context_active_tab').val('users_deactivated');
										$('#_context_active_tab_subOrdersWithItems_10').val('users_deactivated');
										ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/10' , 'subOrdersWithItems_10'  );
										"	
									>
									Deactivated 
										<span id="count_items_order_status_10">
											{% if count_deactivated > 0 %} ({{ count_deactivated }}) {% endif %}
										</span>
								</a>
							</li>

						</ul>

						<!-- Sadržaj Tab-ova -->
						<div class="tab-content">

							<!-- Ads -->
							<div
								role="tabpanel"
								class="tab-pane {% if _active_tab == 'users_ads' %} active {% endif %} "
								id="users_ads">

								{{ partial("partials/wraSubFormAjaxV2",[
									'_window_id' : 'subUsersAds'
								] ) }}
							</div>
							
							<!-- Single Ad -->
							<div
								role="tabpanel"
								class="tab-pane {% if _active_tab == 'single_ad' %} active {% endif %}"
								id="single_ad"
								>
								{{ partial("ads2/subSingleAd" ) }}
							</div>
				

							<!-- User's Cart -->
							<div
								role="tabpanel"
								class="tab-pane {% if _active_tab === 'users_orders_items' %} active {% endif %} "
								id="users_orders_items">

								{{ partial("partials/wraSubFormAjaxV2",[
									'_window_id' : 'subUsersCart'
								] ) }}
								
								


							</div>

							<!-- User's Offers -->
							<div
								role="tabpanel"
								class="tab-pane {% if _active_tab === 'users_offers' %} active {% endif %} "
								id="users_offers">

								{{ partial("partials/wraSubFormAjaxV2",[
								  '_window_id' : 'subOrdersWithItems_3'
								] ) }}

							</div>

							<!-- User's Free Ads -->
							<div
								role="tabpanel"
								class="tab-pane {% if _active_tab === 'users_free_ads' %} active {% endif %} "
								id="users_free_ads"
								>

								{{ partial("partials/wraSubFormAjaxV2",[
								  '_window_id' : 'subUsersFreeAds'
								] ) }}
							</div>

							<!-- User's Invoices -->
							<div

								role="tabpanel"
								class="tab-pane {% if _active_tab === 'users_invoices' %} active {% endif %} "
								id="users_invoices"
								>

								{{ partial("partials/wraSubFormAjaxV2",[
								  '_window_id' : 'subOrdersWithItems_4'
								] ) }}
								
							</div>


							<!-- User's Paid  -->
							<div
								role="tabpanel"
								class="tab-pane {% if _active_tab === 'users_paid' %} active {% endif %} "
								id="users_paid"
								>
								{{ partial("partials/wraSubFormAjaxV2",[
								  '_window_id' : 'subOrdersWithItems_5'
								] ) }}
							</div>

							<!-- User's Cancelled  -->
							<div
								role="tabpanel"
								class="tab-pane {% if _active_tab === 'users_cancelled' %} active {% endif %} "
								id="users_cancelled"
								>
								{{ partial("partials/wraSubFormAjaxV2",[
								  '_window_id' : 'subOrdersWithItems_6'
								] ) }}

							</div>

							<!-- User's Late  -->
							<div
								role="tabpanel"
								class="tab-pane {% if _active_tab === 'users_late' %} active {% endif %} "
								id="users_late"
								>
								{{ partial("partials/wraSubFormAjaxV2",[
								  '_window_id' : 'subOrdersWithItems_9'
								] ) }}

							</div>

							<!-- User's Deactivated  -->
							<div
								role="tabpanel"
								class="tab-pane {% if _active_tab === 'users_deactivated' %} active {% endif %} "
								id="users_deactivated">
								{{ partial("partials/wraSubFormAjaxV2",[
								  '_window_id' : 'subOrdersWithItems_10'
								] ) }}

							</div>

						</div>
					
					</div>
				</div>
			</div>
			{#
			<div class="col-lg-12 col-md-12">
				{{ partial("ads2/subAdCrud" ) }}
			</div>
			#}

		</div>
	{% else %}

		<div class="row">
			<div class="col-lg-12 col-md-12">
				{{ flashSession.output() }}
			</div>
			<div class="col-lg-12 col-md-12">
				
				{#ad.user_id {{ ad.user_id }}<br/>
				ad.category_id {{ad.category_id}}<br/>
				{{var_dump (ad)}}#}
				
					<h4> KORISNIK NEMA NI JEDAN OGLAS. KREIRAJTE OGLAS ZA OVOG KORISNIKA PREKO  +New Ad ili +New Display Ad</h4>
			</div>

		</div>
	{% endif %}




	<!-- REFRESHANJE PRVI PUTA, OVISNO O TRENUTNO AKTIVNOM TABU -->
	{% if _context['_active_tab'] === 'users_ads' %} <script> ajaxSubmit( '/suva/ads2/subUsersAds/{{ad.user_id}}' , 'subUsersAds'  ); </script> {% endif %}
	{% if _context['_active_tab'] === 'basic' %} <script> ajaxSubmit( '/suva/ads2/subInsertions/{{ad.id}}' , '1_2'  ); </script> {% endif %}
	{% if _context['_active_tab'] === 'users_orders_items' %} <script> ajaxSubmit( '/suva/ads2/subUsersCart/{{ad.user_id}}' , 'subUsersCart'  ); </script> {% endif %}
	{% if _context['_active_tab'] === 'users_offers' %} <script> ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/3' , 'subOrdersWithItems_3'  ); </script> {% endif %}
	{% if _context['_active_tab'] === 'users_free_ads' %} <script> ajaxSubmit( '/suva/ads2/subUsersFreeAds/{{ad.user_id}}' , 'subUsersFreeAds'  ); </script> {% endif %}	
	{% if _context['_active_tab'] === 'users_invoices' %} <script> ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/4' , 'subOrdersWithItems_4'  ); </script> 
	{% endif %}
	{% if _context['_active_tab'] === 'users_paid' %} <script> ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/5' , 'subOrdersWithItems_5'  ); </script> {% endif %}
	{% if _context['_active_tab'] === 'users_cancelled' %} <script> ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/6' , 'subOrdersWithItems_6'  ); </script> 
	{% endif %}
	{% if _context['_active_tab'] === 'users_late' %} <script> ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/9' , 'subOrdersWithItems_9'  ); </script> {% endif %}
	{% if _context['_active_tab'] === 'users_deactivated' %} <script> ajaxSubmit( '/suva/ads2/subOrdersWithItems/{{ad.user_id}}/10' , 'subOrdersWithItems_10'  ); </script> 
	{% endif %}
	

{%  if ad %}
<script> 
	ads2RefreshAd({
		adId : {{ad.id}}
		, userId : {{ user.id }}
		, isDisplayAd : {{ ( ad.n_is_display_ad ? 'true' : 'false' ) }}
		, isSubmitForm : false
		});

</script>

{% endif %}
{%  if _active_tab == 'users_ads' and user.id > 0 %}
<script>
	ajaxSubmitP ({ 
		url: '/suva/ads2/subUsersAds/{{user.id}}' 
		,windowId: 'subUsersAds'
		,isDebug: false
	});
</script>
{%  endif %}
	
	
<!-- {% if strlen(ad.title) == 0 and errors is not defined %}
<script>
	//$('#frm_ads').submit();
</script>

{% endif %} -->
