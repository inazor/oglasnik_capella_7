<?php

namespace Baseapp\Suva\Controllers;

class IssuesController extends IndexController{

    public function indexAction() {
			$this->tag->setTitle("Issues");
			$_SESSION['_context']['step_rec'] = 1000;
			
			$this->n_query_index("Issues"); //ovo je definirano u BaseController
    }

	public function crudAction($pEntityId = null) {
		//$this->printr($_REQUEST);
		$this->n_crud_action("Issues", $pEntityId); //ovo je definirano u BaseController
		$this->view->pick('issues/crud');
	}
}