<?php

namespace Baseapp\Suva\Controllers;

class PublicationsCategoriesController extends IndexController{

    public function indexAction( $pPublicationsId = null ) {
		if (!$pPublicationsId) 
			return $this->greska ("PublicationsCategoriesController:indexAction - Publications ID not supplied");
		
		//postavljanje defrulatnog filtera
		if (!array_key_exists('default_filter', $_SESSION['_context'])) 
			$_SESSION['_context']['default_filter'] = array();
		$_SESSION['_context']['default_filter']['publication_id'] = $pPublicationsId;
		
		
		$this->tag->setTitle("PublicationsCategories");
		$this->n_query_index("PublicationsCategories"); //ovo je definirano u BaseController
    }
	
}