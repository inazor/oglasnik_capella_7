<?php
namespace Baseapp\Suva\Controllers;

use Phalcon\Dispatcher;
use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Library\Utils;
use Baseapp\Library\Moderation;
use Baseapp\Library\Parameters\Parametrizator;
use Baseapp\Library\Avus\ExportToAvus;
use Baseapp\Suva\Models\Ads;
use Baseapp\Suva\Models\N_Publications;
use Baseapp\Suva\Models\N_Products;
use Baseapp\Suva\Models\N_Issues;
use Baseapp\Suva\Models\Categories;
use Baseapp\Suva\Models\AdsHomepage;

use Baseapp\Suva\Models\Users; //igor AAA
use Baseapp\Suva\Models\OrdersItems; //igor da se nađu povezani order itemi sa ad-om ;
use Baseapp\Suva\Library\Nava; // igor - radi sqlToArray
use Baseapp\Suva\Models\Products;
use Baseapp\Suva\Models\Orders;
use Baseapp\Suva\Models\OrdersItemsEx;
use Baseapp\Suva\Models\OrdersStatuses;
use Baseapp\Suva\Models\Publications;
use Baseapp\Suva\Models\Issues;

use Baseapp\Suva\Models\Actions;

use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;
use Baseapp\Traits\ParametrizatorHelpers;
use Baseapp\Library\Products\BackendProductsSelector;
use Phalcon\Paginator\Adapter\QueryBuilder;

/**
 * Backend Ads Controller
*/
class Ads2Controller extends IndexController {
    use ParametrizatorHelpers;
    use CrudActions;
    use CrudHelpers;

    public $crud_model_class = 'Baseapp\Suva\Models\Ads';

    // Overriding the default list of backend-allowed roles to exclude 'content' here
    protected $allowed_roles = array('admin', 'support', 'finance', 'moderator');

    public function indexAction( $pUserId = null ) {
			$debug = 1;


			$this->assets->addCss('assets/vendor/sb-admin-2/css/plugins/morris.css');
			$this->assets->addJs('assets/backend/js/classifieds-index.js');


			//KLASIČNOJ LISTI AD-ova SE DODAJE DEFAULTNI FILTER(PO user_id) I DEFAULTNI ORDER BY(PO DATUMU)
			if (!array_key_exists('_context',$_SESSION)) $_SESSION['_context'] = array();
			if (!array_key_exists('_context',$_SESSION)) $_SESSION['_context'] = array();

			//postoječi context-filter se briše pod nekim uvjetima, dodan je default_filter koji je uvijek aktivan
			if (!array_key_exists('default_filter',$_SESSION['_context'])) $_SESSION['_context']['default_filter'] = array();

			//ako je otprije nešto postojalo, da se ne pregazi za ubuduće, spremi se, pozove funkcija, pa se posli opet vrati
			$oldDefaultFilter = $_SESSION['_context']['default_filter'];
			unset($_SESSION['_context']['default_filter']);

			if ($pUserId){

				$user = \Baseapp\Suva\Models\Users::findFirst($pUserId);
				if ($user){
					//$this->poruka("USER FOUND");
					$_SESSION['_context']['default_filter']['user_id'] = $pUserId;
					$this->view->setVar('form_title', "Ads for user $user->first_name $user->last_name $user->company_name");
				}
			}


			//postojeći context-filter se briše pod nekim uvjetima, dodan je default_filter koji je uvijek aktivan
			if (!array_key_exists('default_order_by',$_SESSION['_context'])) $_SESSION['_context']['default_order_by'] = array();

			//ako je otprije nešto postojalo, da se ne pregazi za ubuduće, spremi se, pozove funkcija, pa se posli opet vrati
			$oldDefaultOrderBy = $_SESSION['_context']['default_order_by'];
			unset($_SESSION['_context']['default_order_by']);
			$_SESSION['_context']['default_order_by']['id'] = 'desc';

			//poziva se funkcija koja radi listu id-jeva
			$this->n_query_index("Ads"); //ovo je definirano u BaseController
			//$this->printr($_SESSION['_context']);


			//nakon poziva funkcije vraća se ono šta je bilo prije (ako je išta bilo)
			$_SESSION['_context']['default_filter'] = $oldDefaultFilter;
			$_SESSION['_context']['default_order_by'] = $oldDefaultOrderBy;


			// $this->tag->setTitle('Ads');

			// $user = \Baseapp\Suva\Models\Users::findFirst($pUserId);
			// if ($user){
				// $ads = \Baseapp\Suva\Models\Ads::find(
					// array(
						// "user_id = $user->id",
						// "order" => "modified_at DESC",
						// "limit" => 100
					// )
				// );

				// $this->view->setVar('ads',$ads);
			// }


    }

	public function editAction($pAdId = null, $pUserId = null) {
		/*
			
		
		*/
		
		// Nava::sysMessage($_SESSION['_context']);
		$ad = $pAdId > 0 ? \Baseapp\Suva\Models\Ads::findFirst($pAdId) : null;
		$user = $pUserId > 0 ?  \Baseapp\Suva\Models\Users::findFirst($pUserId) : null;
		
		if ( $ad ) $user = $ad->User();
		elseif ( $user ) $ad = \Baseapp\Suva\Models\Ads::findFirst (" user_id = $user->id ");
		
		if ($pUserId > 0){
			$user = \Baseapp\Suva\Models\Users::findFirst( $pUserId);
			if (!$user) return $this->greska ("Nije pronađen korisnik s ID-jem $pUserId");
			$_SESSION['_context']['selected_user_id'] = $user->id;
			//error_log("KorisniciController:chooseAction: selected_user_id je ".$_SESSION['_context']['selected_user_id']);
		} 
		else {
			if (! array_key_exists("_context", $_SESSION)) return $this->greska("Nije definirana varijabla context");
			if (! array_key_exists("selected_user_id", $_SESSION['_context'])) return $this->greska("Nije definirana varijabla selected_user_id");
			
			$id = $_SESSION['_context']['selected_user_id'];
			if ( !$id > 0 ) return $this->greska ("Poslan je netočan ID odabranog korisnika, $id");
		
			$user = \Baseapp\Suva\Models\Users::findFirst( $id);
			if (!$user) return $this->greska ("Nije pronađen korisnik s ID-jem $id");
		}	
		$_SESSION['_context']['_active_tab'] = 'single_ad';
		
		$this->view->setVar('ad', $ad);
		$this->view->setVar('user', $user);
		
		$this->view->pick("ads2/edit");
		
		
	}
	
	public function createAction() {
        //$this->poruka("adscontroller:cretae: REQUEST ".print_r($_REQUEST, true));
		// Only 'admin' and 'support' roles are allowed to create ads from the backend
        if (!$this->auth->logged_in(array('admin', 'supersupport', 'sales'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('suva/ads2');
        }

		//selected_category_id može i preko contexta
		if (array_key_exists ('selected_category_id', $_SESSION['_context'] ) ){
			$selected_category_id = (int)$_SESSION['_context']['selected_category_id'];
		}
		else{
			if (!$this->request->has('category_id')) {
				$this->flashSession->error('Missing required category ID for create action!');
				return $this->redirect_to('suva/ads2');
			}
			$selected_category_id = (int)$this->request->get('category_id');
		}

        if (!$selected_category_id) {
            $this->flashSession->error('Invalid category ID!');
            return $this->redirect_to('suva/ads2');
        }

        $selected_category = Categories::findFirst($selected_category_id);

        if (!$selected_category) {
            $this->flashSession->error(sprintf('Category not found [ID: %s]', $selected_category));
            return $this->redirect_back();
        } else {
            if (0 == count($selected_category->FieldsetParameters)) {
                $this->flashSession->error('Selected category does not have parameters set up!');
                return $this->redirect_back();
            }

            $this->tag->setTitle('Create new ad');
            $this->view->setVar('form_title_long', 'Create new ad');
            $this->view->setVar('form_action', 'add');
            $this->add_common_crud_assets();
            // Use the same form/view as editAction
            $this->view->pick('ads2/create');

            $ad = new Ads();
			 // error_log("1Ads2Controller.php PHONE = $ad->phone1 , $ad->phone2");
            $ad->created_by_user_id = $this->auth->get_user()->id;
			 // error_log("2Ads2Controller.php PHONE = $ad->phone1 , $ad->phone2");
			if($_SESSION['_context']['n_source'] = 'backend'){
					$ad->n_source = 'backend';
			}
			
            $parametrizator = new Parametrizator();
            $parametrizator->setModule('backend');
            $parametrizator->setCategory($selected_category);
            $parametrizator->setAd($ad);
			 // error_log("3Ads2Controller.php PHONE = $ad->phone1 , $ad->phone2");

			
			
            if ($this->request->isPost()) {
				
				$jeGreska = false;
				
				//20.12.2016 - KZ-002 - u oglasu više nema ### nego se dodaje u Issues.php
				// if ($this->request->hasPost('ad_description_offline') && strlen( $this->request->getPost('ad_description_offline') ) > 0 ){
					// //$this->poruka("1111");
					// $desc = $this->request->getPost('ad_description_offline');
					// if (strpos($desc, '###') == false){
						// $this->greska ("Opis za tisak ne sadrži kontakt broj (###)");
						// $jeGreska = true;
					// }
				// }
				
				if ( ! $jeGreska ) {
					//$this->poruka("lkjlkjlkjljkNOT");
					$created = $ad->backend_add_new($this->request);
					if ($created instanceof Ads) {
						$this->flashSession->success('<strong>Ad created!</strong> ' . $ad->get_frontend_view_link('html'));
						$save_action = $this->get_save_action();
						$next_url = $this->get_next_url();
						
						//20.12.2016 - dogovor da se ovo miče
						//Nava::runSql("update ads set active = 0, n_frontend_sync_status = 'no_sync' where id = ".$created->id);
						
						//29.12.2016 - nakon snimanja ovo ostaje, miče se samo nakon edita
						$unix_time=time();
						Nava::runSql("update ads set active = 0, n_frontend_sync_status = 'no_sync', modified_at =".$unix_time."  where id = ".$created->id);

						
						// Override next url to go to entity edit in save button case
						if ('save' === $save_action) {
							$_SESSION['_context']['_active_tab'] = 'single_ad'; 
							$_SESSION['_context']['_keep_active_tab_setting'] = true;
							// $this->poruka("nije greska");
							$next_url = "suva/ads2/edit/$created->id/$ad->user_id";
						}
						// $this->poruka("next url $next_url");
						return $this->redirect_to($next_url);
					} else {
						$this->view->setVar('errors', $created);
						$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.');
						$parametrizator->setRequest($this->request);
						$parametrizator->setErrors($created);
					}

				}
				else{
					//$this->poruka("lkjlkjlkjljkYES");
					$parametrizator->setRequest($this->request);
				}

            }
            //$this->setup_products_chooser($ad, $selected_category);
            $this->setup_rendered_parameters($parametrizator->render_input());
            $this->view->setVar('category', $selected_category);
            $this->view->setVar('ad', $ad);
            $this->view->setVar('show_save_buttons', true);
        }
    }
	
	
	public function crudAction($pEntityId = null) {
		Nava::sysMessage($_REQUEST);
		
		if ($this->request->isGet() && $pEntityId > 0 ) {
			$entity = \Baseapp\Suva\Models\Ads::findFirst($pEntityId);
			$this->view->setVar('entity', $entity);
		}
		
		// $defaultValues = array('price' => ($_REQUEST['price']) * 100 );
		//Nava::sysMessage($defaultValues, "prije slanje u crud");
		
		//provjera jesu li svi obavezni parametri ubačeni
		if(array_key_exists('ad_params_required', $_SESSION['_context'])){
			$required_parameters_ids = $_SESSION['_context']['ad_params_required'];
			$parameters_value = $_SESSION['_context']['ad_params'];
			$parameters_value[1] = $_REQUEST['title'];
			$parameters_value[2] = $_REQUEST['price'];
			$parameters_value[3] = $_REQUEST['description'];
			$proceed = true;
			
			foreach($required_parameters_ids as $parameter_id => $required_parameter_id) {
				if($parameters_value[$parameter_id] == null) {
					$parameter = \Baseapp\Suva\Models\Parameters::findFirst($parameter_id);
					Nava::greska('<b>Greska</b> kod unosa parametra, parametar:  '.$parameter->name.' nije unesen');
					$proceed = false;
				}
			}
			if($proceed){
				//ako su sve obavezne vrijednosti poslane, onda idi u crud
				$entity = 	$this->n_crud_action("Ads", $pEntityId, false, $defaultValues); 				
								
				
				// ako crud vrati ad-id, onda radi s parametrima
				// prvo se brisu svi parametri pa se snimaju novi koji su poslani u requestu
				if($entity->id > 0){
					$parameters_value = $_SESSION['_context']['ad_params'];			
					Nava::runSql("delete from ads_parameters where ad_id = $entity->id");
					foreach($parameters_value as $parameter_id => $parameter_value) {
						//ako je poslan checkbox group, vise cekiranih vrijednosti
						//idi kroz svaku vrijednost parametra i spremi ga,promijeni mu item_index
						if(is_array($parameter_value) && !empty($parameter_value) ){
							foreach($parameter_value as $i => $p_value){
								$parameter = \Baseapp\Suva\Models\Parameters::findFirst($parameter_id);
								if($parameter){
									$ad_parameter = new \Baseapp\Suva\Models\AdsParameters();
									$ad_parameter->ad_id = $pEntityId;
									$ad_parameter->parameter_id = $parameter_id;
									$ad_parameter->level = 0;
									$ad_parameter->item_index = $i;
									$ad_parameter->value = $p_value;
									$ad_parameter->create();
									
								}else{
									Nava::greska('Neispravan parametar id:'.$parameter_id);
								}
							}
						}elseif($parameter_value != null){
							// error_log(print_r($parameter_value,true)); 
							$parameter = \Baseapp\Suva\Models\Parameters::findFirst($parameter_id);
							if($parameter){
								
								$ad_parameter = new \Baseapp\Suva\Models\AdsParameters();
								$ad_parameter->ad_id = $pEntityId;
								$ad_parameter->parameter_id = $parameter_id;
								$ad_parameter->level = 0;
								$ad_parameter->item_index = 0;
								$ad_parameter->value = $parameter_value;
								$ad_parameter->create();
								
							}else{
								Nava::greska('Neispravan parametar id:'.$parameter_id);
							}
						}
											
					}
					
					//sql jer price treba množiti sa 100
					if (array_key_exists ('price', $_REQUEST) && $_REQUEST['price'] > 0 ){
						$price = intval( $_REQUEST['price'] * 100 );
						Nava::runSql("update ads set price = $price where id = $entity->id ");
						$entity->price = $price;
						$this->view->setVar('entity', $entity);
					}
					
				}else{
					Nava::greska('Neispravan ad id '.$entity->id);
				}
												
				
			}else{
				$entity = \Baseapp\Suva\Models\Ads::findFirst($pEntityId);
				$entity->initialize_model_with_post($this->request);
				$this->view->setVar('entity', $entity);
			}

		}
		
						
		$this->view->pick('ads2/crud');
		
		
	}
	
	public function crudDisplayAction($pEntityId = null , $pUserId = null ) {
		$defaultValues = array();
		if (array_key_exists('default_values',$_SESSION['_context'])){
			$defaultValues = $_SESSION['_context']['default_values'];
			if (array_key_exists('user_id', $defaultValues)){
				if ( is_numeric($defaultValues['user_id'])){
					$user = \Baseapp\Suva\Models\Users::findFirst($defaultValues['user_id']);
					if ($user) $defaultValues['user_id'] = $user->id;
				}
			}
		}
		
		if ($pUserId > 0) $defaultValues['user_id'] = $pUserId;
		
		$defaultValues['created_at'] = time(); //je li vraća unix timestamp?
		$defaultValues['description'] = 'display ad';
		$defaultValues['product_sort'] = 100;
		// $defaultValues['category_id'] = 919;		
		$defaultValues['category_id'] = \Baseapp\Suva\Library\libAppSetting::get('display_ad_default_category_id');;
		$defaultValues['n_is_display_ad'] = 1;
		$defaultValues['n_source'] = 'backend';
		$defaultValues['n_frontend_sync_status'] = 'no_sync';
		
		if ($pEntityId == 0) $pEntityId = null;
		$entity = $this->n_crud_action("DisplayAds", $pEntityId, false, $defaultValues); //ovo je definirano u BaseController
  
		// error_log("ads2controller::crudDisplayAction ".print_r($_SESSION['_context'],true));
		$this->view->pick('ads2/crudDisplay');
		 
		if ( $entity && $entity->id > 0 && ! $this->request->isAjax() ) {
			$_SESSION['_context']['_active_tab'] = 'single_ad'; 
			$_SESSION['_context']['_keep_active_tab_setting'] = true;
			$this->response->redirect("suva/ads2/edit/$entity->id");
		}
		
	}
	
	public function crudBasicAction($pAdId = null, $pUserId = null) {
		//Nava::sysMessage("Ads2Controller:crudBasicAction START: ".microtime(true));

		if ($pAdId > 0 ) $ad = \Baseapp\Suva\Models\Ads::findFirst($pAdId); else $ad = null;
		if ($pUserId > 0) $user = \Baseapp\Suva\Models\Users::findFirst($pUserId); else $user = null;
		if ( !$user && !$ad ){
			$this->sysMessage ("Ads2Controller::crudBasicAction: user and ad are not supplied");
			// error_log('crudBasicAction 2');
			return $this->redirect_back();
		}
		
		//AKTIVNI TAB
		if( $ad && $ad->n_is_display_ad == 1 ) $_SESSION['_context']['_active_tab'] = 'display_ad';
		elseif ( $ad && $ad->n_is_display_ad == 0 ) $_SESSION['_context']['_active_tab'] = 'basic';
		elseif ( $user && ! $ad ) $_SESSION['_context']['_active_tab'] = 'users_ads';

		// error_log('crudBasicAction 3');
		//PORUKA DA USER NEMA AD-OVA
		if ($user) $countAds = $user->Ads()->count();
		elseif ( $ad->User() ) $countAds = $ad->User()->Ads()->count();
		if ( $countAds == 0 ) {
			$this->poruka("Selected user has no ads. Create an ad using <span style='padding: 4px;  font-size:14px; text-decoration:none; color:black; border:1px solid black; background-image:-webkit-linear-gradient(bottom, 0,  100%);  margin-right:5px;' ><span class='fa fa-plus fa-fw'></span>&nbsp;Ad</span>");
			return $this->redirect_back();
		}
		// error_log('crudBasicAction 4');
		//PORUKA DA NA ADU NIJE ODABRAN NIJEDAN PROIZVOD
		//if ( $ad && $ad->OrdersItems()->count() == 0) $this->greska("Na ovom oglasu niste odabrali nijedan proizvod.");

		
		$this->view->pick('ads2/crudBasic');
		$this->tag->setTitle("Edit Ad");
		
		$_SESSION['_context']['selected_ad_id'] = $ad->id;
		// error_log('crudBasicAction 5');
		//Čemu ovo služi - lista kategorija
		$this->category_id_dropdown(
			'new_category_id',
			null,
			array(
				'disabled' => Categories::getCategoriesWithoutTransactionType()
			)
		);

		//$this->poruka ("step 4 - prije provjere je li post");
		//ad_category ko se izmijeni kategorija, dodavanje nove ??
		// error_log('crudBasicAction 6');
		
		// Ako je izmijenjena kategorija updateaj je preko SQL-a ???
		$ad_category = $ad->Category();
		if ($this->request->hasPost('ad_category_id')) {
			//$this->poruka ("step 5");
			$rqstCategoryId = $this->request->getPost('ad_category_id');
			if ($rqstCategoryId != $ad->category_id){
				$ad->category_id = $rqstCategoryId;
				$ad_category = Categories::findFirst( $ad->category_id );
				Nava::runSql( "update ads set category_id = $rqstCategoryId where id = $ad->id" );
			}
		}

		// $this->poruka ("korak 6  ".$this->request->getPost('ad_description_offline'));

		$parametrizator = new Parametrizator();
		$parametrizator->setModule('suva');
		$parametrizator->setCategory($ad_category);
		$parametrizator->setAd( $ad );

		//Ako se editira ad koji je soft-deleted - greška
		if ($this->request->isPost() && $this->request->hasPost('category_id') && $ad->isSoftDeleted()) {
			// error_log('crudBasicAction 6.1');
			//$this->poruka ("step 7");
			//error_log("ads2controller::editAction 13");
			
			//ako je isSoftDeleted poslan bez parametara samo vraća vrijednost polja u bazi
			$this->flashSession->error('<strong>Error!</strong> SoftDeleted ad cannot be modified!');
			return $this->redirect_to($next_url);
		}
		

		//glavna petlja
		//Ako je snimanje oglasa
		// error_log('crudBasicAction 7');
		if ($this->request->isPost() && $this->request->hasPost('category_id')) {
			//$this->poruka ("step 6 - je post i ima category id");
			$next_url = $this->get_next_url();
			// error_log('crudBasicAction 8');
			
			// //ako je u bazi postavljen soft_delete ne smije se editirat
			// if ($ad->isSoftDeleted()) {
				// // error_log('crudBasicAction 9');
				// //$this->poruka ("step 7");
				// //error_log("ads2controller::editAction 13");
				
				// //ako je isSoftDeleted poslan bez parametara samo vraća vrijednost polja u bazi
				// $this->flashSession->error('<strong>Error!</strong> SoftDeleted ad cannot be modified!');
				// return $this->redirect_to($next_url);
			// }
			
			//GLAVNA PETLJA ZA SNIMANJE
			if ($this->request->hasPost('savemoderation')) {
				// SNIMA SE MODERACIJA
				// $this->poruka ("step 8  - hasPost savemoderation");
				//snimanje moderacije
				$saved = $ad->save_moderation($this->request);
				$success_msg = '<strong>Ad moderated!</strong>';
				$save_action = 'savemoderation';
				$_SESSION['_context']['_active_tab'] = 'basic';
				// error_log('crudBasicAction 10');
			} else {
				//SNIMANJE NORMALNO - nije moderacija
				//$this->poruka ("step 9 - normalno snimanje, prije backend_save_changes  ad->description_offline: $ad->description_offline");
				// error_log("step 9 - normalno snimanje, prije backend_save_changes  ad->description_offline: $ad->description_offline");
				// error_log ("step 9.1 - normalno snimanje, prije backend_save_changes  REQUEST->description_offline:".$this->request->getPost('ad_description_offline'));
				
				//snimanje oglasa normalno
				$saved = $ad->backend_save_changes($this->request);
				//$this->poruka ("step 9.2 - normalno snimanje, nakon backend_save_changes  ad->description_offline: $ad->description_offline");
				//$this->poruka ("step 9.3 - normalno snimanje, nakon backend_save_changes  saved->description_offline: $saved->description_offline");
				// error_log ("step 9.2 - normalno snimanje, nakon backend_save_changes  ad->description_offline: $ad->description_offline");
				// error_log ("step 9.3 - normalno snimanje, nakon backend_save_changes  saved->description_offline: $saved->description_offline");
				
				
				$success_msg = '<strong>Ad updated!</strong>';
				$save_action = $this->get_save_action();
				//stavlja ad.active = 1
				
				//20.12.2016 - dogovor da se ovo miče
				//Nava::runSql("update ads set  active = 0 where id = $ad->id");
				
				//Oglasi koji su editirani preko backenda se ne syncaju pomoću cli/UpdateOrders.php
				Nava::runSql("update ads set  n_frontend_sync_status='no_sync' where id = $ad->id");
				
				// error_log($ad->id);
				// error_log('crudBasicAction 11');
			}
			

			// detect which button submitted the form.. in case the detected button is 'savemoderation' then we
			// handle only saving of 'Ads moderation part', otherwise, save all submitted data.
			if ($saved instanceof Ads) {
				
				// error_log('crudBasicAction 12');
				
				//AKO JE SNIMLJENO BEZ GREŠAKA
				//$this->poruka ("step 10 - snimljeno uspješno");
				//error_log("ads2controller::editAction 16");
				
				//Nakon uspješnog snimanja - ako postoje greške na n_after_save?
				$greska = $ad->n_after_save($this->request, $this->auth->get_user()->id);
				//$this->poruka ("step 10.1 greaska nakon after save $greska");
				if ($greska) {
					$this->poruka ("step 11");
					$this->greska($greska);
					// error_log('crudBasicAction 13');
					return $this->redirect_back();
				} 
				//$this->poruka ("step 10.11");
				$this->flashSession->success($success_msg . ' ' . $saved->get_frontend_view_link('html'));
				//error_log("ads2controller::editAction 17");
				// error_log('crudBasicAction 14');
				if ('save' === $save_action) {
					//$this->poruka ("step 12");
					// //error_log("editAction 18");
					// error_log('crudBasicAction 15');
					$next_url = 'suva/ads2/crud/' . $saved->id;
				}
				//$this->poruka ("step 10.3");

				if ($this->request->hasPost("display_title") && $this->request->hasPost("n_is_display_ad"))
					if($this->request->getPost("display_title") > '' && $this->request->getPost("n_is_display_ad") == 1)
					Nava::runSql("update ads set title = '".$this->request->getPost("display_title")."' where id = $saved->id");
				if ($this->request->hasPost("ad_title") && $this->request->hasPost("n_is_display_ad"))
					if($this->request->getPost("ad_title") > '' && $this->request->getPost("n_is_display_ad") == 0)
					Nava::runSql("update ads set title = '".$this->request->getPost("ad_title")."' where id = $saved->id");
				if ($this->request->hasPost("n_360_spin_id"))
					Nava::runSql("update ads set n_360_spin_id = '".$this->request->getPost("n_360_spin_id")."' where id = $saved->id");
				if ($this->request->hasPost("n_is_display_ad"))
					Nava::runSql("update ads set n_is_display_ad = ".$this->request->getPost("n_is_display_ad")." where id = $saved->id");

				Nava::runSql("update ads set n_modified_by_user_id = ".$this->auth->get_user()->id." where id = $saved->id");
				//$this->poruka ("step 10.4");
				$parametrizator->setData( $saved->getData() );
				//$this->printr($saved->getData(), "step 11.5 saved->getdata");
				//$this->printr($saved->getData(), "step 11.5 saved->getdata");
				
				$ad2 = \Baseapp\Suva\Models\Ads::findFirst($saved->id);
				$parametrizator->setData( $saved->getData() );
				
				//ovdje još zamijeniti slike iz requesta
				$arrSlike = $this->request->hasPost('ad_media_gallery') ? $this->request->getPost('ad_media_gallery') : null;

				
				\Baseapp\Suva\Library\Nava::runSql("delete from ads_media where ad_id = $saved->id");
				// error_log('crudBasicAction 16');
				if ($arrSlike){
					$i=1;
					foreach( $arrSlike as $row ){
						$mediaId = $row;
						\Baseapp\Suva\Library\Nava::runSql("insert into ads_media (ad_id, media_id, sort_idx) values ( $saved->id, $row, $i )");
						$i++;
					}
					// error_log('crudBasicAction 17');
				}	
				
				
				$rsSlike = $ad2->AdsMedia();
				//ima li ih isto?
				if ( count($arrSlike) == $rsSlike->count() ){
					//imaju li sve iste elemente
					$jeImaIsto = true;
					foreach ($rsSlike as $row){
						if ( !in_array( $row->media_id, $arrSlike ) ){
							$jeImaIsto = false;
						}
					}
					if ($jeImaIsto){
						$i=1;
						foreach( $rsSlike as $row ){
							$row->media_id = $arrSlike[$i-1];
							$row->sort_idx = $i;
							$row->update();
							$i+=1;
						}
					}
					// error_log('crudBasicAction 18');
				}
				
			
				
				//$this->printr($ad2->getData(), "step 11.6 ad2->getdata");
				// error_log("update ads set active = 1 where id = $saved->id");

				//20.12.2016 dogovor da se ovo miče
				//Nava::runSql("update ads set active = 0 where id = $saved->id");
				
				// error_log('crudBasicAction 12');
				// error_log('crudBasicAction 19');
				$ad = $ad2;
				$parametrizator->setAd( $ad);
				$parametrizator->setData( $ad->getData() ); //POSTAVLJA PARAMETRE ZA SLANJE NA STRANICU
			}
			else {
				//NIJE SNIMLJEN AD, u  $saved je spremljen popis grešaka
				//$this->poruka ("step 13");
				$err_msg = "";
				foreach ($saved as $greskaModela)
					$err_msg .= $greskaModela.", ";
				$this->view->setVar('errors', $saved);
				$this->greska("<strong>Error!</strong> Fix all fields that failed to validate.: $err_msg");
				$parametrizator->setRequest($this->request);
				$parametrizator->setErrors($saved);
				// error_log('crudBasicAction 13');
				// error_log("Ads2Controller.php GRESKA : $err_msg");
				$this->greska("GRESKA:". $err_msg );
				//return $this->response->redirect($_SERVER['HTTP_REFERER']);
				//return $this->redirect_back();
				//return;
				// error_log('crudBasicAction 20');
   
			}
		}
		else {
			// error_log('crudBasicAction 21');
			//AKO NEMA ID KATEGORIJE, ŠTO ZNAČI DA SE SNIMA MODERACIJA
			//Ako je snimanje moderacije
			if ($this->request->hasPost('savemoderation')) {
				//$this->poruka ("step 14");
				// Ako je poslan parametar
				$saved = $ad->save_moderation($this->request);			
				$save_action = 'savemoderation';
				$this->poruka('Ad succesfully moderated!');
				// error_log('crudBasicAction 22');
				// error_log('crudBasicAction 14');
			}else {
				//$this->poruka ("step 15 - prije backend save changes, request nema category_id");
				//error_log("ads2controller::editAction 23.2");
				$saved = $ad->backend_save_changes($this->request);
				$success_msg = '<strong>1Ad updated!</strong>';
				$save_action = $this->get_save_action();
				// error_log('crudBasicAction 15');
				// error_log('crudBasicAction 23');
				//$ad->$saved; //Notice: Access to undefined property
			}
			
			//$this->printr($ad->getData(), "step 15.1 getData ");
			//prebacivanje polja iz ad-a u one njegove parametre ad_title, ad_location_1 ...
			// error_log('crudBasicAction 24');
			$parametrizator->setData( $ad->getData() );
			$this->processModerationReasons($ad->moderation, $ad->moderation_reason);
			// error_log('crudBasicAction 16');
		}
		//$this->poruka ("step 16");
		
		
		//OVO SE IZVRŠAVA NEOVISNO O TOME JE LI SNIMLJEN ILI NIJE
		//Nava::sysMessage("Ads2Controller:crudBasicAction 2: ".microtime(true));
		$this->setup_products_chooser($ad, $ad_category);
		
		//$this->printr ( $parametrizator->render_input(), "renderInput" );

		/*
		RENDER INPUT - generira HTML sa svim kontrolama
		
		- dio sa uploadanim slikama se generira na D:\OGLASNIK_GIT\150\app\common\library\Parameters\UploadableParameter.php:126
		  - file se traži u Media::get_original_file
		  
		*/
		// error_log('crudBasicAction 25');
		$this->setup_rendered_parameters($parametrizator->render_input());
		$this->view->setVar('category', $ad_category);
		//Nava::sysMessage("Ads2Controller:crudBasicAction 3: ".microtime(true));

		//error_log("ads2controller::editAction 27");
		$can_be_moderated = true;
		$moderation_warning = null;
		$ad_status = $ad->get_status();
		if ($ad_status == 0) {
			// error_log('crudBasicAction 26');
			// //error_log("ads2controller::editAction 28");
			$can_be_moderated = false;
			$moderation_warning = 'This ad has not yet finished with <b>ad submission</b> process and cannot be moderated!';
		}
		if ($ad_status == 20) {
			// error_log('crudBasicAction 27');
			// //error_log("ads2controller::editAction 29");
			$can_be_moderated = false;
			$moderation_warning = 'This ad has not yet finished with <b>ad republish</b> process (<b>waiting for payment</b>) and cannot be moderated!';
		}
		if ($ad->soft_delete === 1) {
			// error_log('crudBasicAction 28');
			// //error_log("ads2controller::editAction 30");
			$can_be_moderated = false;
			$moderation_warning = 'This ad is <b>soft deleted</b> and cannot be moderated or edited!';
		}
		if ($ad->isSoftDeleted()) {
			// error_log('crudBasicAction 29');
			$show_save_buttons = false;
		} else $show_save_buttons = true;

	// error_log('crudBasicAction 17');
		$this->view->setVar('can_be_moderated', $can_be_moderated);
		$this->view->setVar('moderation_warning', $moderation_warning);
		// $this->view->setVar('ad_reports', $ad->getReports());
		//Nava::sysMessage("Ads2Controller:crudBasicAction 4: ".microtime(true));
		
		$this->view->setVar('ads_remark', (isset($ad->AdditionalData->remark) ? $ad->AdditionalData->remark : ''));
		$this->view->setVar('show_save_buttons', $show_save_buttons);
		//$this->view->setVar('ad_taker_username', $ad_taker_username);

		
		//IGOR: ovoje maknuto, trebalo mu je 2 sekunde a ne služi
		//$this->view->setVar('orders', Nava::arrToDict(Orders::find()->toArray()));
		
		
		//Nava::sysMessage("Ads2Controller:crudBasicAction 5: ".microtime(true));
		$this->view->setVar('orders_statuses', Nava::arrToDict(OrdersStatuses::find()->toArray()));
		$this->view->setVar('publications', Publications::find("is_active=1"));
		$this->view->setVar('products', Nava::arrToDict(Products::find()->toArray()));
		$this->view->setVar('category', Categories::findFirst($ad->category_id));
		$this->view->setVar('ad', $ad);
		// error_log('crudBasicAction 30');
		//error_log("ads2controller::crudBasicAction ".print_r($_SESSION['_context'],true));
		//Nava::sysMessage("Ads2Controller:crudBasicAction END: ".microtime(true));
	}
	
	public function insertionsAction($pAdId = null){
		//error_log('insertionsAction:: REQUEST:'.print_r($_REQUEST,true));

		$this->view->pick("ads2/subInsertions");
		$ad = \Baseapp\Suva\Models\Ads::findFirst($pAdId);
		$this->view->setVar('entity', $ad);
		//$this->n_crud_action("Ads", $pAdId);
		if ($this->request->isPost()){

			$clickedAction = $_SESSION['_context']['clicked_action'];
			//error_log("Ads::n_after_save 1.1  clickedAction:  $clickedAction");



			//spremanje slika kod display oglasa
			$imageName = $this->request->getPost('imageUpload');
			if($imageName > ''){


				$targetPath = "repository/dtp/originals/".$imageName; // Path di je spremljena slika
				$targetPath2 = "repository/dtp/to_process/".$imageName;

				$renamedPath = "repository/dtp/originals/".$created->id.".jpg";


				if (file_exists($targetPath)) {
					rename($targetPath, "repository/dtp/originals/".$created->id.".jpg");
					Nava::runSql("update ads set n_picture_path = '$renamedPath' where id = $created->id");

				}
				if (file_exists($targetPath2)) {
					rename($targetPath2, "repository/dtp/to_process/".$created->id.".jpg");


				}




	// 				$allowed_extensions =  array('jpg');
	// 				//error_log("n_after_save::pictureUpload 6");
	// 				if(!in_array($file_extension, $allowed_extensions) ) {
	// 					//error_log("n_after_save::pictureUpload 7");
	// 					echo $file_extension." is not a valid extension, you can only upload jpg extension";
	// 					return;
	// 				}
	// 			}
			}


			$okMsg = '';
			$errMsg = '';
			switch($clickedAction){
				case 'lock_insertions':
					$model = new \Baseapp\Suva\Models\Model();
					$selectedInsertions = $this->request->hasPost('_users_insertions') ? $this->request->getPost('_users_insertions') : null;
					if (!array_key_exists('_context', $_SESSION)) $_SESSION['_context'] = array();
// 					$_SESSION['direct_back();

				//$this->insertToCartDefaultOnlineProduct($created->id);
				//error_log("ads2::createAction 18");
				//error_log("ads2 createAction 11");
				return $this->redirect_to($next_url);
			}
			// else {
				// //error_log("ads2 createAction 12");
				// foreach ($ad->getMessages() as $message)  $err_msg .= $message.", ";
				// $this->flashSession->error('<strong>Create Error!</strong> Fix all fields that failed to validate.'.$err_msg);
				// $this->view->setVar('errors', $created);
				// //$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate.'.print_r($created));
				// $parametrizator->setRequest($this->request);
				// $parametrizator->setErrors($created);
				// //error_log("ads2 createAction 12.1");
				// //error_log("ads2::createAction 19");
			// }

			//error_log("ads2::createAction 20");

			//error_log("ads2 createAction 13");
		} else return $this->greska("Zahtjev za kreiranja ad-a mora doći preko posta.");

		//error_log("ads2 createAction 14 ".print_r($ad->toArray(),true));

		$this->setup_products_chooser($ad, $category);
		$this->setup_rendered_parameters($parametrizator->render_input());
		$this->view->setVar('category', $category);
		$this->view->setVar('ad', $ad);
		$this->view->setVar('show_save_buttons', true);

		//error_log("ads2::createAction 21");





	}

    protected function getPossibleOrderByOptions() {
        $order_bys = array(
            'id'                 => 'ID',
            'created_at'         => 'Date created',
            'modified_at'        => 'Date modified',
            'first_published_at' => 'First Publish date',
            'published_at'       => 'Last Publish date',
            'sort_date'          => 'Virtual publish date',
            'expires_at'         => 'Date expired',
            'title'              => 'Title'
        );

        return $order_bys;
    }

    /**
     * Helper method to process moderation filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterModeration($default = null) {
        $moderation = new Moderation();

        $moderation_option = $this->processOptions(
            'moderation',
            array_merge(
                array('' => 'Any moderation'),
                $moderation->getPossibleModerationStatuses()
            ),
            (string) $default,
            'string'
        );

        if (trim($moderation_option)) {
            return (string)$moderation_option;
        }

        return null;
    }

    /**
     * Helper method to process products filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterProducts($default = null) {
        $selector = new BackendProductsSelector();

        $product_option = $this->processOptions(
            'product',
            array_merge(
                array('' => 'Any product'),
                $selector->getPossibleIdsList()
            ),
            (string) $default,
            'string'
        );

        if (trim($product_option)) {
            return (string)$product_option;
        }

        return null;
    }

    /**
     * Helper method to process ad's payment state filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterPaymentState($default = null) {
        $payment_state_option = $this->processOptions(
            'payment_state',
            array_merge(
                array('0' => 'Any payment state'),
                (new Ads())->getAllPaymentStates()
            ),
            (string) $default,
            'string'
        );

        if (trim($payment_state_option)) {
            return (string)$payment_state_option;
        }

        return null;
    }

    /**
     * Helper method to process ad's payment type filter dropdown (note: currently this
     * method depends on CrudHelpers trait method processOptions()!)
     *
     * @param string $default Default value
     * @return string|null
     */
    protected function processFilterPaymentType($default = null) {
        $payment_type_option = $this->processOptions(
            'payment_type',
            array_merge(
                array('0'       => 'Any payment type'),
                array('paid'    => 'Paid ads'),
                array('ordered' => 'Ordered paid ads'),
                array('free'    => 'Free ads')
            ),
            (string) $default,
            'string'
        );

        if (trim($payment_type_option)) {
            return (string)$payment_type_option;
        }

        return null;
    }

    /**
     * Helper method to generate moderation dropdowns for ad editing..
     *
     * @param string $status Default status
     * @param string $reason Default reason
     */
    protected function processModerationReasons($status, $reason) {
        $moderation = new Moderation();
        $moderation_statuses = $moderation->getPossibleModerationStatuses();
        $moderation_reasons = $moderation->getModerationReasonsData();
        $moderation_statuses_final = array();
        $moderation_reasons_final = array();

        foreach ($moderation_statuses as $k => $title) {
            if (isset($moderation_reasons[$k])) {
                $moderation_statuses_final[$k] = array(
                    'text' => $title,
                    'attributes' => array(
                        'data-has-reasons' => 1
                    )
                );

                $curr_reason = $reason;

                $available_reasons = array();
                foreach ($moderation_reasons[$k] as $row_key => $row_data) {
                    $available_reasons[$row_key] = $row_data['title'];
                }


                // Prevent invalid choices from being set as default
                if (!array_key_exists($curr_reason, $available_reasons)) {
                    $curr_reason = null;
                }

                // Preselected value, if there's a valid one
                if ($curr_reason) {
                    Tag::setDefault('moderation_reason_' . $k, $curr_reason);
                }

                // Default options
                $moderation_reason_options = array(
                    'moderation_reason_' . $k,
                    $available_reasons,
                    'useEmpty'   => false,
                    'class'      => 'form-control show-tick moderation-reasons'
                );
                // Build the dropdown
                $moderation_reasons_final[$k] = Tag::select($moderation_reason_options);
            } else {
                $moderation_statuses_final[$k] = array(
                    'text' => $title,
                    'attributes' => array(
                        'data-has-reasons' => 0
                    )
                );
            }
        }

        // Prevent invalid choices from being set as default
        if (!array_key_exists($status, $moderation_statuses_final)) {
            $status = null;
        }

        // Preselected value, if there's a valid one
        if ($status) {
            Tag::setDefault('moderation', $status);
        }

        // Default options
        $moderation_select_options = array(
            'moderation',
            $moderation_statuses_final,
            'useEmpty'   => false,
            'class'      => 'form-control show-tick'
        );

        // Build the dropdown
        $moderation_dropdown = Tag::select($moderation_select_options);
        $this->view->setVar('moderation_dropdown', $moderation_dropdown);
        $this->view->setVar('moderation_reasons', $moderation_reasons_final);
    }

    /**
     * @param string $dropdown_field_name Name/ID of the dropdown
     * @param int|null $selected Value to preselect
     * @param array|null $options Tag::select() option overrides
     *
     * @return array|null
     */
    public function category_id_dropdown($dropdown_field_name, $selected = null, $options = array()) {

        // Category tree is used so we don't have to call getPath for every node,
        // (and instead just get already queried data) + to populate the categories dropdown
        $model = new Categories();
        $tree = $this->getDI()->get('categoryTree');
        $selectables = $model->getSelectablesFromTreeArray($tree, '', $with_root = false, $depth = null, $breadcrumbs = true);

        // Prevent invalid choices from being set as default
        if (!array_key_exists($selected, $selectables)) {
            $selected = null;
        }

        // Preselected value, if there's a valid one
        if ($selected) {
            Tag::setDefault($dropdown_field_name, $selected);
        }

        // Process $selectables before using them in Tag::select() in order to
        // display a more useful dropdown (for when it's not being handled by selectpicker())
        if (isset($options['subtext']) && !$options['subtext']) {
            foreach ($selectables as $k => $data) {
                if (isset($data['attributes'])) {
                    if (isset($data['attributes']['data-subtext'])) {
                        $subtext = $data['attributes']['data-subtext'];
                        $selectables[$k]['text'] = $subtext . ' › ' . $selectables[$k]['text'];
                        unset($selectables[$k]['attributes']['data-subtext']);
                    }
                }
            }
            unset($options['subtext']);
        }

        // Default options
        $defaults = array(
            $dropdown_field_name,
            $selectables,
            'useEmpty'   => true,
            'emptyText'  => 'Choose category...',
            'emptyValue' => '',
            'class'      => 'form-control'
        );

        // Merge passed in $options
        $options = array_merge($defaults, $options);

        // Build the dropdown
        $category_id_dropdown = Tag::select($options);
        $this->view->setVar($dropdown_field_name . '_dropdown', $category_id_dropdown);

        // Set the entire tree as a view variable (for displaying an article's category path or similar)
        $this->view->setVar('tree', $tree);

        return $tree;
    }

    /**
     * AwaitingModeration Action
     */
    public function awaitingModerationAction() {
        $this->tag->setTitle('Ads awaiting moderation');

        $page = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode = $this->processSearchMode();
        $dir = $this->processAscDesc('asc');
        $order_by = $this->processOrderBy('created_at');
        $payment_type = $this->processFilterPaymentType();
        $limit = $this->processLimit();

        Tag::setDefaults(array(
            'q'            => $search_for,
            'mode'         => $mode,
            'dir'          => $dir,
            'order_by'     => $order_by,
            'payment_type' => $payment_type,
            'limit'        => $limit
        ), true);

        $filter_category_id = null;
        $filter_category = null;
        if ($this->request->has('filter_category_id')) {
            $filter_category_id = $this->request->get('filter_category_id', 'int', null);
            $filter_category = Categories::findFirst($filter_category_id);
        }

        // Filter category tree (view variable) + category dropdown generation
        $this->category_id_dropdown(
            'filter_category_id',
            $filter_category_id,
            array(
                'emptyText' => 'Any category',
                'subtext'   => false
            )
        );

        // Populates the hidden dropdown for the 'create new ad' modal
        $this->category_id_dropdown(
            'category_id',
            $filter_category_id,
            array(
                'disabled' => Categories::getCategoriesWithoutTransactionType()
            )
        );

        // Available search fields
        $available_fields = array(
            'id',
            'title',
            'description',
            'active',
            'user_id',
            'username',
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date',
            'import_id'
        );
        $date_fields = array(
            'created_at',
            'first_published_at',
            'published_at',
            'modified_at',
            'expires_at',
            'sort_date'
        );
        $field = $this->processOptions('field', $available_fields, null);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        $acceptable_payment_states = array(
            Ads::PAYMENT_STATE_FREE,
            Ads::PAYMENT_STATE_ORDERED,
            Ads::PAYMENT_STATE_PAID
        );

        // Build the query
        $builder = $this->modelsManager->createBuilder()
            ->columns(array(
                'ad.*',
                'user.type AS user_type',
                'user.username AS user_username',
                'user.remark AS user_remark',
                'shop.title AS user_shop_title'
            ))
            ->addFrom('Baseapp\Suva\Models\Ads', 'ad')
            ->innerJoin('Baseapp\Suva\Models\Users', 'ad.user_id = user.id', 'user')
            ->leftJoin('Baseapp\Suva\Models\UsersShops', 'ad.user_id = shop.id', 'shop')
            ->where('ad.id > 0 AND ad.soft_delete = 0')   // for query optimization because of GROUP BY usage
            ->andWhere('ad.online_product_id IS NOT NULL')
            ->andWhere(
                'ad.moderation = :moderation_status:',
                array(
                    'moderation_status' => 'waiting'
                )
            )
            ->andWhere('ad.latest_payment_state IN (' . implode(',', $acceptable_payment_states) . ')');

        // only if there is a filtering by a specific category, inner join categories model
        if (null !== $filter_category) {
            $builder->innerJoin('Baseapp\Suva\Models\Categories', 'ad.category_id = category.id', 'category');
            $builder->andWhere(
                'category.lft >= :category_left: AND category.rght <= :category_right:',
                array(
                    'category_left'  => $filter_category->lft,
                    'category_right' => $filter_category->rght
                )
            );
        }

        if (null !== $payment_type) {
            if ('paid' == $payment_type) {
                $builder->andWhere('ad.latest_payment_state = :latest_payment_state:', array('latest_payment_state' => Ads::PAYMENT_STATE_PAID));
            } else if ('ordered' == $payment_type) {
                $builder->andWhere('ad.latest_payment_state = :latest_payment_state:', array('latest_payment_state' => Ads::PAYMENT_STATE_ORDERED));
            } else if ('free' == $payment_type) {
                $builder->andWhere('ad.latest_payment_state = :latest_payment_state:', array('latest_payment_state' => Ads::PAYMENT_STATE_FREE));
            }
        }

        // If a specific field is specified, search only that one, otherwise search across all the fields
        // (but only if there's an actual search term entered)
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        $search_params = Utils::build_phalcon_search_sql_params($fields_to_search, $search_for, $mode);
        if (null !== $search_params && is_array($search_params)) {
            $conditions = array();
            $binds = array();
            foreach ($search_params as $fld => $val) {
                $field_bind_name = sprintf(':%s:', $fld);
                if (in_array($fld, $date_fields)) {
                    $conditions[] = 'DATE(FROM_UNIXTIME(ad.' . $fld . ')) LIKE ' . $field_bind_name;
                } else {
                    if ('username' === $fld) {
                        $conditions[] = 'user.' . $fld . ' LIKE ' . $field_bind_name;
                    } else {
                        $conditions[] = 'ad.' . $fld . ' LIKE ' . $field_bind_name;
                    }
                }
                $binds[$fld] = $val;
            }
            $builder->andWhere(implode(' OR ', $conditions), $binds);
        }

        // $builder->groupBy('ad.id');
        //$builder->orderBy('ad.active ASC, ad.product_sort ASC, ad.' . $order_by . ' ' . strtoupper($dir));
		$builder->orderBy('ad.' . $order_by . ' ' . strtoupper($dir));

        $paginator = new QueryBuilder(array('builder' => $builder, 'limit' => $limit, 'page' => $page));
        $current_page = $paginator->getPaginate();

        // Process the resulting page and extend with some additional data (ad image if it exists)
        $results_array = Ads::buildResultsArray($current_page->items);
        $current_page->items = $results_array;

        $pagination_links = Tool::pagination(
            $current_page,
            'suva/ads2/awaiting-moderation',
            'pagination',
            $this->config->settingsBackend->pagination_count_out,
            $this->config->settingsBackend->pagination_count_in
        );

        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);

        $this->assets->addJs('assets/backend/js/classifieds-index.js');
    }

    public function add_common_crud_assets() {
        // $this->assets->addCss('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.css');
        // // $this->assets->addJs('assets/vendor/jquery-ui-1.11.2-custom/jquery-ui.min.js');

        // $this->assets->addCss('assets/vendor/magicsuggest/magicsuggest-min.css');
        // $this->assets->addJs('assets/vendor/magicsuggest/magicsuggest-min.js');

        // $this->assets->addCss('assets/vendor/datepicker3.css');
        // $this->assets->addJs('assets/vendor/bootstrap-datepicker.js');

        // $this->assets->addJs('assets/backend/js/classifieds-edit.js');;

        // $this->assets->addCss('assets/vendor/awesome-bootstrap-checkbox.css');
        // $this->assets->addJs('assets/js/classified-submit-choose-ad-type.js');


		// $this->assets->addJs('assets/vendor/jquery.mask.min.js');
        // $this->assets->addJs('assets/backend/js/issues-edit.js');
        // $this->assets->addJs('assets/backend/js/users-edit.js');
        // $this->assets->addJs('assets/backend/js/input_search.js');
        // $this->assets->addJs('assets/backend/js/ads.js');


		// $this->assets->addJs('assets/backend/js/contact_info.js');



    }

    /**
     * Edit Action
     */
    public function editActionOLD($entity_id) {
			$ad = Ads::findFirst((int)$entity_id);
			if (!$ad) return $this->greska('Ad not found!');
			$_SESSION['_context']['selected_ad_id'] = $ad->id;

			$this->add_common_crud_assets();

			//$this->printr($_REQUEST);
			//error_log("edit");
// 			$this->category_id_dropdown(
// 				'new_category_id',
// 				null,
// 				array(
// 						'disabled' => Categories::getCategoriesWithoutTransactionType()
// 				)
// 			);

			if (!$ad->Category()) return $this->greska("Ad's Category not found");

// 			if ($this->request->isPost() && $ad->isSoftDeleted())
// 				return $this->greska('<strong>Error!</strong> SoftDeleted ad cannot be modified!');

			$parametrizator = new Parametrizator();
			$parametrizator->setModule('backend');
			$parametrizator->setCategory($ad->Category());
			$parametrizator->setAd($ad);

			if ($this->request->isPost()) {

				//error_log("prosa je if post");
				//$this->poruka("test");
// 				$moderation = $this->request->hasPost('_moderation') ? $this->request->getPost('_moderation') : null;


				// detect which button submitted the form.. in case the detected button is 'savemoderation' then we
				// handle only saving of 'Ads moderation part', otherwise, save all submitted data.
				if ($this->request->hasPost('savemoderation')) {
					$saved = $ad->save_moderation($this->request);
					$success_msg = '<strong>Ad moderated!</strong>';
					$save_action = 'savemoderation';
					//error_log("save moderation ");
				} else {
					//BORNA: ž
					//umjerto ovoga stavi
					/*
					if ($this->update() === true)  $saved = $this;
                    else $saved =               return $this->getMessages();
//                 }
//             }



					*/

					$saved = $ad->backend_save_changes($this->request);
					$success_msg = '<strong>Ad updated!</strong>';
					$save_action = $this->get_save_action();
					//error_log("backend save change");
					//error_log(print_r($saved));
				}

				if ($saved instanceof Ads) {
					//error_log("prosa je sejvanje ada");

					//$this->printr($checkedAction);
					$greska = $ad->n_after_save($this->request, $this->auth->get_user()->id);
					if ($greska) return $this->greska($greska);

					$this->flashSession->success($success_msg . ' ' . $saved->get_frontend_view_link('html'));
					if ('save' === $save_action)
						$next_url = 'suva/ads2/edit/' . $saved->id;

					return $this->redirect_to($next_url);
				} else {
					//error_log("nije prosa sejvanje ada");
					//ako nije instanceOf Ads onda je Phalcon\Validation\Message\Group
					foreach ($ad->getMessages() as $message)
						$err_msg .= $message.", ";
					$this->flashSession->error('<strong>Error!</strong> Fix all fields that failed to validate..'.$err_msg);
					$this->view->setVar('errors', $saved);
					$parametrizator->setRequest($this->request);
					$parametrizator->setErrors($saved);
					$this->processModerationReasons($this->request->getPost('moderation', 'string', null), $this->request->getPost('moderation_reason', 'string', null));
				}

			} else {
				$parametrizator->setData($ad->getData());
				$this->processModerationReasons($ad->moderation, $ad->moderation_reason);
			}

			//svi Issuei na svim publikacijama. U Publications je dodana funkcija Issues koja vraća issues za pojedini publication
			$this->setup_products_chooser($ad, $ad_category);
			$this->setup_rendered_parameters($parametrizator->render_input());
			$this->view->setVar('ad', $ad);


			//dohvar order itema sa odgovarajućim dopuštenim akcijam
			//lista order itema vezanih uz tekuću akciju sa dodanim poljem n_status
			//79 - lista related order items mora biti sortirana silazno prema vremenu unošenja
			$orders_items = Nava::arrToDict(
				Nava::sqlToArray(
					" select "
					."	oi.* "
					."	,o.n_status "
					."	,os.description as status_name "
					."	,(case when oi.n_expires_at < UNIX_TIMESTAMP() then 0 else 1 end) as is_active_now "
					." from orders_items oi "
					." 	join orders o on (oi.order_id = o.id and oi.ad_id = $entity_id  and oi.n_is_active = 1) "
					."		join n_order_statuses os on (o.n_status = os.id) "
					." order by oi.id desc"
				)
			);
			foreach ($orders_items as $oi){
				// dopuštene akcije na  related order itemu za pojedinog usera i status ordera
				$oi['actions'] = Nava::arrToDict( Nava::getAllowedActions($this->auth->get_user()->id, $oi['n_status'], 'related-order-item') );
				$orders_items[$oi['id']]['actions'] = Nava::arrToDict( Nava::getAllowedActions($this->auth->get_user()->id, $oi['n_status'], 'related-order-item') );
				$orders_items[$oi['id']]['n_status'] = $oi['n_status'];
				$orders_items[$oi['id']]['user_id'] = $this->auth->get_user()->id;

			}
			$this->view->setVar('orders_items',$orders_items);
	// 		$this->view->setVar('orders_items', Nava::sqlToArray("select oi.*, o.n_status from orders_items oi join orders o on (oi.order_id = o.id and oi.ad_id = $entity_id )"));
			$this->view->setVar('publications',\Baseapp\Suva\Models\Publications::find("is_active = 1"));

			$can_be_moderated = true;
			$moderation_warning = null;

			$ad_status = $ad->get_status();
			if ($ad_status == 0) {
					$can_be_moderated = false;
					$moderation_warning = 'This ad has not yet finished with <b>ad submission</b> process and cannot be moderated!';
			}
			if ($ad_status == 20) {
					$can_be_moderated = false;
					$moderation_warning = 'This ad has not yet finished with <b>ad republish</b> process (<b>waiting for payment</b>) and cannot be moderated!';
			}
			if ($ad->soft_delete === 1) {
					$can_be_moderated = false;
					$moderation_warning = 'This ad is <b>soft deleted</b> and cannot be moderated or edited!';
			}

			$this->view->setVar('can_be_moderated', $can_be_moderated);
			$this->view->setVar('moderation_warning', $moderation_warning);
			$this->view->setVar('ad_reports', $ad->getReports());

			$show_save_buttons = true;
			if ($ad->isSoftDeleted()) {
					$show_save_buttons = false;
			}
			$this->view->setVar('ads_remark', (isset($ad->AdditionalData->remark) ? $ad->AdditionalData->remark : ''));
			$this->view->setVar('show_save_buttons', $show_save_buttons);
			$this->view->setVar('ad_taker_username', $ad_taker_username);
			$this->view->setVar('orders', Nava::arrToDict(Orders::find()->toArray()));
			$this->view->setVar('orders_statuses', Nava::arrToDict(OrdersStatuses::find()->toArray()));
    }

    public function saveRemarkAction() {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['msg'] = 'Bad Request';
        } else {
            $ad = Ads::findFirst(array(
                'conditions' => 'id = :id:',
                'bind'       => array(
                    'id' => $_POST['ad_id']
                )
            ));
            if ($ad) {
                if ($ad->save_remark($_POST['remark'])) {
                    $response_array['status'] = true;
                } else {
                    $response_array['msg'] = 'Error saving remark for this ad!';
                }
            } else {
                $response_array['msg'] = 'Ad could not be found!?!';
            }
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    public function softDeleteAction($entity_id = null) {
        // Soft delete allowed only for 'admin' and 'support' roles
        if (!$this->auth->logged_in(array('admin', 'support'))) {
            $this->flashSession->error($this->insufficient_privileges);
            return $this->redirect_to('suva/ads2');
        }

        $this->view->pick('chunks/delete');

        $title = 'SoftDelete ad';
        $this->tag->setTitle($title);
        $this->view->setVar('form_title_long', $title);
        $this->view->setVar('is_soft_delete', true);
        $this->add_common_crud_assets();

        // Check if multiple resources are to be handled
        if (false !== strpos($entity_id, ',')) {
            $entity_ids = explode(',', $entity_id);
        } else {
            $entity_id = (int) $entity_id;
            if (!$entity_id) {
                $this->flashSession->error('Missing required ID parameter for SoftDelete action');

                return $this->redirect_back();
            }
            $entity_ids = array($entity_id);
        }

        /* @var $model \Baseapp\Suva\Models\BaseModel */
        $model = $this->crud_model_class;

        $ids = implode(',', array_map(function($id){
            return (int) trim($id);
        }, $entity_ids));

        /* @var $entities \Baseapp\Suva\Models\BaseModel[]|\Phalcon\Mvc\Model\Resultset */
        $condition = 'id IN (' . $ids . ')';
        $entities = $model::find($condition);

        // Do actual deletion if needed
        if ($this->request->isPost()) {
            foreach ($entities as $entity) {
                $ident = $entity->ident();
                $result = $entity->soft_delete();
                if ($result) {
                    // When SoftDeleting an ad, delete the potential AdsHomepage record too
                    AdsHomepage::deleteByAdId($entity->id);
                    $this->flashSession->success('Resource <strong>' . $ident . '</strong> has been SoftDeleted.');
                } else {
                    $this->flashSession->error('Failed SoftDeleting <strong>' . $ident . '</strong>!');
                }
            }
            return $this->redirect_to($this->get_next_url());
        }

        // Setup view vars
        $this->tag->setTitle('SoftDelete Confirmation');
        Tag::setDefault('entity_id', $entity_id);
        $this->view->setVar('entity_id', $entity_id);
        $this->view->setVar('entities', $entities);
        $this->view->setVar('model', $model);
    }

    public function refreshOfflineDescriptionAction() {
        $this->disable_view();
        $this->response->setContentType('application/json', 'UTF-8');
        $response_array = array('status' => false);

        if (!$this->request->isAjax()) {
            $this->response->setStatusCode(400, 'Bad Request');
            $response_array['msg'] = 'Bad Request';
        } else {
            $ad = new Ads();
            // clear the offline description!
            $_POST['ad_description_offline'] = null;
            $parametrizator = new Parametrizator();
            $parametrizator->setValidation((new \Baseapp\Library\Validations\AdsBackend())->get());
            $parametrizator->setModule('suva');
            $parametrizator->setMode('edit');
            $parametrizator->setCategoryById($this->request->getPost('category_id'));
            $parametrizator->setAd($ad);
            $parametrizator->setRequest($this->request);
            $parametrizator->prepare_and_validate();
            $parametrizator->parametrize();

            if (!empty($ad->description_offline)) {
                $export2avus = new ExportToAvus($ad);
                $ads_avus_category = $export2avus->get_offline_category_id($ad);

                $response_array['status'] = true;
                $response_array['data']   = array(
                    'avus'    => '[AVUS ID: ' . ($ads_avus_category ? '<span class="text-success">' . $ads_avus_category . '</span>' : '<span class="text-danger">unknown</span>') . ']',
                    'content' => $ad->description_offline
                );
            } else {
                $response_array['msg'] = 'Not enough parameters were filled!';
            }
        }

        $this->response->setJsonContent($response_array);
        return $this->response;
    }

    /**
     * @param Ads $entity
     * @param Categories $category
     */
    protected function setup_products_chooser( $entity,  $category) {
        // only certain roles have ability to attach/update ad's product
			// igor - html je definiran na \app\common\library\Products\BackendProductsChooser.php:48

        if ($this->auth->logged_in(array('admin', 'support'))) {
            $chooser = $entity->getProductsChooser($this->request, 'backend', $category);
            $this->view->setVar('products_chooser_markup', $chooser->getMarkup());
        }
    }

    /**
     * spamToggle Action
     */
    public function spamToggleAction($entity_id) {
        $entity_id = (int)$entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for this action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Suva\Models\Ads */
        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Suva\Models\Ads */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($entity->spamToggle()) {
            $this->flashSession->success('Spam status for ad <strong>' . $entity->ident() . '</strong> changed successfully.');
        } else {
            $this->flashSession->error('Failed changing spam status for ad <strong>' . $entity->ident() . '</strong>.');
        }

        return $this->redirect_back();
    }

    /**
     * activeToggle Action
     */
    public function activeToggleAction($entity_id) {
        $entity_id = (int)$entity_id;
        if (!$entity_id) {
            $this->flashSession->error('Missing required ID parameter for this action!');
            return $this->redirect_back();
        }

        /* @var $model \Baseapp\Suva\Models\Ads */
        $model = $this->crud_model_class;
        /* @var $entity \Baseapp\Suva\Models\Ads */
        $entity = $model::findFirst($entity_id);

        if (!$entity) {
            $this->flashSession->error(sprintf('Entity not found [Model: %s, ID: %s]', $this->crud_model_class, $entity_id));
            return $this->redirect_back();
        }

        if ($entity->activeToggle()) {
            $this->flashSession->success('Active status for ad <strong>' . $entity->ident() . '</strong> changed successfully.');
        } else {
            $this->flashSession->error('Failed changing active status for ad <strong>' . $entity->ident() . '</strong>.');
        }

        return $this->redirect_back();
    }

    public function publicationsAction($publication_id, $ad_id = null, $pCategoryId = 0) {
		$debug = null;
		//vraća popis proizvoda koji su bazirani na kategoriji i publikaciji
		//tj. popis proizvoda na temelju publikacije sa cijenama na temelju kategorije
		//error_log("publicationsAction 1 publication_id: $publication_id ,category_id: $pCategoryId");

		//194 - New Offline product -> Issues - nudi prošla izdanja novina
		// Automatsko mijenjanje statusa u deadline issues
		Nava::updateIssuesStatuses();

        $this->view->disable();

        $request =$this->request;

        if ($request->isPost()==true) {
			if ($debug) //error_log("publicationsAction 2");
            if ($request->isAjax() == true) {
				if ($debug) //error_log("publicationsAction 3");

                /* GET  PRODUCT ITEMS BASED ON THE PRODUCT ID*/
				if (is_int($ad_id)){
					//error_log("publicationsAction 4");
					//na temlju ada- doznat kategoroiju i izuračunat cijene proizvoda..

// 					$result_products = Nava::sqlToArray(
// 						"select p.* from n_products p"
// 						."  join n_products_categories_prices pcp on (p.id = pcp.products_id)"
// 						."  where pcp.categories_id = (select category_id from ads where id= ".$ad_id.")"
// 					);

					//INT56 - AdsController publicationsAction - riješit popis proizvoda
					$ad_rec = Nava::sqlToArray("select * from ads where id = ".$ad_id);
					$lCategoryId = ($pCategoryId > 0) ?  $pCategoryId : $ad_rec[0]['category_id'];


					$result_products = Nava::getProductsWithPricesForCategoryAndPublication( $lCategoryId, $publication_id);
					//error_log("PCP PROIZVODI:".print_r($result_products,true));


				}else{
					//error_log("publicationsAction 5");
					// ovo se nikad neće izvršit jer ad_id uvijek dolazi
					$result_products = N_Products::find(
							 array(
								   "conditions" => "publication_id = ?1 ",
									"bind"      => array(1 => $publication_id)
							)
						)->toArray();

				}
				if ($debug) //error_log("publicationsAction 6");

				//dodavanje display ad-a
// 				$result_products = Nava::arrToDict($result_products);
// 				if ($is_display_ad == 1) {
// 					//error_log("publicationsAction 7");
// 					foreach ($result_products as $rp){
// 						if ($rp['is_display_ad'] == 0) {
// 							//error_log("publicationsAction 8");
// 							unset($result_products[$rp['id']]);
// 						}
// 					}
// 				}

				$data['products'] = $result_products;

				//igor 3.3.2016 - ako je publikacija ONLINE, nema issues

				$publication_rec = Publications::findFirst($publication_id)->toArray();
				$data['publications_rec'] = $publication_rec;
				//error_log ("PUBS_REC:".print_r($publication_rec,true));
				//error_log ("PUBS_REC->is_online:".$publication_rec->is_online);
				if ($publication_rec['is_online'] == 0){
					if ($debug) //error_log("publicationsAction 9");
					if ($debug) //error_log ("NIJE ONLINE $publication_id");
					$result_issues = N_Issues::find(
							array(
								   "conditions" => "publications_id = ?1 AND status='prima'",
									"bind"      => array(1 => $publication_id)
							)
						)->toArray();

					$txtSql = "select p1.*, concat(day,'.',month,'.',year,' ',day_of_week)  as name from ("
					." select iss.* ,"
					." year(iss.date_published) as year, "
					." month(iss.date_published) as month, "
					." day(iss.date_published) as day, "
					." iss.date_published as datum, "
					." (case "
					."          when weekday(iss.date_published) = 0 then 'Pon' "
					."          when weekday(iss.date_published) = 1 then 'Uto' "
					."          when weekday(iss.date_published) = 2 then 'Sri' "
					."          when weekday(iss.date_published) = 3 then 'Čet' "
					."          when weekday(iss.date_published) = 4 then 'Pet' "
					."          when weekday(iss.date_published) = 5 then 'Sub' "
					."          when weekday(iss.date_published) = 6 then 'Ned' "
					."          else '' end ) as day_of_week "
					.", 0 as is_online"
					." from n_issues iss "
					." where "
					." iss.status = 'prima' "
// 					." and iss.date_published >= CURDATE()-1 "
					." and iss.publications_id = " . $publication_id
					." ) p1"
					." order by p1.datum ";

					$data['issues'] = Nava::sqlToArray($txtSql);

				} else {
					//error_log("publicationsAction 10");
					//error_log ("JE ONLINE $publication_id");
// 					//ako je publikacija online, ne klikaju se issues nego se odabire proizvod koji na sebi ima trajanje
					$data['issues'] = Array();
				}

			if ($debug) //error_log("publicationsAction 11");
            echo json_encode($data);
            }
        }
    }

	//akcije za dijelove koji se pozivaju ajaxom
	public function subInsertionsAction($pAdId = null){
		//$this->poruka("subInsertions");
		if (! $pAdId > 0 ) return $this->greska ("Ad id not supplied to partial subInsertions");
		$ad = \Baseapp\Suva\Models\Ads::findFirst( $pAdId );
		if (! $pAdId ) return $this->greska ("Ad Nr. $pAdId not found in partial subInsertions");
		$this->view->setVar('ad', $ad);
		\Baseapp\Suva\Library\libUserActions::runUserAction($this);
		$this->view->pick('ads2/subInsertions');


	}

	public function subAdsPublicationsAndIssuesAction(){
		if (! $pAdId > 0 ) return $this->greska ("Ad id not supplied to partial subInsertions");
		$ad = \Baseapp\Suva\Models\Ads::findFirst( $pAdId );
		if (! $pAdId ) return $this->greska ("Ad Nr. $pAdId not found in partial subInsertions");
		$this->view->setVar('ad', $ad);
		$this->view->pick('ads2/subAdsPublicationsAndIssues');

		$clickedAction = $_SESSION['_context']['clicked_action'];
		$request = $this->request;
		switch($clickedAction){
			case 'add_insertions_for_ad':
				$checkedPublications = $request->hasPost('_checked_publications2') ? $request->getPost('_checked_publications2') : null;
				$checkedDates = $request->hasPost('_checked_dates2') ? $request->getPost('_checked_dates2') : null;
				if ($checkedPublications || $checkedDates){
// 					//error_log("Ads::n_after_save 2 ");
					//DODAVANJE ORDER ITEMA NA TEMELJU KLIKNUTIH PUBLIKACIJA
					$checkedPublications = $request->hasPost('_checked_publications2') ? $request->getPost('_checked_publications2') : null;
					$checkedDates = $request->hasPost('_checked_dates2') ? $request->getPost('_checked_dates2') : null;
					$checkedProducts = $request->hasPost('_checked_products2') ? $request->getPost('_checked_products2') : null;
					$checkedDiscounts = $request->hasPost('_checked_discounts2') ? $request->getPost('_checked_discounts2') : null;
					$selectedAds = $request->hasPost('_users_ads') ? $request->getPost('_users_ads') : null;
					// offline izdanja - preko checked_publications
					if ($checkedPublications) {
// 						//error_log("Ads::n_after_save 3 ");
						foreach($checkedPublications as $key => $value){
							//return 'id publikacije';
							foreach($selectedAds as $adId){
								$ad = \Baseappq\Models\Ads::findFirst($adId);
								if ($checkedProducts){
									$oi = new OrdersItems();
									$oi->n_set_default_values();
									$oi->n_publications_id = $key;
	// 								$oi->ad_id = $this->id;
									$oi->n_category_id = $ad->category_id;
									$oi->n_products_id = $checkedProducts[$key];
									$oi->n_frontend_sync_status = 'no_sync';

									$oi->ad_id = $adId;


										//slanje parametara u before save i after save
									$p = array();
									$p['publications'] = [$key];
									$p['user_id'] = $ad->user_id;
									$p['offline_issues'] = $value;
									if ($checkedDiscounts) $p['offline_discounts'] = $checkedDiscounts[$key];

									$greska = $oi->n_before_save(null,$pLoggedUserId, $p);
									if ($greska) return $greska;

									$result = $oi->create();
									$greska = $oi->n_after_save(null, $p);
									if ($greska) return $greska;
									else $okMsg .= "Order Item No. $oi->id (offline) created.".PHP_EOL;

								}else{
									return "Odabrana je publikacija ".\Baseapp\Suva\Models\Publications::findFirst($key)->name." na kojoj nije odabran proizvod";
								}
							}
						}
					}

					//online izdanja - preko checked_dates  key-publication_id,  value-n_first_published_at
					if ($checkedDates) {
// 						//error_log("Ads::n_after_save 4 ");
						foreach($checkedDates as $key => $value){
// 							//error_log("Ads::n_after_save 4.1 key $key, value $value");
							//uvijek šalje checked_dates,(defaultne datume), TREBA BITI CHECKIRAN I PROIZVOD
							foreach($selectedAds as $adId){

								if (array_key_exists($key, $checkedProducts)){
// 									//error_log("Ads::n_after_save 4.5 array_key_exists($key, checkedProducts) ");
									$oi = new OrdersItems();
									$oi->n_set_default_values();
									$oi->n_publications_id = $key;
	// 								$oi->ad_id = $this->id;
									$oi->n_category_id = $this->category_id;
									$oi->n_products_id = $checkedProducts[$key];
									$oi->n_first_published_at = $oi->n_interpret_value_from_request(trim($value), 'date');
									$oi->n_frontend_sync_status = 'no_sync';
									$oi->ad_id = $adId;

// 									//error_log("Ads::n_after_save 4.5.1  oi->n_first_published_at: $oi->n_first_published_at");
									//slanje parametara u before save i after save
									$p = array();
									$p['publications'] = [$key];
									$p['user_id'] = $this->user_id;
									$p['offline_issues'] = $value;
									if ($checkedDiscounts) $p['offline_discounts'] = $checkedDiscounts[$key];

									$greska = $oi->n_before_save(null,$pLoggedUserId, $p);

									if ($greska) {
// 										//error_log("Ads::n_after_save 4.6 greška u oi->n_before_save:".$greska);
										return $greska;
									} else
// 										//error_log("Ads::n_after_save 4.7 NOT greska oi->n_before_save");


									$result = $oi->create();
// 									//error_log("Ads::n_after_save 4.7.1 result class:".get_class($result).", od->id:".$oi->id);
									$greska = $oi->n_after_save(null, $p);
								}
								if ($greska) {
// 									//error_log("Ads::n_after_save 4.8 greška u oi->n_after_save:".$greska);
									return $greska;
								}
								else{
									$okMsg .= "Order Item No. $oi->id (online) created.".PHP_EOL;
// 									//error_log("Ads::n_after_save 4.9 NOT greska oi->n_after_save");
								}

							}
						}
					}
				}
			break;


		}
	}

	public function subUsersAdsAction( $pUserId = null ){

		//error_log("Ads2Controller:subUsersAdsAction");
		if (! $pUserId > 0 ) {
			return $this->greska ("User id not supplied to partial subInsertions");
			//error_log ("subUsersAdsAction: nije poslan $pUserId ");
		}
		$user = \Baseapp\Suva\Models\Users::findFirst( $pUserId );
		$this->view->setVar('user', $user);
		$this->view->pick('ads2/subUsersAds');
		//error_log ("subUsersAdsAction: 2");

		$clickedAction = array_key_exists('clicked_action', $_SESSION['_context']) ? $_SESSION['_context']['clicked_action'] : null; //Dodano N
		//$clickedAction = $_SESSION['_context']['clicked_action']; //if this enabled -> Undefined index: clicked_action
		//$this->poruka ( $clickedAction );
		//$this->poruka ( print_r($_REQUEST, true) );

		$request = $this->request;
		\Baseapp\Suva\Library\libUserActions::runUserAction($this);
		// switch($clickedAction){
			// case 'add_insertions_for_ad':
			// //error_log('clickedAction add_insertions_for_ad 1');
				// $checkedPublications = $request->hasPost('_checked_publications2') ? $request->getPost('_checked_publications2') : null;
				// $checkedDates = $request->hasPost('_checked_dates2') ? $request->getPost('_checked_dates2') : null;
				// if ($checkedPublications || $checkedDates){
					// //error_log('clickedAction add_insertions_for_ad 2');
// // 					//error_log("Ads::n_after_save 2 ");
					// //DODAVANJE ORDER ITEMA NA TEMELJU KLIKNUTIH PUBLIKACIJA
					// $checkedPublications = $request->hasPost('_checked_publications2') ? $request->getPost('_checked_publications2') : null;
					// $checkedDates = $request->hasPost('_checked_dates2') ? $request->getPost('_checked_dates2') : null;
					// $checkedProducts = $request->hasPost('_checked_products2') ? $request->getPost('_checked_products2') : null;
					// $checkedDiscounts = $request->hasPost('_checked_discounts2') ? $request->getPost('_checked_discounts2') : null;
					// $selectedAds = $request->hasPost('_users_ads') ? $request->getPost('_users_ads') : null;
					// // offline izdanja - preko checked_publications


					// if ($checkedPublications) {
						// //error_log('clickedAction add_insertions_for_ad 3');
// // 						//error_log("Ads::n_after_save 3 ");
						// foreach($checkedPublications as $key => $value){
							// //return 'id publikacije';
							// foreach($selectedAds as $adId){
								// //error_log('clickedAction add_insertions_for_ad 4');
								// $ad = \Baseapp\Suva\Models\Ads::findFirst($adId);
								// if ($checkedProducts){
									// $oi = new OrdersItems();
									// $oi->n_set_default_values();
									// $oi->n_publications_id = $key;
	// // 								$oi->ad_id = $this->id;
									// $oi->n_category_id = $ad->category_id;
									// $oi->n_products_id = $checkedProducts[$key];
									// $oi->n_frontend_sync_status = 'no_sync';
									// $oi->ad_id = $adId;


										// //slanje parametara u before save i after save
									// $p = array();
									// $p['publications'] = [$key];
									// $p['user_id'] = $ad->user_id;
									// $p['offline_issues'] = $value;
									// if ($checkedDiscounts) $p['offline_discounts'] = $checkedDiscounts[$key];

									// $greska = $oi->n_before_save(null,$pLoggedUserId, $p);
									// if ($greska) return $greska;
									// //error_log('clickedAction add_insertions_for_ad 3.1');
									// $result = $oi->create();
									// $greska = $oi->n_after_save(null, $p);
									// if ($greska) return $greska;
									// else $okMsg .= "Order Item No. $oi->id (offline) created.".PHP_EOL;

								// }else{
									// return "Odabrana je publikacija ".\Baseapp\Suva\Models\Publications::findFirst($key)->name." na kojoj nije odabran proizvod";
								// }
							// }
						// }
					// }
					// //error_log('clickedAction add_insertions_for_ad 6');
					// //online izdanja - preko checked_dates  key-publication_id,  value-n_first_published_at
					// if ($checkedDates) {
						// //error_log('clickedAction add_insertions_for_ad 7');
// // 						//error_log("Ads::n_after_save 4 ");
						// foreach($checkedDates as $key => $value){
// // 							//error_log("Ads::n_after_save 4.1 key $key, value $value");
							// //uvijek šalje checked_dates,(defaultne datume), TREBA BITI CHECKIRAN I PROIZVOD
							// foreach($selectedAds as $adId){
								// //error_log('clickedAction add_insertions_for_ad 8');
								// if (array_key_exists($key, $checkedProducts)){
// // 									//error_log("Ads::n_after_save 4.5 array_key_exists($key, checkedProducts) ");
									// $oi = new OrdersItems();
									// $oi->n_set_default_values();
									// $oi->n_publications_id = $key;
	// // 								$oi->ad_id = $this->id;
									// $oi->n_category_id = $this->category_id;
									// $oi->n_products_id = $checkedProducts[$key];
									// $oi->n_first_published_at = $oi->n_interpret_value_from_request(trim($value), 'date');
									// $oi->n_frontend_sync_status = 'no_sync';
									// $oi->ad_id = $adId;

									// // //error_log("Ads::n_after_save 4.5.1  oi->n_first_published_at: $oi->n_first_published_at");
									// //slanje parametara u before save i after save
									// $p = array();
									// $p['publications'] = [$key];
									// $p['user_id'] = $this->user_id;
									// $p['offline_issues'] = $value;
									// if ($checkedDiscounts) $p['offline_discounts'] = $checkedDiscounts[$key];

									// $greska = $oi->n_before_save(null,$pLoggedUserId, $p);

									// if ($greska) {
// // 										//error_log("Ads::n_after_save 4.6 greška u oi->n_before_save:".$greska);
										// return $greska;
									// } else
// // 										//error_log("Ads::n_after_save 4.7 NOT greska oi->n_before_save");

									// //error_log('clickedAction add_insertions_for_ad 8.1');
									// $result = $oi->create();
// // 									//error_log("Ads::n_after_save 4.7.1 result class:".get_class($result).", od->id:".$oi->id);
									// $greska = $oi->n_after_save(null, $p);
								// }
								// if ($greska) {
// // 									//error_log("Ads::n_after_save 4.8 greška u oi->n_after_save:".$greska);
									// return $greska;
								// }
								// else{
									// $okMsg .= "Order Item No. $oi->id (online) created.".PHP_EOL;
									// $this->poruka("Order Item No. $oi->id (online) created.".PHP_EOL);
// // 									//error_log("Ads::n_after_save 4.9 NOT greska oi->n_after_save");
								// }

							// }
						// }
					// }
				// }
			// break;


		// }



	}

  //BORNA
	public function subUsersOffersAction_OLD( $pUserId = null ){

	//error_log("Ads2Controller:subUserOffersAction");
	if (! $pUserId > 0 ) {
		return $this->greska ("User id not supplied to partial subUsersCart");
		//error_log ("subUsersOffersAction: nije poslan $pUserId ");
	}
	$user = \Baseapp\Suva\Models\Users::findFirst( $pUserId );
	$this->view->setVar('user', $user);
	$this->view->pick('ads2/subUsersOffers');
	//error_log ("subUsersOffersAction: 2");
	$clickedAction = $_SESSION['_context']['clicked_action'];
    switch($clickedAction){
		case 'preview_pdf':
		//error_log(print_r($_REQUEST,true));
		
		//TODO:Zašto 2 puta haspost?
		if($this->request->hasPost('_users_orders_ids')) $selectedOrders = $this->request->hasPost('_users_orders_ids') ? $this->request->
		
		getPost('_users_orders_ids') : null;


        if (!array_key_exists('_context', $_SESSION)) $_SESSION['_context'] = array();
        $_SESSION['_context']['orderPdf_report_order_ids'] = $selectedOrders;
        //error_log("Ads::n_after_save preview_pdf  selectedOrders:".print_r($selectedOrders, true));

          $this->dispatcher->forward(
            array(
              "controller" => "reports",
              "action"     => "orderPdf",
              "params"     => array()
            )
          );
        break;
    }
  }

	//IGOR
	public function subUsersCartAction( $pUserId = null ){

		//error_log("Ads2Controller:subUsersCartAction");
		if (! $pUserId > 0 ) {
			return $this->greska ("User id not supplied to partial subUsersCart");
			//error_log ("subUsersCartAction: nije poslan $pUserId ");
		}
		$user = \Baseapp\Suva\Models\Users::findFirst( $pUserId );
		$this->view->setVar('user', $user);
		$this->view->pick('ads2/subUsersCart');
		//error_log ("subUsersCartAction: 2");
		//$clickedAction = $_SESSION['_context']['clicked_action'];

		//OVDJE SE OBRAĐUJU AKCIJE, PARAMETRI SE VADE IZ CONTEXTA
		//\Baseapp\Suva\Library\Nava::clickedActions();
		\Baseapp\Suva\Library\libUserActions::runUserAction($this);

	}

	
	public function subUsersFreeAdsAction( $pUserId = null ){

		//error_log("Ads2Controller:subUsersFreeAdsAction");
		if (! $pUserId > 0 ) {
			return $this->greska ("User id not supplied to firm subUsersInvoices");
			//error_log ("subUsersFreeAdsAction: nije poslan $pUserId ");
		}
		$user = \Baseapp\Suva\Models\Users::findFirst( $pUserId );
		$this->view->setVar('user', $user);
		$this->view->pick('ads2/subUsersFreeAds');
		//error_log ("subUsersFreeAdsAction: 2");
		
		//\Baseapp\Suva\Library\Nava::clickedActions();
		\Baseapp\Suva\Library\libUserActions::runUserAction($this);

		return;
	
	}
	
	public function subOrdersWithItemsAction( $pUserId = null, $pOrderStatusId = null ){

		//error_log("Ads2Controller:subOrdersWithItemsAction");
		if (! $pUserId > 0 ) {
			return $this->greska ("User id not supplied to firm subOrdersWithItemsAction");
			//error_log ("subOrdersWithItemsAction: nije poslan $pUserId ");
		}
		$user = \Baseapp\Suva\Models\Users::findFirst( $pUserId );
		
		if (! $pOrderStatusId > 0 ) {
			return $this->greska ("pOrderStatusId id not supplied to subOrdersWithItemsAction");
			//error_log ("subOrdersWithItemsAction: nije poslan $pOrderStatusId ");
		}
		
		$this->view->setVar('user', $user);
		
		$this->view->setVar('_order_status_id', $pOrderStatusId );
		$this->view->pick('ads2/subOrdersWithItems');
		//error_log ("subOrdersWithItemsAction: 2");
		
		/*
			razdvajaju se kliknuti orderi od kliknutih order itema
			kliknuti orderi se spremaju u $_SESSION['_context']['clicked_action_params']['orders']
			a kliknuti itemi u $_SESSION['_context']['clicked_action_params']['orders_items']
		
		*/
		$action = array_key_exists('clicked_action', $_SESSION['_context']) ? $_SESSION['_context']['clicked_action'] : null; 
		$params = array_key_exists('clicked_action_params', $_SESSION['_context']) ? $_SESSION['_context']['clicked_action_params'] : array(); 
		
		if (array_key_exists('orders_and_items', $params) ) {
			$checkedItems = array();
			foreach ( $params['orders_and_items'] as $oAndI ){
				//error_log("subOrdersWithItemsAction: 1.1 $oAndI");
				if (substr($oAndI, 0,5 ) == 'item_'  ) {
					array_push( $checkedItems, (int) substr( $oAndI, 5 ) );
				}
				else {
					//error_log("subOrdersWithItemsAction: 1.2 substr:".substr($oAndI, 0,4 ));
				}	
			}
			if (count($checkedItems) > 0) $_SESSION['_context']['clicked_action_params']['orders_items'] = $checkedItems;
			
			$checkedOrders = array();
			foreach ( $params['orders_and_items'] as $oAndI ){
				//error_log("subOrdersWithItemsAction: 1.1 $oAndI");
				if (substr($oAndI, 0,6 ) == 'order_'  ) {
					array_push( $checkedOrders, (int) substr( $oAndI, 6 ) );
				}
				// else 
					// //error_log("subOrdersWithItemsAction: 1.2 substr:".substr($oAndI, 0,6 ));
			 }
			if (count($checkedOrders) > 0) $_SESSION['_context']['clicked_action_params']['orders'] = $checkedOrders;
			
		}
		//\Baseapp\Suva\Library\Nava::clickedActions($this);
		//\Baseapp\Suva\Library\libUserActions::runUserAction($this);
		\Baseapp\Suva\Library\libUserActions::runUserAction($this);
		
	}


	public function subPublicationsAndIssuesAjaxAction( $pAdId = null ){
		//$this->printr($_REQUEST,'Ads2Controller::subPublicationsAndIssuesAjaxAction request');
		// error_log("Ads2Controller:subPublicationsAndIssuesAjaxAction");
		if (! $pAdId > 0 ) {
			return $this->greska ("Ad id not supplied to partial subPublicationsAndIssuesAjaxAction");
		}
		// error_log("Ads2Controller:subPublicationsAndIssuesAjaxAction 2");
		$ad = \Baseapp\Suva\Models\Ads::findFirst( $pAdId );
		
		$this->processModerationReasons($this->request->getPost('moderation', 'string', null), $this->request->getPost('moderation_reason', 'string', null));
		
		$this->view->setVar('ad', $ad);
		\Baseapp\Suva\Library\libUserActions::runUserAction($this);
		$this->view->pick("ads2/subPublicationsAndIssuesAjax");
	}

	public function subModerationAction( $pAdId = null ){
		//$this->printr($_REQUEST,'Ads2Controller::subModerationAction request');
		// error_log("Ads2Controller:subModerationAction");
		if (! $pAdId > 0 ) {
			return $this->greska ("Ad id not supplied to partial subModerationAction");
		}
		// error_log("Ads2Controller:subModerationAction 2");
		$ad = \Baseapp\Suva\Models\Ads::findFirst( $pAdId );
		
		$this->processModerationReasons($this->request->getPost('moderation', 'string', null), $this->request->getPost('moderation_reason', 'string', null));
		
		$this->view->setVar('ad', $ad);

	}

	
	
	//TODO: DI SE OVO KORISTI?
	public function insertToCartDefaultOnlineProduct($ad_id) {


// 					$this->flashSession->error(sprintf('TU SAM u insertToCartDefaultOnlineProduct'));
// 					return $this->redirect_back();

					$orders_items = Nava::sqlToArray(
						"select * from orders_items oi "
						." join n_publications pub on (pub.id = oi.n_publications_id) "
						." where oi.ad_id = ".$ad_id
						." and pub.is_online = 1"
					);

					//ako već postoji neki online proizvod
					if (sizeof($orders_items) > 0){
// 							$this->flashSession->error(sprintf('IMA ORDER ITEMA'));
// 							return $this->redirect_back();
								return;

					}

// 					$this->flashSession->error(sprintf('NEMA ORDER ITEMA'));
// 					return $this->redirect_back();

					$default_product = Products::findFirst( array("conditions" => "is_default_product = 1" ) );
					if (!$default_product) {
						/*
						GREŠKA - treba definirat defaultni proizvod
						*/
							$this->flashSession->error(sprintf('NEMA DEFAULTNOG PRODUCTA'));
							return $this->redirect_back();

					}

// 					$this->flashSession->error(sprintf('IMA DEFAULTNOG PRODUCTA'));
// 					return $this->redirect_back();


					$ad = Ads::findFirst($ad_id);

					if (!$ad){
							$this->flashSession->error(sprintf('NEMA ADA'));
							return $this->redirect_back();


					}

// 							$this->flashSession->error(sprintf('IMA ADA '.$ad->id));
// 							return $this->redirect_back();


					$product_id  = $default_product->id;
					$discount_id = Null;
					$publications_id = $default_product->publication_id;
// 				$offline_issues = $_POST['offline-issues'];

					$user_id = $ad->user_id;

					 $this->disable_view();
					 $this->response->setContentType('application/json', 'UTF-8');
					 $response_array = array('status' => true);



							$order = Orders::findFirst(
													array(
															"conditions" => "n_status = 1 AND user_id = ?1",
															"bind" => array(1 => $_POST['user_id'])
															)
													);

							if (!$order){
								$order = new Orders();

								$order->user_id = $user_id;
								$order->status = 1;
								$order->created_at = date('Y-m-d H:i:s');
								$order->created_at = date('Y-m-d H:i:s');
								$order->ip = $this->request->getClientAddress();
								$order->n_status = 1;
								$order->n_frontend_sync_status = 'no_sync';

								if ($order->save() == false) {
									$response_array['status'] = false;
									$err_msg = "";
									foreach ($order->getMessages() as $message) {
													 $err_msg .= $message . "\n\r";
									}
								 $response_array['msg'] = $err_msg;
								}
							}

							$product = N_Products::findFirst((int)$product_id);


							$product_add = new OrdersItemsEx();

							$product_add->n_products_id = $product_id;
							$product_add->n_publications_id = $publications_id;
							$product_add->order_id = $order->id;
							$product_add->ad_id = $ad_id;
							$product_add->price = 0;
							$product_add->qty = 1;
							$product_add->title = $product->name;
							$product_add->total =  $product_add->price * $product_add->qty;
							$product_add->tax_rate = 0;
							$product_add->tax_amount = 0;
							$product_add->product = serialize($product_add);
							$product_add->product_id = $product->name." OBAVEZNA OBJAVA ONLINE OGLASA";

							//IGOR - dodano da se koristi za online izdanja, kopira se sa Ad-a
							$product_add->n_first_published_at = $ad->first_published_at;
							$product_add->n_expires_at = $ad->expires_at;

							if ($product_add->save() == false) {
									$response_array['status'] = false;
									$err_msg = "";
									foreach ($product_add->getMessages() as $message) {
													 $err_msg .= $message . "\n\r";
									}
									$this->flashSession->error(sprintf($err_msg));
									return $this->redirect_back();
// 									 $response_array['msg'] = $err_msg;
							}else{
									$order->total = $order->total + ($product_add->total / 100 );
									if ($order->save() == false) {
											$response_array['status'] = false;
											$err_msg = "";
											foreach ($order->getMessages() as $message) {
															 $err_msg .= "INSERT INSERTION - " . $message . "\n\r";
											}
// 											$response_array['msg'] = $err_msg;
											$this->flashSession->error(sprintf($err_msg));
											return $this->redirect_back();
									}
									if ($response_array['status'] == true){
											$response_array['msg'] = 'Added item!';
									}
							}

						return;
	}

	
	//CATEGORY MAPPINGS - DTP
	public function adsInCategoryMappingAction ( $pCategoryMappingId = null ){
		
		if (! $pCategoryMappingId > 0 ) {
			$this->sysMessage (" Ads2Controller::adsInCategoryMappingAction  pCategoryMappingId not supplied : $pCategoryMappingId ");
			return $this->redirect_back();
		}
		
		
		$adIds = \Baseapp\Suva\Library\libPCM::getIds(
			array (
				"modelName" => "Ads"
				
				,"filters" => array (
					array(
						"modelName" => "CategoriesMappings"
						,"idValue" => $pCategoryMappingId
					)
				)
			)
		);

		$_SESSION['_context']['default_filter']['id'] = $adIds;
		
		$this->n_query_index ("Ads");

		$this->view->pick("ads2/index");
		
		
	}

	public function adsInIssueAction ( $pIssueId = null ){
		
		if (! $pIssueId > 0 ) {
			$this->sysMessage (" Ads2Controller::adsInIssueAction  pIssueId not supplied : $pIssueId ");
			return $this->redirect_back();
		}
		
		
		$adIds = \Baseapp\Suva\Library\libPCM::getIds(
			array (
				"modelName" => "Ads"
				
				,"filters" => array (
					array(
						"modelName" => "Issues"
						,"idValue" => $pIssueId
					)
				)
			)
		);

		$_SESSION['_context']['default_filter']['id'] = $adIds;
		
		$this->n_query_index ("Ads");

		$this->view->pick("ads2/index");
		
		
	}	
}