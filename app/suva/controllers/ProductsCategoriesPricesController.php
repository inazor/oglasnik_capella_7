<?php

namespace Baseapp\Suva\Controllers;

class ProductsCategoriesPricesController extends IndexController{

    public function indexAction() {
		//$this->printr($_SESSION['_context'], "ProductsCategoriesPricesController::indexAction CONTEXT");
		//$this->printr($_REQUEST, "ProductsCategoriesPricesController::indexAction REQUEST");
		
		$this->tag->setTitle("Product, Categories and Prices");
		
		$joins = null;
		$publicationId = null;
		if (array_key_exists('filter', $_SESSION['_context']))
			if ( is_array($_SESSION['_context']['filter']) )  
				if ( array_key_exists( '__Products__publication_id' , $_SESSION['_context']['filter'] ) )
					if ( $_SESSION['_context']['filter']['__Products__publication_id']  > 0 )
						$publicationId = $_SESSION['_context']['filter']['__Products__publication_id'];
 
		if ( $publicationId ) { 
			$publication = \Baseapp\Suva\Models\Publications::findFirst( $publicationId );
			if (!publication){
				$this->sysMessage ("ProductsCategoriesPricesController::indexAction - Publication $publicationId not found.");
				return $this->redirect_back();
			}
			$this->tag->setTitle("PCP for $publication->name");
			$joins = array (
				array(
					'joinedModelTable' => 'n_products',
					'originalModelField' => 'products_id',
					'joinedModelField' => 'id',
					'joinedModelConditions' => array (
						array(
							'field' => 'publication_id', 
							'operator' => '=',
							'value' => $_SESSION['_context']['filter']['__Products__publication_id']
						)
					)
				)
			);
			
			
		}
		
		$this->n_query_index( "ProductsCategoriesPrices", null, $joins ); //ovo je definirano u BaseController
    }

		public function crudAction($pEntityId = null) {
			$this->n_crud_action("ProductsCategoriesPrices", $pEntityId); //ovo je definirano u BaseController
			$this->view->pick('products-categories-prices/crud');
		}
}