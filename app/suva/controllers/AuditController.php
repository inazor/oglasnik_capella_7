<?php

namespace Baseapp\Suva\Controllers;

use Baseapp\Extension\Tag;
use Baseapp\Library\Tool;
use Baseapp\Suva\Models\Audit;
use Baseapp\Traits\CrudActions;
use Baseapp\Traits\CrudHelpers;

class AuditController extends IndexController
{
    use CrudActions;
    use CrudHelpers;

    public $crud_model_class = 'Baseapp\Suva\Models\Audit';

    protected $allowed_roles = array('admin');

    protected function get_models2controller_map()
    {
        return array(
            'Baseapp\Suva\Models\Dictionaries' => 'dictionaries',
            'Baseapp\Suva\Models\Users' => 'users',
            'Baseapp\Suva\Models\Ads' => 'ads',
            'Baseapp\Suva\Models\Sections' => 'sections',
            'Baseapp\Suva\Models\Categories' => 'categories',
            'Baseapp\Suva\Models\Parameters' => 'parameters',
            'Baseapp\Suva\Models\Media' => 'media',
            'Baseapp\Suva\Models\ImageStyles' => 'image-styles',
        );
    }

    protected function get_modelNames()
    {
        return array(
            'Baseapp\Suva\Models\Dictionaries' => 'Dictionaries',
            'Baseapp\Suva\Models\Users' => 'Users',
            'Baseapp\Suva\Models\Ads' => 'Ads',
            'Baseapp\Suva\Models\Sections' => 'Sections',
            'Baseapp\Suva\Models\Categories' => 'Categories',
            'Baseapp\Suva\Models\Parameters' => 'Parameters',
            'Baseapp\Suva\Models\Orders' => 'Orders',
            'Baseapp\Suva\Models\Media' => 'Media',
            'Baseapp\Suva\Models\ImageStyles' => 'Image styles',
        );
    }

    /**
     * Index Action
     */
    public function indexAction()
    {
        $title = 'Audit Log Viewer';
        $this->tag->setTitle($title);
        $this->view->setVar('page_title', $title);

        $page       = $this->getPageParam();
        $search_for = $this->processSearchParam();
        $mode       = $this->processSearchMode();

        $available_fields = array(
            'ip',
            'message',
            //'model_name',
            'model_pk_val',
            'controller',
            'action',
            'params',
            'type',
            'username',
            'user_id'
        );
        $field = $this->processOptions('field', $available_fields, null);
        Tag::setDefault('field', $field);
        if (null === $field || empty($field)) {
            $field = 'all';
        }

        $model = $this->processOptions('model', $this->get_modelNames(), null);
        Tag::setDefault('model', $model);
        if (null === $model || empty($model)) {
            $model = 'all';
        }


        // If a specific field is searched/requested, use only that one, otherwise search all the fields
        $fields_to_search = (null === $field || 'all' === $field) ? $available_fields : array($field);
        // If a specific model is searched/requested, use only that one, otherwise search all the models
        $model_to_search = (null === $model || 'all' === $model) ? null : $model;

        $paginator = new \Baseapp\Extension\Paginator\Adapter\SimpleMySQL(array(
            'sql' => Audit::build_search_sql($model_to_search, $fields_to_search, $search_for, $mode),
            'limit' => $this->config->settingsSuva->pagination_items_per_page,
            'page' => $page
        ));
        $current_page = $paginator->getPaginate();

        // Populates the detail records of each log record about to be shown on this page by only
        // issuing one query with an IN() statement (containing ids of matching detail records)
        Audit::hydrate_audit_details($current_page->items);

        // Now finally build our bootstrap pagination (if there's something to build)
        $pagination_links = Tool::pagination(
            $current_page,
            'admin/audit',
            'pagination',
            $this->config->settingsSuva->pagination_count_out,
            $this->config->settingsSuva->pagination_count_in
        );

        Tag::setDefaults(array(
            'q'        => $search_for,
            'mode'     => $mode,
            'field'    => $field,
            'model'    => $model
        ), true);
        $this->view->setVar('page', $current_page);
        $this->view->setVar('pagination_links', $pagination_links);
        $this->view->setVar('models_controllers_map', $this->get_models2controller_map());
    }

}
