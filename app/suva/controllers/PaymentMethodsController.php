<?php

namespace Baseapp\Suva\Controllers;

class PaymentMethodsController extends IndexController{

    public function indexAction() {
			$this->tag->setTitle("Payment Methods");
			$this->n_query_index("PaymentMethods"); //ovo je definirano u BaseController
    }

		public function crudAction($pEntityId = null) {
			$this->n_crud_action("PaymentMethods", $pEntityId); //ovo je definirano u BaseController
			$this->view->pick('payment-methods/crud');
		}
}