<?php

namespace Baseapp\Suva\Models;

// use Baseapp\Bootstrap;
// use Phalcon\Di;
// use Phalcon\Mvc\Model\Resultset;
// use Baseapp\Library\Validations\Categories as CategoriesValidations;
// use Baseapp\Library\Behavior\NestedSet as NestedSetBehavior;
// use Baseapp\Traits\FormModelControllerTrait;
// use Baseapp\Traits\NestedSetActions;
// use Baseapp\Suva\Models\CategoriesSettings as CategorySettings;
// use Baseapp\Library\Parameters\Parametrizator;

/**
 * Categories Model
 */
//class Categories extends BaseModelBlamable
class Categories extends \Baseapp\Models\Categories
{
	use traitBaseSuvaModel;
	
	
	
//DODANO

//veze one2many

	public function CategoriesFieldsets ( $p = null ){
		if (! $this->id > 0 ) return null;
		$findParam = array();
		if (gettype($p)=='string') $findParam[0] = " and $p";
		elseif (is_array($p)){
			if ($p[0]) $findParam[0] = " and $p[0]";
			if ($p["order"]) $findParam["order"] = $p["order"];
			if (array_key_exists('limit',$p)) $findParam["limit"] = $p["limit"]; else $findParam["limit"] = 20;
		} 
		$findParam[0] = "category_id = $this->id ".$findParam[0];
		
		return \Baseapp\Suva\Models\CategoriesFieldsets::find($findParam);
	}

	public function CategoriesFieldsetsParameters ( $p = null ){
		if (! $this->id > 0 ) return null;
		$findParam = array();
		if (gettype($p)=='string') $findParam[0] = " and $p";
		elseif (is_array($p)){
			if ($p[0]) $findParam[0] = " and $p[0]";
			if ($p["order"]) $findParam["order"] = $p["order"];
			if (array_key_exists('limit',$p)) $findParam["limit"] = $p["limit"]; else $findParam["limit"] = 20;
		} 
		$findParam[0] = "category_id = $this->id ".$findParam[0];
		
		return \Baseapp\Suva\Models\CategoriesFieldsetsParameters::find($findParam);
	}

	public function PublicationsCategoriesMappings ( $p = null ){
		if (! $this->id > 0 ) return null;
		$findParam = array();
		if (gettype($p)=='string') $findParam[0] = " and $p";
		elseif (is_array($p)){
			if ($p[0]) $findParam[0] = " and $p[0]";
			if ($p["order"]) $findParam["order"] = $p["order"];
			if (array_key_exists('limit',$p)) $findParam["limit"] = $p["limit"]; else $findParam["limit"] = 20;
		} 
		$findParam[0] = "mapped_category_id = $this->id ".$findParam[0];
		
		return \Baseapp\Suva\Models\PublicationsCategoriesMappings::find($findParam);
	}	

}
