<?php

namespace Baseapp\Suva\Models;

/**
 * Publications Model
 */
class RolesPermissions extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_roles_permissions");
    }

	public function getSource()
    {
        return 'n_roles_permissions';
    }
}