<?php

namespace Baseapp\Suva\Models;
use Baseapp\Suva\Library\Nava;


/**
 * Publications Model
 */
class CategoriesMappings extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_categories_mappings");
    }

        public function getSource()
    {
        return 'n_categories_mappings';
    }

	public function n_before_save($request = null){
		//error_log ("CategoriesMappings:n_before_save 1 this:".print_r($this->toArray(), true));
		if (!$this->level && $this->parent_id) {
			//error_log ("CategoriesMappings:beforeSave 2");
			$this->level = $this->ParentCategory()->level + 1;
		}
		if (!$this->level && !$this->parent_id) {
			//error_log ("CategoriesMappings:beforeSave 3");
			$this->level =  1;
		}
		
		
		$this->n_path = $this->path();
		
		
		
		return null;
	}
	
	public function beforeSave(){
		//error_log ("CategoriesMappings:beforeSave 1 this:".print_r($this->toArray(), true));
		// if (!$this->level && $this->parent_id) {
			// error_log ("CategoriesMappings:beforeSave 2");
			// $this->level = $this->ParentCategory()->level + 1;
		// }
		
		$this->level = $this->getLevel();
		$this->n_path = $this->path();
		
		//return parent::beforeSave();
		 
		
	}
	// public function beforeCreate(){
		// error_log ("CategoriesMappings:beforeSave 1 this:".print_r($this->toArray(), true));
		// if (!$this->level && $this->parent_id) {
			// error_log ("CategoriesMappings:beforeSave 2");
			// $this->level = $this->ParentCategory()->level + 1;
		// }
		// $this->n_path = $this->name;
		
		
		
		
	// }

	public function getLevel(){
		$lvl = 1;
		$parent1 = $this->ParentCategory();
		if ($parent1){
			$lvl += 1;
			$parent2 = $parent1->ParentCategory();
			if ($parent2){
				$lvl += 1;
				$parent3 = $parent2->ParentCategory();
				if ($parent3){
					$lvl += 1;
					$parent4 = $parent3->ParentCategory();
					if ($parent4){
						$lvl += 1;
						//$parent4 = $parent3>ParentCategory();
					}

				}

			}
			
		}
	return $lvl;	
		
	}
	
	public function getPath(){
		
		$separator = " > ";
		//TODO: recursion, this is temporary
		if ( $this->level == 2 || $this->level == 1 || !$this->level ) return $this->name;
		elseif ($this->level == 3){
			$parent1 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($this->parent_id);
			return $parent1->name.$separator.$this->name;
		}
		elseif ($this->level == 4){
			$parent1 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($this->parent_id);
			$parent2 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent1->parent_id);
			return $parent2->name.$separator.$parent1->name.$separator.$this->name;
		}
		elseif ($this->level == 5){
			$parent1 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($this->parent_id);
			$parent2 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent1->parent_id);
			$parent3 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent2->parent_id);
			return $parent3->name.$separator.$parent2->name.$separator.$parent1->name.$separator.$this->name;
		}
		elseif ($this->level == 6){
			$parent1 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($this->parent_id);
			$parent2 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent1->parent_id);
			$parent3 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent2->parent_id);
			$parent4 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent3->parent_id);
			return $parent4->name.$separator.$parent3->name.$separator.$parent2->name.$separator.$parent1->name.$separator.$this->name;
		}
		else return ("ERROR: Function CategoriesMappings::path needs to be upgraded for levels over 6");
	}

	
	public function path(){
		
		$separator = " > ";
		//TODO: recursion, this is temporary
		if ( $this->level == 2 || $this->level == 1 || !$this->level ) return $this->name;
		elseif ($this->level == 3){
			$parent1 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($this->parent_id);
			return $parent1->name.$separator.$this->name;
		}
		elseif ($this->level == 4){
			$parent1 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($this->parent_id);
			$parent2 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent1->parent_id);
			return $parent2->name.$separator.$parent1->name.$separator.$this->name;
		}
		elseif ($this->level == 5){
			$parent1 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($this->parent_id);
			$parent2 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent1->parent_id);
			$parent3 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent2->parent_id);
			return $parent3->name.$separator.$parent2->name.$separator.$parent1->name.$separator.$this->name;
		}
		elseif ($this->level == 6){
			$parent1 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($this->parent_id);
			$parent2 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent1->parent_id);
			$parent3 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent2->parent_id);
			$parent4 = \Baseapp\Suva\Models\CategoriesMappings::findFirst($parent3->parent_id);
			return $parent4->name.$separator.$parent3->name.$separator.$parent2->name.$separator.$parent1->name.$separator.$this->name;
		}
		else return ("ERROR: Function CategoriesMappings::path needs to be upgraded for levels over 6");
	}


//veze one2many
	public function OrdersItems( $p = null ){ return $this->getMany('OrdersItems', $p, " n_categories_mappings_id = $this->id " ); }
	public function PublicationsCategoriesMappings( $p = null ) { return $this->getMany('PublicationsCategoriesMappings', $p, " category_mapping_id = $this->id " ); }
	
	
	
	//vezw many2one
	public function Layout(){
		$id = $this->layout_id > 0 ? $this->layout_id : -1;
		return \Baseapp\Suva\Models\Layouts::findFirst($id);
	}
	public function DtpGroup(){
		$id = $this->dtp_group_id > 0 ? $this->dtp_group_id : -1;
		return \Baseapp\Suva\Models\DtpGroups::findFirst($id);
	}	

	public function ParentCategory(){
		$id = $this->parent_id > 0 ? $this->parent_id : -1;
		return \Baseapp\Suva\Models\CategoriesMappings::findFirst($id);
	}
	

	public function Publication(){
		$pubId = $this->n_publications_id > 0 ? $this->n_publications_id : -1;
		return \Baseapp\Suva\Models\Publications::findFirst($pubId);
	}
	

	
}
