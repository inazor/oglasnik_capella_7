<?php

namespace Baseapp\Suva\Models;

/**
 * Publications Model
 */
class Discounts extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("n_discounts");
    }

        public function getSource()
    {
        return 'n_discounts';
    }


    /**
     * Add a new Discount
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request)
    {
        $this->initialize_model_with_post($request);

            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
    }

    /**
     * Save Publication data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request)
    {

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }

    
    public function n_fields($pFieldName = null){
        
        $data = array(
            'id' =>         array( 'type'=> 'int',      'description' => 'ID'),
            'name' =>       array( 'type'=> 'text',     'description' => 'Name',    'size' => 45),
            'type' =>       array( 'type'=> 'text',     'description' => 'Type',    'size' => 45),
            'amount' =>     array( 'type'=> 'int',      'description' => 'Amount'),
            'tag' =>        array( 'type'=> 'text',     'description' => 'Tag',     'size' => 50),
            'category_id'=> array( 'type'=> 'one2Many', 'description' => 'Category','one2many' => array( 'model' => 'Categories'
                                                                                                        ,'where'=>'is_active = 1'
                                                                                                        ,'return'=>'n_path'    
                                                                                                       )),
            'is_active' =>  array( 'type'=> 'yesNo',      'description' => 'Is Active')        
        );
        
        if ($pFieldName) return $data[$pFieldName];
        else return $data;
    }
	
	
	//veze many2One

	// public function Category(){
		// return \Baseapp\Suva\Models\Categories::findFirst($this->category_id);
	// }
	public function Category(){
		$id = $this->category_id > 0 ? $this->category_id : - 1;
		return $this->many2one("Categories", "id = $id ");
    }

	//veze one2many
	public function OrdersItems($p){
		return $this->one2many("OrdersItems", "n_discounts_id", $p);
	}
}
