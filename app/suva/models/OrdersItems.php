<?php

namespace Baseapp\Suva\Models;

use Baseapp\Library\Products\ProductsInterface;
use Baseapp\Library\Utils;

use Baseapp\Suva\Library\Nava;

use Baseapp\Suva\Models\Ads;
use Baseapp\Suva\Models\Products;
use Baseapp\Suva\Models\Orders;
use Baseapp\Suva\Models\Publications;
use Baseapp\Suva\Models\Categories;
use Baseapp\Suva\Models\Discounts;
use Baseapp\Suva\Models\Issues;
use Baseapp\Suva\Models\Users;


class OrdersItems extends \Baseapp\Models\OrdersItems
{
	 use traitBaseSuvaModel;

	public function create($data = NULL, $whiteList = NULL){
		//error_log(print_r($this->toArray(),true));
		
		parent::create($data, $whiteList);
	}
	
  	public function n_before_save( $request = null, $pLoggedUserId = null, $p = null ){
		// Nava::sysMessage("$pLoggedUserId");
		/*
		prije ovoga se OrderItem puni sa sadrzajem requesta i dodaju se foreign key veze
		mora postojati:
			- id
		*/
		//error_log('OrdersItems::n_before_save 1');
		$ad = ($this->ad_id > 0 ) ? Ads::findFirst($this->ad_id) : null;
		$publication = ($this->n_publications_id > 0 ) ? Publications::findFirst($this->n_publications_id) : null;
		$product = ($this->n_products_id > 0) ? Products::findFirst($this->n_products_id) : null;
		$order = ($this->order_id > 0) ? Orders::findFirst($this->order_id) : null;

		//13.1.2017 
		if ( ! $this->n_created_at ) {
			Nava::poruka("Mijenja se created at, prije je bilo $this->n_created_at");
			$this->n_created_at = date('Y-m-d H:i:s');
		}
		
		
		
		//varijable iz requesta
		// ako se ne pošalje request mora se poslati p
		if ($request){
			$r_x_user_id = $request->hasPost('x_user_id') ? (int)trim($request->getPost('x_user_id')) : null;
			$r_offline_issues = $request->hasPost('offline-issues') ? $request->getPost('offline-issues') : null;
			$r_offline_discounts = $request->hasPost('offline-discounts') ? $request->getPost('offline-discounts') : null;
		} 
		elseif ($p) {
			$r_x_user_id = array_key_exists('user_id', $p) ? $p['user_id'] : null;
			$r_offline_issues = array_key_exists('offline_issues', $p) ? $p['offline_issues'] : null;
			$r_offline_discounts = array_key_exists('offline_discounts', $p) ? $p['offline_discounts'] : null;
		} 
		elseif ($request == null && $pLoggedUserId == null && $p == null && $order && $this->id > 0) {
			//error_log("OrdersItems:n_before_save 5: pozivanje bez parametara, sa postojećim OrderItemom ID=$this->id");
			$r_x_user_id = $order->user_id;
			//error_log("OrdersItems:n_before_save 5: r_x_user_id:$r_x_user_id");
			
			$r_offline_issues = array();
			foreach ($this->Insertions() as $insertion){
				//error_log("OrdersItems:n_before_save 5.1  insertion issue id=  $insertion->issues_id");
				
				array_push($r_offline_issues, $insertion->issues_id);
			}
			if (count($r_offline_issues) == 0) $r_offline_issues = null; 
			//error_log("OrdersItems:n_before_save 5.2: r_offline_issues:".print_r($r_offline_issues,true));
			
			$r_offline_discounts = Array();
			foreach ($this->Discounts() as $discount)
				array_push($r_offline_discounts, $discount->discounts_id);
			if (count($r_offline_discounts) == 0) $r_offline_discounts = null; 
			//error_log("OrdersItems:n_before_save 5.3: r_offline_discounts:".print_r($r_offline_discounts,true));
			
		}
		else {
			$this->sysMessage("Nisu poslani parametri u funkciju n_before_save.");
			return true;
		}

		//20.1.2017 zašto je ovo stavljeno, ne postoji polje u bazi?
		// if($this->n_created_by_user_id == null){
			
				// $this->n_created_by_user_id = $pLoggedUserId;
		// }
		
		//ako postoji ad kategorija se mora uzeti iz ad-a
		if ($ad) $this->n_category_id = $ad->category_id;
		$category = ($this->n_category_id > 0) ? Categories::findFirst($this->n_category_id) : null;
		
		if (!$product || !$publication) return ("Za spremanje je potrebno odabrati publikaciju i proizvod");
		
		// traženje ordera
		if (!$order){
			if ($ad) $user = Users::findFirst($ad->user_id);
			elseif ( !$ad && $r_x_user_id ) $user = Users::findFirst( $r_x_user_id );
			else return "Nije moguće pronaći korisnika, niti dodati proizvod na Cart";

			if ($user) $order = Orders::findFirst("n_status = 1 and user_id = $user->id");  // Preko usera naći je li postoji Cart
			if (!$order) { //... ako već ne postoji userov Cart kreirati ga
				$arrOrder = Nava::createNewCart($user->id, $pLoggedUserId );
				$order = Orders::findFirst($arrOrder['id']);
			}
		}
		if (!$order) return "Neuspješno kreiranje novog Carta";
		
		$this->order_id = $order->id;
		
		
		//50 - proizvod pushup se može dodijeliti samo adovima koji imaju moderation status OK
		if ($ad && $product){
			if ($product->is_pushup_product == 1 && $ad->moderation != 'ok'){
				return("Odabran je Pushup proizvod za Ad koji nije moderiran. Promjena nije snimljena.");
			}
		}		

		//qty
		//$this->printr($product->toArray(), "PRODUCT");
		if ($product && $product->is_published && $product->is_online_product ) $this->qty = 1;
		elseif ($product && $product->is_published && ! $product->is_online_product && $r_offline_issues ) $this->qty =  count( $r_offline_issues );
		elseif ($product && ! $product->is_published) { 
			//ništa, quantity je iz requesta
		}

		
		//246 - - ad new item - max 2 je 2 insertiona a spremila sam 5
		// na offline izdanju: ako je na odabranom productu upisan podatak days_published, mora biti toliko objava
		if ($publication ->is_online == 0 && $product->days_published > 0)
		if ($r_offline_issues) {
			if (count($r_offline_issues) != $product->days_published)
			return ("Proizvod ".$publication->slug.": $product->name (oglas br. $this->ad_id ) mora imati $product->days_published objava, a odabrano ih je ".count($r_offline_issues));
		} else return ("Proizvod ".$publication->slug.": $product->name (oglas br. $this->ad_id )mora imati $product->days_published objava, a nije odabrana nijedna");
			
		

// 		//ako na offlineu je označen broj objava, treba provjerit jw li toliko objava odabrano
// 		if ($publication && $product)
// 			if ($publication->is_online == 0 && $product->days_published > 0 && $product->days_published != $this->qty)
// 				return ("Ova vrsta proizvoda mora imati točno  $product->days_published objava.");


		//63 - Kad je na order itemu odabrana offline publikacija polja "first fublished at" i "expires at" se popunjavaju automatski nakon snimanja order linea
		//kad je online publikacija i definirani su publications_id i products_id onda se namješta first published i expires at

		//n_first_published_at
		//n_expires_at
		
		if ($publication){
			if ( $publication->is_online == 0 && $r_offline_issues ){
				//ako je offline i ako postoji lista kliknutih issues-a
				$min = strtotime('2100-01-01');  //za offline izdanje traži se najmanji i najveći datum objave
				
				$max = 0;
				foreach( $r_offline_issues as $issue_id){
					$issue = Issues::findFirst($issue_id);
					if ($issue) {
						$date = strtotime($issue->date_published);
						if ($min > $date) $min = $date;
						if ($max < $date) $max = $date;
					}
				}
				//$this->n_first_published_at = $min;
				$this->n_first_published_at = date("Y-m-d",$min);
				//$this->n_expires_at = $max;
				$this->n_expires_at = date("Y-m-d",$max);
			} 
			elseif ( $publication->is_online == 1 && $product && $this->n_first_published_at != null) {
				//ako je online, odabran je proizvod i postavljen je datum objave
				//$this->n_expires_at = $this->n_first_published_at + $product->days_published * 24 * 60 * 60;
				
				//21.12.2016 - expires at treba imati i minute
				$this->n_expires_at = date('Y-m-d H:i:s',strtotime( $this->n_first_published_at) + $product->days_published * 24 * 60 * 60);
			} 	
		}

		if ($product){
			//n_price
			//$this->sysMessage("poruka");
			$productPrice = $product->unit_price;

			// ako je odabrana kategorija treba provjerit ima li cijena koje ovise o kategoriji proizvoda, ako ima uzet tu cijenu
				
			//novo računanje cijene preko catMap i pcp
			if (!$ad) {
				Nava::sysMessage("Na Order Itemu $this->id nije specificiran ad");
				return;
			}
			if ( !$publication) {
				Nava::sysMessage("Na Order Itemu $this->id nije specificiran publication");
				return "ERROR";
			}
			

			//6.6.2017 - nakon što je javljalo grešku na display ad-u
			if ($ad->n_is_display_ad == 1 && $ad->n_category_mapping_id > 0 ){
				$categoryMappingId = $ad->n_category_mapping_id;
			}
			elseif ($ad->n_is_display_ad == 0) {
				$categoryMappingId = $ad->getCategoryMappingForPublication ( $publication->id);
				if ( !$categoryMappingId) {
					$this->greska ("Na Order Itemu $this->id nije pronađen CategoryMapping");
					return "ERROR";
				}		
			}
			else {
				$categoryMappingId = null;
				Nava::greska("OrdersItems.php - CategoryMapping not found (presuming for display ad)");
			} 
	
			$categoryMapping = \Baseapp\Suva\Models\CategoriesMappings::findFirst( $categoryMappingId );
			if ( !$categoryMapping) {
				$this->greska ("Nije pronađen CategoryMapping $categoryMappingId");
				return "ERROR";
			}
			$pcp = \Baseapp\Suva\Models\ProductsCategoriesPrices::findFirst( 
				array(
					" categories_mappings_id = $categoryMapping->id and products_id = $product->id ",
					"order" => "id desc" 
				));
			if ($pcp){
				//error_log("OrdersItems::n_after_save: PCP $pcp->unit_price");
				//$this->printr($pcp->toArray(), "OrdersItems::nAfterSave PCP");
				if ($pcp->unit_price != null) {
					$productPrice = $pcp->unit_price;
				}
			}
			else{
				$this->sysMessage ("Na Order Itemu $this->id za Category $categoryMapping->id i proizvod $product->id nije pronađen objekt Publications Categories Prices");
			};
			
			if ( $product->is_published ){
				$this->n_price = $productPrice;
			}
			elseif  ( ! $product->is_published &&  ! $this->n_price ){
				$this->n_price = $productPrice;
				
			}
			elseif ( ! $product->is_published ){
				//ništa, uzima se iz requesta
			}
			
			//price
			$this->price = Utils::kn2lp( $this->n_price * 1.25 ) ;
			
			
			//Online proizvod mora imati old_product_id radi kompatibilnosti, a offline ne treba. Polje je obavezno.
			if ($product->is_published){
				if (!$product->old_product_id) {
					if ($product->Publication()->is_online)
						return "ERROR: Online product $product->id $product->name does not have old_product_id specified.";
					else
						$product->old_product_id = "offline";
				}
			}
			else{
				$product->old_product_id = "not_published";
			}
			
			//treba naći Agimovu klasu koja opisuje proizvod i stavit joj sve opcije
			$classProduct = null;
			if ($product->is_online_product == 1){
				switch ( $product->old_product_id ){
					case 'osnovni':
						$classProduct = new \Baseapp\Library\Products\Online\Osnovni();
						break;
					case 'istaknuti-online':
						$classProduct = new \Baseapp\Library\Products\Online\IstaknutiOnline();
						break;
					case 'istaknuti-online-bez':
						$classProduct = new \Baseapp\Library\Products\Online\IstaknutiNoExtras();
						break;
					case 'pinky':
						$classProduct = new \Baseapp\Library\Products\Online\Pinky();
						break;
					case 'platinum':
						$classProduct = new \Baseapp\Library\Products\Online\Platinum();
						break;
					case 'premium':
						$classProduct = new \Baseapp\Library\Products\Online\Premium();
						break;
					case 'premium-bez':
						$classProduct = new \Baseapp\Library\Products\Online\PremiumNoExtras();
						break;
					case 'pushup':
						$classProduct = new \Baseapp\Library\Products\Online\Pushup();
						break;
					default:
						Nava::sysMessage("Proizvod $product->id $product->name nema polje old_product_id ($product->old_product_id)");
				}
			}
			$this->product = $classProduct ? serialize($classProduct) : 'n/a';
			//$this->product = serialize($this); //ovo je radi glupog načina kako je radilo prije, trebat će to eliminirat
			//$this->product = "n/a";
			$this->product_id = $product->old_product_id;
			$this->title = $product->name . " - " . $this->qty . " pcs" ;
			$this->n_frontend_sync_status = 'no_sync';
			//total
			//Računanje ukupnog popusta - množenje svih popusta i  upisivanje u bazu
			$total_discount = 1;
			if ( $r_offline_discounts ){
				foreach ( $r_offline_discounts as $discount) {
					$objDisc = Discounts::findFirst($discount);
					$currDisc = ( 100 - $objDisc->amount ) / 100;
					$total_discount = $total_discount * $currDisc;
				}
			}
			if (!$order) return ("Nije pronađen order na order itemu br. $this->id ");
			$user = \Baseapp\Suva\Models\Users::findFirst($order->user_id);
			
			$totalFull = $this->n_price * $this->qty;
			
			$this->n_discount_amount = $totalFull * ( 1 - $total_discount );
			
			$this->n_discount_percent = ( 1 - $total_discount ) * 100;
			
			$this->n_total = ( $totalFull  - $this->n_discount_amount );
			
			$this->total = Utils::kn2lp($this->n_total);
			
			$this->tax_rate = $user->n_pays_vat == 1 ? 0.25 : 0.00;
			
			$this->tax_amount = ( $totalFull  - $this->n_discount_amount ) * $this->tax_rate;
			
			$this->n_total_with_tax = $this->n_total + $this->tax_amount;
			
			 // if(!$this->n_created_at > 0){
				 // $this->n_created_at = date('Y-m-d H:i:s');
			 // } 
				 
			//13.1.2017 ovo je maknuto u liniju 42
			//$this->n_created_at = date('Y-m-d H:i:s');
			
			
			
			
		}
		
		//n_is_published
		if ($product && ! $product->is_published ){
			$this->n_is_published = 0;
		}
		else{
			$this->n_is_published = 1;
		}
		
		//$this->printr($this->toArray(), "OrdersItems.php::n_before_save:Order item nakon n_before_save");
		
		
		//07.04.2017 - ažuriranje CategoryMapping
		if ( $ad && $publication){
			Nava::poruka($ad->id);
			if($ad->n_is_display_ad == 1 and $ad->n_category_mapping_id > 0 ){
				$this->n_categories_mappings_id = $ad->n_category_mapping_id;
			}else{
				$catMapId = $ad->getCategoryMappingForPublication( $publication->id );
				if (!$catMapId) {
					Nava::sysMessage("Za order item $this->id nije pronađen Category Mapping");
				}
				else {
					$this->n_categories_mappings_id = $catMapId;
				}
			} 
			
		}
		
		return null;
  }
  	
	public function n_after_save( $request = null, $p = null ){
		
		//varijable iz requesta
		// ako se ne pošalje request mora se poslati p

		// //error_log(print_r($p,true));
		if ($request){
			$r_offline_issues = $request->hasPost('offline-issues') ? $request->getPost('offline-issues') : null;
			$r_offline_discounts = $request->hasPost('offline-discounts') ? $request->getPost('offline-discounts') : null;
		} elseif ($p) {
			$r_offline_issues =  array_key_exists('offline_issues', $p ) ? $p['offline_issues'] : null;
			$r_offline_discounts = array_key_exists('offline_discounts', $p ) ? $p['offline_discounts'] : null;
		} else {
			$r_offline_issues = null;
			$r_offline_discounts = null;
			//return ("Nisu poslani paramtetri u funkciju n_before_save."); - ovo ne treba, može se updateat ad i bez requesta
		}
		
		//error_log($this->ad_id);
		//tablice koje su povezane sa Order Itemom
		$ad = ($this->ad_id > 0 ) ? Ads::findFirst($this->ad_id) : null;
		$publication = ($this->n_publications_id > 0 ) ? Publications::findFirst($this->n_publications_id) : null;
		$product = ($this->n_products_id > 0) ? Products::findFirst($this->n_products_id) : null;
		$order = ($this->order_id > 0) ? Orders::findFirst($this->order_id) : null;
		$category = ($this->n_category_id > 0) ? Categories::findFirst($this->n_category_id) : null;

		//DISCOUNTS - UBACIVANJE U TABLICU n_orders_items_discounts  
		//error_log("ORDERS ITEM ID = $this->id");
		if ($this->id >0 )
			Nava::runSql("delete from n_orders_items_discounts where orders_items_id = ".$this->id);
		if ($product && $r_offline_discounts ) {
			foreach ($r_offline_discounts as $discount) {	
				$txtSql = "insert into n_orders_items_discounts "
				." (id, orders_items_id, discounts_id) VALUES "
				." (null, '" . $this->id . "', '" . $discount . "')";
				Nava::runSql($txtSql);
			}
		}
		
		//ISSUES - UBACIVANJE U TABLICU n_insertions 
		if ($this->id >0 && $r_offline_issues)
			Nava::runSql("delete from n_insertions where orders_items_id = ". $this->id);
		
		if ($publication && is_array($r_offline_issues)){
			//error_log(1);
			$offline_issues = $r_offline_issues;
			//error_log(2);
			foreach ($offline_issues as $issue) {
				//error_log(3);
				$txtSql = "insert into n_insertions "
				." ( orders_items_id, issues_id) VALUES "
				." (  $this->id , $issue  )";
				//error_log($this->id);
				//error_log($txtSql);
				// $this->poruka ($txtSql);
				Nava::runSql($txtSql);
			}
		}

		// //170 - kad se dodaje platinum proizvod na neki ad pomoću prder itema, treba u online-product-id staviti "platinum
		// //ako postoji publikacija i ako je is_online = 1
		// //ako postoji proizvod nac mu old_product_id i stavit mu to ime u online_product_id iz tablice ads
		// if($publication && $product && $ad && $publication->is_online == 1 ) {
			
			// if( strlen($product->old_product_id) > 0 ){
				// Nava::runSql("update ads set online_product_id = '$product->old_product_id' where id = $ad->id");
			// }
			// else {
				// $this->sysMessage("WARNING: Product $product->name (ID:$product->id) does not have the Old Product ID specified. ");
			// }
			// //26.10.2016 zato jer proizvodi koji nemaju online_product nisu aktivni
			// if( strlen($product->old_product_id) > 0  && strlen( $ad->online_product ) == 0 ){
				// Nava::runSql("update ads set online_product = '$product->old_product_id' where id = $ad->id");
			// }
			
		// }
		
		// //57 - Adu se dodjeljuje proizvod pushup (17.5.2015) ad je u statusu moderacije OK
		// if ($ad && $product)
			// if ($product->is_pushup_product == 1)
				// Nava::runSql("update ads set modified_at = UNIX_TIMESTAMP(), sort_date = UNIX_TIMESTAMP() where id = $ad->id");

		
		// //59 - adu koji na onlineu ističe 5.6.2015 se dodjeljuje PLAĆENO OFFLINE izdanje u periodu 1.6.2015 - 20.6.2015. čovjek je zvao 29.5.2015
		// //		ako je na adu manji expires_at raniji od expires_at plaćenog oglasa, produžuje se datum expires_at i mijenja se sort_date
		// if ($publication && $ad){
			// if ($publication->is_online == 0 && $this->n_total > 0){
				// Nava::runSql("update ads set "
							 // ." modified_at = UNIX_TIMESTAMP(), "
							 // ." sort_date = ".strtotime($this->n_first_published_at)
							 // ." where id = $ad->id ");
				// if ($ad->expires_at <= strtotime($this->n_expires_at)){
					// Nava::runSql("update ads set expires_at = ".strtotime($this->n_expires_at)." where id = $ad->id ");
				// }
				
			// }
		// }


		// //60 - adu koji na onlineu ističe 5.6.2015 se dodjeljuje BESPLATNO OFFLINE izdanje u periodu 1.6.2015 - 20.6.2015
		// //ako ad expire-a prije plaćenog offline oglasa
		// if ($publication && $ad)
			// if ($publication->is_online == 0 && $this->n_total = 0 && $ad->expires_at < $this->n_expires_at)
				// Nava::runSql("update ads set modified_at = UNIX_TIMESTAMP(), expires_at = ".strtotime($this->n_expires_at)." where id = $ad->id");


		// //61 - ad je istekao 5.6.2015 traženo je offline izdanje od 10.6.2015 do 15.6.2015. čovjek je zvao 7.6.2015
		// //ako ad expire-a prije plaćenog offline oglasa
		// if ($publication && $ad)
			// if ($publication->is_online == 0 && $this->n_total = 0 && $ad->expires_at < strtotime())
				// Nava::runSql("update ads set "
					// ." 	modified_at = UNIX_TIMESTAMP() "
					// ."	,published_at = ".strtotime($this->n_first_published_at)
					// ."	,expires_at = ".strtotime($this->n_expires_at)
					// ."	,sort_date = ".strtotime($this->n_first_published_at)
					// ."	,active = 1 "	
					// ." where id = $ad->id ");
		
		
		// //306 - oglas je aktivan, a još je u cartu. oglas ne može biti aktivan ako: a) nije plaćen, b) nije free, osim toga, start date je tek
		// // $ad->active   - dodjela statusa aktivan ako su zadovoljeni uvjeti:
		// if ( $ad && $product && $publication && $order )
			// //ako je online
			// if ( $publication->is_online == 1 ){
				// //error_log("OrdersItems:n_after_save: 9.1 je online");
				// //ako mu je datum objave prije trenutnog vremena
				// if ( $this->n_first_published_at <= date('Y-m-d H:i:s') ){
					// //error_log("OrdersItems:n_after_save: 9.2 datum prije");
					// //ako je besplatan
					// if ( $this->n_total == 0 ){
						// //error_log("OrdersItems:n_after_save: 9.3 je besplatan");
						// //ako je u free ads-u
						// if ( $order->n_status == 2 ){
							// //error_log("OrdersItems:n_after_save: 9.3.1 u free ads");
							// //objavljuje se online
							// $ad->active = 1;
						// }
					// } 
					// //ako nije besplatan
					// else {
						// //error_log("OrdersItems:n_after_save: 9.4 nije besplatan");
						// //ako je plaćen
						// if ( $order->n_status == 5 ){
							// //error_log("OrdersItems:n_after_save: 9.5 je plaćen");
							// //objavljuje se online
							// $ad->active = 1;
						// }
						// //ako nije plaćen
						// else {
							// //error_log("OrdersItems:n_after_save: 9.6 nije plaćen");
							// //ne objavljuje se online
							// // - ovdje ništa, da ne interferira s drugim order itemima zbog kojih bi mogao biti aktivan
						// }	
					// }
				// }
				// else {
					// //error_log("OrdersItems:n_after_save: 9.7 nije dobar datum objave $this->n_first_published_at a danas je ".date('Y-m-d H:i:s'));
				// }
				// //error_log("OrdersItems:n_after_save: 10  ad->active= $ad->active");
				// Nava::runSql("update ads set active = $ad->active where id = $ad->id");
			// }


		//ažuriranje totala na orderu
		Nava::updateOrderTotal($this->order_id);
		//NIKOLA: 
		//UPDATEANJE ASD SEARCH WORDS NAKON SNIMANJA ORDER ITEMA
		if ($ad && $publication){
			\Baseapp\Suva\Library\libAdsSearchWords::updateAdsSearchWordsStatus( $ad->id, $publication->id );
		}

		
		if ($ad){
			\Baseapp\Suva\Library\libCron::setAdsInfoForFrontend($ad->id);
			
			
		}
		return null;
	}
	
	public function initialize(){
		
		//šta je ovo??
		//$var1= new \Baseapp\Suva\Models\Orders();
    	//$order = \Baseapp\Suva\Models\Orders::findFirst($var1+3);
		
		parent::initialize();

        $this->belongsTo(
            'order_id', __NAMESPACE__ . '\Orders', 'id',
            array(
                'alias'    => 'Order',
                'reusable' => true,
            )
        );

        $this->belongsTo(
            'ad_id', __NAMESPACE__ . '\Ads', 'id',
            array(
                'alias'    => 'Ad',
                'reusable' => true,
            )
        );
    }

    /**
     * @return ProductsInterface|null
     */
    public function getProduct()
    {
        $product = null;

        if (null !== $this->product) {
            $product = unserialize($this->product);
        }

        return $product;
    }

    public function getTotal()
    {
        //return $this->total; //nema više lp2kn
		return Utils::lp2kn($this->total);
    }

    public function getTotalRaw()
    {
        return $this->total;
    }

    public function setTotal($total = 0.00)
    {
        //$this->total = $total; //nema više lp2kn
		$this->total = Utils::kn2lp($total);
    }

    public function setTotalRaw($total)
    {
        $this->total = $total;
    }

    public function getPrice()
    {
        //return $this->price;
		return Utils::lp2kn($this->price);
    }

    public function getPriceRaw()
    {
        return $this->price;
    }

    public function setPrice($price = 0.00)
    {
        //$this->price = $price;
		$this->price = Utils::kn2lp($price);
    }

    public function setPriceRaw($price)
    {
        $this->price = $price;
    }


    public function getSource(){
        return 'orders_items';
    }


    public function onValidationFails() {
        return $this->getMessages();
    }
  
      /**
     * Add a new Publication
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function add_new($request) {
        $this->initialize_model_with_post($request);

            if (true === $this->create()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
    }

    /**
     * Save Publication data method
     *
     * @param \Phalcon\Http\RequestInterface $request
     *
     * @return $this|\Phalcon\Mvc\Model\MessageInterface[]|\Phalcon\Validation\Message\Group|void
     */
    public function save_changes($request){

        $this->initialize_model_with_post($request);

        if (count($messages)) {
            return $validation->getMessages();
        } else {
            if (true === $this->update()) {
                return $this;
            } else {
                \Baseapp\Bootstrap::log($this->getMessages());
                return $this->getMessages();
            }
        }
    }
	
	public function makeCopy(){
		if (!$this->id > 0) return null;
		if (!$this->Order()) return null;
		
        //NOVI ORDER ITEM
		// $this->sysMessage ("copying Order Item $oi_id to Order $order_id OrdersItems 614");
        \Baseapp\Suva\Library\Nava::runSql( " insert into orders_items ( "
        ."         order_id "
        ."         ,ad_id "
        ."         ,product_id "
        ."         ,product "
        ."         ,title "
        ."         ,price "
        ."         ,qty "
        ."         ,total "
        ."         ,tax_rate "
        ."         ,tax_amount "
        ."         ,n_discount_percent "
        ."         ,n_discount_amount "
        ."         ,n_total_with_tax "
        ."         ,n_products_id "
        ."         ,n_discounts_id "
        ."         ,n_publications_id "
        ."         ,n_first_published_at "
        ."         ,n_expires_at "
        ."         ,n_remark "
        ."         ,n_eps_filename "
		."         ,n_source "
        ."         )  "
        ." select "
        ." 	       order_id "
        ."         ,ad_id "
        ."         ,product_id "
        ."         ,product "
        ."         ,title "
        ."         ,price "
        ."         ,qty "
        ."         ,total "
        ."         ,tax_rate "
        ."         ,tax_amount "
        ."         ,n_discount_percent "
        ."         ,n_discount_amount "
        ."         ,n_total_with_tax "
        ."         ,n_products_id "
        ."         ,n_discounts_id "
        ."         ,n_publications_id "
        ."         ,n_first_published_at "
        ."         ,n_expires_at "
        ."         ,n_remark "
        ."         ,n_eps_filename "
		."         ,n_source "
        ." from orders_items oi  "
        ."         where id = $this->id " );      
        
        $newOrdersItemsId = \Baseapp\Suva\Library\Nava::sqlToArray("select max(id) as id from orders_items")[0]['id'];

        //4
        \Baseapp\Suva\Library\Nava::runSql(
        " insert into n_insertions ( "
        ."      product_id "
        ." 	    ,orders_items_id "
        ." 	    ,issues_id "
        ." 	    ,n_insertionscol "
        ." 	    ,is_active "
        ." ) select  "
        ." 	    product_id "
        ." 	    ,$newOrdersItemsId "
        ." 	    ,issues_id "
        ." 	    ,n_insertionscol "
        ." 	    ,is_active "
        ." from n_insertions  "
        ." where orders_items_id = $this->id "
         );
        
        //5
        \Baseapp\Suva\Library\Nava::runSql(
            " insert into n_orders_items_discounts ( "
            ."	    orders_items_id "
            ."	    ,discounts_id "
            .")  "
            ." select "
            ."	    $newOrdersItemsId "
            ."	    ,discounts_id "
            ." from n_orders_items_discounts "
            ." where orders_items_id = $this->id ");	
		
		$result = \Baseapp\Suva\Models\OrdersItems::findFirst($newOrdersItemsId);
		return $result;
	}


//DODANO:
    // public function beforeSave(){
		// error_log ("OrdersItems.php::beforeSave 1, total = $this->total, n_total = $this->n_total");
		// if ($this->id){
			// $xRec = \Baseapp\Suva\Models\OrdersItems::findFirst ( $this->id );
			
			
			// //ako se mijenja  total
			// if ($xRec->total !== $this->total && $xRec->n_total === $this->total)
				// $this->total = intval ( $this->total ) / 100;
		// }
		
		// return $this;
	// }
	
	// public function beforeCreate(){
		// error_log ("OrdersItems.php::beforeCreate 1, total = $this->total, n_total = $this->n_total");
		
		// $decTotal = $this->total ? intval ( $this->total ) : 0.0;
		// $decNtotal = $this->n_total ? $this->n_total : 0.00;
		
		// if ( $decTotal && !$decNtotal ) $this->n_total = $decTotal;
		// if ( !$decTotal && $decNtotal ) $this->total = strval ( intval( $decNtotal * 100) );
		
		// return  $this;
		
	// }


	
	//akcije
	public function copyToCart($pLoggedUserId = null){
		if (!$pLoggedUserId) return null;
		if (!$this->id > 0) return null;
		if (!$this->Order()) return null;
		if (!$this->Order()->User()) return null;
		
		$userId = $this->Order()->user_id;
		
		//find Cart, or create new
		$cart = \Baseapp\Suva\Models\Orders::findFirst("user_id = $userId and n_status = 1");
		if (!$cart) {
			$cartId = \Baseapp\Suva\Library\Nava::createNewCart( $userId, $pLoggedUserId)['id'];
			if (!$cartId > 0) return null;
			$cart = \Baseapp\Suva\Models\Orders::findFirst($cartId);
			if (!$cart) return null;
		}
		
	        //4
        
        //Nava::runSql("update orders_items set order_id = $cart_id where id = $pOrdersItemsId ");
        //NOVI ORDER ITEM
				// $this->sysMessage ("copying Order Item $oi_id to Order $order_id OrdersItems 750");
        \Baseapp\Suva\Library\Nava::runSql( " insert into orders_items ( "
        ."         order_id "
        ."         ,ad_id "
        ."         ,product_id "
        ."         ,product "
        ."         ,title "
        ."         ,price "
        ."         ,qty "
        ."         ,total "
        ."         ,tax_rate "
        ."         ,tax_amount "
        ."         ,n_discount_percent "
        ."         ,n_discount_amount "
        ."         ,n_total_with_tax "
        ."         ,n_products_id "
        ."         ,n_discounts_id "
        ."         ,n_publications_id "
        ."         ,n_first_published_at "
        ."         ,n_expires_at "
        ."         ,n_remark "
        ."         ,n_eps_filename "
		."         ,n_total "
		."         ,n_price "
		."         ,n_source "
        ."         )  "
        ." select "
        ." 	      $cart->id "
        ."         ,ad_id "
        ."         ,product_id "
        ."         ,product "
        ."         ,title "
        ."         ,price "
        ."         ,qty "
        ."         ,total "
        ."         ,tax_rate "
        ."         ,tax_amount "
        ."         ,n_discount_percent "
        ."         ,n_discount_amount "
        ."         ,n_total_with_tax "
        ."         ,n_products_id "
        ."         ,n_discounts_id "
        ."         ,n_publications_id "
        ."         ,n_first_published_at "
        ."         ,n_expires_at "
        ."         ,n_remark "
        ."         ,n_eps_filename "
		."         ,n_total "
		."         ,n_price "
		."         ,'backend' "
        ." from orders_items oi  "
        ."         where id = $this->id " );      
        
        $newOrdersItemsId = \Baseapp\Suva\Library\Nava::sqlToArray("select max(id) as id from orders_items")[0]['id'];

        //4
        \Baseapp\Suva\Library\Nava::runSql(
        " insert into n_insertions ( "
        ."      product_id "
        ." 	    ,orders_items_id "
        ." 	    ,issues_id "
        ." 	    ,n_insertionscol "
        ." 	    ,is_active "
        ." ) select  "
        ." 	    product_id "
        ." 	    ,$newOrdersItemsId "
        ." 	    ,issues_id "
        ." 	    ,n_insertionscol "
        ." 	    ,is_active "
        ." from n_insertions  "
        ." where orders_items_id = $this->id "
         );
        
        //5
        \Baseapp\Suva\Library\Nava::runSql(
            " insert into n_orders_items_discounts ( "
            ."	    orders_items_id "
            ."	    ,discounts_id "
            .")  "
            ." select "
            ."	    $newOrdersItemsId "
            ."	    ,discounts_id "
            ." from n_orders_items_discounts "
            ." where orders_items_id = $this->id ");	
		
		return $cart->id;
	}
	
	public function number_total(){
		//error_log("test number_total intiger");
		return $this->total;
	}
	
	
	//BACKWARD COMPATIBILITY ZA AGIMOVU APLIKACIJU
	public function updateAdLatestPaymentState(){
		//Latest payment state služi jer se prema njemu listaju oglasi za moderaciju na adminu
		if ( ! $this->id > 0 ) return;
		
		if ( $this->Product() && $this->Product()->is_online_product && $this->Ad() && $this->Order() ) {
			$order = $this->Order();
			Nava::runSql("update ads set latest_payment_state = $order->status where id = $this->ad_id ");
			
		}
	}
	
	
	//veze many2One
	public function Ad(){
		$id = $this->ad_id > 0 ? $this->ad_id : -1;
		return \Baseapp\Suva\Models\Ads::findFirst($id);
	}
	public function AdTaker(){
		$id = $this->n_ad_taker_id > 0 ? $this->n_ad_taker_id : -1;
		return \Baseapp\Suva\Models\Users::findFirst($id);
	}

	
	public function Product(){
		$id = $this->n_products_id > 0 ? $this->n_products_id : -1;
		return \Baseapp\Suva\Models\Products::findFirst($id);
	}
	

	public function Publication(){
		$id = $this->n_publications_id > 0 ? $this->n_publications_id : -1;
		return \Baseapp\Suva\Models\Publications::findFirst($id);
	}
	public function Category(){
		$id = $this->n_category_id > 0 ? $this->n_category_id : -1;
		return \Baseapp\Suva\Models\Categories::findFirst($id);
	}
	public function CategoryMapping(){
		$id = $this->n_categories_mappings_id > 0 ? $this->n_categories_mappings_id : -1;
		return \Baseapp\Suva\Models\CategoriesMappings::findFirst( $id );
	}
	
	public function Order(){
		$id = $this->order_id > 0 ? $this->order_id : -1;
		return \Baseapp\Suva\Models\Orders::findFirst($id);
	}
	
	//veze many2many
	public function Insertions(){
		return \Baseapp\Suva\Models\Insertions::find("orders_items_id = $this->id and is_active = 1");
	}
	
	
	public function Issues(){ //"Isfabricirana" veza preko insertions.. radi kontrole many2many, treba bolje koncipirati
		$id = $this->id > 0 ? $this->id : -1;
		return \Baseapp\Suva\Models\Issues::find("id in (select issues_id from \Baseapp\Suva\Models\Insertions where orders_items_id = $id )");
	}
	
	public function Discounts(){
		return \Baseapp\Suva\Models\OrdersItemsDiscounts::find("orders_items_id = $this->id");
	}

	public function OrdersItemsDiscounts(){
		return \Baseapp\Suva\Models\OrdersItemsDiscounts::find("orders_items_id = $this->id");
	}
	
}