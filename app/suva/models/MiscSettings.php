<?php

namespace Baseapp\Suva\Models;

// use Baseapp\Traits\CachedAllCollection;

class MiscSettings extends \Baseapp\Models\MiscSettings
{
	use traitBaseSuvaModel;
    // use CachedAllCollection;

    // const SLUG_AKTUALNO = 'aktualno';
    // const SLUG_LATEST_ADS_EXCLUSIONS_CATS = 'latest-ads-excluded-cat-ids';
    // const SLUG_LATEST_ADS_EXCLUSIONS_USERS = 'latest-ads-excluded-user-ids';

    // public function initialize()
    // {
        // parent::initialize();

        // // Cache entire table on initialize
        // $this->cachedAll();
    // }

    // /**
     // * Overriding CachedAllCollection\cachedAllBuildCacheData so that we can
     // * change the cached data so that it's keyed on both "id" and "slug"
     // *
     // * @inheritdoc
     // */
    // public function cachedAllBuildCacheData($records)
    // {
        // $data = array();

        // foreach ($records as $record) {
            // $data[$record->id] = $data[$record->slug] = $record->toArray();
        // }

        // return $data;
    // }

    // public static function getIDArray()
    // {
        // return static::all();
    // }

    // // as self::getIDArray() returns doubled array (both with id and short_name
    // // as keys), we need this to filter-out those rows where key is represented
    // // with a short_name
    // public static function getIDArrayOnly()
    // {
        // $rows = self::getIDArray();
        // $settings = array();

        // foreach ($rows as $row_key => $row_value) {
            // if (intval($row_key)) {
                // $settings[$row_key] = $row_value;
            // }
        // }

        // return $settings;
    // }

    // public static function getAktualnoRecord()
    // {
        // $record = self::findFirst('slug = "' . self::SLUG_AKTUALNO . '"');

        // return $record;
    // }

    // public static function getHomepageAktualnoContents()
    // {
        // $aktualno = self::getAktualnoRecord();
        // $aktualno_contents = '';

        // if ($aktualno) {
            // $aktualno_contents = trim($aktualno->value);
        // }

        // return nl2br($aktualno_contents);
    // }

    // public static function saveHomepageAktualno($contents)
    // {
        // $aktualno = self::getAktualnoRecord();

        // // If no record exists, create one (only used once really...)
        // if (!$aktualno) {
            // $aktualno = new MiscSettings();
            // $aktualno->slug = self::SLUG_AKTUALNO;
        // }

        // return $aktualno->save(array('value' => $contents));
    // }

    // /**
     // * @return \Phalcon\Mvc\Model
     // */
    // public static function getLatestAdsCategoryIdExclusionsRecord()
    // {
        // $record = self::findFirst('slug="' . self::SLUG_LATEST_ADS_EXCLUSIONS_CATS . '"');

        // return $record;
    // }

    // /**
     // * @return \Phalcon\Mvc\Model
     // */
    // public static function getLatestAdsUserIdExclusionsRecord()
    // {
        // $record = self::findFirst('slug="' . self::SLUG_LATEST_ADS_EXCLUSIONS_USERS . '"');

        // return $record;
    // }

    // /**
     // * @return array
     // */
    // public static function getLatestAdsCategoryIdExclusionList()
    // {
        // $list = array();

        // $record = self::getLatestAdsCategoryIdExclusionsRecord();

        // if ($record) {
            // // Saving should ensure there's no misc crap stored...
            // $value = $record->value;
            // if (!empty($value)) {
                // $list = explode(',', $value);
            // }
        // }

        // return $list;
    // }

    // /**
     // * @return array
     // */
    // public static function getLatestAdsUserIdExclusionList()
    // {
        // $list = array();

        // $record = self::getLatestAdsUserIdExclusionsRecord();

        // if ($record) {
            // // Saving should ensure there's no misc crap stored...
            // $value = $record->value;
            // if (!empty($value)) {
                // $list = explode(',', $value);
            // }
        // }

        // return $list;
    // }

    // /**
     // * @param string|array $ids
     // *
     // * @return bool
     // */
    // public static function saveLatestAdsCategoryIdExclusions($ids)
    // {
        // $record = self::getLatestAdsCategoryIdExclusionsRecord();

        // // If no record exists, create one (only used once really...)
        // if (!$record) {
            // $record = new MiscSettings();
            // $record->slug = self::SLUG_LATEST_ADS_EXCLUSIONS_CATS;
        // }

        // if (is_array($ids)) {
            // $ids = implode(',', $ids);
        // }

        // // Making some simple validation that everything in the list is a number and not repeated
        // $ids = array_unique(array_map('intval', array_filter(explode(',', $ids), 'is_numeric')));

        // // Now we save a string that's hopefully cleared of potential crap...
        // $ids = implode(',', $ids);

        // return $record->save(array('value' => $ids));
    // }

    // /**
     // * @param string|array $ids
     // *
     // * @return bool
     // */
    // public static function saveLatestAdsUserIdExclusions($ids)
    // {
        // $record = self::getLatestAdsUserIdExclusionsRecord();

        // // If no record exists, create one (only used once really...)
        // if (!$record) {
            // $record = new MiscSettings();
            // $record->slug = self::SLUG_LATEST_ADS_EXCLUSIONS_USERS;
        // }

        // if (is_array($ids)) {
            // $ids = implode(',', $ids);
        // }

        // // Making some simple validation that everything in the list is a number and not repeated
        // $ids = array_unique(array_map('intval', array_filter(explode(',', $ids), 'is_numeric')));

        // // Now we save a string that's hopefully cleared of potential crap...
        // $ids = implode(',', $ids);

        // return $record->save(array('value' => $ids));
    // }
}
