<?php

namespace Baseapp\Suva\Models;

/**
 * DisplayAds Model
 */
class DisplayAds extends BaseModelBlamable
{
    public function initialize()
    {
        $this->setSource("ads");
    }

        public function getSource()
    {
        return 'ads';
    }

	public function AdsMedia( $p = null ) { return $this->getMany('AdsMedia', $p, " ad_id = $this->id ");}
	
	public function n_before_save( $request = null ) {
		//error_log('DisplayAds::n_before_save 1');
		// $this->poruka("displayad nbegoresave");
		$jeGreska = false;
		// error_log('DisplayAds::n_before_save 2');
		
		
		if( !$this->n_publication_id > 0 ){
			$jeGreska = $this->greskaSnimanje("Publication ID je obavezno polje ", 'n_publication_id');
			// error_log('DisplayAds::n_before_save 3'.$jeGreska);
		}
		
			// error_log("publication_id :" .$this->n_publication_id);
			// error_log("category_mapping_id : ".$this->n_category_mapping_id);
		
		if( !$this->n_category_mapping_id > 0 ){
			$jeGreska = $this->greskaSnimanje("Category Mapping ID je obavezno polje ",'n_category_mapping_id');
			// error_log('DisplayAds::n_before_save 4'.$jeGreska);
		}
		
		if( !$this->title > 0 ){
			$jeGreska = $this->greskaSnimanje("Title je obavezno polje ",'title');
			// error_log('DisplayAds::n_before_save 5'.$jeGreska);
		}
		
		if ($jeGreska) return "ERRORS:"; else return null;
		
		
		
	}
	
}
