<?php

namespace Baseapp\Suva\Library;


use Baseapp\Suva\Library\Nava;

class libNavision {
	
	static $dbNav = null;
	static $navCompanyName = 'Oglasnik d_o_o_$';

	/* 
		funkcija za dohvat računa u jsonu
		
		funkcija za slanje na fiskalizaciju
	
	*/

	public static function sendUnfiscalizedOrdersToNav(){
		$orders = \Baseapp\Suva\Models\Orders::find ("n_status in (4,5) and n_nav_sync_status = 'unsynced'");
		$results_array = \Baseapp\Suva\Models\Orders::buildResultsArray($orders);
		$strJson = self::modifySearchResultsForJsonExport($results_array);
		
		$objJson = json_decode( $strJson );
		foreach( $objJson as $orderJson ){
			self::insertOrderInNav( $orderJson );
		}
	}
	

// //$oglasi = mb_convert_encoding($oglasi, 'UTF-8', 'UTF-8');
// $racuni = json_decode($get, true);
// $proizvodi = array();

//foreach ($racuni as $racun) {
public function insertOrderInNav($pOrder){
	
	Nava::printr($pOrder,'pOrder');
	
	
	$racun = $pOrder;
	$navTablePrefix = 'Oglasnik d_o_o_$'; 
	
	// if ($racun['STATUSTEXT'] !== 'Completed') {
		// Nava::greska("Račun ".$racun['ID']." nije Completed");
		// return;
	// }
	
		
	//je li račun već postoji?
	$navTableName = self::$navCompanyName."AVUS-Invoice";
	$invoiceNumber = $racun["INVOICENUMBER"];
	$txtSql = "select * from [$navTableName] where INVOICENUMBER = '$invoiceNumber'";
	$rsExisting = self::sqlToArray( $txtSql );
	if ( $rsExisting ) {
		Nava::poruka("Račun s PBO: $invoiceNumber je već upisan u NAV!");
		return;
	}

	//ako nema invoice s PBO iz računa, dodaje se novi
	$newId = self::getNewHeaderId();
	
	Nava::poruka("New Order ID = $newId");
	//return;
		
	/*PAYMENTMETHOD*/
	if ($racun['PAYMENTMETHOD'] == '1') {
		$paymentformid = 5;
	} 
	else { 
		$paymentformid = 1;
	}

	$creditcard = "";
	/*CREDITCARDID*/
	if ($paymentformid == 5) {
		$creditcard = $racun['CREDITCARDID'];
	}
	
	/*
	given ($tip) {
	when ('master') {$creditcard = 1; $paymenttypeid = 5;}
	when ('visa') {$creditcard = 2; $paymenttypeid = 5;}
	when ('amex') {$creditcard = 3; $paymenttypeid = 5;}
	when ('diners') {$creditcard = 4; $paymenttypeid = 5;}
	}*/

	$creditcardid = 0;
	switch ($creditcard) {
		case "opca_virman":
			$creditcardid = 0;
			$paymentformid = 1;
			break;
		case "mastercard":
			$creditcardid = 1;
			$paymentformid = 5;
			break;
		case "master";
			$creditcardid = 1;
			$paymentformid = 5;
			break;
		case "maestro";
			$creditcardid = 1;
			$paymentformid = 5;
			break;
		case "visa":
			$creditcardid = 2;
			$paymentformid = 5;
			break;
		case "amex":
			$creditcardid = 3;
			$paymentformid = 5;
			break;
		case "american express":
			$creditcardid = 3;
			$paymentformid = 5;
			break;
		case "diners":
			$creditcardid = 4;
			$paymentformid = 5;
			break;
		default:
			$creditcardid = 0;
			$paymentformid = 1;
	};
	
	// ?? COMPANY SE NE ŠALJE IZ OGLASNIKA??
	if ($racun['COMPANY'] == 0) {
		$customernumber = 9999999;
		$taxnumber = 9999999;
		$first_name = $racun['FIRST_NAME'];
		$last_name = $racun['LAST_NAME'];
	} else {
		$customernumber = $racun['USER_ID'];
		$taxnumber = $racun['TAXNUMBER'];
		$first_name = $racun['CUSTOMERNAME'];
		$last_name = "";
	}
	/*if(!$racun['TAXNUMBER']) {
	$customernumber = 9999999;
	$taxnumber = 9999999;
	}
	else{

	}*/

	/*HEADERID*/$headerid = $newId;
	/*CUSTOMERKINDID*/$customerkindid = 0;
	/*CUSTOMERCLASSID*/$customerclassid = 0;
	/*CREATEDBY*/$createdby = 0;
	/*CREATEDAT*/
	$originalDate = $racun['INVOICEDATE'];
	$createdat = date("Y-m-d", strtotime($originalDate));

	/*TAXCOUNTRYCODE*/$taxcountrycode = $racun['TAXCOUNTRYCODE'];
	/*CREDITLIMIT*/$creditlimit = '0.00';
	/*DAYSTOPAYTHEINVOICE*/$daystopaytheinvoice = 0;
	/*CASHDISCOUNTPERCENTAGE*/$cashdiscountpercentage = '0.00';
	/*INVOICEFORMID*/$invoiceformid = 1;
	/*CUSTOMERINVOICETYPEID*/$customerinvoicetypeid = 1;
	/*ACCOUNTANCYSTATEID*/$accountancystateid = 0;
	/*ACCOUNTINGMODIFY*/$accountingmodify = 0;
	/*BRANCHGROUPID*/$branchgroupid = 0;
	/*CURRENCYID*/$currencyid = 0;
	/*AMOUNTTOPAY*/$amounttopay = $racun['AMOUNTTOPAY'];
	/*VATID*/$vatid = 0;
	/*VATPERCENTAGE*/$vatpercentage = '25';
	/*VATAMOUNT*/$vatamount = ($amounttopay * 25) / 125;

	/*CUSTOMERNAME*/$customername = $racun['CUSTOMERNAME'];
	/*INVOICETYPEID*/$invoicetypeid = 0;
	/*DEPARTMENTGROUPID*/$departmentgroupid = 0;
	
	//IGOR:IZMJENA
	///*OFFICENUMBER*/$officenumber = 7041;
	$officenumber = $racun['OFFICENUMBER'];
	
	/*OFFERNUMBER*/$offernumber = $racun['INVOICENUMBER'];

	/*EMail*/$email = $racun['EMAIL'].";racuni@oglasnik.hr";
	/*SendInoice*/$sendinvoice = 1;
	
	//IGOR:IZMJENA
	///*FiscOperator*/$FiscOperator = 1000;
	$FiscOperator = $racun['FiscOperator'];
	
	//IGOR:IZMJENA
	/*FiscLocation*/$FiscLocation = 'ONL1';
	$FiscLocation = $racun['FiscLocation'];
	
	$entrycreationdate = date("Y-m-d");
	$entrycreationtime = date("Y-m-d");
	$entrycreationdatetime = date("Y-m-d");
	/*OBJECTID
	oglasnik.hr - 110
	oglasnik tiskano utorak - 104
	oglasnik tiskano petak - 102
	oglasnikpopusti
	 */
	echo $headerid . ' - ' . $racun['INVOICENUMBER'] . " - ";
	$i = 1;
	foreach ($racun['PRODUCTS'] as $proizvod) {
		$i++;
	}
	$invoicerowcount = $i;
	$values = "'" . $headerid . "','" . $customernumber . "','" . $customerkindid . "','" . $customerclassid . "','" . $createdby . "','" . $createdat . "','" . $taxcountrycode . "','" . $creditlimit . "','" . $daystopaytheinvoice . "','" . $cashdiscountpercentage . "','" . $invoiceformid . "','" . $customerinvoicetypeid . "','" . $paymentformid . "','" . $accountancystateid . "','" . $accountingmodify . "','" . $branchgroupid . "','" . $currencyid . "','','" . $vatid . "','" . $vatpercentage . "','" . $customername . "','" . $taxnumber . "','" . date("Y-m-d") . "'";

	/*AVUSHeader*/
	// $sql = "INSERT into [".$navTablePrefix."AVUS-Header] (HEADERID, CUSTOMERNUMBER, CUSTOMERKINDID, CUSTOMERCLASSID,CREATEDBY,CREATEDAT,TAXCOUNTRYCODE,CREDITLIMIT,DAYSTOPAYTHEINVOICE,CASHDISCOUNTPERCENTAGE,INVOICEFORMID,CUSTOMERINVOICETYPEID,PAYMENTFORMID,ACCOUNTANCYSTATEID,ACCOUNTINGMODIFY,BRANCHGROUPID,CURRENCYID,DEBITORACCOUNTNEW,VATID,VATPERCENTAGE,CUSTOMERNAME,TAXNUMBER,EntryCreationDate_Time) VALUES (" . $values . ")";
	
	
	$sql = "
INSERT INTO [Oglasnik d_o_o_".'$'."AVUS-Header] (
	HEADERID
	,CUSTOMERNUMBER
	,CUSTOMERKINDID
	,CUSTOMERCLASSID
	,CREATEDBY
	,CREATEDAT
	,TAXCOUNTRYCODE
	,CREDITLIMIT
	,DAYSTOPAYTHEINVOICE
	,CASHDISCOUNTPERCENTAGE
	,INVOICEFORMID
	,CUSTOMERINVOICETYPEID
	,PAYMENTFORMID
	,ACCOUNTANCYSTATEID
	,ACCOUNTINGMODIFY
	,BRANCHGROUPID
	,CURRENCYID
	,DEBITORACCOUNTNEW 
	,VATID
	,VATPERCENTAGE
	,CUSTOMERNAME
	,TAXNUMBER
	,[Entry Creation Date]
	,[Entry Creation time]
	,[Entry Creation Date_Time]
	,[Processing Error] 
	,[Processing Status]
	,[Ready To Process]
	,STATUS
	,[Error description]
	,[Ready for creation]
	,[Customer No_]
	,[Import ID]
	)
VALUES (
	$headerid
	,'$customernumber'
	, $customerkindid
	, $customerclassid
	, $createdby
	,'$createdat'
	,'$taxcountrycode'
	, $creditlimit
	, $daystopaytheinvoice
	, $cashdiscountpercentage
	, $invoiceformid
	, $customerinvoicetypeid
	, $paymentformid
	, $accountancystateid
	, $accountingmodify
	, $branchgroupid
	, $currencyid
	,''
	, $vatid
	, $vatpercentage
	,'$customername'
	,'$taxnumber'
	,'$entrycreationdate'
	,'$entrycreationtime'
	,'$entrycreationdatetime'
	,0
	,0
	,0
	,0
	,''
	,0
	,''
	,''
	)	
	
	
	
	
	";
	
	
	
	
	Nava::poruka($sql);
	//return;
	
	//self::runSql($sql);
	// $stmt = $database->prepare($sql);
	// $stmt->execute();


	/*AVUSInvoice*/

	$sql = "INSERT into [".$navTablePrefix.'AVUS-Invoice] (HEADERID,INVOICENUMBER,CUSTOMERNUMBER,INVOICETYPEID,INVOICEFORMID,DEPARTMENTGROUPID,CURRENCYID,AMOUNTTOPAYPRECASHDISCOUNT,CASHDISCOUNTPERCENTAGE,CACHDISCOUNTNETAMOUNT,CACHDISCOUNTGROSSAMOUNT,VATID,VATPERCENTAGE,VATAMOUNT,AMOUNTTOPAY,INVOICEDATE,SETTLEMENTDATE,PAYMENTFORMID,INVOICEROWCOUNT,OFFICENUMBER,TAXCOUNTRYCODE,OFFERNUMBER,PAYMENTTYPEID,CREDITCARDID,INVOICENUMBERREFERENCE,EMail,SendInvoice,PrinterName,PrintInvoice,Fisc_Operator,Fisc_Location) VALUES ("' . $headerid . '","' . $racun["INVOICENUMBER"] . '","' . $customernumber . '","' . $invoicetypeid . '","' . $invoiceformid . '","' . $departmentgroupid . '","' . $currencyid . '","0.00","0.00","0.00","0.00","0","' . $vatpercentage . '","' . $vatamount . '","' . $amounttopay . '","' . $createdat . '","' . $createdat . '","' . $paymentformid . '","' . $invoicerowcount . '","' . $officenumber . '","' . $taxcountrycode . '","' . $offernumber . '","' . $paymentformid . '","' . $creditcardid . '"," ","' . $email . '","' . $sendinvoice . '","","0","' . $FiscOperator . '","' . $FiscLocation . '")';
	
	Nava::poruka($sql);
	//self::runSql($sql);
	// $stmt = $database->prepare($sql);
	// $stmt->execute();


	/*AVUSInvoiceRow*/
	/*

	online - duration = broj dana
	istaknuti-online-bez i premium-bez nemaju mogućnost objave na naslovnici.

	osnovni				 nav-id: 31171
	istaknuti-online	 nav-id: 30046
	istaknuti-online-bez nav-id: 30046
	premium				 nav-id: 30136
	premium-bez			 nav-id: 30136
	pinky 				 nav-id:


	pushup 				 nav-id: 31172
	izlog				 nav-id: 690
	naslovnica			 nav-id: 31173

	offline-duration =	broj objava

	mali-privatni		nav-id: 30102
	mali-poslovni		nav-id: 30105
	mali-foto		nav-id: 30138
	mali-bold		nav-id: 31131
	istaknuti-offline	nav-id: 30135
	istaknuti-boja		nav-id: 31113

	 */
	$i = 1;
	
	/*
	update n_products set id_resursa_nav = '30136' where is_online_product = 1;
	update n_products set id_resursa_nav = '30107' where is_online_product = 0;
	update n_products set id_resursa_nav = '31171' where old_product_id = 'osnovni';
	update n_products set id_resursa_nav = '30046' where old_product_id = 'istaknuti-online';
	update n_products set id_resursa_nav = '30046' where old_product_id = 'istaknuti-online-bez';
	update n_products set id_resursa_nav = '30136' where old_product_id = 'premium';
	update n_products set id_resursa_nav = '30136' where old_product_id = 'premium-bez';
	update n_products set id_resursa_nav = '31172' where old_product_id = 'pushup';
	update n_products set id_resursa_nav = '30103' where old_product_id = 'mali-privatni';
	update n_products set id_resursa_nav = '30105' where old_product_id = 'mali-poslovni';
	update n_products set id_resursa_nav = '30138' where old_product_id = 'mali-foto';
	update n_products set id_resursa_nav = '31131' where old_product_id = 'mali-bold';
	update n_products set id_resursa_nav = '30135' where old_product_id = 'istaknuti-offline';
	update n_products set id_resursa_nav = '31113' where old_product_id = 'istaknuti-boja';
	update n_products set id_resursa_nav = '690' where old_product_id = 'izlog';
	
	// Ivana Arežina: 12.12.2016
	update n_products set id_resursa_nav = id;
	
	
	
	
	*/
	
	foreach ($racun['PRODUCTS'] as $proizvod) {
		/*Create $objectid depending on adtype. Compere ads online/offline to navision
		oglasnik online - 110
		oglasnik tiskano - 104*/
		$description='';
		$onlineads = array('osnovni', 'istaknuti-online', 'premium', 'pinky', 'pushup', 'istaknuti-online-bez', 'premium-bez');
		if (in_array($proizvod['PRODUCT_ID'], $onlineads)) {
			$objectid = 110;
			$adformatid = 30136;
			if($proizvod['PRODUCT_ID'] == 'osnovni'){$adformatid = 31171;}
			if($proizvod['PRODUCT_ID'] == 'istaknuti-online'){$adformatid = 30046;}
			if($proizvod['PRODUCT_ID'] == 'istaknuti-online-bez'){$adformatid = 30046;}
			if($proizvod['PRODUCT_ID'] == 'premium'){$adformatid = 30136;}
			if($proizvod['PRODUCT_ID'] == 'premium-bez'){$adformatid = 30136;}
			if($proizvod['PRODUCT_ID'] == 'pushup'){$adformatid = 31172;}
			if (is_array($proizvod['DURATION'])) {
				$duration = $proizvod['DURATION'][0];
				//$datefuture = new DateTime(date("d.m.YY"));
				//$datefuture->modify('+' . $duration . ' day');
				//$description = date("d.m.y") . '-' . $datefuture->format('d.m.y');
				//echo $description . "\r\n";
			}
			else{
				$duration = $proizvod['DURATION'];
				//$datefuture = new DateTime(date("d.m.YY"));
				//$datefuture->modify('+' . $duration . ' day');
				//$description = date("d.m.y") . '-' . $datefuture->format('d.m.y');
				//echo $description . "\r\n";
			}
		} else {
			if($proizvod['PRODUCT_ID'] == 'mali-privatni'){$adformatid = 30103;}
			else {$adformatid = 30107;}
			if($proizvod['PRODUCT_ID'] == 'mali-poslovni'){$adformatid = 30105;}
			if($proizvod['PRODUCT_ID'] == 'mali-foto'){$adformatid = 30138;}
			if($proizvod['PRODUCT_ID'] == 'mali-bold'){$adformatid = 31131;}
			if($proizvod['PRODUCT_ID'] == 'istaknuti-offline'){$adformatid = 30135;}
			if($proizvod['PRODUCT_ID'] == 'istaknuti-boja'){$adformatid = 31113;}
			$objectid = 104;
			#$adformatid = 30107;
			$duration = $proizvod['DURATION'];
			$description = 'Broj objava: ' . $duration;
			echo $description . "\r\n";
		}
		if ($proizvod['PRODUCT_ID'] == 'izlog') {
			$objectid = 110;
			$adformatid = 690;
		}
		
		
		$adformatid = intval($proizvod['ADFORMATID']);
		$objectid = intval($proizvod['OBJECTID']);
		
		
		/*"PRODUCTNAME": "Online Istaknuti oglas - 5 dana, Objava na naslovnici - 3 dana [Oglas: 70152]",*/
		/*AVUSRevenue*/
		$sql = "INSERT into [".$navTablePrefix.'AVUS-Revenue] (HEADERID,INVOICELINENO,ROWPOSITION,LINENO,INSERTIONREVENUETYPEID,REVENUE_ACCOUNTCODE,REVENUE_AMOUNT,REVENUESHARE,OBJECTID,ACCOUNTINGDATE,REVERSE,INVOICENUMBER) VALUES ("' . $headerid . '","0","' . $i . '","0","0","","0.00","100.00","' . $objectid . '","' . date("Y-m-d") . '","0","' . $offernumber . '")';
		
		Nava::poruka($sql);
		//self::runSql($sql);
		// $stmt = $database->prepare($sql);
		// $stmt->execute();
		
		$vatamount = ($proizvod['PRODUCTPRICE'] * 25) / 125;
		$netamount = $proizvod['PRODUCTPRICE'] / 1.25;

		/*AVUSInvoiceRow*/
		$sql = "INSERT into [".$navTablePrefix.'AVUS-InvoiceRow] (HEADERID,INVOICELINENO,ROWPOSITION,CUSTOMERNUMBER,OBJECTID,DEPARTMENTGROUPID,DEPARTMENTID,ADTYPEID,ADFORMATID,ADLAYOUTID,ORDERCLASSID,APPEARANCEDATE,REALAPPEARANCEDATE,ISSUENUMBER,ORDERID,ORDERNUMBERSTRING,INVOICEROWTEXT,CLASSID,CLASSNUMBER,CLASSNAME,UNITID,HEIGHT,WIDTH,ADRECORDID,OFFERNUMBER,COLUMNS,RATECARDPRICEUNITID,BASICPRICE,SINGLEPRICE,FACTOR,NETAMOUNT,GROSSAMOUNT,VATID,VATPERCENTAGE,VATAMOUNT,AMOUNTTOPAY,CUSTOMERCLASSID,PAYMENTCLASSID,PAYMENTSTATEID,AGENCYCOMMISSIONAMOUNT,SALESMANCOMMISSIONAMOUNT,REVENUEAMOUNT,ISSUEACCOUNTINGDATE,PAYMENTFORMID,NETTOFLAG,OFFICESALESMANNUMBER,SALESMANSHORTNAME,PLACEMENTCOLORID,ORDERPACKAGEID,TOGETHERIDX,TOGETHEREXT,INVOICENUMBER,OFFICENUMBER,PARTNERSHIPCOMMISSION,OFFERINVOICENUMBER,ROWPOSITIONREFERENCE,Description) VALUES ("' . $headerid . '","0","' . $i . '","' . $offernumber . '","' . $objectid . '","0","0","0","' . $adformatid . '","0","0","' . $createdat . '","' . $createdat . '","","0",""," ","0","","","0","0.00","0.00","0","' . $offernumber . '","0.00","0","0.00","0.00","1","' . $netamount . '","0.00","0","' . $vatpercentage . '","' . $vatamount . '","' . $proizvod["PRODUCTPRICE"] . '","0","0","0","0.00","0.00","0.00","1999-12-31","' . $paymentformid . '","0","","","0","0","0","0","' . $offernumber . '","' . $officenumber . '","0.00","' . $offernumber . '","0","' . $description . '")';
		
		Nava::poruka($sql);
		//self::runSql($sql);
		// $stmt = $database->prepare($sql);
		// $stmt->execute();
		$i++;
	}
	
	/*AVUSAddress*/

	$sql = "INSERT into [".$navTablePrefix."AVUS-Address] (HEADERID,LINENO,FORMOF,FIRST_NAME,LAST_NAME,STREET,ZIPCODE,CITY,TELCOUNTRYCODE,TELAREACODE,TELEPHONENUMBER) VALUES ( $headerid, 0 , 0 ,'$first_name','$last_name','".$racun["STREET"].','.$racun["ZIPCODE"] . '","' . $racun["CITY"] . '","385","0' . $racun["TELAREA"] . '","' . $racun["TELPHONENUMBER"] . ')';
	
	Nava::poruka($sql);
	//self::runSql($sql);
	// $stmt = $database->prepare($sql);
	// $stmt->execute();

	
	/*AVUSPayment*/

	$sql = "INSERT into [".$navTablePrefix.'AVUS-Payment] (HEADERID,INVOICELINENO,ROWPOSITION,LINENO,PAYMENTID,PAYMENTAMOUNT,PAYMENTFORMID, PAYMENTSOURCEID,PAYMENTDESTINATIONID,PAYMENTACCOUNTCODE,ACCOUNTINGDATE,INVOICENUMBER,CREDITCARDID,PAYMENTTYPEID) VALUES ("' . $headerid . '","0","1","0","0","' . $amounttopay . '","' . $paymentformid . '","0","0","","' . date("Y-m-d") . '0000-00-00","' . $offernumber . '","' . $creditcardid . '","' . $paymentformid . '")';
	
	Nava::poruka($sql);
	//self::runSql($sql);
	// $stmt = $database->prepare($sql);
	// $stmt->execute();

}

	
	
public function getNewHeaderId(){

	$navTableName = self::$navCompanyName."AVUS-Header";
	$rs = self::sqlToArray("select max (HEADERID) as id from [$navTableName]");
	return $rs[0]['id'] + 1;

}
/*	
// public static function InsertInMSSQL(  $pOrderJson = null ){
    
	// $order = $pOrderJson;
	
	// my $headerId = shift;

    // my $lineNum01;

    // #1)INSERT PODATKE U AVUSHeader
    // #print "INSERT PODATKE U AVUS-Header\n";
    
    // // my $select01 = "SELECT * FROM AVUSHeader WHERE HEADERID =?";
    // // my $sthSelect01= $dbh2->prepare($select01) or die($dbh2->errstr);
    // // $sthSelect01->execute($headerId) or die($dbh2->errstr);
    // // while(my $ref01= $sthSelect01->fetchrow_hashref()){
        // // $ref01->{'CUSTOMERNAME'} = Encode::decode('utf8',$ref01->{'CUSTOMERNAME'});
        // // insertIntoAVSHed($ref01);
    // // }
	
	// insertIntoAVSHed($order);
    
    // #2)INSERTIRAMO AVUSInvoice
    // #print "INSERT PODATKE U AVUS-Invoice\n";
    
    // my $select02 = "SELECT * FROM AVUSInvoice WHERE HEADERID =?";
    // my $sthSelect02= $dbh2->prepare($select02) or die($dbh2->errstr);
    // $sthSelect02->execute($headerId) or die($dbh2->errstr);
    // while(my $ref02= $sthSelect02->fetchrow_hashref()){
        // if(!defined($ref02->{'Fisc_Operator'}) || ! defined($ref02->{'Fisc_Location'})){
            // print "Nema fiskalnog operatera ili lokacije za $ref02->{'HEADERID'}\n";
        // }else{
            // $lineNum01 = insertIntoAVSInv($ref02);
            // $report{$ref02->{'INVOICENUMBER'}} = [$ref02->{'CREDITCARDID'}, $ref02->{'AMOUNTTOPAY'}, $ref02->{'INVOICEDATE'},$ref02->{'INVOICENUMBER'}];
        // }
        
    // }
    // #print "AvusInvoice nam je dao $lineNum01";
    
    // #3)INSERTAMO AVUS-InvoiceRow
    // #print "INSERT PODATKE U AVUS-InvoiceRow\n";
    
    // my $select03 = "SELECT * FROM AVUSInvoiceRow WHERE HEADERID=?";
    // my $sthSelect03= $dbh2->prepare($select03) or die($dbh2->errstr);
    // $sthSelect03->execute($headerId);
    // while(my $ref03= $sthSelect03->fetchrow_hashref()){
        // #print "InvoiceRow -u sam dao $lineNum01\n";
        // insertIntoAVSInvRow($ref03,$lineNum01);
    // }
    
    // #4)INSERTIRAMO AVUS-Address
    // #print "INSERT PODATKE U AVUS-Address\n";
    
    // my $select04 = "SELECT * FROM AVUSAddress WHERE HEADERID =?";
    // my $sthSelect04= $dbh2->prepare($select04) or die($dbh2->errstr);
    // $sthSelect04->execute($headerId) or die($dbh2->errstr);
    // while(my $ref04= $sthSelect04->fetchrow_hashref()){
        // $ref04->{'FIRST_NAME'} = Encode::decode('utf8',$ref04->{'FIRST_NAME'});
        // $ref04->{'LAST_NAME'} = Encode::decode('utf8',$ref04->{'LAST_NAME'});
        // $ref04->{'CITY'} = Encode::decode('utf8',$ref04->{'CITY'});
        // $ref04->{'STREET'} = Encode::decode('utf8',$ref04->{'STREET'});
        // insertIntoAVSAddr($ref04);
    // }
    
    // #5) INSERTIRAMO AVUS-Revenue
    // #print "INSERT PODATKE U AVUS-Revenue\n";
    
    // my $select05 = "SELECT * FROM AVUSRevenue WHERE HEADERID =?";
    // my $sthSelect05= $dbh2->prepare($select05) or die($dbh2->errstr);
    // $sthSelect05->execute($headerId) or die($dbh2->errstr);
    // while(my $ref05= $sthSelect05->fetchrow_hashref()){
        // insertIntoAVSRevenue($ref05,$lineNum01);
    // }
    
    // #6) INSERTIRAMO AVUS-Payment
    // #print "INSERT PODATKE U AVUS-Payment\n";
    
    // my $select06 = "SELECT * FROM AVUSPayment WHERE HEADERID =?";
    // my $sthSelect06= $dbh2->prepare($select06) or die($dbh2->errstr);
    // $sthSelect06->execute($headerId) or die($dbh2->errstr);
    // while(my $ref06= $sthSelect06->fetchrow_hashref()){
        // insertIntoAVSPayment($ref06,$lineNum01);
    // }
    
    // #7)AK je sve prošlo O.K onda moramo unijeti Ready To Process]=1
    // #print "Radimo na updateu AVUS-Header\n";
    
    // my $update01 = 'UPDATE [Oglasnik d_o_o_$AVUS-Header] SET [Ready To Process]=1 WHERE HEADERID=?';
    // my $sthUpdate01= $dbh->prepare($update01);
    // $sthUpdate01->execute($headerId);

    // #8)Sada se pobrinemo da ne pokupimo opet isti
    // my $update02 = "UPDATE AVUSHeader SET exported='y', exportDate=NOW() WHERE HEADERID=?";
    // my $sthUpdate02= $dbh2->prepare($update02);
    // $sthUpdate02->execute($headerId);
// }

// sub insertIntoAVSHed(  $pOrder = null ){
	
	// $order = $pOrder;
    // my $ref = shift;

	
    // $order['CREATEDAT'] = $order['CREATEDAT'].'.000';
    
    // my $query = 'INSERT INTO [Oglasnik d_o_o_$AVUS-Header](HEADERID ,CUSTOMERNUMBER ,CUSTOMERKINDID 
        // ,CUSTOMERCLASSID  ,CREATEDBY   ,CREATEDAT,TAXCOUNTRYCODE  ,CREDITLIMIT   
        // ,DAYSTOPAYTHEINVOICE  ,CASHDISCOUNTPERCENTAGE ,INVOICEFORMID,CUSTOMERINVOICETYPEID 
        // ,PAYMENTFORMID  ,ACCOUNTANCYSTATEID  ,ACCOUNTINGMODIFY,BRANCHGROUPID  
        // ,CURRENCYID  ,VATID  ,VATPERCENTAGE  ,CUSTOMERNAME  ,TAXNUMBER ,[Processing Error]  
        // ,[Processing Status] ,[Ready To Process],[Entry creation Date_Time], DEBITORACCOUNTNEW, 
        // [Entry creation Date], [Entry creation Time], Status, [Error description], 
        // [Ready for creation],[Customer No_], [Import ID]) VALUES(';
    // $query .= $order['HEADERID'] . ", ";
    // $query .= "'" .$order['CUSTOMERNUMBER'] . "', ";
    // $query .= $order['CUSTOMERKINDID'] . ", ";
    // $query .= $order['CUSTOMERCLASSID'] . ",";
    // $query .= $order['CREATEDBY'] . ",";
    // $query .= "'" . $order['CREATEDAT'] . "', ";
    // $query .= "'" . $order['TAXCOUNTRYCODE'] . "', ";
    // $query .= $order['CREDITLIMIT'] . ", ";
    // $query .= $order['DAYSTOPAYTHEINVOICE'] . ", ";
    // $query .= $order['CASHDISCOUNTPERCENTAGE'] . ", ";
    // $query .= $order['INVOICEFORMID'] . ", ";
    // $query .= $order['CUSTOMERINVOICETYPEID'] . ", ";
    // $query .= $order['PAYMENTFORMID'] . ",";
    // $query .= $order['ACCOUNTANCYSTATEID'] . ",";
    // $query .= $order['ACCOUNTINGMODIFY'] . ",";
    // $query .= $order['BRANCHGROUPID'] . ",";
    // $query .= $order['CURRENCYID'] . ",";
    // $query .= $order['VATID'] . ",";
    // $query .= $order['VATPERCENTAGE'] . ",";
    // $query .= "'" . $order['CUSTOMERNAME'] . "',";
    // $query .= "'" . $order['TAXNUMBER'] . "',";
    // $query .= "0,0,0,";
    // $query .= "'" . $order['CREATEDAT'] . "', ";
    // $query .= "'','1753-01-01 00:00:00.000', '1753-01-01 00:00:00.000', 0, '', 0, '', '')";

    // //$query  = Encode::encode('cp1250', $query);
    
	
	
    // my $sth = $dbh->prepare($query) or die($dbh->errstr);
    // $sth->execute() or die($dbh->errstr);
// }

// sub insertIntoAVSInv{
    // my $ref = shift;
    
    // my $lineNum = selectMaxLinenoInvoice();

    // my $sql ='INSERT INTO [Oglasnik d_o_o_$AVUS-Invoice](HEADERID, INVOICENUMBER, CUSTOMERNUMBER, INVOICETYPEID,
            // INVOICEFORMID, DEPARTMENTGROUPID, CURRENCYID, AMOUNTTOPAYPRECASHDISCOUNT, CASHDISCOUNTPERCENTAGE,
            // CACHDISCOUNTNETAMOUNT, VATID, VATPERCENTAGE, VATAMOUNT, AMOUNTTOPAY, INVOICEDATE, SETTLEMENTDATE, 
            // PAYMENTFORMID, INVOICEROWCOUNT, OFFICENUMBER, TAXCOUNTRYCODE, OFFERNUMBER, PAYMENTTYPEID, 
            // CREDITCARDID, [Late Delivery], [Fisc_ Doc_ No_], [E-Mail for Sending Invoice], [Send Invoice], [Printer Name], 
            // [Print Invoice], [Fisc_ Operator], [Fisc _Location], [LINENO],
            // [Processing Status], CACHDISCOUNTGROSSAMOUNT,
            // INVOICENUMBERREFERENCE, [Ready for creation], [Error description], [Invoice No_], [Posted document No_], 
            // [Currency Code], [Payment Method Code], [Fisc_ Subject], [Fisc_ Wholesale Terminal],
            // [Fisc_ Location Code], [Posting DateTime], [Issuer Protection Code], [Unique Invoice No_],
            // [Fisc_ Registered])VALUES(';
    // $sql.= $ref->{'HEADERID'} . ", ";
    // $sql.= "'" . $ref->{'INVOICENUMBER'} . "',";
    // $sql.= "'" . $ref->{'CUSTOMERNUMBER'} . "',";
    // $sql.= $ref->{'INVOICETYPEID'} . ", ";
    // $sql.= $ref->{'INVOICEFORMID'} . ", ";
    // $sql.= $ref->{'DEPARTMENTGROUPID'} . ", ";
    // $sql.= $ref->{'CURRENCYID'} . ", ";
    // $sql.= $ref->{'AMOUNTTOPAYPRECASHDISCOUNT'} . ", ";
    // $sql.= $ref->{'CASHDISCOUNTPERCENTAGE'} . ", ";
    // $sql.= $ref->{'CACHDISCOUNTNETAMOUNT'} . ", ";
    // $sql.= $ref->{'VATID'} . ", ";
    // $sql.= $ref->{'VATPERCENTAGE'} . ", ";
    // $sql.= $ref->{'VATAMOUNT'} . ", ";
    // $sql.= $ref->{'AMOUNTTOPAY'} . ", ";
    // $sql.= "'" . $ref->{'INVOICEDATE'} .".000',";
    // $sql.= "'" . $ref->{'SETTLEMENTDATE'} .".000',";
    // $sql.= $ref->{'PAYMENTFORMID'} .", ";
    // $sql.= $ref->{'INVOICEROWCOUNT'} . ", ";
    // #$sql.= "'" . $ref->{'OFFICENUMBER'} . "',";
    // $sql.= (defined($ref->{'OFFICENUMBER'})) ? $ref->{'OFFICENUMBER'} ."," : "'7041', ";
    // $sql.= "'" . $ref->{'TAXCOUNTRYCODE'} . "',";
    // $sql.= "'" . $ref->{'OFFERNUMBER'} . "',";
    // $sql.= $ref->{'PAYMENTTYPEID'} . ", "; 
    // $sql.= $ref->{'CREDITCARDID'} . ", "; 
    // $sql.= "0, '',";
    // $sql.= "'" . $ref->{'EMail'} . "',";
    // $sql.= "1, "; 
    // $sql.= "'" . $ref->{'PrinterName'} . "',"; 
    // $sql.= "1, ";
    // $sql.= "'" . $ref->{'Fisc_Operator'} . "',";
    // $sql.= "'" . $ref->{'Fisc_Location'} . "',";
    // $sql.= $lineNum . ", ";
    // $sql.= "0,0.00,'',0,'','','','','',0,'','','1753-01-01 00:00:00.000','','',0)"; 

    // my $sth = $dbh->prepare($sql) or die($dbh->errstr);
    // $sth->execute() or die($dbh->errstr);

    // return $lineNum;
// }

// sub insertIntoAVSInvRow{
    // my $ref = shift;
    // my $lineNum = shift;
    
    // my $query = 'INSERT INTO [Oglasnik d_o_o_$AVUS-InvoiceRow](HEADERID, INVOICELINENO, ROWPOSITION, CUSTOMERNUMBER,
    // OBJECTID, DEPARTMENTGROUPID, DEPARTMENTID, ADTYPEID, 
    // ADFORMATID, ADLAYOUTID, ORDERCLASSID, APPEARANCEDATE, REALAPPEARANCEDATE, ISSUENUMBER, ORDERID, 
    // ORDERNUMBERSTRING, INVOICEROWTEXT, CLASSID, CLASSNUMBER, CLASSNAME, UNITID, HEIGHT, WIDTH, ADRECORDID, 
    // OFFERNUMBER, COLUMNS, RATECARDPRICEUNITID, BASICPRICE, SINGLEPRICE, FACTOR, NETAMOUNT, GROSSAMOUNT, VATID, 
    // VATPERCENTAGE, VATAMOUNT, AMOUNTTOPAY, CUSTOMERCLASSID, PAYMENTCLASSID, PAYMENTSTATEID, AGENCYCOMMISSIONAMOUNT, 
    // SALESMANCOMMISSIONAMOUNT, REVENUEAMOUNT, ISSUEACCOUNTINGDATE, PAYMENTFORMID, NETTOFLAG, OFFICESALESMANNUMBER,
    // SALESMANSHORTNAME, PLACEMENTCOLORID, ORDERPACKAGEID, TOGETHERIDX, TOGETHEREXT, INVOICENUMBER, OFFICENUMBER,
    // PARTNERSHIPCOMMISSION, OFFERINVOICENUMBER, ROWPOSITIONREFERENCE, [Ready for creation], [Error description],
    // [Resource No_],[Description 2])VALUES(';

    // $query .= $ref->{'HEADERID'} . ", ";
    // $query .= $lineNum . ", "; 
    // $query .= (defined($ref->{'ROWPOSITION'})) ? $ref->{'ROWPOSITION'} ."," : "0, ";
    // $query .= (defined($ref->{'CUSTOMERNUMBER'})) ? "'". $ref->{'CUSTOMERNUMBER'} ."'," : "'', ";
    // $query .= (defined($ref->{'OBJECTID'})) ? $ref->{'OBJECTID'} ."," : "0, ";
    // $query .= (defined($ref->{'DEPARTMENTGROUPID'})) ? $ref->{'DEPARTMENTGROUPID'} ."," : "0, ";
    // $query .= (defined($ref->{'DEPARTMENTID'})) ? $ref->{'DEPARTMENTID'} ."," : "0, ";
    // $query .= (defined($ref->{'ADTYPEID'})) ? $ref->{'ADTYPEID'} ."," : "0, ";
    // $query .= (defined($ref->{'ADFORMATID'})) ? $ref->{'ADFORMATID'} ."," : "0, ";
    // $query .= (defined($ref->{'ADLAYOUTID'})) ? $ref->{'ADLAYOUTID'} ."," : "0, ";
    // $query .= (defined($ref->{'ORDERCLASSID'})) ? $ref->{'ORDERCLASSID'} ."," : "0, ";
    // $query .= (defined($ref->{'APPEARANCEDATE'})) ? "'". $ref->{'APPEARANCEDATE'} .".000'," : "'1753-01-01 00:00:00.000', ";
    // $query .= (defined($ref->{'REALAPPEARANCEDATE'})) ? "'". $ref->{'REALAPPEARANCEDATE'} .".000'," : "'1753-01-01 00:00:00.000', ";
    // $query .= (defined($ref->{'ISSUENUMBER'})) ? "'". $ref->{'ISSUENUMBER'} ."'," : "'', ";
    // $query .= (defined($ref->{'ORDERID'})) ? $ref->{'ORDERID'} ."," : "0, ";
    // $query .= (defined($ref->{'ORDERNUMBERSTRING'})) ? "'". $ref->{'ORDERNUMBERSTRING'} ."'," : "'', ";
    // #$query .= (defined($ref->{'INVOICEROWTEXT'})) ? "'". uc(Encode::decode('utf8',$ref->{'INVOICEROWTEXT'})) ."'," : "'', ";
    // $query .= "'', ";
    // $query .= (defined($ref->{'CLASSID'})) ? $ref->{'CLASSID'} ."," : "0, ";
    // $query .= (defined($ref->{'CLASSNUMBER'})) ? "'". $ref->{'CLASSNUMBER'} ."'," : "'', ";
    // #$query .= (defined($ref->{'CLASSNAME'})) ? "'". uc(Encode::decode('utf8',$ref->{'CLASSNAME'})) ."'," : "'', ";
    // $query .= "'', ";
    // $query .= (defined($ref->{'UNITID'})) ? $ref->{'UNITID'} ."," : "0, ";
    // $query .= (defined($ref->{'HEIGHT'})) ? $ref->{'HEIGHT'} ."," : "0, ";
    // $query .= (defined($ref->{'WIDTH'})) ? $ref->{'WIDTH'} ."," : "0, ";
    // $query .= (defined($ref->{'ADRECORDID'})) ? $ref->{'ADRECORDID'} ."," : "0, ";
    // $query .= (defined($ref->{'OFFERNUMBER'})) ? "'". $ref->{'OFFERNUMBER'} ."'," : "'', ";
    // $query .= (defined($ref->{'COLUMNS'})) ? $ref->{'COLUMNS'} ."," : "0.00, ";
    // $query .= (defined($ref->{'RATECARDPRICEUNITID'})) ? $ref->{'RATECARDPRICEUNITID'} ."," : "0, ";
    // $query .= (defined($ref->{'BASICPRICE'})) ? $ref->{'BASICPRICE'} ."," : "0, ";
    // $query .= (defined($ref->{'SINGLEPRICE'})) ? $ref->{'SINGLEPRICE'} ."," : "0, ";
    // $query .= (defined($ref->{'FACTOR'})) ? $ref->{'FACTOR'} ."," : "0, ";
    // $query .= (defined($ref->{'NETAMOUNT'})) ? $ref->{'NETAMOUNT'} ."," : "0.00, ";
    // $query .= "0.00, ";
    // $query .= (defined($ref->{'VATID'})) ? $ref->{'VATID'} ."," : "0, ";
    // $query .= (defined($ref->{'VATPERCENTAGE'})) ? $ref->{'VATPERCENTAGE'} ."," : "0.00, ";
    // #$query .= (defined($ref->{'VATAMOUNT'})) ? $ref->{'VATAMOUNT'} ."," : "0.00, ";
    // $query .= "0.00, ";
    // #$query .= (defined($ref->{'AMOUNTTOPAY'})) ? $ref->{'AMOUNTTOPAY'} ."," : "0.00, ";
    // $query .= "0.00, ";
    // $query .= (defined($ref->{'CUSTOMERCLASSID'})) ? $ref->{'CUSTOMERCLASSID'} ."," : "0, ";
    // $query .= (defined($ref->{'PAYMENTCLASSID'})) ? $ref->{'PAYMENTCLASSID'} ."," : "0, ";
    // $query .= (defined($ref->{'PAYMENTSTATEID'})) ? $ref->{'PAYMENTSTATEID'} ."," : "0, ";
    // $query .= (defined($ref->{'AGENCYCOMMISSIONAMOUNT'})) ? $ref->{'AGENCYCOMMISSIONAMOUNT'} ."," : "0.00, ";
    // $query .= (defined($ref->{'SALESMANCOMMISSIONAMOUNT'})) ? $ref->{'SALESMANCOMMISSIONAMOUNT'} ."," : "0.00, ";
    // $query .= (defined($ref->{'REVENUEAMOUNT'})) ? $ref->{'REVENUEAMOUNT'} ."," : "0.00, ";
    // $query .= "'1999-12-31 00:00:00.000', ";
    // $query .= (defined($ref->{'PAYMENTFORMID'})) ? $ref->{'PAYMENTFORMID'} ."," : "0, ";
    // $query .= (defined($ref->{'NETTOFLAG'})) ? $ref->{'NETTOFLAG'} ."," : "0, ";
    // #$query .= (defined($ref->{'OFFICESALESMANNUMBER'})) ? "'". $ref->{'OFFICESALESMANNUMBER'} ."'," : "'0', ";
    // $query .= "'0', ";
    // $query .= (defined($ref->{'SALESMANSHORTNAME'})) ? "'". $ref->{'SALESMANSHORTNAME'} ."'," : "'', ";
    // $query .= (defined($ref->{'PLACEMENTCOLORID'})) ? $ref->{'PLACEMENTCOLORID'} ."," : "0, ";
    // $query .= (defined($ref->{'ORDERPACKAGEID'})) ? $ref->{'ORDERPACKAGEID'} ."," : "0, ";
    // $query .= (defined($ref->{'TOGETHERIDX'})) ? $ref->{'TOGETHERIDX'} ."," : "0, ";
    // $query .= (defined($ref->{'TOGETHEREXT'})) ? $ref->{'TOGETHEREXT'} ."," : "0, ";
    // $query .= (defined($ref->{'INVOICENUMBER'})) ? $ref->{'INVOICENUMBER'} ."," : "'', ";
    // $query .= (defined($ref->{'OFFICENUMBER'})) ? $ref->{'OFFICENUMBER'} ."," : "'', ";
    // $query .= (defined($ref->{'PARTNERSHIPCOMMISSION'})) ? $ref->{'PARTNERSHIPCOMMISSION'} ."," : "0.00, ";
    // $query .= (defined($ref->{'OFFERINVOICENUMBER'})) ? "'". $ref->{'OFFERINVOICENUMBER'} ."'," : "'', ";
    // $query .= "0,0,'','','";
    // $query .= $ref->{'Description'}."')";
    
    // my $sth = $dbh->prepare($query) or die($dbh->errstr);
    // $sth->execute() or die($dbh->errstr);

// }

// sub insertIntoAVSAddr{
    // my $ref = shift;
 
    // my $lineNum = selectMaxLinenoAddress();

    // my $query = 'INSERT INTO [Oglasnik d_o_o_$AVUS-Address](HEADERID, [LINENO], FORMOF, FIRST_NAME, LAST_NAME, 
    // STREET, ZIPCODE, CITY, TELCOUNTRYCODE, TELAREACODE, TELEPHONENUMBER)
    // VALUES(';
    // $query .= $ref->{'HEADERID'} . ", ";
    // $query .= $lineNum . ", ";
    // $query .= (defined($ref->{'FORMOF'})) ? $ref->{'FORMOF'} ."," : "0, ";
    // $query .= (defined($ref->{'FIRST_NAME'})) ? "'" . $ref->{'FIRST_NAME'} ."'," : "'',";
    // $query .= (defined($ref->{'LAST_NAME'})) ? "'" . $ref->{'LAST_NAME'} ."'," : "'',";
    // $query .= (defined($ref->{'STREET'})) ? "'" . $ref->{'STREET'} ."'," : "'',";
    // $query .= (defined($ref->{'ZIPCODE'})) ? "'" . $ref->{'ZIPCODE'} ."'," : "'',";
    // $query .= (defined($ref->{'CITY'})) ? "'" . $ref->{'CITY'} ."'," : "'',";
    // $query .= (defined($ref->{'TELCOUNTRYCODE'})) ? "'" . $ref->{'TELCOUNTRYCODE'} ."'," : "'',";
    // $query .= (defined($ref->{'TELAREACODE'})) ? "'" . $ref->{'TELAREACODE'} ."'," : "'',";
    // $query .= (defined($ref->{'TELEPHONENUMBER'})) ? "'" . $ref->{'TELEPHONENUMBER'} ."')" : "'')";

    // $query  = Encode::encode('cp1250', $query);
    
    // my $sth = $dbh->prepare($query) or die($dbh->errstr);
    // $sth->execute() or die($dbh->errstr);
// }

// sub insertIntoAVSRevenue{
    // my $ref = shift;
    // my $lineNum =shift;
    
    // my $lineNum02 = selectMaxLinenoRevenue($dbh);

    // my $sql ='INSERT INTO [Oglasnik d_o_o_$AVUS-Revenue](HEADERID, INVOICELINENO, 
        // ROWPOSITION, [LINENO], INSERTIONREVENUETYPEID, REVENUE_ACCOUNTCODE,
        // REVENUE_AMOUNT, REVENUESHARE, OBJECTID, ACCOUNTINGDATE, REVERSE, 
        // INVOICENUMBER)VALUES(';

    // $sql .= $ref->{'HEADERID'} . ", ";
    // $sql .= $lineNum . ", ";
    // $sql .= "$ref->{'ROWPOSITION'}, ";
    // $sql .= $lineNum02 . ", ";
    // $sql .= "$ref->{'INSERTIONREVENUETYPEID'}, ";
    // $sql .= "'" .$ref->{'REVENUE_ACCOUNTCODE'}."', ";
    // $sql .= "$ref->{'REVENUE_AMOUNT'}, ";
    // $sql .= "$ref->{'REVENUESHARE'}, ";
    // $sql .= "$ref->{'OBJECTID'}, ";
    // $sql .= "'1753-01-01 00:00:00.000',";
    // $sql .= "0, ";
    // $sql .= "'" . $ref->{'INVOICENUMBER'} . "')";
    
    // my $sth = $dbh->prepare($sql) or die($dbh->errstr);
    // $sth->execute() or die($dbh->errstr);

// }

// sub insertIntoAVSPayment{
    // my $ref = shift;
    // my $lineNum = shift;
    
    // my $lineNum02 = selectMaxLinenoPayment($dbh);

    // my $sql = 'INSERT INTO [Oglasnik d_o_o_$AVUS-Payment](HEADERID ,INVOICELINENO ,ROWPOSITION ,[LINENO] 
        // ,PAYMENTID ,PAYMENTAMOUNT ,PAYMENTFORMID ,PAYMENTSOURCEID ,PAYMENTDESTINATIONID ,PAYMENTACCOUNTCODE ,
        // ACCOUNTINGDATE ,INVOICENUMBER ,CREDITCARDID ,PAYMENTTYPEID, [Payment Method Code] 
        // ,[Ready for creation] ,[Error description] ,[aifra kupca-kartiYar] ,[Processing Status])
    // VALUES(';

    // $sql .= $ref->{'HEADERID'} . ", ";
    // $sql .= $lineNum . ", ";
    // $sql .= "$ref->{'ROWPOSITION'}, ";
    // $sql .= $lineNum02 . ", ";
    // $sql .= "$ref->{'PAYMENTID'}, ";
    // $sql .= "$ref->{'PAYMENTAMOUNT'}, ";
    // $sql .= "$ref->{'PAYMENTFORMID'}, ";
    // $sql .= "$ref->{'PAYMENTSOURCEID'}, $ref->{'PAYMENTDESTINATIONID'}, '', '1753-01-01 00:00:00.000',";
    // $sql .= "$ref->{'INVOICENUMBER'}, $ref->{'CREDITCARDID'},$ref->{'PAYMENTTYPEID'},'',0,'','',0)";
    
    // my $sth = $dbh->prepare($sql) or die($dbh->errstr);
    // $sth->execute() or die($dbh->errstr);

// }

// sub selectMaxLinenoInvoice{
    
    // my $sql = 'SELECT MAX([LINENO]) +1 AS lineNum FROM [Oglasnik d_o_o_$AVUS-Invoice]';
    // my $sth = $dbh->prepare($sql) or die($dbh->errstr);
    // $sth->execute() or die($dbh->errstr);
    // my $ref = $sth->fetchrow_hashref();
    // return $ref->{'lineNum'};
// }

// sub selectMaxLinenoPayment{
    
    // my $sql = 'SELECT MAX([LINENO]) +1 AS lineNum FROM [Oglasnik d_o_o_$AVUS-Payment]';
    // my $sth = $dbh->prepare($sql) or die($dbh->errstr);
    // $sth->execute() or die($dbh->errstr);
    // my $ref = $sth->fetchrow_hashref();
    // return $ref->{'lineNum'};    
// }

// sub selectMaxLinenoRevenue{
    
    // my $sql = 'SELECT MAX([LINENO]) +1 AS lineNum FROM [Oglasnik d_o_o_$AVUS-Revenue]';
    // my $sth = $dbh->prepare($sql) or die($dbh->errstr);
    // $sth->execute() or die($dbh->errstr);
    // my $ref = $sth->fetchrow_hashref();
    // return $ref->{'lineNum'};    
// }

// sub selectMaxLinenoAddress{
    
    // my $sql = 'SELECT MAX([LINENO]) +1 AS lineNum FROM [Oglasnik d_o_o_$AVUS-Address]';
    // my $sth = $dbh->prepare($sql) or die($dbh->errstr);
    // $sth->execute() or die($dbh->errstr);
    // my $ref = $sth->fetchrow_hashref();
    // return $ref->{'lineNum'};    
// }

// ###############################################################################
// ##
// ## Saljemo mail
// ##
// ###############################################################################
// sub send_mail{
    // my $file = shift;
    // my $filename = shift;
    // my $datum = shift;
    
    // my $msg = MIME::Lite->new(
        // From    => 'reporter@oglasnik.hr',
        // To      => 'racunovodstvo@oglasnik.hr',
        // Cc      => 'bnad@oglasnik.hr',
        // Subject => 'Oglasnik2Nav ' . $datum . ' [automatski mail]',
        // Type    => 'multipart/mixed'
    // );

    // ### Add parts (each "attach" has same arguments as "new"):
    // $msg->attach(
        // Type     => 'text/plain; charset=UTF-8',
        // Data     => 'Report plačanja poslanih u Navision sa http://www.oglasnik.hr',
        // Encoding => 'quoted-printable'
    // );

    // $msg->attach(
        // Type     => 'application/vnd.ms-excel',
        // Path     => $file,
        // Filename => $filename,
        // Disposition => 'attachment'
    // );
    // ### use Net:SMTP to do the sending
    // $msg->send('smtp','mail.oglasnik.hr', Debug=>0 );
// }

// ###############################################################################
// ##
// ## SELECT koliko polja imamo
// ##
// ###############################################################################

// sub selectCount{
    // my $sql = "SELECT count(*) AS kaunt FROM AVUSHeader 
        // LEFT JOIN AVUSInvoice ON AVUSHeader.HEADERID = AVUSInvoice.HEADERID 
        // WHERE exported='n' AND LEFT(CREATEDAT,10) <> '0000-00-00' 
        // AND (AVUSInvoice.INVOICENUMBER like '800%' or AVUSInvoice.INVOICENUMBER like '900%')";
    // my $sth = $dbh2->prepare($sql) or die($dbh2->errstr);
    // $sth->execute() or die($dbh2->errstr);
    // my $ref= $sth->fetchrow_hashref();
    // return $ref->{'kaunt'};
// }

*/	
	
	
    /**
     * Transforms data returned by `Orders::buildResultsArray()` into a slightly
     * different array (specified by Oglasnik, since they want their JSON export fields
     * named differently and certain data in a different format)
     *
     * @param array $results
     *
     * @return array
     */
	public static function modifySearchResultsForJsonExport(array $results){
        $modified_results = array();
        $order_modified   = array();

        foreach ($results as $order) {
            $order_modified['ID'] = $order['id'];

            // Merge user invoice details
            $user_invoice_details            = \Baseapp\Models\Users::buildInvoiceDetailsArray($order);
            $order_modified                  = array_merge($order_modified, $user_invoice_details);

            $order_modified['USER_ID']       = $order['user_id'];
            $order_modified['USERNAME']      = $order['username'];

            // Add other stuff they required
            $order_modified['INVOICENUMBER'] = $order['pbo'];
            $order_modified['AMOUNTTOPAY']   = $order['total'];
            $order_modified['AMOUNTTOPAYLP'] = $order['total_lp'];
            $order_modified['INVOICEDATE']   = $order['modified_at'];

            // Adding some stuff I think will come in handy down the line
            $order_modified['STATUS']     = $order['status'];
            $order_modified['STATUSTEXT'] = $order['status_text'];

			//IGOR: DODAVANJE PREMA ZAHTJEVIMA OGLASNIKA 02.12.2016
			$operator = \Baseapp\Suva\Models\Users::findFirst($order['n_operator_id']);
			$FiscOperator = $operator ? $operator->n_oznaka_operatera : 1000;
			$order_modified['FiscOperator'] = $FiscOperator;

			
			$location = \Baseapp\Suva\Models\FiscalLocations::findFirst($order['n_fiscal_location_id']);
			$FiscLocation = $location ? $location->nav_fiscal_location_id : 'ONL1';
			$order_modified['FiscLocation'] = $FiscLocation;

			
			$salesrep = \Baseapp\Suva\Models\Users::findFirst($order['n_sales_rep_id']);
			$OFFICENUMBER = $salesrep ? $salesrep->n_nav_department_id : 7041;
			$order_modified['OFFICENUMBER']     = $OFFICENUMBER;
			
            if ('offline' === $order['payment_method'] || empty($order['payment_method'])) {
                $order_modified['PAYMENTMETHOD'] = 0;
                $order_modified['CREDITCARDID']  = 'opca_virman';
            } else {
                $order_modified['PAYMENTMETHOD'] = 1;
                $order_modified['CREDITCARDID']  = $order['payment_method'];
            }

            $order_modified['PRODUCTS'] = array();
            foreach ($order['details'] as $k => $product) {
                $price_kn = Utils::lp2kn($product->price);
                $total_kn = Utils::lp2kn($product->total);

                // Simple attempt to get durations as requested. Might suffice.
                $duration = self::extractDurationNumbersFromDurationPhrases($product->title);
                if (null !== $duration) {
                    $order_modified['PRODUCTS'][$k]['DURATION'] = $duration;
                }

				//ako nije nađen agimov proizvod neka iz Producta doda Navision id_resursa_nav  
                $order_modified['PRODUCTS'][$k]['PRODUCT_ID']             = $product->product_id ? $product->product_id : $product->Product()->id_resursa_nav ;
                
				$order_modified['PRODUCTS'][$k]['ADFORMATID']             = $product->id_resursa_nav;
				$order_modified['PRODUCTS'][$k]['OBJECTID']             	= $product->is_online_product ? 110 : 104;
				
				$order_modified['PRODUCTS'][$k]['PRODUCTNAME']            = $product->title;
                $order_modified['PRODUCTS'][$k]['PRODUCTQTY']             = $product->qty;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICE']           = $price_kn;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICELP']         = $product->price;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICETOTAL']      = $total_kn;
                $order_modified['PRODUCTS'][$k]['PRODUCTPRICETOTALLP']    = $product->total;
                $order_modified['PRODUCTS'][$k]['PRODUCTDISCOUNT']        = 0;
                $order_modified['PRODUCTS'][$k]['PRODUCTDISCOUNTPRICE']   = $price_kn;
                $order_modified['PRODUCTS'][$k]['PRODUCTDISCOUNTPRICELP'] = $product->price;
            }

            $modified_results[] = $order_modified;
        }

        return $modified_results;
    }
	
    public function extractDurationNumbersFromDurationPhrases($string)
    {
        $duration = null;

        // Looking for numbers separated by a space followed by certain known words
        $regex = '/(\d+)\s(dana?|objav[ae]?)/i';

        $matches = array();
        $matched = preg_match_all($regex, $string, $matches);
        if (false !== $matched && $matched > 0) {
            // Only want the numbers (first capturing group)
            if (isset($matches[1]) && !empty($matches[1])) {
                $duration = array_map('intval', $matches[1]);
            }
        }

        if (1 === count($duration)) {
            $duration = (int) $duration[0];
        }

        return $duration;
    }


//SQL FUNKCIJE
	function fConnectNav (){

		//NAVISION SQL SERVER POSTAVKE
		$navHost = "195.245.255.22";
		$navPort = "1433";
		$navDb = "OGLASNIK_TEST_NEW";
		$navUser = "sa";
		$navPassword = "Tassadar1337";

		try {
			
			error_log("Connecting to $navHost ..");
			$db = new \PDO("dblib:host=$navHost;port=$navPort;dbname=$navDb", $navUser, $navPassword );
			$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);// set the PDO error mode to exception 
			error_log("Connected to $navHost successfully"); 
			return $db;
		}
		catch(\PDOException $e){ 
			error_log("SQL failed: " . $e->getMessage());
		}
	}

	function sqlToArray ( $pSql){
		self::$dbNav = self::$dbNav ? self::$dbNav : self::fConnectNav();
		//$dbNav = self::fConnectNav();
		$this->poruka($pSql);
		$rs = self::$dbNav->query($pSql);
		$result = array();
		foreach ($rs as $row)
			array_push ($result, $row);
		return $result;
	}

	function runSql ( $pDb, $pSql ){
		try {
			self::$dbNav = self::$dbNav ? self::$dbNav : fConnectNav();
			//self::$dbNav->query($pSql);
			$this->poruka($pSql);
		}
		catch(\PDOException $e){ 
			error_log("SQL failed: " . $e->getMessage());
		}
	}
	
	
}