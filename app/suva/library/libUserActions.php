<?php



namespace Baseapp\Suva\Library;


use Baseapp\Suva\Library\Nava;

class libUserActions {
		//greške ERR200 - ERR299

	//IGOR - sve akcije koje se klikaju iz klijenta
	public static function runUserAction ( $pCallingObject = null ){
		//ZA SADA VADI AKCIJU I PARAMETRE SAMO IZ KONTEKSTA.
		
		//Nava::poruka("libUsersActions");
		
		if ($pCallingObject) $request = $pCallingObject->request;
		$action = array_key_exists('clicked_action', $_SESSION['_context']) ? $_SESSION['_context']['clicked_action'] : null; 
		$params = array_key_exists('clicked_action_params', $_SESSION['_context']) ? $_SESSION['_context']['clicked_action_params'] : array(); 

		//error_log ("Nava::clickedActions $action");
		//error_log ("Nava::clickedActions params: ".print_r($params, true));
		//Nava::poruka("action $action");
		if (!$action ) return;

		switch ( $action ){
		
			case 'to_offer': 
			case 'actItemMoveToOffer': {
				$checkedOrderItems = array_key_exists('orders_items', $params) ? $params['orders_items']: null;
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				$selectedUserId = array_key_exists('selected_user_id', $params) ? $params['selected_user_id']: null;
				$selectedUserId = $selectedUserId > 0 ? $selectedUserId : ' null ';
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				$loggedUserId = $loggedUserId > 0 ? $loggedUserId : ' null ';
				
				
				if ($checkedOrderItems && $userId){
					//error_log("Ads2Controller:subUsersCartAction offer2");
// 					//error_log("Ads::n_after_save 8  PREBACIVANJE ODABRANIH ORDER_ITEMA U CART");
					//PREBACIVANJE ODABRANIH ORDER_ITEMA U CART

					//napravi novi Offer
					$imaPlacenih = false;
					foreach ($checkedOrderItems as $oi_id){
						//error_log("Ads2Controller:subUsersCartAction offer3");
						$oi = \Baseapp\Suva\Models\OrdersItems::findFirst($oi_id);
						if ($oi->n_total > 0) $imaPlacenih = true;
						
						//greska da se ulovi automatsko slanje u offer
						if ( !$selectedUserId ) {
							Nava::greska("ERR201: Pokrenuto je slanje iz carta u offer za OI $oi_id, a nije poslan Sales Rep.");
							return;
						}
					}

					if ($imaPlacenih){
						// $pboPrefix = \Baseapp\Suva\Library\libAppSetting::get('fin_invoice_prefix');
						// //error_log("Ads2Controller:subUsersCartAction offer4");
						// Nava::runSql("insert into orders (status, total, created_at, modified_at) values (0,0, now(), now())");
						// $order_id = Nava::sqlToArray("select max(id) as id  from orders ")[0]['id'];
						// $pbo = (intval($pboPrefix) + intval($order_id));
						// //Nava::poruka("PBO JE".$pbo);

						// $txtSql =
							// " update orders set "
						  // ." user_id = $selectedUserId "
						  // .", n_sales_rep_id = $loggedUserId "
						  // .", n_status = 3 "
						  // .", status = 1 "
						  // .", n_quotation_date = now() "
						  // .", n_pbo = '$pbo'"
						  // ." where id = $order_id ";
						// //error_log ($txtSql);ž
						// //Nava::poruka($txtSql);
						// Nava::runSql( $txtSql );
						
						$offer = \Baseapp\Suva\Models\Orders::createOffer ( $loggedUserId, $selectedUserId );
						$order_id = $offer->id;
						
						foreach ($checkedOrderItems as $oi_id){
							//error_log("Ads2Controller:subUsersCartAction offer5");
							$oi = \Baseapp\Suva\Models\OrdersItems::findFirst($oi_id);
							if ($oi)
								if ($oi->n_total > 0){
									Nava::sysMessage ("moving Order Item $oi_id to Order $order_id libUA");
									Nava::runSql ("update orders_items set order_id = $order_id where id = $oi_id ");
									$oi->updateAdLatestPaymentState();
									Nava::poruka("Order Item No. $oi_id transferred to offer No. $order_id".PHP_EOL);
								}
								else{
									Nava::greska("ERR202: Order Item No. $oi->id has total 0.00 and cannot be transferred to Offer.");
								}
									
							else{
								Nava::greska("ERR203: Order Item No. $oi_id, selected for transferring to Offer, was not found.");
								return;
								// return "Order Item No. $oi_id, selected for transferring to Offer, was not found.";
							} 
						}
						Nava::updateOrderTotal($order_id);
						
					}
					else
						Nava::greska("ERR204: None of the selected Order Items were transferred to offer (have you selected only free items?).".PHP_EOL);
						// $errMsg .= "None of the selected Order Items were transferred to offer (have you selected only free items?).".PHP_EOL;
				}
				break;
			}
			
			case 'to_free_ads':
			case 'actMoveItemToFreeAds': {
				$checkedOrderItems = array_key_exists('orders_items', $params) ? $params['orders_items']: null;
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				
				if ($checkedOrderItems && $userId){
					//error_log("Ads::n_after_save to_free_ads");
					//PREBACIVANJE ODABRANIH ORDER_ITEMA U CART
					foreach ($checkedOrderItems as $oi_id){

						$oi = \Baseapp\Suva\Models\OrdersItems::findFirst($oi_id);
						if (!$oi ) {
							 return("Order Item No. $oi_id not found");
						}
						//error_log("ActionsController:actMoveItemToFreeAdsAction 2");
						$order = \Baseapp\Suva\Models\Orders::findFirst($oi->order_id);
						if ( !$order ) {
							Nava::greska("ERR205: Order No. $oi->order_id, reffered to by Order Item No. $oi->id, not found.");
							// return("Order No. $oi->order_id, reffered to by Order Item No. $oi->id, not found.");
							return;
						}
						//error_log("ActionsController:actMoveItemToFreeAdsAction 3");
						//1
						if ( $oi->n_total == 0.0) {
							//error_log("ActionsController:actMoveItemToFreeAdsAction 4");
							//traženje Free Ads liste
							$free = \Baseapp\Suva\Models\Orders::findFirst( " user_id = $order->user_id and n_status = 2 ");
							if (!$free) {
								//error_log("ActionsController:actMoveItemToFreeAdsAction 5");
								// Kreiranje novog free ordera
								$free = new \Baseapp\Suva\Models\Orders();
								$free->n_set_default_values($free->getSource());
								$free->n_status = 2;
								$free->user_id = $order->user_id;
								$free->status = 2;
								$free->n_total = 0.0;
								$free->total = "0";
								$free->n_frontend_sync_status = "no_sync";
								$free->created_at = date("Y-m-d H:i:s", time());
								$free->modified_at = date("Y-m-d H:i:s", time());
								
								$success = $free->create();
								//hendlanje greske
								if ($success === true){
									//error_log("ActionsController:actMoveItemToFreeAdsAction 6");
									$greska = $free->n_after_save($request);
									if ($greska) return ($greska);
								}
								else {
									//error_log("ActionsController:actMoveItemToFreeAdsAction 7");
									$greske = $free->getMessages();
									foreach ($greske as $greska)
										$err_msg .= $greska.", ";
										Nava::greska("ERR206: <strong>Errors while creating new cart: </strong>".$err_msg);
									// return ("<strong>Errors while creating new cart: </strong>".$err_msg);
									return;
								}
							}
							//error_log("ActionsController:actMoveItemToFreeAdsAction 8");
							$oi->order_id = $free->id;
							
							//13.1.2017 - 2470 - u trenutku prebacivanja u free, trenutni datum
							if ( $oi->Product() && $oi->Product()->is_online_product == 1 && $oi->n_first_published_at < date("Y-m-d H:i:s",time()) ){
								Nava::sysMessage("Mijenja se n_first_published_at");
								$oi->n_first_published_at = date("Y-m-d H:i:s",time());
								$success = $oi->n_before_save();
							}
							
							$success = $oi->update();
							
					// 		385 - sat - time stamp kad će oglas biti objavljen. sada nije dobro. 
					// 		treba napraviti da se sat (ako je u postavkama kod plaćanja 00.00) automatski postavi s trenutkom plaćanja.
							
							if($oi && $oi->n_products_id > 0){			
								$product = \Baseapp\Suva\Models\Products::findFirst($oi->n_products_id);
	// 								//error_log(1);
								if($product && $product->is_online_product == 1){
									if($oi->n_first_published_at > 0){
										$order_item_hours = explode(" ", $oi->n_first_published_at);
										if($order_item_hours[1] == '00:00:00'){
											$order_item_hours[1] = date("H:i:s");
	// 											//error_log(2);
											$order_item_date = implode(" ",$order_item_hours);
	// 											//error_log(3);
	// 											//error_log(4);
	// 											//error_log($order_item_date);
											Nava::runSql("update orders_items set n_first_published_at = '$order_item_date' where order_id = $oi->order_id");
	// 											//error_log(5);
										}

									}
								}
							}
							if ($success === true) {
								// $okMsg .="Order Item No. $oi->id has been moved to Free Ads (No.:$free->id).".PHP_EOL;
								Nava::poruka("Order Item No. $oi->id has been moved to Free Ads (No.:$free->id).".PHP_EOL);
								$oi->n_after_save();
								
								//12.1.2017 - pokretanje obrade za aktiviranje oglasa nakon snimanja
								//\Baseapp\Suva\Library\libCron::setAdsInfoForFrontend($oi->ad_id);
							}
							else {
								//error_log("ActionsController:actMoveItemToFreeAdsAction 9");
								$greske = $oi->getMessages();
								foreach ($greske as $greska)
									// $err_msg .= $greska.", ";
								Nava::greska("ERR207: <strong>Errors while transferring order item No. $oi->id to Free ads: </strong>".$err_msg);
								// return("<strong>Errors while transferring order item No. $oi->id to Free ads: </strong>".$err_msg); 
								return;

							}
							//error_log("ActionsController:actMoveItemToFreeAdsAction 10");
						}
						else
							// $errMsg .= "Order Item No. $oi->id has total of $oi->n_total and cannot be transferred to free ads list.".PHP_EOL;
							Nava::greska("ERR208: Order Item No. $oi->id has total of $oi->n_total and cannot be transferred to free ads list.".PHP_EOL);
					}
				}
				break;
			}

			case 'actExtendFreeAds': {
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				$selectedAds = array_key_exists('ads', $params) ? $params['ads']: null;
				// Nava::poruka( "Extend Free Ads" );
				$model = new \Baseapp\Suva\Models\Model();
				if ( ! $selectedAds ){
					Nava::greska( "ERR209: Nisu odabrani oglasi za prebacivanje u besplatne." );
					return;
				}
				
				$orderFree = \Baseapp\Suva\Models\Orders::findFirst ("n_status = 2 and user_id = $userId");
				if (!$orderFree){
					return Nava::greska ("Na useru $userId nije pronađen free ads list");
				}
				
				//$arrFreeAds = $model->isAdFree ( $selectedAds, null, false );
				// Nava::sysMessage($selectedAds,"SELECTED ADS");
				$arrFreeAds = $model->isAdExtendable ( $selectedAds, null, false );
				
				// Nava::sysMessage($arrFreeAds, "Free Ads");
				$arrObradjeniOnline = array();
				$arrObradjeniOffline = array();
				foreach ( $arrFreeAds as $freeAd ){
						// if ( $freeAd['isFree'] == 'FREE' ){
					if ( $freeAd['isOnlineExt'] == 'Y' || $freeAd['isOfflineExt'] == 'Y' ){
						// Nava::sysMessage ("Ad ".$freeAd['ad_id']. " is extendable.");
						
						$oiOL = $model->OrderItem( $freeAd['oiOLP_id'] );
						$oiOF = $model->OrderItem( $freeAd['oiOFP_id'] );
						
						if ( $oiOL && $freeAd['isOnlineExt'] == 'Y'){
							if ( !array_key_exists( $freeAd['ad_id'], $arrObradjeniOnline )){
								array_push($arrObradjeniOnline[$freeAd['ad_id']] = 1);
							
							
								$oi = $oiOL;
								$product = $oiOL->Product();
								if ( $product->is_online_product ){	
									//ako je online onda se produžuje
									//prvo naći defulatni proizvod za tu publikaciju
									$defaultProductForPub = \Baseapp\Suva\Models\Products::findFirst("is_default_product = 1 and publication_id = $product->publication_id");
									if ( $defaultProductForPub ){
										//kreirat novi order item s $defaultProductForPub
										// error_log("actExtendFreeAds 1");
										$n_oi = new \Baseapp\Suva\Models\OrdersItems();
										$n_oi->n_set_default_values();
										// error_log("actExtendFreeAds 2");
										$n_oi->n_publications_id = $oi->n_publications_id;
										$n_oi->ad_id = $oi->ad_id;
										$n_oi->n_category_id = $oi->n_category_id;
										$n_oi->n_products_id = $defaultProductForPub->id;
										$n_oi->n_first_published_at = date('Y-m-d H:i:s',time());//?? 
										$n_oi->order_id = $orderFree->id;
										
										// error_log("actExtendFreeAds 3");
										//slanje parametara u before save i after save						
										$p = array();
										//$p['publications'] = $product->publication_id];
										$p['user_id'] = $userId;

										$greska = $n_oi->n_before_save(null,$loggedUserId, $p);
										// error_log("actExtendFreeAds 3 greska".$greska);
										if ($greska) {
											Nava::greska("ERR210: ".$greska); 
										} 
										// error_log("actExtendFreeAds 4");
										
										
										$result = $n_oi->create();
										if (!$result){
											//error_log("actExtendFreeAds 5");
											$greske = $n_oi->getMessages();
											foreach ($greske as $greska)  
												$err_msg .= $greska.", ";
											Nava::greska($err_msg);
											//error_log('Suva/Ads::n_after_save  create greške:'.$err_msg);
										}
										//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 6.1 nakon create:'.print_r($oi->toArray(), true));
										

										
										// error_log("actExtendFreeAds 6");
										$greska = $n_oi->n_after_save(null, $p);
										// error_log("actExtendFreeAds 7 greska".$greska);
										
										
										
										//12.1.2017 - pokretanje obrade za aktiviranje oglasa nakon snimanja
										\Baseapp\Suva\Library\libCron::setAdsInfoForFrontend($oi->ad_id);
										
										if ($greska) return $greska;
										else
										Nava::poruka("Order Item No. $n_oi->id (offline) created.");
									}
									else{
										Nava::greska("ERR212: Nije pronađen defaultni proizvod za publikaciju $product->publication_id");
									}
								}
								else {
									Nava::poruka("Proizvod $product->name na Order Itemu $oi->id nije online i nije produžen.");
								}
							}
						}
						if ( $oiOF  && $freeAd['isOfflineExt'] == 'Y'){
							if ( !array_key_exists( $freeAd['ad_id'], $arrObradjeniOffline )){
								array_push($arrObradjeniOffline[$freeAd['ad_id']] = 1);
							
								$oi = $oiOF;
								
								$product = $oi->Product();
								Nava::sysMessage("Free Offline product $product->name found on order item $oi->id");
								if ( ! $product->is_online_product ){
									$publication = $product->Publication();
									//ako je offline onda se produžuje
									//prvo naći defulatni proizvod za tu publikaciju
									$defaultProductForPub = \Baseapp\Suva\Models\Products::findFirst("is_default_product = 1 and publication_id = $publication->id");
									if ( $defaultProductForPub ){
										Nava::sysMessage("Default Offline product $product->id $product->name found for publication $publication->name ");
										//kreirat novi order item s $defaultProductForPub
										// error_log("actExtendFreeAds 1");
										//traženje next issue
										$nextIssue = $publication->getNextIssue();
										if ($nextIssue ){
											Nava::sysMessage ("Next issue $nextIssue->id on $nextIssue->date_published found for publication $publication->id");
											$n_oi = new \Baseapp\Suva\Models\OrdersItems();
											$n_oi->n_set_default_values();
											error_log("actExtendFreeAds 2");
											$n_oi->n_publications_id = $oi->n_publications_id;
											$n_oi->ad_id = $oi->ad_id;
											$n_oi->n_category_id = $oi->n_category_id;
											$n_oi->n_products_id = $defaultProductForPub->id;
											//$n_oi->n_first_published_at = date('Y-m-d H:i:s',time());//?? 
											$n_oi->order_id = $oi->order_id;
											
											// error_log("actExtendFreeAds 3");
											//slanje parametara u before save i after save						
											$p = array();
											//$p['publications'] = $product->publication_id];
											$p['user_id'] = $userId;
											
											//slanje parametara u before save i after save						
											$p = array();
											
											$p['user_id'] = $userId;
											$p['offline_issues'] = array ( $nextIssue->id );

											$greska = $n_oi->n_before_save(null,$loggedUserId, $p);
											// error_log("actExtendFreeAds 3 greska".$greska);
											if ($greska) {
												Nava::greska("ERR212: ".$greska); 
											} 
											// error_log("actExtendFreeAds 4");
											
											
											$result = $n_oi->create();
											if (!$result){
												// error_log("actExtendFreeAds 5");
												$greske = $n_oi->getMessages();
												foreach ($greske as $greska)  
													$err_msg .= $greska.", ";
												Nava::greska($err_msg);
												//error_log('Suva/Ads::n_after_save  create greške:'.$err_msg);
											}
											//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 6.1 nakon create:'.print_r($oi->toArray(), true));
											
											// error_log("actExtendFreeAds 6");
											$greska = $n_oi->n_after_save(null, $p);
											// error_log("actExtendFreeAds 7 greska".$greska);
											if ($greska) return $greska;
											else
											Nava::poruka("Order Item No. $n_oi->id (offline) created.");
										}
										else{
											Nava::greska("ERR213: Next issue for publication <b>$product->Publication()->name </b> not found.");
										}
									}
									else{
										Nava::greska("ERR214: Nije pronađen defaultni proizvod za publikaciju $product->publication_id");
									}
								}
								else {
									Nava::poruka("Proizvod $product->name na Order Itemu $oi->id nije online i nije produžen.");
								}
							}
						}
					}
					else{
						Nava::greska("ERR252: Ad ".$freeAd['ad_id']." is not extendable and was not extended. ");
						// Nava::sysMessage($freeAd, "Ad $freeAd is not extendable and was not extended. ");
						// Nava::printr($freeAd, "FREE AD DATA");
					}

				}
				break;		
			}

			case 'actDeactivateAdOglasnikOnline': {
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				$selectedAds = array_key_exists('ads', $params) ? $params['ads']: null;
				$model = new \Baseapp\Suva\Models\Model();
				if ( ! $selectedAds ){
					Nava::greska( "ERR215: Nisu odabrani oglasi za deaktivaciju." );
					return;
				}

				foreach ( $selectedAds as $adId ){
					$isDeactivate = true;
					$ad = \Baseapp\Suva\Models\Ads::findFirst($adId);
					
					if ($ad->active == 0){
						Nava::greska("ERR216: Oglas $ad->id ne može biti deaktiviran jer je već neaktivan.");
						$isDeactivate = false;
					}
					
					
					if ($isDeactivate == true){
						Nava::runSql ("update ads set active = 0, manual_deactivation = 1, n_active_online_order_item_id = null  where id = $ad->id ");
						Nava::poruka("Oglas $ad->id je deaktiviran.");
					}
				}
				break;		
				
			}
			case 'actReactivateAdOglasnikOnline': {
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				$selectedAds = array_key_exists('ads', $params) ? $params['ads']: null;
				$model = new \Baseapp\Suva\Models\Model();
				if ( ! $selectedAds ){
					Nava::greska( "ERR217: Nisu odabrani oglasi za reaktivaciju." );
					return;
				}

				foreach ( $selectedAds as $adId ){
					$isDeactivate = true;
					$ad = \Baseapp\Suva\Models\Ads::findFirst($adId);
					
					if ($ad->active == 1){
						Nava::greska("ERR218: Oglas $ad->id ne može biti reaktiviran jer je već aktivan.");
						$isDeactivate = false;
					}
					if ($ad->manual_deactivation == 0){
						Nava::greska("ERR219: Oglas $ad->id ne može biti reaktiviran jer nije ručno deaktiviran.");
						$isDeactivate = false;
					}
					
					
					if ($isDeactivate == true){
						Nava::runSql ("update ads set manual_deactivation = 0 where id = $ad->id ");
						Nava::poruka("Oglas $ad->id je reaktiviran. Promjena će biti vidljiva najkasnije za 1 minutu");
					}
				}
				break;		
				
			}
			
			case 'actResyncFrontend': {
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				$selectedAds = array_key_exists('ads', $params) ? $params['ads']: null;
				// Nava::poruka( "Extend Free Ads" );
				$model = new \Baseapp\Suva\Models\Model();
				if ( ! $selectedAds ){
					Nava::greska( "ERR220: Nisu odabrani oglasi za ponovnu sinhronizaciju." );
					return;
				}

				foreach ( $selectedAds as $adId ){
					Nava::runSql ("update ads set n_frontend_sync_status = 'unsynced' where id = $adId and n_frontend_sync_status = 'synced'");
					Nava::poruka("Oglas $adId će se ponovo sinhronizirati. Prijaviti grešku aministratoru ");
				}
				break;		
			}

			
			case 'remove_items_from_cart':
			case 'actItemDelete': {
				if (array_key_exists('orders_items', $params)) {
					$checkedOrderItems = array_key_exists('orders_items', $params) ? $params['orders_items']: null;
				}
				else{
					Nava::greska("ERR221: No orders items selected for this operation.");
				}
				if ( $checkedOrderItems ){
// 					//error_log("Ads::n_after_save 8  PREBACIVANJE ODABRANIH ORDER_ITEMA U CART");
					//PREBACIVANJE ODABRANIH ORDER_ITEMA U CART
					foreach ($checkedOrderItems as $oi_id){
						//error_log("ads2controller:  remove orderite,s. item $oi_id");
						$oi = \Baseapp\Suva\Models\OrdersItems::findFirst($oi_id);
						if (!$oi){
							Nava::greska("ERR222: Order Item No. $oi_id, selected for removal was not found.");
							return;
							// return "Order Item No. $oi_id, selected for removal was not found.";
						} 
						if ( ! $oi->Order()->n_status == 1 ){
							Nava::greska("ERR223: Order Item No. $oi_id, selected for removal belongs to order $oi->order_id, with status ".$oi->Order()->OrderStatus()->name." (instead of Cart).");
							return;
							// return "Order Item No. $oi_id, selected for removal belongs to order $oi->order_id, with status ".$oi->Order()->OrderStatus()->name." (instead of Cart).";
						}
							
						$order_id = $oi->order_id;
						Nava::runSql("delete from orders_items where id= $oi->id");
						// 03.01.2017 NIKOLA brisanje insertiona da ne bi postojali insertioni bez OI
						Nava::runSql("delete from n_insertions where orders_items_id = $oi->id");
						Nava::updateOrderTotal($order_id);
						Nava::poruka ("Order Item No. $oi->id removed from Cart");
					}
				}
				break;
			}
			
			case 'copy_orders_to_cart':
			case 'actOrderCopyToCart':{
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				if ( !$userId ) return;
				//error_log("actItemCopyToCart: 2 params['orders']:".print_r($params['orders'], true));
				if (array_key_exists('orders', $params)) {
					foreach( $params['orders'] as $oid){
						if (! $oid > 0) return ("Wrong order id received: $oid");
						$order = \Baseapp\Suva\Models\Orders::findFirst($oid);
						if (!$order) return ("Order No. $oid not found");
						$response = $order->copyToCart( $userId );
						if (!$response > 0) return $response;
					}
				}
				else{
					Nava::greska("ERR224: No orders selected for this operation.");
				}
				break;
			}

			case 'actUpdateOrderTotal':{
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				if ( !$userId ) return;
				//error_log("actUpdateOrderTotal: 2 params['orders']:".print_r($params['orders'], true));
				if (array_key_exists('orders', $params)) {
					foreach( $params['orders'] as $oid){
						Nava::updateOrderTotal($oid);
					}
				}
				else{
					Nava::greska("ERR225: No orders selected for this operation.");
				}
				break;
			}
			
			case 'copy_orders_items_to_cart':
			case 'actItemCopyToCart':{
				//error_log("actItemCopyToCart: 1");
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				if ( !$userId ) return;
				//error_log("actItemCopyToCart: 2 params['orders_items']:".print_r($params['orders_items'], true));
				if (array_key_exists('orders_items', $params)) {
					foreach( $params['orders_items'] as $oi_id ){
						if (! $oi_id > 0){
							Nava::greska("ERR226: Wrong order id received: $oi_id");
							return;
							// return ("Wrong order id received: $oi_id");
						} 
						$orderItem = \Baseapp\Suva\Models\OrdersItems::findFirst($oi_id);
						if (!$orderItem){
							Nava::greska("ERR227: orderItem No. $oi_id not found");
							return;
							// return ("orderItem No. $oi_id not found");
						} 
						//error_log("actItemCopyToCart: 2.1 before orderItem->copyToCart");
						$response = $orderItem->copyToCart( $userId );				
						if (!$response > 0){
							return $response;
						}else{
							Nava::poruka("Order item No .$orderItem->id succesfully copied to cart");
						} 
					}
				}
				else{
					Nava::greska("ERR228: No orders items selected for this operation.");
				}
				break;
			}
			
			case 'actOrderStatusFaktura': {
				$selectedUserId = array_key_exists('selected_user_id', $params) ? $params['selected_user_id']: null;
				$selectedUserId = $selectedUserId > 0 ? $selectedUserId : ' null ';
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				$loggedUserId = $loggedUserId > 0 ? $loggedUserId : ' null ';
				if (! $loggedUserId ){
					Nava::greska("ERR229: Nije poslan parametar Logged User Id");
					return;
				}
				
				$defaultToInvoiceFiscalLocation = \Baseapp\Suva\Library\libAppSetting::get('fin_default_to_invoice_fiscal_location') ;				
				if(!$defaultToInvoiceFiscalLocation) $defaultToInvoiceFiscalLocation = null;
				$selectedFiscalLocation = array_key_exists('_fiscal_location', $_SESSION['_context']) ? $_SESSION['_context']['_fiscal_location'] : null; 
				if( !$selectedFiscalLocation )
					return Nava::greska("Niste odabrali fiskalnu lokaciju");					
				else if($selectedFiscalLocation != $defaultToInvoiceFiscalLocation)
					return Nava::greska("Da bi prebacili offer u invoice potrebno je izabrati fiskalnu lokaciju $defaultToInvoiceFiscalLocation");
					
				
				
				//TODO popreviti, vraća null umjesto 17
				// $paymentMethodId = \Baseapp\Suva\Library\libAppSetting::get('fin_default_unpaid_invoice_payment_method_id');
				// if (! $paymentMethodId > 0 ){
					// return Nava::greska("Wrong payment method ID ($paymentMethodId) supplied in app parameter fin_default_unpaid_invoice_payment_method_id");
				// }
				
				
			// foreach ($params['orders'] as $oid){
				
				// $order = \Baseapp\Suva\Models\Orders::findFirst($oid); 
				// if (!$order) return;
				
				// $userId = array_key_exists('user_id', $params) ? $params['user_id']: null; 
				// if (!$userId) return;
				
				// //19 - Ako na košarici postoje samo besplatni oglasi, mogu ići samo na Besplatni Oglas (ne u Ponudu i Fakturu). Ako postoje miješano, mogu ići na ponudu i u fakturu zajedno s besplatnima
				// //OVO VIŠE NE VAŽI, SAMO PONUDA MOŽE IĆI U FAKTURU
				
				// //134 - dodati novo polje operator_id koje će kod akcije StatusFaktura ubaciti ID trenutno spojenog korisnika
				// //status = 2 - completed, njih se automatski šalje na fiskalizaciju 
				// Nava::runSql("update orders set n_status = 4, status = 2, modified_at = NOW(), n_operator_id=$userId where id = $order->id ");    
			// }
					
                //error_log('Nava::actOrderStatusFaktura 1');
				foreach ($params['orders'] as $oid){		
                    //error_log('Nava::actOrderStatusFaktura 2');
					$id = $oid > 0 ? $oid : -1;
					$order = \Baseapp\Suva\Models\Orders::findFirst( $id ); 
					if (!$order) {
						Nava::sysMessage("Order $oid not found for function actOrderStatusFaktura");
						return;
					}
					//error_log('Nava::actOrderStatusFaktura 3');
					$userId = array_key_exists('user_id', $params) ? $params['user_id']: null; 
					if (!$userId) {
						Nava::sysMessage("User not supplied for function actOrderStatusFaktura");
						return;
					}
					$user = \Baseapp\Suva\Models\Users::findFirst( $userId );
					if ( $user->n_payment_type_id == 1 ){
						$pt = $user->PaymentType();
						Nava::greska("ERR230: User $user->username has payment type $pt->name. Invoice not created.");
						return;
					}
                    
					//error_log('Nava::actOrderStatusFaktura 4');
					//134 - dodati novo polje operator_id koje će kod akcije StatusFaktura ubaciti ID trenutno spojenog korisnika
					//status = 2 - completed, njih se automatski šalje na fiskalizaciju 
					
					//13.1.2017 - 2470 - u trenutku prebacivanja u invoice, trenutni datum
					$jeGreske = false;
					foreach ($order->OrdersItems() as $oi) {
						if ( $oi->Product() && $oi->Product()->is_online_product == 1 && $oi->n_first_published_at < date("Y-m-d H:i:s",time()) ){
							
							// Nava::poruka("Mijenja se n_first_published_at na order itemu $oi->id, trenutni je $oi->n_first_published_at a treba biti ".date("Y-m-d H:i:s",time()));
							Nava::poruka("Mijenja se n_first_published_at na order itemu $oi->id, u trenutno vrijeme".date("Y-m-d H:i:s",time()));
							$oi->n_first_published_at = date("Y-m-d H:i:s",time());
							$greska = $oi->n_before_save();
							if ($greska) return Nava::greska("ERR253: GREŠKA KOD MIJENJANJA DATUMA NA ORDER ITEMU $greska");
							$success = $oi->update();
							if ($success === true) {
								Nava::sysMessage("Order item $oi->id updated");
							}
							else {
								$greske = $oi->getMessages();
								foreach ($greske as $greska)
								Nava::greska("ERR252: <strong>Errors while updating order item No. $oi->id: </strong>".$err_msg);
								$jeGreske = true;
							}				
						}
					}
					if ($jeGreske ){
						return null;
					}
					
					//IGOR: ŠTA OVO RADI? - mijenja status u Invoice
					Nava::runSql("
						update orders 
						set n_status = 4
						,status = 2
						,n_payment_methods_id = 17
						,n_fiscal_location_id = $defaultToInvoiceFiscalLocation
						,modified_at = NOW()
						,n_operator_id= $loggedUserId 
						where cast(total as decimal) > 0 and id = $oid
						");    
					$order->updateAdLatestPaymentState();
					
					//12.1.2017 - pokretanje obrade za aktiviranje oglasa nakon snimanja
					\Baseapp\Suva\Library\libCron::setAdsInfoForFrontend($oi->ad_id);
					
					$result = \Baseapp\Suva\Library\libNavision::send_order_to_nav($order->id);
					$navResponse = \Baseapp\Suva\Library\libNavision::waitForFiscalization($order->id);
			
					if ($navResponse){
						$jir = $navResponse[0]['Unique Invoice No_'] > '' ? $navResponse[0]['Unique Invoice No_'] : null;
						$zik = $navResponse[0]['Issuer Protection Code'] > '' ? $navResponse[0]['Issuer Protection Code'] : null;
						// $invoiceNo = $navResponse['Invoice No_'] > '' ? $navResponse['Invoice No_'] : null;
						
						//5.1.2017
						$n_invoice_fiscal_number = $navResponse[0]['Fisc_ Doc_ No_']."-".$navResponse[0]['Fisc_ Location Code']."-".$navResponse[0]['Fisc_ Wholesale Terminal'];
						
						//ne gleda se isfiscalized nego se salje u svakom slucaju
						$isFiscalized = $order->PaymentMethod()->is_fiscalized == 1 ? true : false;
						//9.1.2017
						//AKO JE U STATUSU 4, ISFISCALIZED = 1 ILI 0 I IMA FISCAL_NUMBER ONDA STAVI STATUS SYNCED
						//INACE STATUS RETRY
						if ($isFiscalized == false && $n_invoice_fiscal_number){
							//sve je OK
							// $order->n_invoice_zik = $zik;
							// $order->n_invoice_jir = $jir;
							$order->n_invoice_fiscal_number = $n_invoice_fiscal_number;
							$order->n_nav_sync_status = 'synced';
							$order->update();
						}
						else{
							Nava::greska ("ERR231: Greška pri fiskalizaciji: Order:$order->id, Is Fiscalized: $isFiscalized, Invoice No: $invoiceNo, JIR: $jir, ZKI: $zik");
						}
					}else{
						$order->n_nav_sync_status = 'retry';
						$order->update();
						Nava::greska ("ERR232: Greška pri fiskalizaciji, Navision nije uspio fiskalizirati fakturu: Order:$order->id");
					}
						Nava::poruka("The status of Order No. $oid has been changed to 'Invoice");
				}
				break; 
			}   
			
			case 'actOrderPaidWithCcMaster':{
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				if (!$userId) {
					Nava::sysMessage("User ID not supplied to actOrderPaidWithCcMaster action ");
					return;
				}
				
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				if (!$loggedUserId) {
					Nava::sysMessage("11 Logged-in User ID not supplied to actOrderPaidWithCcMaster action ");
					return;
				}

				
				foreach ($params['orders'] as $oid){
					//Nava::actOrderPayWithMethod('ECMC', $oid, $userId, $loggedUserId);
					//Nava::poruka("Invoice $oid paid with Master. ");
					
					$greska = self::actOrderPayWithMethod('ECMC', $oid, $userId, $loggedUserId);
					if ( !$greska ){
						Nava::poruka("Invoice $oid paid with Mastercard. ");
						// \Baseapp\Suva\Library\libNavision::getJirZikForOrder($oid);
					}												
					else {
						Nava::greska("ERR233: Invoice not paid due to errors. $greska");
						//Nava::poruka("Invoice not paid due to errors. 2");
					}
				}
				break;
			}
			case 'actOrderPaidWithCcMaestro':{
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				if (!$userId) {
					Nava::sysMessage("User ID not supplied to actOrderPaidWithCcMaestro action ");
					return;
				}
				
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				if (!$loggedUserId) {
					Nava::sysMessage("11 Logged-in User ID not supplied to actOrderPaidWithCcMaestro action ");
					return;
				}

				
				foreach ($params['orders'] as $oid){
					//Nava::actOrderPayWithMethod('ECMC', $oid, $userId, $loggedUserId);
					//Nava::poruka("Invoice $oid paid with Master. ");
					
					$greska = self::actOrderPayWithMethod('MAESTRO', $oid, $userId, $loggedUserId);
					if ( !$greska ){
						Nava::poruka("Invoice $oid paid with Maestro. ");
						// \Baseapp\Suva\Library\libNavision::getJirZikForOrder($oid);
					}					
					else {
						Nava::greska("ERR235: Invoice not paid due to errors. $greska");
						//Nava::poruka("Invoice not paid due to errors. 2");
					}
				}
				break;
			}
			case 'actOrderPaidWithVirman':{
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				if (!$userId) {
					Nava::sysMessage("User ID not supplied to actOrderPayWithVirman action ");
					return;
				}
				
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				if (!$loggedUserId) {
					Nava::sysMessage("11 Logged-in User ID not supplied to actOrderPayWithVirman action ");
					return;
				}

				
				foreach ($params['orders'] as $oid){
					//Nava::actOrderPayWithMethod('ECMC', $oid, $userId, $loggedUserId);
					//Nava::poruka("Invoice $oid paid with Master. ");
					
					//10.1 PROVJERA DA LI JE UNESEN PAYMENT DATE KOD PLACANJA S VIRMANOM
					
					$order = \Baseapp\Suva\Models\Orders::findFirst($oid);
					
					if(strlen($order->n_payment_date) > 2){
						$greska = self::actOrderPayWithMethod('VIRMAN', $oid, $userId, $loggedUserId, true,$order->n_payment_date );
						if ( !$greska ){
							Nava::poruka2("Invoice %s paid with VIRMAN. ", $oid);
							// \Baseapp\Suva\Library\libNavision::getJirZikForOrder($oid);
						}						
						else {
							Nava::greska("ERR237: Invoice not paid due to errors.  $greska");
							//Nava::poruka("Invoice not paid due to errors. 2");
						}
					}
					else{
						Nava::greska('ERR236: Niste unijeli datum plaćanja na orderu');
					}
				}
				break;
			}
			
			case 'actOrderPaidWithCcVisa':{
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				if (!$userId) {
					Nava::sysMessage("User ID not supplied to actOrderPaidWithCcVisa action ");
					return;
				}
				
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				if (!$loggedUserId) {
					Nava::sysMessage("11 Logged-in User ID not supplied to actOrderPaidWithCcVisa action ");
					return;
				}

				
				foreach ($params['orders'] as $oid){
					//Nava::actOrderPayWithMethod('VISA', $oid, $userId, $loggedUserId);
					//Nava::poruka("Invoice $oid paid with Master. ");
					
					$greska = self::actOrderPayWithMethod('VISA', $oid, $userId, $loggedUserId);
					if ( !$greska ){
						Nava::poruka("Invoice $oid paid with Visa. ");
						// \Baseapp\Suva\Library\libNavision::getJirZikForOrder($oid);
					}						
					else {
						Nava::greska("ERR238: Invoice not paid due to errors. $greska");
						//Nava::poruka("Invoice not paid due to errors. 2");
					}
				}
				break;
			}
			
			case 'actOrderPaidWithCcAmex':{
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				if (!$userId) {
					Nava::sysMessage("User ID not supplied to actOrderPaidWithCcAmex action ");
					return;
				}
				
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				if (!$loggedUserId) {
					Nava::sysMessage("11 Logged-in User ID not supplied to actOrderPaidWithCcAmex action ");
					return;
				}

				
				foreach ($params['orders'] as $oid){
					//Nava::actOrderPayWithMethod('AMEX', $oid, $userId, $loggedUserId);
					//Nava::poruka("Invoice $oid paid with Master. ");
					
					$greska = self::actOrderPayWithMethod('AMEX', $oid, $userId, $loggedUserId);
					if ( !$greska ){
						Nava::poruka("Invoice $oid paid with Amex. ");
						// \Baseapp\Suva\Library\libNavision::getJirZikForOrder($oid);
					}					
					else {
						Nava::greska("ERR239: Invoice not paid due to errors. $greska");
						//Nava::poruka("Invoice not paid due to errors. 2");
					}
				}
				break;
			}
			case 'actOrderPaidWithCcDiners':{
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				if (!$userId) {
					Nava::sysMessage("User ID not supplied to actOrderPaidWithCcDiners action ");
					return;
				}
				
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				if (!$loggedUserId) {
					Nava::sysMessage("11 Logged-in User ID not supplied to actOrderPaidWithCcDiners action ");
					return;
				}

				
				foreach ($params['orders'] as $oid){
					//Nava::actOrderPayWithMethod('DINERS', $oid, $userId, $loggedUserId);
					//Nava::poruka("Invoice $oid paid with Master. ");
					
					$greska = self::actOrderPayWithMethod('DINERS', $oid, $userId, $loggedUserId);
					if ( !$greska ){
						Nava::poruka("Invoice $oid paid with Diners. ");
						// \Baseapp\Suva\Library\libNavision::getJirZikForOrder($oid);
					}					
					else {
						Nava::greska("ERR240: Invoice not paid due to errors. $greska");
						//Nava::poruka("Invoice not paid due to errors. 2");
					}
				}
				break;
			}
			
			case 'actOrderPaidWithCash':{
				//error_log('Nava::actOrderPaidWithCash 1');
				$userId = array_key_exists('user_id', $params) ? $params['user_id']: null;
				if (!$userId) {
					//error_log('Nava::actOrderPaidWithCash 2');
					Nava::sysMessage("User ID not supplied to actOrderPaidWithCash action ");
					return;
				}
				
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				if (!$loggedUserId) {
					Nava::sysMessage("11 Logged-in User ID not supplied to actOrderPaidWithCcMaster action ");
					return;
				}
				
				foreach ($params['orders'] as $oid){
					//error_log('Nava::actOrderPaidWithCash 3');
					$greska = self::actOrderPayWithMethod('GOTOVINA', $oid, $userId, $loggedUserId);
					if ( !$greska ){
						Nava::poruka("Invoice $oid paid with Cash. ");
						// \Baseapp\Suva\Library\libNavision::getJirZikForOrder($oid);
					}						
					else {
						Nava::greska("ERR241: Invoice not paid due to errors. $greska");
						//Nava::poruka("Invoice not paid due to errors. 2");
					}
				}
				break;
			}
						
			case 'actOrderStatusPlaceno':{
				foreach ($params['orders'] as $oid){
					Nava::actOrderStatusPlaceno( $oid, date ("Y-m-d", time()) );
				Nava::poruka("Invoice $oid has been moved to Paid. ");
				}
				break;
			}
			
			case 'actOrderStatusKasnjenje':{
				//error_log('actOrderStatusKasnjenje 1');
				foreach( $params['orders'] as $oid){
					//error_log('actOrderStatusKasnjenje 2');
					$order = \Baseapp\Suva\Models\Orders::findFirst($oid);
					if (!$order){
						$this->sysMessage ("ERROR: actOrderStatusKasnjenje: order No. $oid not found.");
						return;
					}
					
					
					if ( $order && $order->n_status != 5  ) {
						
						//$this->greska(" Order $order->id nije placen i ne moze ici u LATE PAID ");
						Nava::greska("ERR242: Order $order->id ne moze ici u LATE PAID jer nije placen! ");
						return;
						
					}
					$order->n_status = 9;
					$order->status = 2;
					$order->update();
					$order->updateAdLatestPaymentState();
					Nava::poruka("The status of Order No. $oid has been changed to 'Late Paid'");
					// Nava::runSql("update orders set n_status = 9, status = 4, modified_at = NOW() where id =".$oid);    
				}
				break;
			}
	
			case 'add_insertions': {
				// error_log("add_insertions1");
				//Nava::greska("add_insertions11111");
				// Nava::sysMessage($_REQUEST);
				$loggedUserId = array_key_exists('logged_user_id', $params) ? $params['logged_user_id']: null;
				if (!$loggedUserId) {
					Nava::sysMessage("11 Logged-in User ID not supplied to add_insertions action ");
					return;
				}
				// else
					// Nava::poruka ("logged user id $loggedUserId");
				
				// error_log('Suva/Ads::n_after_save clickedAction case_clicked_action add_insertions');
				$adId = $request->hasPost('ad_id') ? $request->getPost('ad_id') : null;
				
				if ( $adId ) $ad = \Baseapp\Suva\Models\Ads::findFirst(  $adId );
				else {
					Nava::sysMessage ("Parameter ad_id not supplied in request ");
					return;
				}
				if (!$ad){
					Nava::greska ("ERR243:  Ad $adId not found ");
					return;
				}
				
				$checkedPublications = $request->hasPost('_checked_publications') ? $request->getPost('_checked_publications') : null;
				$checkedDates = $request->hasPost('_checked_dates') ? $request->getPost('_checked_dates') : null;
				if ($checkedPublications || $checkedDates){
					//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 2');
// 					//error_log("Suva/Ads::n_after_save 2 ");
					//DODAVANJE ORDER ITEMA NA TEMELJU KLIKNUTIH PUBLIKACIJA
					$checkedPublications = $request->hasPost('_checked_publications') ? $request->getPost('_checked_publications') : null;
					$checkedDates = $request->hasPost('_checked_dates') ? $request->getPost('_checked_dates') : null;
					$checkedProducts = $request->hasPost('_checked_products') ? $request->getPost('_checked_products') : null;
					$checkedDiscounts = $request->hasPost('_checked_discounts') ? $request->getPost('_checked_discounts') : null;

					// offline izdanja - preko checked_publications
					if ($checkedPublications) {
						//error_log('Ads::n_after_save clickedAction case_clicked_action 3');
// 						//error_log("Ads::n_after_save 3 ");
						foreach($checkedPublications as $key => $value){
							//return 'id publikacije';
							//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 4');
							if ($checkedProducts){
								
								
								//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 5');
								$oi = new \Baseapp\Suva\Models\OrdersItems();
								$oi->n_set_default_values();
								
								$oi->n_publications_id = $key;
								$oi->ad_id = $ad->id;
								$oi->n_category_id = $ad->category_id;
								$oi->n_products_id = $checkedProducts[$key];
								$oi->n_frontend_sync_status = 'no_sync';
								$oi->n_ad_taker_id = $loggedUserId;
								//error_log('order item n_products_id : ' .$oi->n_products_id);
								
								//slanje parametara u before save i after save						
								$p = array();
								$p['publications'] = [$key];
								$p['user_id'] = $ad->user_id;
								$p['offline_issues'] = $value;
								if ($checkedDiscounts){
									$p['offline_discounts'] = $checkedDiscounts[$key];
								} 

								
								//n_before_save briše product_id

								$greska = $oi->n_before_save(null,isset($pLoggedUserId), $p);
								if ($greska) return $greska; 
								
								//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 6 prije create:'.print_r($oi->toArray(), true));
								
								$result = $oi->create();
								if (!$result){
									$greske = $oi->getMessages();
									if($greske){
										foreach ($greske as $greska)  
											$err_msg .= $greska.", ";
									}
									//Nava::greska(isset($err_msg));
									//error_log('Suva/Ads::n_after_save  create greške:'.$err_msg);
								}

								$greska = $oi->n_after_save(null, $p);
								if ($greska) return $greska;
								else Nava::poruka("Order Item No. $oi->id (offline) created.");
								
							}else{
								Nava::greska( "ERR243: Odabrana je publikacija ".\Baseapp\Suva\Models\Publications::findFirst($key)->name." na kojoj nije odabran proizvod");
							}
						} 
					}
					//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 7');
					
					//online izdanja - preko checked_dates  key-publication_id,  value-n_first_published_at 
					if ($checkedDates) {
						// Nava::poruka("ONLINE IZDANJA");
						//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 8');
// 						//error_log("Ads::n_after_save 4 ");
						foreach($checkedDates as $key => $value){ 
							$allowAddProduct = true;
							//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 9');
// 							//error_log("Ads::n_after_save 4.1 key $key, value $value");
							//uvijek šalje checked_dates,(defaultne datume), TREBA BITI CHECKIRAN I PROIZVOD
							if (array_key_exists($key, $checkedProducts)){
								//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 10');
// 								//error_log("Ads::n_after_save 4.5 array_key_exists($key, checkedProducts) ");
								//ako je checkiran pushun proizvod
								$checkedProductId = $checkedProducts[$key];
								$checkedProduct = \Baseapp\Suva\Models\Products::findFirst ($checkedProductId);
								$checkedProductPublication = $checkedProduct->Publication();

								//logika za pushup proizvode
								if ($checkedProduct->is_pushup_product){
									$isPushupAllowed = false;
									
									//trazenje trenutno aktivnog order itema
									$strNow = date("Y-m-d H:i:s",time());
		
									$txtWhere = "ad_id = $ad->id and n_first_published_at <= '$strNow' and n_expires_at > '$strNow'";
									Nava::poruka($txtWhere);
									$ois = \Baseapp\Suva\Models\OrdersItems::find($txtWhere);
									
									foreach($ois as $oi){
										// Nava::poruka("Order item $oi->id");
										$oiProduct = $oi->n_products_id > 0 ? $oi->Product() : null;
										//Nava::poruka("oiProduct $oiProduct->id ");
										if ($oiProduct && $oiProduct->is_online_product == 1 && $oiProduct->is_pushup_product == 0 ){
											// Nava::poruka("sve ok");
											$isPushupAllowed = true;
										}
										
										// if ($oiProduct && $oiProduct->is_online_product == 1 && strpos($oiProduct->name,'Istaknuti') || strpos($oiProduct->name,'Premium')){
											// //Nava::poruka("sve ok");
											// $isPushupAllowed = true;
										// }
										
										
									}
									if (!$isPushupAllowed ){
										Nava::greska("ERR244: Ne može se dodati pushup proizvod $oiProduct->name jer ne postoji trenutno aktivni online proizvod na publikaciji $checkedProductPublication->name ");
										$allowAddProduct = false;
										
									}
								}



								//logika za naslovnica proizvode
								// Nava::poruka("prije naslovnice");
								if ($checkedProduct->is_naslovnica){
									// Nava::poruka("Poslije naslovnice");
									$isNaslovnicaAllowed = false;
									
									//trazenje trenutno aktivnog order itema
									$strNow = date("Y-m-d h:i:s",time());
									$txtWhere = "ad_id = $ad->id and n_first_published_at < '$strNow' and n_expires_at > '$strNow 23:59:59'";
									Nava::poruka($txtWhere);
									$ois = \Baseapp\Suva\Models\OrdersItems::find($txtWhere);
									
									foreach($ois as $oi){
										Nava::poruka("Order item $oi->id");
										$oiProduct = $oi->n_products_id > 0 ? $oi->Product() : null;
										Nava::poruka("oiProduct $oiProduct->id ");
										//if ( $oiProduct && $oiProduct->is_online_product == 1 && $oiProduct->is_naslovnica_allowed == 1 && (abs(strtotime($oi->n_expires_at) - time())/60/60/24) > $oiProduct->days_published  ){
										if ( $oiProduct && $oiProduct->is_online_product == 1 && $oiProduct->is_naslovnica_allowed == 1 ){
											Nava::poruka("sve ok naslovnica");
											
											$isNaslovnicaAllowed = true;
										}
									}
									if (!$isNaslovnicaAllowed ){
										Nava::greska("ERR245: Ne može se dodati naslovnica jer ne postoji trenutno aktivni online proizvod na publikaciji $checkedProductPublication->name koji dopušta naslovnicu ili proizvod koji želite dodati traje dulje od trajanja oglasa!");
										$allowAddProduct = false;
									}
								}
								
								// //NIKOLA dodavanje product sort na ad s proizvoda
								// $strNow = date("Y-m-d h:i:s",time());
								// $txtWhere = "ad_id = $ad->id and n_first_published_at < '$strNow' and n_expires_at > '$strNow 23:59:59'";
								// //Nava::poruka($txtWhere);
								// $ois = \Baseapp\Suva\Models\OrdersItems::find($txtWhere);
																	
								// foreach($ois as $oi){
										// $max = 0;
										// $min = 9999;
										// if( $oi->n_is_active == 1 ) {
										// // if( $oi->n_expires_at > $max && $oi->n_is_active == 1 ) {
											// // $max = $oi->n_expires_at;
											// $oiID = $oi->n_products_id;
											// $oiProduct = $oiID > 0 ? $oi->Product($oiID) : null;
											// if( $oiProduct->priority_online < $min && $oiProduct->is_online_product == 1 )
												// $min = $oiProduct->priority_online;
											// error_log("MIN --- ".$min);
										// }	
										
										// Nava::poruka("Order item $oi->id");
										// //$oiProduct = $oiID > 0 ? $oi->Product($oiID) : null;
										// Nava::poruka("oiProduct $oiProduct->id ");
										// Nava::poruka("AD PRODUCT SORT : $ad->product_sort OI_PRODUCT_PRIORITY : $oiProduct->priority_online");
										// if ( $oiProduct && $oiProduct->is_online_product == 1 )
										
											// // $ad->product_sort = $oiProduct->priority_online;
											// error_log("MINIMUM ".$min);
											// $ad->product_sort = $min;
											// // Nava::runSql ("update ads set active = 0 where id = $ad->id ");
								// }
													
								
								
								if ($allowAddProduct){
									// Nava::poruka("allowAddProduct $allowAddProduct ");
									// error_log('libUserActions::$allowAddProduct 1');
									
									
									//Nava::poruka("aaaaa");
									$oi = new \Baseapp\Suva\Models\OrdersItems();
									$oi->n_set_default_values();
									$oi->n_publications_id = $key;
									$oi->ad_id = $ad->id;
									$oi->n_category_id = $ad->category_id;
									$oi->n_products_id = $checkedProducts[$key];
									//$currentTime = substr(date('Y-m-d H:i:s',time()), strpos(date('Y-m-d H:i:s',time()), " ") + 1);    
									$currentTime = date('H:i:s',time());    
									
									//na Add Insertions se dodaje samo datum, bez  vremena
									$requestDate = $ad->n_interpret_value_from_request(trim($value), 'date');
									$oi->n_first_published_at = $requestDate." ".$currentTime;
									//error_log("VRIME1   =   ".$oi->n_first_published_at);
									//error_log("VRIME2   =   ".$value);
									$oi->n_ad_taker_id = $loggedUserId;
									// error_log('libUserActions::$allowAddProduct 2');
									//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 14');
									$p = array();
									$p['publications'] = [$key];
									$p['user_id'] = $ad->user_id;
									$p['offline_issues'] = $value;
									if ($checkedDiscounts) $p['offline_discounts'] = $checkedDiscounts[$key];
									// error_log('libUserActions::$allowAddProduct 3');
									
									
									
															
									
									$greska = $oi->n_before_save(null, isset($pLoggedUserId), $p);
									if ($greska) {
										// error_log('libUserActions::$allowAddProduct 4');
										//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 15');
	// 									//error_log("Ads::n_after_save 4.6 greška u oi->n_before_save:".$greska);
										Nava::greska("ERR246: ".$greska);
										return $greska;
									} else 
	// 									//error_log("Ads::n_after_save 4.7 NOT greska oi->n_before_save");
									
									//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 16');
									$result = $oi->create();	
	// 								//error_log("Ads::n_after_save 4.7.1 result class:".get_class($result).", od->id:".$oi->id);
									$greska = $oi->n_after_save(null, $p);
									// error_log('libUserActions::$allowAddProduct 5');
									if ($greska) {
										//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 17');
	// 									//error_log("Ads::n_after_save 4.8 greška u oi->n_after_save:".$greska);
										Nava::greska("ERR247: ".$greska);
										return $greska;
									} 
									else {
										
										// error_log('libUserActions::$allowAddProduct 6');
										// $okMsg .= "Order Item No. $oi->id (online) created.".PHP_EOL;
										Nava::poruka("Order Item No. $oi->id (online) created.");
										//error_log('Suva/Ads::n_after_save clickedAction case_clicked_action 18');
										//ad ne postaje aktivan samo zato jer je dodan order item. 
										//Trebalo bi na order itemu stavit da je ad aktivan
										
										//Nava::runSql("update ads set active = 1 where id =".$this->id); 
	// 									//error_log("Ads::n_after_save 4.9 NOT greska oi->n_after_save");
									}
								}
							}
						}
					}
				}
				break;
			}
				
			case 'add_insertions_for_ad': {
				$checkedPublications = $request->hasPost('_checked_publications2') ? $request->getPost('_checked_publications2') : null;
				$checkedDates = $request->hasPost('_checked_dates2') ? $request->getPost('_checked_dates2') : null;
				if ($checkedPublications || $checkedDates){
					//error_log("Ads::n_after_save 2 ");
					//DODAVANJE ORDER ITEMA NA TEMELJU KLIKNUTIH PUBLIKACIJA
					$checkedPublications = $request->hasPost('_checked_publications2') ? $request->getPost('_checked_publications2') : null;
					$checkedDates = $request->hasPost('_checked_dates2') ? $request->getPost('_checked_dates2') : null;
					$checkedProducts = $request->hasPost('_checked_products2') ? $request->getPost('_checked_products2') : null;
					$checkedDiscounts = $request->hasPost('_checked_discounts2') ? $request->getPost('_checked_discounts2') : null;
					$selectedAds = $request->hasPost('_users_ads') ? $request->getPost('_users_ads') : null;
					// offline izdanja - preko checked_publications
					if ($checkedPublications) {
						//error_log("Ads::n_after_save 3 ");
						foreach($checkedPublications as $key => $value){
							//return 'id publikacije';
							foreach($selectedAds as $adId){
								if ($checkedProducts){
									$oi = new \Baseapp\Suva\Models\OrdersItems();
									$oi->n_set_default_values();
									$oi->n_publications_id = $key;
	// 								$oi->ad_id = $this->id;
									$oi->n_category_id = $this->category_id;
									$oi->n_products_id = $checkedProducts[$key];

									$oi->ad_id = $adId;


										//slanje parametara u before save i after save						
									$p = array();
									$p['publications'] = [$key];
									$p['user_id'] = $this->user_id;
									$p['offline_issues'] = $value;
									if ($checkedDiscounts) $p['offline_discounts'] = $checkedDiscounts[$key];

									$greska = $oi->n_before_save(null,$pLoggedUserId, $p);
									if ($greska) return $greska; 

									$result = $oi->create();						 
									$greska = $oi->n_after_save(null, $p);
									if ($greska) return $greska;
									else $okMsg .= "Order Item No. $oi->id (offline) created.".PHP_EOL;

								}else{
									return "Odabrana je publikacija ".\Baseapp\Suva\Models\Publications::findFirst($key)->name." na kojoj nije odabran proizvod";
								}
							}
						} 
					}
					
					//online izdanja - preko checked_dates  key-publication_id,  value-n_first_published_at 
					if ($checkedDates) {
						//error_log("Ads::n_after_save 4 ");
						foreach($checkedDates as $key => $value){ 
							///error_log("Ads::n_after_save 4.1 key $key, value $value");
							//uvijek šalje checked_dates,(defaultne datume), TREBA BITI CHECKIRAN I PROIZVOD
							foreach($selectedAds as $adId){
								
								if (array_key_exists($key, $checkedProducts)){
									// error_log("Ads::n_after_save 4.5 array_key_exists($key, checkedProducts) ");
									$oi = new \Baseapp\Suva\Models\OrdersItems();
									$oi->n_set_default_values();
									$oi->n_publications_id = $key;
	// 								$oi->ad_id = $this->id;
									$oi->n_category_id = $this->category_id;
									$oi->n_products_id = $checkedProducts[$key];
									//$txtDatum = date("Y-m-d H:i:s");
									$oi->n_first_published_at = $this->n_interpret_value_from_request(trim($value), 'date');
									 //$oi->n_first_published_at = $txtDatum;

									$oi->ad_id = $adId;
									
									//error_log("Ads::n_after_save 4.5.1  oi->n_first_published_at: $oi->n_first_published_at");
									//slanje parametara u before save i after save		 				
									$p = array();
									$p['publications'] = [$key];
									$p['user_id'] = $this->user_id;
									$p['offline_issues'] = $value;
									if ($checkedDiscounts) $p['offline_discounts'] = $checkedDiscounts[$key];

									$greska = $oi->n_before_save(null,$pLoggedUserId, $p);

									if ($greska) {
										error_log("Ads::n_after_save 4.6 greška u oi->n_before_save:".$greska);
										return $greska;
									} else 	
										error_log("Ads::n_after_save 4.7 NOT greska oi->n_before_save");


									$result = $oi->create();	
									//error_log("Ads::n_after_save 4.7.1 result class:".get_class($result).", od->id:".$oi->id);
									$greska = $oi->n_after_save(null, $p);
								}
								if ($greska) {
									// error_log("Ads::n_after_save 4.8 greška u oi->n_after_save:".$greska);
									return $greska;
								} 
								else{
									$okMsg .= "Order Item No. $oi->id (online) created.".PHP_EOL;
									// error_log("Ads::n_after_save 4.9 NOT greska oi->n_after_save");
								}
							
							}
						}
					}
				}
				break;
			}
	
			case 'lock_insertions': {
				$model = new \Baseapp\Suva\Models\Model();
				$selectedInsertions = $request->hasPost('_users_insertions') ? $request->getPost('_users_insertions') : null;
				if (!array_key_exists('_context', $_SESSION)) $_SESSION['_context'] = array();
				$_SESSION['_context']['orderPdf_report_order_ids'] = $selectedInsertions;
// 				//error_log("Ads::n_after_save preview_pdf  selectedInsertions:".print_r($selectedInsertions, true));
				foreach( $selectedInsertions as $insId ) {
					$ins = $model->Insertion($insId);
					if ($ins){
						$issue = $ins->Issue();
						//418 - lock insertion: lock mogu dobiti samo insertioni u statusu deadline i prima. ukoliko je objavljeno, nema lock.
						if ($issue->status == 'prima' || $issue->status == 'deadline'){
							$ins->published_status = 'lock';
							$ins->update();
						}
						else{
							Nava::greska("ERR248: Insertion $ins->id (issue $issue->id $issue->date_published ) not locked because issue is in state $issue->status ");
						}
					}	
				}
				break;
			}
			
			case 'unlock_insertions': {
				$model = new \Baseapp\Suva\Models\Model();
				$selectedInsertions = $request->hasPost('_users_insertions') ? $request->getPost('_users_insertions') : null;
// 				//error_log("Ads::n_after_save preview_pdf  selectedInsertions:".print_r($selectedInsertions, true));
				foreach( $selectedInsertions as $insId ) {
					$ins = $model->Insertion($insId);
					if ($ins){
						$ins->published_status = 'ok';
						$ins->update();
					}	
				}
				break;
			}
	} //switch	

		
	}//function clickedActions
	
	
	public static function actOrderPayWithMethod ( $pMethod = null, $pOrdersId = null, $pUserId = null, $pLoggedUserId = null, $pIsFiscalizeImmediately = true, $pPaymentDate = null ){
		// error_log(1111);
		if (!$pMethod) {
			// error_log('Nava::actOrderPayWithMethod 1');
			Nava::sysMessage("Payment method not supplied to actOrderPayWithMethod action ");
			return  true;
		}
		if(!$pOrdersId){
			// error_log('Nava::actOrderPayWithMethod 2');
			Nava::sysMessage("Order ID not supplied ");
			return true;
		}
		if(!$pUserId){
			// error_log('Nava::actOrderPayWithMethod 3');
			Nava::sysMessage("User ID not supplied ");
			return true;
		}
		if(!$pLoggedUserId){
			// error_log('Nava::actOrderPayWithMethod 4');
			Nava::sysMessage("33 Logged-in User ID not supplied ");
			return true;
		}
		
		// error_log("libUserActions::actOrderPayWithMethod payment date ".$pPaymentDate);
		// error_log("libUserActions::actOrderPayWithMethod payment date TYPE ".gettype($pPaymentDate));
		
		if (!$pPaymentDate){
			$pPaymentDate = date("Y-m-d", time());
		}
		
		$order = \Baseapp\Suva\Models\Orders::findFirst($pOrdersId);
		if (!$order){
			// error_log('Nava::actOrderPayWithMethod 5');
			Nava::sysMessage("Order ID not supplied to actOrderPaidWithCcMaster action ");
			return true;
		}

		
		$user = \Baseapp\Suva\Models\Users::findFirst( $pUserId );
		// error_log('Nava::actOrderPayWithMethod 6');
		if (!$user){
			Nava::sysMessage("User $pUserId not found in actOrderPaidWithCcMaster action ");
			return true;
		}
		
		$loggedUser = \Baseapp\Suva\Models\Users::findFirst( $pLoggedUserId );
		//error_log('Nava::actOrderPayWithMethod 6');
		if (!$loggedUser){
			Nava::sysMessage("User $pLoggedUserId not found in actOrderPaidWithCcMaster action ");
			return true;
		}

		
		// error_log('Nava::actOrderPayWithMethod 7');
        // //error_log($method->slug);
        $method = \Baseapp\Suva\Models\PaymentMethods::findFirst("slug = '$pMethod'");
		if (!$method->slug){
			// error_log('Nava::actOrderPayWithMethod 8');
			Nava::sysMessage("Slug for payment method $method->id not entered in database");
			return true;
		}
		
		if ( array_key_exists('_fiscal_location',$_SESSION['_context'])) {
			// $order->n_fiscal_location_id = $_SESSION['_context']['_fiscal_location'];
			$fiscalLocationId = $_SESSION['_context']['_fiscal_location'];
			// error_log('Nava::actOrderPayWithMethod 10');
		}
		else {
			Nava::sysMessage("Nema fiskalne lokacije");
			// error_log('Nava::actOrderPayWithMethod 11');
			return true;
		}
		
		//14.2.2017
		if ( $order->n_source == 'frontend' ){
			$operatorId = \Baseapp\Suva\Library\libAppSetting::get('frontend_default_operator_id');
			$fiscalLocationId = \Baseapp\Suva\Library\libAppSetting::get('fin_default_frontend_fiscal_location');	
			$fiscalLocationId = $fiscalLocationId > 0 ? $fiscalLocationId : null;
			if ($operatorId > 0){
				$operator = \Baseapp\Suva\Models\Users::findFirst($operatorId);
				if ($operator) {
					$pLoggedUserId = $operatorId;
				}
				else{
					Nava::greska("User $operatorId, supplied via frontend_default_operator_id, not found.");
				}
			}
			else{
				Nava::greska("Parameter frontend_default_operator_id not found");
			}
			if(!$fiscalLocationId) 
				Nava::greska("Parameter frontend_default_fiscal_location_id not found");
		}
		
		$order->n_fiscal_location_id = $fiscalLocationId;
		$order->n_payment_methods_id  = $method->id;
		$order->payment_method = $method->slug;
		$order->n_payment_date = $pPaymentDate;
		
		$order->n_operator_id = $pLoggedUserId;
		
		$order->n_invoice_date_time_issue = $order->n_invoice_date_time_issue ? $order->n_invoice_date_time_issue : date ("Y-m-d H:i:s", time());
		// error_log('Nava::actOrderPayWithMethod 9');
		

		if ($order->update() == false){
			// error_log('Nava::actOrderPayWithMethod 12');
			Nava::greska("ERR248: Order $order->id not updated.");
			return true;
		} 
		
		//13.1.2017 - 2470 - u trenutku prebacivanja u plaćeno, trenutni datum
		$jeGreske = false;
		foreach ($order->OrdersItems() as $oi) {
			if ( $oi->Product() && $oi->Product()->is_online_product == 1 && $oi->n_first_published_at < date("Y-m-d H:i:s",time()) ){
				
				Nava::poruka("Mijenja se n_first_published_at na order itemu $oi->id, trenutni je $oi->n_first_published_at a treba biti ".date("Y-m-d H:i:s",time()));
				$oi->n_first_published_at = date("Y-m-d H:i:s",time());
				$greska = $oi->n_before_save();
				if ($greska) return Nava::greska("ERR254: GREŠKA KOD MIJENJANJA DATUMA NA ORDER ITEMU $greska");
				$success = $oi->update();
				if ($success === true) {
					Nava::sysMessage("Order item $oi->id updated");
				}
				else {
					$greske = $oi->getMessages();
					foreach ($greske as $greska)
						$err_msg .= " ".$greska;
					Nava::greska("ERR251: <strong>Errors while updating order item No. $oi->id: </strong>".$err_msg);
					$jeGreske = true;
				}				
			}
		}
		if (!$jeGreske ){
			return self::actOrderStatusPlaceno( $pOrdersId, date ("Y-m-d", time()) , $pIsFiscalizeImmediately);
		}
		else {
			return null;
		}

		
	}
	
	
	public static function actOrderStatusPlaceno( $pOrdersId = null, $pPaymentDate = null, $pIsFiscalizeImmediately = true ) {

        /* 
        time() - trenutno vrijeme u unix timestampu
        
        
        */
        // error_log('Nava::actOrderStatusPlaceno 1');
//         $this->printr($_REQUEST);
		// Nava::poruka("actOrderStatusPlaceno");
		if (!$pOrdersId > 0 ){
			// error_log('Nava::actOrderStatusPlaceno 2');
			Nava::sysMessage("Parameter Order ID not sent  - $pOrdersId");
			return;
		} 
		$order = \Baseapp\Suva\Models\Orders::findFirst($pOrdersId);
		// error_log('Nava::actOrderStatusPlaceno 3');
		if (!$order ){
			// error_log('Nava::actOrderStatusPlaceno 4');
			Nava::sysMessage("Order No.  $pOrdersId not found.");
			return true;
		} 
		
		if (!$pPaymentDate) $pPaymentDate = date("Y-m-d" , time());
		if ($order->n_payment_date) $pPaymentDate = $order->n_payment_date;
		
		
		if (!$order->n_payment_methods_id){
			Nava::sysMessage("Payment method not supplied.");
			return true;
		} 
		
// 		385 - sat - time stamp kad će oglas biti objavljen. sada nije dobro. 
// 		treba napraviti da se sat (ako je u postavkama kod plaćanja 00.00) automatski postavi s trenutkom plaćanja.
		//Nava::poruka("prije foreach");
		$ois = $order->OrdersItems();
		//error_log ("libUsersActions::102 ois:".print_r($ois->toArray(),true));
		foreach ( $order->OrdersItems() as $oi ){
			if ( $oi->Product() && $oi->Product()->is_online_product == 1 ){
				$txtDatum = date ("Y-m-d H:i:s");
				// Nava::sysMessage("Order Item $oi->id , first published at = $txtDatum ");
				//$stariDatumVrijeme = $oi->n_first_published_at;
				//$oi->n_first_published_at = date ("Y-m-d H:i:s");
				//$oi->update();
			}
			else{
				Nava::sysMessage("Nije online product");
			}
		}

        $lPrvi = Nava::sqlToArray("select min(n_first_published_at) as prvi from orders_items where total > 0 and order_id = $pOrdersId")[0]['prvi'];
		// error_log('Nava::actOrderStatusPlaceno 7');
        if (!$lPrvi){
			// error_log('Nava::actOrderStatusPlaceno 8');
			Nava::greska("ERR249: Could not find first publication date on order No. $pOrdersId");
			return true;
			// return ("Could not find first publication date on order No. $pOrdersId");
		} 

        //26.1.2017 - ovo ne važi za unpaid invoices, samo za ponude
        if ($order->n_status == 3 && strtotime($pPaymentDate) > strtotime($lPrvi)) {
		//if (strtotime($pPaymentDate) > strtotime($lPrvi)) {
            //ako ima zakašnjelih ide u late payment
			 //$order->status = 2;
			 //$order->n_status = 9;
			
			//$order->update();
			//$order->updateAdLatestPaymentState();
			
			//Nava::greska("Status of Order No. $pOrdersId has been changed to 'Late Paid' because there was a paid product that was published ".$order->n_dateTimeUs2DateTimeHr($lPrvi).", before payment date (".$order->n_dateUs2DateHr($pPaymentDate).")");
			//Nava::greska("Status of Order No. $pOrdersId cannot be Paid.");
			//error_log('Nava::actOrderStatusPlaceno 9');
			return "Status of Order No. $pOrdersId cannot be Paid, because iz has ads published before today.";
			// return ("Status of Order No. $pOrdersId has been changed to 'Late Paid' because there was a paid product that was published "
				// .$order->n_dateTimeUs2DateTimeHr($lPrvi).", before payment date (".$order->n_dateUs2DateHr($pPaymentDate).")");
        
		} else {
			if ($pIsFiscalizeImmediately ) {
				$result = \Baseapp\Suva\Library\libNavision::send_order_to_nav($order->id);
			}
			
			$order->n_status = 5;
			$order->status = 2;
			$order->n_payment_date = $pPaymentDate;
			$order->update();
			$order->updateAdLatestPaymentState();
			
			
			
			//12.1.2017 - pokretanje obrade za aktiviranje oglasa nakon snimanja
			\Baseapp\Suva\Library\libCron::setAdsInfoForFrontend($oi->ad_id);
			
			
			
	        // //56 - Kad fakturi promijeniš status u "plaćeno", svi ad-ovi koji su na njoj a dodijeljen im je plaćeni proizvod postaju aktivni
			// $lPaidAds = Nava::sqlToArray("select ad_id from orders_items where order_id = $pOrdersId and total > 0 and ad_id > 0 group by ad_id");
			// foreach($lPaidAds as $ad) {
// // 				Nava::activateAd( $ad['ad_id'] );
				// Nava::runSql("update ads set online_product_id = 1 where id =".$ad['ad_id']); 
				// Nava::runSql("update ads set online_product = 'O:47:' where id =".$ad['ad_id']); 
			// }

			// $this->poruka("Status of Order No. $pOrdersId has been changed to 'Paid'.  "
				// ." First publication date is ".$order->n_dateTimeUs2DateTimeHr($lPrvi)
				// .". Payment date is ".$order->n_dateUs2DateHr($pPaymentDate));
			// return $this->redirect_back();
			// error_log('Nava::actOrderStatusPlaceno 10');
			
			if ($pIsFiscalizeImmediately ) {
				$navResponse = \Baseapp\Suva\Library\libNavision::waitForFiscalization($order->id);
			
				if ($navResponse){
					$jir = $navResponse[0]['Unique Invoice No_'] > '' ? $navResponse[0]['Unique Invoice No_'] : null;
					$zik = $navResponse[0]['Issuer Protection Code'] > '' ? $navResponse[0]['Issuer Protection Code'] : null;
					$invoiceNo = $navResponse[0]['Invoice No_'] > '' ? $navResponse[0]['Invoice No_'] : null;
					$n_invoice_fiscal_number = $navResponse[0]['Fisc_ Doc_ No_']."-".$navResponse[0]['Fisc_ Location Code']."-".$navResponse[0]['Fisc_ Wholesale Terminal'];
					$isFiscalized = $order->PaymentMethod()->is_fiscalized == 1 ? true : false;
					//9.1.2017
					//AKO JE U STATUSU 5, ISFISCALIZED = 1 I IMA JIR,ZIK,FISCAL_NUMBER ONDA STAVI STATUS SYNCED
					//AKO JE U STATUSU 5, ISFISCALIZED = 0 I IMA FISCAL_NUMBER ONDA STAVI STATUS SYNCED
					//INACE STATUS RETRY
					if ($isFiscalized && $jir && $zik && $n_invoice_fiscal_number){
						//sve je OK
						$order->n_invoice_zik = $zik;
						$order->n_invoice_jir = $jir;
						$order->n_invoice_fiscal_number = $n_invoice_fiscal_number;
						$order->n_nav_sync_status = 'synced';
						$order->update();
					}
					elseif ($n_invoice_fiscal_number && $isFiscalized == false){
						//sve je OK
						$order->n_invoice_fiscal_number = $n_invoice_fiscal_number;
						$order->n_nav_sync_status = 'synced';
						$order->update();
					}
					else{
						Nava::greska ("ERR250: Greška pri fiskalizaciji: Order:$order->id, Is Fiscalized: $isFiscalized, Invoice No: $invoiceNo, JIR: $jir, ZKI: $zik");
					}
				}else{
					$order->n_nav_sync_status = 'retry';
					$order->update();
					Nava::greska ("ERR251: Greška pri fiskalizaciji, Navision nije uspio fiskalizirati fakturu: Order:$order->id");
				}
			}
			
		}
        // error_log('Nava::actOrderStatusPlaceno 11');    
		return false; // nema greske
    }
	

}