<?php
namespace Baseapp\Suva\Library;


use Baseapp\Suva\Library\Nava;

class libAppSetting {
	
// Postavke aplikacije
	public static function get( $pName = null, $pDefaultValue = null ){
		
		//sprema se u session jer se app settings ne mijenja cesto
		if ( array_key_exists('_app_settings', $_SESSION ))
			if ( array_key_exists ( $pName , $_SESSION['_app_settings']  ) ){
				return $_SESSION['_app_settings'][ $pName ];
		}
		
		$rs = Nava::sqlToArray("select * from n_app_settings where name = '$pName' ");
		//Nava::poruka("select * from n_app_settings where name = '$pName' ");
		if ( count( $rs ) === 1 ){
			$rec = $rs[0];
			$type = $rec['type'];
			$valDecimal = $rec['value_decimal'];
			$valText = $rec['value_text'];
			$modelName = $rec['model_name'];
			
			switch ($type){
				case 'text': 	$result = $valText;							break;
				case 'decimal':	$result = $valDecimal;						break;
				case 'integer':	$result = intval( $valDecimal);				break;
				case 'boolean': $result = ( $valDecimal == 0 ? false : true);break;
				case 'id': 
						
						if ($modelName && $valDecimal > 0){
							$id = intval($valDecimal) > 0 ? intval($valDecimal) : -1;
							$class = '\\Baseapp\\Suva\\Models\\'.$modelName;
							$result = $class::findFirst($id);
						}
						break;
				default: 		$result = $pDefaultValue;
			}
		}
		else {
			$result = $pDefaultValue;
		}
		
		return $result;
		
		
		
	}
	
	public static function set( $pName = null, $pValue = null, $pType = null ){
		
		/*
		  - ako pName ne postoji u bazi, mora postojat $pType i treba kreirat
		  - ako pName ne postoji u bazi a nije poslan pType treba javit grešku
		  - ako pName postoji u bazi, treba izmijenit vrijednost. 
				Ako pType = 'text' onda se puni tekstualni polje value_text, a inače se puni value_decimal'
		  
		
		*/
		
		//error_log("select * from n_app_settings where name = '$pName' ");
		$rs = Nava::sqlToArray("select * from n_app_settings where name = '$pName' ");
		$value = self::valueToSqlValue( $pType, $pValue );
		
		$isError = false;
		if ( count( $rs ) === 1 ){
			$rec = $rs[0];
			$type = $rec['type'];

			if ( $pType &&  $type !== $pType ){
				//ako je poslan tip, a razlikuje se od onoga u bazi, onda greška
				Nava::greska("Type nije isti u n_app_settings je $type a poslani tip je $pType");
				$isError = true;
			}
			elseif ( !$pType || ( $pType &&  $type === $pType ) )   {
				//ako nije poslan tip, ili je poslan i isti je kao u bazi, onda OK i snima se
				$value = self::valueToSqlValue( $type, $pValue );
				$valueFieldName = $type == 'text' ? 'value_text' : 'value_decimal';
				$txtSql = " update n_app_settings set $valueFieldName = $value where name = '$pName' ";
			}	
		}
		elseif ( count($rs) === 0 ) {
			//ako varijable još nema u bazi, mora biti poslan pType, kreira se nova vrijednost
			if($pType){
				$valueFieldName = $pType == 'text' ? 'value_text' : 'value_decimal';
				$txtSql = "insert into n_app_settings ( name, type, $valueFieldName ) values ( '$pName', '$pType', $value )";
			}
			else{
				Nava::greska("app setting name: $pName ne postoji u bazi, i type nije poslan");
				$isError = true;
			}
		}
		else {
			Nava::greska("app setting name: $pName postoji u bazi ".count($rs)." puta");
			$isError = true;
		}
		
		if (! $isError ){
			error_log($txtSql);
			Nava::runSql ($txtSql);
			
			if ( array_key_exists('_app_settings', $_SESSION )){
				$_SESSION['_app_settings'][ $pName ] = $pValue;
			}
		}
	
		$result = ! $isError;
		return $result;
	}

	public static function cancel( $pName = null ){
		//cancelling value is setting it to null
		return self::set ( $pName );
	}
	
	private static function valueToSqlValue ( $pType = null, $pValue = null  ){
		error_log ("valuesql type $pType");
		error_log ("valuesql value $pValue");
		if ( $pValue === null ){
			$result = "null";
		}
		else{
			switch ($pType){
				case 'text': 	$result = "'$pValue'";						break;
				case 'decimal':	$result = "$pValue";						break;
				case 'integer':	$result = "".intval( $pValue );				break;
				case 'boolean': 
					error_log ("valuesql value switch $pValue");
					$pValue = intval($pValue);
					switch ( $pValue ){
						case 0:		error_log ("case 0"); $result = '0';	break;
						case false:	error_log ("case FALSE"); $result = '0';	break;
						case 0.0: 	error_log ("case 0.0"); $result = '0';	break;
						case '0':	error_log ("case '0'"); $result = '0';	break;
						case '0.0':	error_log ("case '0.0'"); $result = '0';	break;
								
						case true:	error_log ("case TRUE"); $result = '1';	break;
						case 1:		error_log ("case 1"); $result = '1';	break;
						case '1':	error_log ("case '1'"); $result = '1';	break;
						case '1.0':	error_log ("case '1.0'"); $result = '1';	break;
								
						default:
								$isError = true;
								$result = 'null';
								error_log ("case DEFAULT"); 						
					}
					break;
				default: 		$result = 'null';
			}
		}
		return $result;
	}
}