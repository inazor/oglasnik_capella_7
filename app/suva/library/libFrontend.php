<?php

namespace Baseapp\Suva\Library;


use Baseapp\Suva\Library\Nava;
use Baseapp\Suva\Library\libAppSetting;
use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileAdapter;




class libFrontend{
 
	// NIKOLA $logger se nalazi u funkcijama self::fMsg i self::fError i trenutno loga u public/test.log jer u app/logs nemamo pristup 
	private static $loggerPath = 'libCron_application_log.log';
	

	


//glavne funkcije
public static function fUpdateAdForSuva ( \Baseapp\Models\Ads $frontendAd = null , $pShowMessages = false ){
	//sinhronizira se ad iz Agimove funkcije na predajaOglasaController (oba order itema već postoje)

	global $gDebugLevel;
	
	//Agimova funkcija šalje njegovu klasu Ad, iz koje se učitava naš ad
	if (!$frontendAd){
		Nava::sysError("updateFroontendAdForSuva: Ad not supplied");
		return;
	} 
	$ad = \Baseapp\Suva\Models\Ads::findFirst($frontendAd->id);
	
	
	if ($ad->n_frontend_sync_status == 'synced') {
		Nava::greska ("Frontend ad $ad->id is already synced");
		return;
	}
	
	
	
	$paidOrderId = null;
	
	//syncanje online order itema
	$txtWhere = "ad_id = $ad->id and product_id = '$ad->online_product_id' and n_frontend_sync_status = 'unsynced'";
	$ois = \Baseapp\Suva\Models\OrdersItems::find($txtWhere);
	if ($pShowMessages) Nava::sysMessage(" libFrontend:updateAdForSuva, $txtWhere found ".$ois->count()." records");
	foreach($ois as $oi){
		if (  intval($oi->price) > 0 ) $paidOrderId = $oi->order_id;
		
		$status = self::fUpdateOrderItemForSuva ( $oi,  $ad, $ad->online_product, $pShowMessages );
		if ( !$status ) Nava::greska("Ad $ad->id - order item $o->id  nije updatean");
		if ($status){
			Nava::runSql("update ads set n_fe_online_order_item_id = $oi->id where id = $ad->id");
		}
		else {
			Nava::greska("Ad $ad->id - nije kreiran frontend order item  za online proizvod ");
		}
	}

	//syncanje offline order itema
	$txtWhere = "ad_id = $ad->id and product_id = '$ad->offline_product_id' and n_frontend_sync_status = 'unsynced'";
	$ois = \Baseapp\Suva\Models\OrdersItems::find($txtWhere);
	if ($pShowMessages) Nava::sysMessage(" libFrontend:updateAdForSuva, $txtWhere found ".$ois->count()." records");	
	foreach($ois as $oi){
		if (  intval($oi->price) > 0 ) $paidOrderId = $oi->order_id;
		
		$status = self::fUpdateOrderItemForSuva ( $oi,  $ad, $ad->offline_product, $pShowMessages );
		if ( !$status ) Nava::greska("Ad $ad->id - order item $o->id  nije updatean");
		if ($status){
			Nava::runSql("update ads set n_fe_offline_order_item_id = $oi->id where id = $ad->id");
		}
		else {
			Nava::greska("Ad $ad->id - nije kreiran frontend order item  za online proizvod ");
		}
	}
		
	//Ako postoji plaćeni order updateanje ordera (tj. ako ima plaćenih oglasa - ako nema sve ide u free ads pa ne treba nista updateat)
	if ($paidOrderId){
		$status = self::fUpdateOrderForSuva ( $paidOrderId, $pShowMessages );
		if ( !$status ) Nava::greska ("Ad $ad->id - order $paidOrderId  nije updatean");
	}
}


public static function fUpdateOrderItemForSuva( \Baseapp\Suva\Models\OrdersItems $oi = null, \Baseapp\Suva\Models\Ads $ad = null, $pProductSerialized = null, $pShowMessages = false ){
	/*
	funkcija updateOrderItemForSuva (order_item.id)
		processSerializedProduct(oi.product)
		findSuvaProduct()
		usporediti cijenu na order itemu i cijenu na suva proizvodu
			ako nisu iste - error (OrdersItems, cijena nije ista kao i cijena na Productu No.11 )
		updateati na order itemu
			n_price
			n_publications_id
			n_products_id
			n_total
			n_total_with_tax tota- * tax_amount
			n_category_id
			n_is_active
			n_frontend_sync_status = synced
		ako je offline proizvod
			dodati slijedeæih insertiona koliko je broj objava
				iæ po svim issues, status = prima, publikacija je OGLA
		
		vraæa: true ili false
	*/

	global $gDebugLevel;
	
	//PROVJERE PARAMETARA
	if ( !$oi ) {
		Nava::sysError ("Order Item not supplied to public static function self::fUpdateOrderItemForSuva ");
		return null;
	}
	
	if ( !$pProductSerialized ) {
		Nava::sysError ("Serialized product not supplied to public static function self::fUpdateOrderItemForSuva ");
		return null;
	}
 
	//vraca distionary s poljima po kojima se traži suva  proizvod
	$frontendProduct = self::fProcessSerializedProduct ( $pProductSerialized );
	
	$suvaProduct = self::fFindSuvaProduct ( $frontendProduct );
	if (!$suvaProduct ) {
		Nava::greska ("Suva product for orderitem $oi->id not found.");
		return false;
	}
	
	$vatRate = \Baseapp\Suva\Library\libAppSetting::get('fin_vat_rate');
	if (!$vatRate){
		Nava::sysError("VAT Rate needs to be entered in the Application Settings");
		return null;
	}
	
	$publication = $suvaProduct->Publication();

	
	//14.10.2016 - dogovor s Patricijom: Ako oglas nije aktivan, onda ostaje u statusu unsynced, i ponovo se pokušava sinhronizirati
	
	// $arrOi = $oi->toArray();
	// $arrOi['product']="";
	// error_log("oi ".print_r($arrOi, true));
	// error_log("oi $oi->price ".((int)$oi->price + 1));
	//PRIPREMA SQL-A
	$n_price = ( (int)$oi->price ) / (1 + $vatRate) ;//patricija - na frintendu su cijene s PDV-om
	$n_publications_id = $publication->id;
	$n_products_id = $suvaProduct->id;
	
	$n_total = ( (int)$oi->total) / (1 + $vatRate) ; //patricija - na frintendu su cijene s PDV-om

	// //kod oglasa koji još nisu aktivni ne postoji first published at, samo created at i expires at
	// $n_first_published_at = date("Y-m-d H:i:s", ( $ad['first_published_at'] === null ? $ad['created_at'] : $ad['first_published_at'] ) );

	//2.1.2017 - dodaje se published_at a ne first_published_at (patricija)
	//kod oglasa koji još nisu aktivni ne postoji first published at, samo created at i expires at
	$n_first_published_at = date("Y-m-d H:i:s", ( $ad->first_published_at === null ? $ad->created_at : $ad->first_published_at ) );

	
	$n_expires_at = date("Y-m-d H:i:s", $ad->expires_at);
	$n_total_with_tax = $n_total * ( 1 + $vatRate );
	$n_category_id = $ad->category_id;
	$n_is_active = 1;
	$n_frontend_sync_status =  'synced';

	
	$txtSql = "
		update orders_items set
			n_price = $n_price
			, n_publications_id = $n_publications_id
			, n_expires_at = '$n_expires_at' 
			, n_products_id = $n_products_id
			, n_first_published_at = '$n_first_published_at' 
			, n_total = $n_total
			, n_total_with_tax = $n_total_with_tax
			, n_category_id = $n_category_id
			, n_is_active = $n_is_active
			, n_frontend_sync_status = '$n_frontend_sync_status'
		where id = $oi->id
	";
	
	if ($pShowMessages) Nava::sysMessage ($txtSql);
	Nava::runSql($txtSql);

	//INSERTIONI - ubacivanje insertiona za offline izdanje

	if ( $suvaProduct->is_online_product == 1 ) return true;
	//ISSUES - dohvat onoliko koliko je definirano na Suva productu
	//PRIPREMA SQL-A
	$publications_id = $publication->id;
	$noIssues = $suvaProduct->days_published > 0 ?  $suvaProduct->days_published : 1 ;
	if ($pShowMessages) Nava::sysMessage ("Suva proizvod $suvaProduct->id ima  $noIssues objava.");
	
	$issues = \Baseapp\Suva\Models\Issues::find(array( " status = 'prima' and publications_id = $publications_id ", "limit" => $noIssues , "order" => "date_published" ) );
	if ( $issues->count() != $noIssues ){
		Nava::sysError (" wrong number of available issues found when creating insertions for order item $oi->id, product ".$suvaProduct['id'].", publication $publications_id, ( $noIssues required and ".$issues->count()." available). MAYBE PRODUCT IS WRONGLY SET TO BE OFFLINE?" );
		return null;
	}
	$maxDate = 0;
	foreach ( $issues as $issue ){
		$dateIssue = strtotime($issue->date_published);
		// self::fMsg("DATE ISSUE $dateIssue");
		if ( $dateIssue > $maxDate ) $maxDate = $dateIssue;
		// self::fMsg("IF DATE ISSUE > MAXDATE $maxDate");
		//PRIPREMA SQL-A
		$created_at = date ("Y-m-d h:I:s");
		$product_id = $suvaProduct->id;
		$orders_items_id = $oi->id;
		$issues_id = $issue->id;

		$txtSql = "
			insert into n_insertions (
				created_at 
				, product_id 
				, orders_items_id 
				, issues_id 
			)
			values (
				'$created_at'
				, $product_id
				, $orders_items_id
				, $issues_id
			);
		";
		if ($pShowMessages) Nava::sysMessage("ISSUE: $issues_id  SQL:".$txtSql);
		Nava::runSql($txtSql);
	}
	if ($maxDate > 0){
		$dateString = date("Y-m-d h:I:s", $maxDate);
		Nava::runSql("update orders_items set n_expires_at = '$dateString' where id = $oi->id ");
		if ($pShowMessages)  Nava::sysMessage("Expires At for OrderItem $oi->id update to $dateString");
	}
	
	return true;
	
}

public static function fUpdateOrderForSuva( $pOrderId ,$pShowMessages = false ){
	/*
	funkcija self::fUpdateOrderForSuva (order_id)
		azurirat polja
			n_status
			n_total
			n_frontend_sync_status
		vraæa: true ili false
	*/
	global $gDebugLevel;
	//PROVJERE PARAMETARA
	if ( !$pOrderId ) {
		Nava::sysError ("Order ID not supplied to public static function self::fUpdateOrderForSuva ", "SYS_ERR");
		return false;
	}
	
	//DOHVAT MODELA
	$order = Nava::sqlToArray("select * from orders where id = $pOrderId")[0];
	$fiscal_location = Nava::sqlToArray("select * from n_fiscal_locations where nav_fiscal_location_id = 'ONL1' limit 1")[0];
	$sumOrdersItems = Nava::sqlToArray("
		select 
			SUM(n_total) as n_total
			,SUM(n_total_with_tax) as n_total_with_tax
		from 
			orders_items
		where order_id = $pOrderId
	")[0];
	
	//PRIPREMA SQL-A
	
	//n_status
	/*
		const STATUS_NEW = 1; // Order record created (when "submit order" is clicked), could be payed offline
		const STATUS_COMPLETED = 2; // Order fulfilled/finalized
		const STATUS_CANCELLED = 3; // Order cancelled by the customer (or admin)
		const STATUS_EXPIRED = 4; // Will probably have to be processed via cron or something...
	*/

	
	$os = $order['status'];
	$orderTotal = (int)$order['total'];
	if ( $os == 1  and $orderTotal == 0 ) $n_status = 2;
	elseif ( $os == 1 and $orderTotal > 0 ) $n_status = 3;
	elseif ( $os == 2 and $orderTotal == 0 ) $n_status = 2;
	elseif ( $os == 2 and $orderTotal > 0 ) $n_status = 5;
	//elseif ( $os == 3 ) $n_status = 5;
	elseif ( $os == 4 ) $n_status = 9;
	else {
		Nava::sysError (" Order $pOrderId : status not recognized: ".$order['status'], "SYS_ERR" );
		return false;
	}

	if ($n_status == 3) {
		//prebačeno u ponudu, dodaje se n_quotation_date	
		$sql_n_quotation_date = " n_quotation_date = '".date("Y-m-d")."'";
	}
	else {
		$sql_n_quotation_date = "n_quotation_date = ''";
	}
	
	$operatorId = \Baseapp\Suva\Library\libAppSetting::get('frontend_default_operator_id');
	if ($n_status == 5) {		
		//plaćeno preko frontenda, poiva se funkcija koja dodjeljuje payment method
		$n_operator_id = " n_operator_id = $operatorId";
		$n_payment_date = "n_payment_date = now()";
	}
	else {
		$n_operator_id = "n_operator_id = ''";
		$n_payment_date = "n_payment_date = null";
	}
	
	//n_invoice_date_time_issue
	$sqlInvoiceDateTimeIssue = $n_status == 5 ? ', n_invoice_date_time_issue = now() ' : '';
	
	
	$n_fiscal_location_id = \Baseapp\Suva\Library\libAppSetting::get('fin_default_wspay_fiscal_location_id');
	$n_nav_sync_status = $n_status == 5 ? 'unsynced' : 'no_sync';	
	$n_total = $sumOrdersItems['n_total'] ? $sumOrdersItems['n_total'] : 0.00;
	$n_total_with_tax = $sumOrdersItems['n_total_with_tax'] ? $sumOrdersItems['n_total_with_tax'] : 0.00;
	$n_tax_amount = $n_total_with_tax - $n_total;
	// $n_fiscal_location_id = $fiscal_location['id'];
	
	//14.2.2017
	$n_sales_rep_id = \Baseapp\Suva\Library\libAppSetting::get('frontend_default_sales_rep_id ');
	$sqlSalesRepId = $n_sales_rep_id > 0 ? ", n_sales_rep_id = $n_sales_rep_id " : "";
	$pboPrefix = \Baseapp\Suva\Library\libAppSetting::get('fin_invoice_prefix');
	$pbo = (intval($pboPrefix) + intval($pOrderId));
	
	//11.04.2017
	$sqlPaymentMethodId = '';
	$frontendPaymentMethod = strlen($order['payment_method']) > 0 ? $order['payment_method'] : 'n/a';
	$pm = \Baseapp\Suva\Models\PaymentMethods::findFirst( "frontend_payment_method = '$frontendPaymentMethod'" );
	if ($pm){
		$sqlPaymentMethodId =  ", n_payment_methods_id = $pm->id "; 
	}

	
	
	
	$txtSql = "
		update orders set
			n_status = $n_status
			, n_total = $n_total
			, n_tax_amount = $n_tax_amount 
			, $sql_n_quotation_date
			, n_total_with_tax = $n_total_with_tax 
			, n_fiscal_location_id = $n_fiscal_location_id
			, $n_operator_id
			, $n_payment_date
			, n_frontend_sync_status = 'synced'
			, n_nav_sync_status = '$n_nav_sync_status'
			, n_pbo = '$pbo'			
			$sqlSalesRepId
			$sqlPaymentMethodId
			$sqlInvoiceDateTimeIssue
		where id = $pOrderId
	";
	//error_log($txtSql);
	// error_log("libCron: fUpdateOrderForSuva : updating order $pOrderId");
	if ($pShowMessages) Nava::sysMessage($txtSql);
	Nava::runSql($txtSql);
	
	
	//ažurirati payment method, ako je plaćeno preko onlinea
	
	
	
	
	
	
	return true;
}

public static function fProcessSerializedProduct( $pSerialized = null ){
	//TODO - sredit greške ako ne naðe nešto obavezno, u ['error'];
	/* REGEX
		'/   - poæetak stringa
		/'   - kraj stringa
		/s'  - kraj stringa, ako se hoæe matchat i linebreakovi
		(aabb) - kad se nadje aabb neka ga se doda u novi red arraya
		
		
	*/
	global $gDebugLevel;
	if (strlen ($pSerialized ) < 10) return null;
	
	$r = array (
		'is_online_product' => null,
		'name' => null,
		'old_product_id' => null,
		'old_product_option' => null,
		'old_product_extras' => null,
		'old_product_extras_options' => null,
		'is_pushup' => false,
		'cost' => null,
		'error' => null
	);
	
	
	//echo PHP_EOL."TEXT:".PHP_EOL.$pSerialized.PHP_EOL.PHP_EOL.PHP_EOL;

	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)"/'; //matchat  Online ili Offline, i onda ime proizvoda 
	//matchat  Online ili Offline, i onda ime proizvoda 
 
	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)".*s:5:"...id";s:[0-9]{1,2}:"([^"]+)";/'; // 
	$rgxOnline = '/Baseapp\\\\Library\\\\Products\\\\([^\\\\]+)\\\\([^\\\\^"]+)":[0-9]{1,2}:{s:5:"...id";s:[0-9]{1,2}:"([^"]+)";/'; // 
	preg_match ($rgxOnline, $pSerialized, $arrMatch);
	if( array_key_exists (1, $arrMatch) ){
		if( $arrMatch[1] == 'Online' ) $r['is_online_product'] = true;
		elseif( $arrMatch[1] == 'Offline' ) $r['is_online_product'] = false;
	}
	$r['name'] = array_key_exists( 2, $arrMatch ) ? $arrMatch[2] : null;
	$r['old_product_id'] = array_key_exists (3, $arrMatch ) ? $arrMatch[3] : null;
	//var_dump ($arrMatch);

	
	if ( $r['name'] === 'Pushup' ){
		//self::fMsg ("tu sam".PHP_EOL);
		//traženje online_product_id  i spremanje u old_product_option
		
		//ako je Pushup onda se traži s:17:"online_product_id";s:16:"istaknuti-online"; i   s:7:" * cost";s:4:"1500";
		$rgxOption = '/s:17:"online_product_id";s:[0-9]{1,2}:"([^"]+).*s:7:"...cost";s:[0-9]{1,2}:"([0-9]+)";/s';
		
		preg_match ($rgxOption, $pSerialized, $arrMatch);
		$r['is_pushup'] = true;
		$r['old_product_id'] = 'push-up';
		$r['old_product_option'] = array_key_exists( 1, $arrMatch ) ? $arrMatch[1] : null;
		$r['cost'] = array_key_exists( 2, $arrMatch ) ? ((int)$arrMatch[2])/100 : null;
		//var_dump ($arrMatch);
	}
	else{
		//za sve koji nisu Pushup
		
		//traženje opcije
		$rgxOption = '/selected";s:[0-9]{1,2}:"([^"]+)"/'; // matchaj ;s:11:" * selected";s:7:"60 dana";s:18:"
		preg_match ($rgxOption, $pSerialized, $arrMatch);
		$r['old_product_option'] = array_key_exists ( 1 , $arrMatch )  ? $arrMatch[1] : null;
		//var_dump ($arrMatch);
		
		//traženje extras 
		$rgxExtras = '/selected_extras";a:[0-9]{1,2}:.s:[0-9]{1,2}:"([^"]+)";s:[0-9]{1,2}:"([^"]+)"/';//matchati: * selected_extras";a:1:{s:20:"Objava na naslovnici";s:6:"3 dana"
		preg_match ($rgxExtras, $pSerialized, $arrMatch);
		$r['old_product_extras'] = array_key_exists( 1, $arrMatch ) ? $arrMatch[1] : null;
		$r['old_product_extras_options'] = array_key_exists( 2, $arrMatch ) ? $arrMatch[2] : null;
		//var_dump ($arrMatch);
	}
	
	
	
	$errMsg = null;
	if ( $r['is_online_product'] === null ) $errMsg .=  "is_online_product, ";
	if (! $r['name'] ) $errMsg .=  "name, ";
	if (! $r['old_product_id'] ) $errMsg .=  "old_product_id, ";
	$errMsg = $errMsg ? "Could not find ".$errMsg : $errMsg ;
	if ( $r['is_pushup']  && !( $r['old_product_option'] && $r['cost'] ) ) $errMsg .= "  Pushup product parameters not found.";
	
	if ($errMsg) {
		$r['error'] = $errMsg;
		Nava::sysError ($errMsg." for serialized product:".PHP_EOL.$pSerialized, "SYS_ERR");
	}

	
	return $r;
	
}

public static function fFindSuvaProduct ( $p , $pShowMessages = false){
	global $gDebugLevel;
	if ( $p['old_product_id'] == null ) {
		Nava::sysError("parameters supplied to self::fFindSuvaProduct do not contain old_product_id :".print_r($p, true), "SYS_ERR");
		return null;
	}
	
	$txt_old_product_id = " and old_product_id = '".$p['old_product_id']."' ";
	
	$txt_old_product_option = $p['old_product_option'] === null ? " and old_product_option is null " : " and old_product_option = '".$p['old_product_option']."' ";
	
	$txt_old_product_extras = $p['old_product_extras'] === null ? " and old_product_extras is null " : " and old_product_extras = '".$p['old_product_extras']."' ";
	
	$txt_old_product_extras_options = $p['old_product_extras_options'] === null ? " and old_product_extras_options is null " : " and old_product_extras_options = '".$p['old_product_extras_options']."' ";
	
	
	$txtWhere = " 1 = 1 $txt_old_product_id $txt_old_product_option $txt_old_product_extras $txt_old_product_extras_options ";

	$txtSql = "select * from n_products where $txtWhere ";

	$products = \Baseapp\Suva\Models\Products::find( $txtWhere );
	
	if ($pShowMessages) Nava::sysMessage("Finding suva products with these conditions: $txtWhere " );
	
	//Ako upit ne naðe točno 1 suva proizvod, greška
	if ($products->count() == 1) {
		foreach($products as $product)
			return $product;
	}
	else{
		Nava::sysError("error, query: $txtSql Found ".count( $rs )." records!!");
		return null;
	}
	
}


	public static function fUpdateOrderStatusForOnlinePayment(){
		/*
			traži ordere koji nisu u statusu paid a pripadaju oglasima kojima je latest payment state 5
		
		*/
		global $gDebugLevel;
		$operatorId = \Baseapp\Suva\Library\libAppSetting::get('fin_default_wspay_operator_id');
		$fiscalLocationId = \Baseapp\Suva\Library\libAppSetting::get('fin_default_wspay_fiscal_location_id');
		
		$orders = \Baseapp\Suva\Models\Orders::find("status = 2 and n_status in ( 3,4 ) and n_frontend_sync_status = 'synced' and n_source = 'frontend'");

		if ( !$operatorId || !$fiscalLocationId || !$orders ){
			//Nava::greska ("")
			Nava::sysError("nije dohvaćen operatorId, FiscalLocationId ili Orders ");
			return false;
		}
		
			
		
		$navSyncStatus = 'unsynced';
		foreach ($orders as $order){
			$frontendPaymentMethod = strlen($order->payment_method) > 0 ? $order->payment_method : 'n/a';
			$pm = \Baseapp\Suva\Models\PaymentMethods::findFirst( "frontend_payment_method = '$frontendPaymentMethod'" );
			if (!$pm){
				Nava::sysError ("nije dohvaćen frontend paymnet method na orderu $order->id");
				return false;
			}
			
				
			$txtSql = "
				update orders set
					n_status = 5
					,n_payment_methods_id = $pm->id
					,n_operator_id = $operatorId
					,n_fiscal_location_id = $fiscalLocationId
					,n_invoice_date_time_issue = now()
					,n_payment_date = now()
					,n_nav_sync_status = '$navSyncStatus'
				where id = $order->id
			";
			// error_log($txtSql);
			if ($pShowMessages)  Nava::sysMessage( $txtSql );
			Nava::runSql ( $txtSql );
			
		}
}


	public static function getFreeAdsListForUser($pUserId = null){
		$existingOrder = \Baseapp\Suva\Models\Orders::findFirst(" user_id = $pUserId and n_status = 2 ");
		if ($existingOrder) return $existingOrder;
		else{
			$order = new Orders();
 
			$order->user_id = $pUserId;
			$order->status = 1;
			$order->created_at = date('Y-m-d H:i:s');
			$order->created_at = date('Y-m-d H:i:s');
			//$cart->ip = $this->request->getClientAddress();
			$order->n_status = 2;
			
			if ($order->save() == true) return $cart;
			else {
				$err_msg = "";
				foreach ($order->getMessages() as $message) {
					$err_msg .= $message . "\n\r";
				return $err_msg;
				}
			}
		}
	}




	
	
}