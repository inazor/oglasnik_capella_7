UPDATE `image_styles` SET `slug` = 'GridView' WHERE id = 1;
UPDATE `image_styles` SET `slug` = 'GalleryThumb' WHERE id = 4;
UPDATE `image_styles` SET `slug` = 'GalleryFullscreen' WHERE id = 7;
UPDATE `image_styles` SET `slug` = 'GalleryBig' WHERE id = 5;
