-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 23, 2016 at 03:23 AM
-- Server version: 10.1.16-MariaDB-1~wily
-- PHP Version: 5.6.11-1ubuntu3.4

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `oglasnik_facelift`
--

-- --------------------------------------------------------

--
-- Table structure for table `infraction_reports`
--

CREATE TABLE IF NOT EXISTS `infraction_reports` (
  `id` bigint(20) unsigned NOT NULL,
  `model_name` varchar(64) COLLATE utf8_croatian_ci NOT NULL,
  `model_pk_val` bigint(20) unsigned NOT NULL,
  `reporter_id` int(11) unsigned NOT NULL,
  `report_reason` varchar(256) COLLATE utf8_croatian_ci NOT NULL,
  `report_message` text COLLATE utf8_croatian_ci,
  `reported_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reported_ip` varchar(45) COLLATE utf8_croatian_ci DEFAULT NULL,
  `resolved_by` int(10) unsigned DEFAULT NULL,
  `resolved_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- RELATIONS FOR TABLE `infraction_reports`:
--   `reporter_id`
--       `users` -> `id`
--   `resolved_by`
--       `users` -> `id`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `infraction_reports`
--
ALTER TABLE `infraction_reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reporter_id` (`reporter_id`),
  ADD KEY `model_name_pk_val` (`model_name`,`model_pk_val`) USING BTREE,
  ADD KEY `resolved_by` (`resolved_by`) USING BTREE,
  ADD KEY `resolved_at` (`resolved_at`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `infraction_reports`
--
ALTER TABLE `infraction_reports`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `infraction_reports`
--
ALTER TABLE `infraction_reports`
  ADD CONSTRAINT `infraction_reports_ibfk_1` FOREIGN KEY (`reporter_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `infraction_reports_ibfk_2` FOREIGN KEY (`resolved_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;
