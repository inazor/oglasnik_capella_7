ALTER TABLE `cms_categories` ADD COLUMN `template` VARCHAR(45) NULL DEFAULT NULL AFTER `type`;
ALTER TABLE `cms_categories_settings` ADD COLUMN `zendesk` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `meta_keywords`;
ALTER TABLE `cms_articles` ADD COLUMN `zendesk` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0 AFTER `full_page_image`;
ALTER TABLE `cms_articles` ADD COLUMN `sort_order` TINYINT(2) UNSIGNED NOT NULL DEFAULT 0 AFTER `zendesk`;
